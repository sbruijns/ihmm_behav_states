"""
    Script for combining local canonical_infos.json and the one from the cluster
"""
import json

dist_info = json.load(open("canonical_infos.json", 'r'))
local_info = json.load(open("canonical_infos_local.json", 'r'))

cluster_subs = ['KS045', 'KS043', 'KS051', 'DY_008', 'KS084', 'KS052', 'KS046', 'KS096', 'KS086', 'UCLA033', 'UCLA005', 'NYU-21', 'KS055', 'KS091']

for key in cluster_subs:
    print('ignore' in dist_info[key])

quit()

for key in cluster_subs:
    if key not in local_info:
        print("Adding all of {} to local info".format(key))
        local_info[key] = dist_info[key]
        continue
    else:
        for sub_key in dist_info[key]:
            if sub_key not in local_info[key]:
                print("Adding {} into local info for {}".format(key))
                local_info[key][sub_key] = dist_info[key][sub_key]
            else:
                if local_info[key][sub_key] == dist_info[key][sub_key]:
                    continue
                else:
                    assert len(dist_info[key][sub_key]) == 16
                    for x in dist_info[key][sub_key]:
                        assert x in local_info[key][sub_key]
                    local_info[key][sub_key] = dist_info[key][sub_key]
