from one.api import ONE
import numpy as np
import pandas as pd
import json
import os

one = ONE()

try:  
    os.mkdir("./fibre_data")  
except OSError as error:  
    print(error)   

subjects = ["fip_{}".format(i) for i in list(range(13, 17)) + list(range(26, 43))]

for subject in subjects:
    print('_____________________')
    print(subject)

    try:  
        os.mkdir("./fibre_data/" + subject)  
    except OSError as error:  
        print(error)   

    eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2025-01-01'], details=True)

    for eid, sess in zip(eids, sess_info):
        try:
            fp = one.load_object(eid, 'fpData')
            photometry = one.load_object(eid, 'photometry')
            trials = one.load_object(eid, 'trials')
            trials['intervals_1'] = trials['intervals'][:, 0]
            trials['intervals_2'] = trials['intervals'][:, 1]
            del trials['intervals']
        except Exception as e:
            print(e)
            continue

        try:  
            os.mkdir("./fibre_data/" + subject + '/' + str(sess['date']))  
        except OSError as error:  
            print(error)   

        fp.raw.to_parquet("./fibre_data/" + subject + '/' + str(sess['date']) + '/' + 'fpData_{}.pqt'.format(subject))
        photometry.signal.to_parquet("./fibre_data/" + subject + '/' + str(sess['date']) + '/' + 'photometry_{}.pqt'.format(subject))
        pd.DataFrame(trials).to_parquet("./fibre_data/" + subject + '/' + str(sess['date']) + '/' + 'trials_{}.pqt'.format(subject))