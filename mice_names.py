subjects1 = ['CSH_ZAD_025', 'ZM_1897', 'KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06', 'SWC_023', 'CSHL051',
            'CSHL054', 'CSHL059', 'CSHL_007', 'CSHL_014', 'CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015',
            'CSH_ZAD_001', 'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011',
            'KS016', 'ibl_witten_16', 'KS019', 'CSH_ZAD_022', 'KS021', 'SWC_022', 'CSHL061', 'CSHL062']
subjects2 = ['CSHL045', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL055', 'CSHL059', 'CSHL061', 'CSHL062', 'CSHL065',
            'CSHL066', 'CSH_ZAD_011', 'CSH_ZAD_017', 'CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022', 'CSH_ZAD_024',
            'CSH_ZAD_026', 'KS002', 'KS014', 'KS016', 'KS022', 'KS023', 'SWC_022', 'ZM_1897']
subjects3 = ['CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013']
subjects4 = ['NYU-06', 'KS014', 'CSHL061', 'ibl_witten_13', 'KS022', 'SWC_021', 'CSH_ZAD_025', 'CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013']
subjects5 = ['CSHL053', 'CSH_ZAD_029', 'CSHL066', 'CSH_ZAD_019', 'CSHL061', 'CSHL055', 'CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013', 'CSHL066', 'CSHL060']
subjects7 = ['SWC_054', 'ZFM-01592', 'SWC_060', 'SWC_043', 'CSHL059', 'ZM_2241', 'CSHL049', 'CSHL051',
            'DY_018', 'DY_013', 'DY_009', 'SWC_052', 'CSHL045', 'DY_020', 'CSHL052', 'SWC_058', 'ZFM-01936',
            'ZFM-01576', 'DY_016']  # CSH_ZAD_029 is missing 0.00005
subjects8 = ['KS014', 'KS015', 'KS022', 'DY_009', 'DY_013', 'DY_016', 'DY_018', 'DY_020',
            'CSHL045', 'CSHL049', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL054', 'CSHL059', 'CSHL060',
            'CSHL061', 'CSHL062', 'CSHL066', 'SWC_021', 'SWC_023', 'SWC_043', 'SWC_052',
            'SWC_054', 'SWC_058', 'SWC_060', 'ZM_1897', 'ZM_2241', 'CSHL_007', 'CSHL_014', 'CSHL_015',
            'CSHL_018', 'ZFM-01576', 'ZFM-01592', 'ZFM-01936', 'CSH_ZAD_001', 'CSH_ZAD_022', 'CSH_ZAD_025', 'CSH_ZAD_026',
            'ibl_witten_13', 'ibl_witten_14', 'ibl_witten_17']

s = subjects1 + subjects2 + subjects3 + subjects4 + subjects5 + subjects7 + subjects8
s = set(s)
