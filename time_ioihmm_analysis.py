

    def pmf_plots(self):
        for i, m in enumerate(self.models):
            for s in self.proto_states:
                # TODO: Hack
                if s not in self.state_to_color:
                    continue
                w = m.obs_distns[s].cats.weights
                label = s if i == 0 else None
                plt.plot(w[:, 0], self.state_to_color[s], label=label)

        plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
        plt.ylim(bottom=0, top=1)
        plt.savefig("figures/convergence/pmfs {}".format(fit))
        plt.show()

    def pmf_posterior_all(self):
        self.percent_counts()
        cm = plt.get_cmap('gist_rainbow')

        for s in range(self.models[0].num_states):
            if self.percent[s] < 0.01:
                continue
            pmfs = np.zeros((len(self.models), 9))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].cats.weights[:, 0]

            print(s)
            print(self.percent[s])
            print(min(1., self.percent[s] / 5 / (1 / self.models[0].num_states)))
            plt.plot(pmfs.mean(axis=0), color=cm(s / self.models[0].num_states), label=s, alpha=min(1., self.percent[s] / 5 / (1 / self.models[0].num_states)))
            plt.plot(np.percentile(pmfs, [2.5, 97.5], axis=0).T, color=cm(s / self.models[0].num_states), ls='--', alpha=min(1., self.percent[s] / 5 / (1 / self.models[0].num_states)))
        plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
        plt.ylim(bottom=0, top=1)
        plt.legend()
        plt.savefig("figures/convergence/pmfs with conf {}".format(fit))
        plt.show()

    def pmf_posterior(self):

        plt.figure(figsize=(11, 8))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), 9))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].cats.weights[:, 0]

            plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
            plt.plot(np.percentile(pmfs, [2.5, 97.5], axis=0).T, color=self.state_to_color[s], ls='--')
        plt.title("{}, {} sessions".format(self.name, len(self.models[0].stateseqs)), size=22)
        plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=22-3)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=8)
        sns.despine()

        plt.tight_layout()
        plt.savefig("figures/convergence/pmfs with conf {}".format(fit))
        plt.show()


    def pmf_stationarity(self):
        n = len(self.models[0].stateseqs)
        # for every state, first half or second half, for every contrast, what was the answer?
        global answers
        answers = np.zeros((len(self.proto_states), 2, 9, 2))
        for i, datum in enumerate(self.models[-1].datas):
            for s in self.proto_states:
                for c in range(9):
                    mask1 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.5, datum[:, 0] == c), datum[:, 1] == 0)
                    mask2 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.5, datum[:, 0] == c), datum[:, 1] == 2)

                    answers[self.state_map[s], int(np.round(i / (n - 1))), c, 0] += np.sum(mask1)
                    answers[self.state_map[s], int(np.round(i / (n - 1))), c, 1] += np.sum(mask2)

        answers /= answers.sum(3)[..., None]
        plt.figure(figsize=(11, 8))
        for i, s in enumerate(self.proto_states):
            if i == 0:
                plt.plot(answers[self.state_map[s], 0, :, 0], color=self.state_to_color[s], label='First Half', ls='-')
                plt.plot(answers[self.state_map[s], 1, :, 0], color=self.state_to_color[s], label='Second Half', ls='--')
            else:
                plt.plot(answers[self.state_map[s], 0, :, 0], color=self.state_to_color[s], ls='-')
                plt.plot(answers[self.state_map[s], 1, :, 0], color=self.state_to_color[s], ls='--')
        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.yticks(size=22-3)
        plt.xticks(np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=22-2)
        plt.ylabel('P(answer right)', size=22)
        plt.xlabel('Contrast', size=22)

        plt.legend(fontsize=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=8)

        plt.tight_layout()
        plt.savefig("figures/convergence/pmf over halves {}".format(self.name))
        plt.show()


    def pmf_compare(self):

        # for s in self.proto_states:
        #     pmfs = np.zeros((len(self.models), 9))
        #     for i, m in enumerate(self.models):
        #         pmfs[i] = m.obs_distns[s].weights[:, 0]
        #
        #     plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
        #     plt.plot(np.percentile(pmfs, [2.5, 97.5], axis=0).T, color=self.state_to_color[s], ls='--')
        # plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
        # plt.ylim(bottom=0, top=1)
        # plt.legend()

        n = len(self.models[0].stateseqs)
        # for every state, first half or second half, for every contrast, what was the answer?
        answers = np.zeros((len(self.proto_states), 9, 2))
        for i, datum in enumerate(self.models[-1].datas):
            for s in self.proto_states:
                for c in range(9):
                    mask1 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.3, datum[:, 0] == c), datum[:, 1] == 0)
                    mask2 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.3, datum[:, 0] == c), datum[:, 1] == 2)

                    answers[self.state_map[s], c, 0] += np.sum(mask1)
                    answers[self.state_map[s], c, 1] += np.sum(mask2)

        answers /= answers.sum(2)[..., None]

        for s in self.proto_states:
            plt.plot(answers[self.state_map[s], :, 0], color=self.state_to_color[s], label=s, ls=':')
        plt.show()

    def empirical_cmf(self):

        rts = np.zeros((len(self.proto_states), 9))
        counts = np.zeros((len(self.proto_states), 9))

        for i, p in enumerate(self.posteriors):
            certain_states = p > 0.6

            try:
                rt = pickle.load(open("./session_data/rt_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                print("warning {} {}".format(self.name, i))
                continue

            for s in self.proto_states:
                for c in range(9):
                    mask = np.logical_and(certain_states[self.state_map[s]], self.models[-1].datas[i][:, 0] == c)
                    if np.sum(mask) == 0:
                        continue
                    counts[self.state_map[s], c] += np.sum(mask) - np.sum(np.isnan(rt[mask]))
                    rts[self.state_map[s], c] += np.sum(np.nan_to_num(rt[mask]))

        print(rts)
        print(counts)
        for s in self.proto_states:
            plt.plot(rts[self.state_map[s]] / counts[self.state_map[s]], color=self.state_to_color[s], label=s)
        plt.legend()
        plt.show()


    def rt_plots(self):
        mod = self.models[-1]
        for i, s in enumerate(self.proto_states):
            print()
            plt.subplot(3, 1, i + 1)
            plt.title('State {}'.format(s), color=self.state_to_color[s], size=22)
            for c in range(9):
                mu, sigma = mod.obs_distns[s].normals[c].mu, mod.obs_distns[s].normals[c].sigma[0]
                print(mu, sigma)
                points = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 100)
                pdf = norm.pdf(points, mu, sigma)
                plt.xlim(left=0, right=3)
                plt.ylim(bottom=0, top=2.8)
                l = num_to_cont[c] if i == 0 else None
                lw = 2.5 if c == 4 else 1
                plt.plot(np.exp(points), pdf, label=l, lw=lw)
                if i == 0:
                    plt.legend()
        plt.show()



from_tills = [(0, 12), (0, 17), (0, 11), (0, 23), (0, 8), (0, 17), (0, 9), (0, 16), (0, 26), (0, 15), (0, 14), (0, 19), (0, 13), (0, 15), (0, 32), (0, 20)]
subjects = ['CSHL059', 'DY_008', 'DY_009', 'CSHL045', 'CSH_ZAD_024', 'ibl_witten_17', 'CSH_ZAD_017', 'CSHL060', 'CSH_ZAD_011', 'ZM_3003', 'CSH_ZAD_025', 'ibl_witten_18', 'ibl_witten_20', 'CSH_ZAD_026', 'CSH_ZAD_019', 'KS020']

from_tills = [(0, 23)]
subjects = ['CSHL045']

switches = []
accs = []
accs0 = []

for from_till, subject in zip(from_tills, subjects):
    fit = 'time ioihmm fit {} {}'.format(subject, from_till)
    models = pickle.load(open("./{}.p".format(fit), "rb"))
    session_names = pickle.load(open("./session_data/{}_session_names.p".format(subject), "rb"))

    result = MCMC_result(models[1000:], threshold=500, name=subject, seq_start=52)


    # plt.figure(figsize=(12, 9))
    # sns.heatmap(np.round(result.models[0].obs_distns[result.proto_states[1]].weights, 2), annot=True, cbar=0, annot_kws={'size': 20})
    # plt.ylabel('Contrast', size=28)
    # plt.yticks(np.arange(9) + 0.5, [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], rotation='horizontal', size=19)
    # plt.xlabel('Answer', size=28)
    # plt.xticks(np.arange(3) + 0.5, ['Right', 'Timeout', 'Left'], size=24)
    # plt.tight_layout()
    # plt.savefig("categoricals.png")
    # plt.show()
    #
    # plt.figure(figsize=(12, 9))
    # plt.plot(result.models[0].obs_distns[result.proto_states[1]].weights[:, 0])
    # plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=19)
    # plt.yticks(size=19)
    # plt.xlabel('Contrast', size=28)
    # plt.ylabel('P(answer rightward)', size=28)
    # plt.ylim(bottom=0, top=1)
    # plt.xlim(left=0, right=8)
    # sns.despine()
    #
    # plt.tight_layout()
    # plt.savefig("simple_pmf")
    # plt.show()
    # quit()

    # result.calc_posterior()
    # result.empirical_cmf()
    # continue

    #result.rt_plots()
    result.calc_posterior()
    result.pmf_posterior()
    result.sequence_posteriors()
    # a, b, c = result.switchiness()
    # switches.append(a)
    # accs.append(b)
    # accs0.append(c)
    # continue

    # result.sequence_posteriors()
    # result.switchiness()
    # #result.pmf_compare()
    # quit()
    # #
    #result.block_performance()
    #result.contrasts_plot(until=5)
    #result.sequence_posteriors(show=True, until=5, save=True)
    #result.state_development()
    #result.block_alignment()

    #result.single_state_resample(100)
    #result.assign_evolution()
    #result.transition_hists()
    # result.sequence_heatmap(normalization='total', until=5)
    #result.pmf_plots()
    #result.param_hist(lambda x, s: x.dur_distns[s].r)
    #result.param_hist(lambda x, s: x.dur_distns[s].p)
    #result.pmf_posterior()
    #result.sequence_posteriors(show=True, until=5, save=True)

quit()
plt.plot(np.concatenate(switches).ravel(), np.concatenate(accs).ravel(), 'ko')
plt.xlabel('Switchiness', size=18)
plt.ylabel('Accuracy', size=18)

plt.tight_layout()
plt.savefig('Switchiness and acc')
plt.show()

plt.plot(np.concatenate(switches).ravel(), np.concatenate(accs0).ravel(), 'ko')
plt.show()
