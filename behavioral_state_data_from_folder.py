import os
from one.api import ONE
import numpy as np
import pandas as pd

one = ONE()

for subject in ['fip_15']: # os.listdir("./fibre_data/"):

	eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2025-01-01'], details=True)

	start_times = [sess['date'] for sess in sess_info]
	protocols = [sess['task_protocol'] for sess in sess_info]
	nums = [sess['number'] for sess in sess_info]

	# print("original # of eids {}".format(len(eids)))

	test = [(y, x) for y, x in sorted(zip(start_times, eids))]

	eids = [x for _, x in sorted(zip(start_times, eids))]
	dates = [x for x, _ in sorted(zip(start_times, eids))]
	nums = [x for _, x in sorted(zip(start_times, nums))]
	start_times = sorted(start_times)

	prev_date = None
	prev_num = -1
	fixed_dates = []
	fixed_eids = []
	fixed_start_times = []
	additional_eids = []
	for d, e, n, st in zip(dates, eids, nums, start_times):
		if d != prev_date:
			fixed_dates.append(d)
			fixed_eids.append(e)
			additional_eids.append([])
			fixed_start_times.append(st)
		else:
			assert n > prev_num
			if n == 1:
				additional_eids.append([e])
			elif n > 1:
				additional_eids[-1].append(e)
		prev_date = d
		prev_num = n

	direct_dates = sorted(os.listdir("./fibre_data/" + subject))

	if len(fixed_dates) != len(direct_dates):
		print(len(fixed_dates), len(direct_dates))

		rel_count = 0
		effective_dates = []
		for i, (eid, extra_eids, start_time) in enumerate(zip(fixed_eids, additional_eids, sorted(fixed_start_times))):
			try:
				trials = one.load_object(eid, 'trials')
				if 'choice' not in trials:
					 continue
			except Exception as e:
				print(e, 'skipped session')
				continue

			if trials['probabilityLeft'] is None: # originally also "or df is None", if this gets reintroduced, probably need to download all data again
				if rel_count != -1:
					 print('lost session, problem')
					 misses.append((subject, i))
				continue
			rel_count += 1
			effective_dates.append(start_time)
			print(start_time, "rel_count {}".format(rel_count), len(trials.choice), np.unique(trials.choice, return_counts=1))
	else:
		for d1, d2 in zip(fixed_dates, direct_dates):
			assert str(d1) == d2
		print(subject, 'good')
	continue

	for date in sorted(os.listdir("./fibre_data/" + subject)):
		print(subject, date)

	quit()
