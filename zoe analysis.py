import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def load_glm_hmm_data(file):
    container = np.load(file, allow_pickle=True)
    data = [container[key] for key in container]
    posterior_probs = data[0]
    inpt = data[1]
    y = data[2]
    session = data[3]
    weight_vectors = data[4]
    transition_matrix = data[5]
    init_state_dist = data[6]
    return posterior_probs, inpt, y, session, weight_vectors, transition_matrix, init_state_dist

subject = 'KS005'
posterior_probs, inpt, y, session, weight_vectors, transition_matrix, init_state_dist = load_glm_hmm_data("./posterior_probs_for_seb/{}_posterior_probs.npz".format(subject))

contrasts = np.flip(np.array([-1, -0.25, -0.125, -.0625, 0, 0.0625, 0.125, .25, 1.]), axis=0)

# for i in range(3):
#     choice_prob = 1 / (1 + np.exp(weight_vectors[i, 0, 0] * contrasts + weight_vectors[i, 0, 3]))
#     plt.plot(choice_prob)
# plt.show()


conts = np.unique(inpt[:, 0])
bias_cont_ticks = (np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
state2col = dict(zip([0, 1, 2], ['k', 'r', 'b']))

plt.figure(figsize=(11, 9))
for s in range(3):
    temp = np.zeros(9)
    for i, c in enumerate(conts):
        relevant = np.logical_and(posterior_probs[:, s] > 0.75, inpt[:, 0] == c)
        temp[i] = np.sum(y[:, 0][relevant]) / np.sum(relevant)
    plt.plot(temp, label=s, c=state2col[s])

plt.xticks(*bias_cont_ticks, size=22-1)
plt.xlim(left=0)
plt.yticks(size=22-2)
plt.xlabel('Contrast', size=22)
plt.ylabel('P(answer rightward)', size=22)

plt.title("Zoe psychometric functions {}".format(subject), size=22)
sns.despine()
plt.tight_layout()
plt.savefig("dynamic_figures/convergence/zoe pmfs with conf {}".format(subject))
plt.show()

for i in range(50):
    plt.title(i+1)
    cs = inpt[i*90:(i+1)*90, 0]
    plt.plot(cs, '.')
    plt.show()
quit()
for i in range(50):
    assert session[i*90] == session[i*90 + 90 - 1]
    plt.figure(figsize=(11, 9))
    plt.plot(posterior_probs[i*90: i*90 + 90, 0], label=0, c=state2col[0], lw=4)
    plt.plot(posterior_probs[i*90: i*90 + 90, 1], label=1, c=state2col[1], lw=4)
    plt.plot(posterior_probs[i*90: i*90 + 90, 2], label=2, c=state2col[2], lw=4)
    plt.title("Zoe {} session {}".format(subject, i + 1), size=22)
    plt.xlim(left=0, right=90)
    plt.xticks(size=22-1)
    plt.yticks(size=22)
    plt.ylabel('P(State)', size=22)
    plt.xlabel('Trial', size=22)
    sns.despine()
    plt.tight_layout()
    print("figures/state_seqs/{} zoe.png".format(session[i*90]))
    plt.savefig("figures/state_seqs/{} zoe.png".format(session[i*90]))
    plt.close()
