"""
Generate data from a simulated mouse.

This mouse has 1 state before the bias starts, then uses two states,
with state identity being dictated by what was inferred for DY_013
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle

pmfs = np.zeros((3, 11))
pmfs[2] = np.array([0.1, 0.22, 0.19, 0.19, 0.37, 0.47, 0.6, 0.8, 0.95, 0.87, 0.97])
pmfs[0] = np.array([0.07, 0.15, 0.06, 0.13, 0.16, 0.22, 0.38, 0.75, 0.8, 0.75, 0.82])
pmfs[1] = np.array([0.25, 0.32, 0.29, 0.27, 0.62, 0.65, 0.8, 0.95, 0.95, 0.87, 0.97])

plt.plot(pmfs.T)
plt.show()

subjects = ['DY_013']
new_name = 'Sim_00'
seed = 2

for subject in subjects:
    print(subject)
    np.random.seed(seed)

    info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
    assert info_dict['subject'] == subject
    info_dict['subject'] = new_name
    pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
    # Determine session numbers
    till_session = info_dict['n_sessions']
    from_session = 0
    # Determine # of inputs

    for j in range(from_session, till_session + 1):
        try:
            data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
            states = pickle.load(open("../states_{}_{}_condition_{}_{}.p".format(subject, 'all', 'nothing', '0_01'), 'rb'))
        except FileNotFoundError:
            continue

        data = data.astype(int)

        assert len(states[j]) == len(data[:, 0])
        rands = np.random.rand(len(states[j]))
        for i, (c, s) in enumerate(zip(data[:, 0], states[j].astype(int))):
            data[i, 1] = rands[i] > pmfs[s, c]
        pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
