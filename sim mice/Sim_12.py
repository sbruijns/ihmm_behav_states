"""
Generate data from a simulated mouse.

This mouse uses 2 maximally distinguishable states with random PMFs and transition matrix
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom
from scipy.linalg import eig

np.set_printoptions(suppress=True)
new_name = 'Sim_12'
seed = 13

n_states = 2

print(new_name)
np.random.seed(seed)

pmfs = np.zeros((n_states, 11))
pmfs[0] = 1
plt.plot(pmfs.T)
plt.show()

transition_mat = np.random.dirichlet(np.ones(n_states) * 0.5, (n_states))
np.fill_diagonal(transition_mat, 0)  # remove self-transition, as model assumes
transition_mat = transition_mat / np.sum(transition_mat, axis=1)[:, None]
S, U = eig(transition_mat.T)
stationary = np.array(U[:, np.where(np.abs(S - 1.) < 1e-8)[0][0]].flat)
stationary = stationary / np.sum(stationary)

print(stationary)

info_dict = {}
info_dict['subject'] = new_name
info_dict['bias_start'] = 10
info_dict['n_sessions'] = 30
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
# Determine session numbers
till_session = info_dict['n_sessions']
from_session = 0
# Determine # of inputs
state_posterior = np.zeros((30, n_states))


for j in range(from_session, till_session):

    n = int(500 + np.random.rand() * 500)
    data = np.zeros((n, 2), dtype=np.int)
    data[:, 0] = np.random.randint(11, size=n)

    rands = np.random.rand(n)
    state_plot = np.zeros(n_states)

    count = 0
    curr_state = np.random.randint(n_states)
    while count < n:
        dur = min(nbinom.rvs(21, 0.3), n - count)
        data[count:count + dur, 1] = rands[count:count + dur] > pmfs[curr_state, data[count:count + dur, 0]]
        state_plot[curr_state] += dur
        count += dur
        curr_state = np.random.choice(n_states, p=transition_mat[curr_state])
    state_posterior[j] = state_plot / len(data[:, 0])

    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))

plt.figure(figsize=(16, 9))
for s in range(n_states):
    print(state_posterior[:, s])
    plt.fill_between(range(till_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)
    plt.plot(np.linspace(till_session, till_session + 3, 11), s + pmfs[s] - 0.5)
plt.show()
