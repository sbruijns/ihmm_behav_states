"""
Generate data from a simulated mouse.

This mouse uses 1 stationary biased state throughout
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle

pmfs = np.array([0.01, 0.02, 0.05, 0.08, 0.08, 0.12, 0.18, 0.36, 0.57, 0.68, 0.8])

subject = 'CSHL059'
new_name = 'Sim_01_biased'
seed = 3

print(subject)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['n_sessions']
from_session = 0


for j in range(from_session, till_session + 1):
    try:
        data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    except FileNotFoundError:
        continue

    data = data.astype(int)

    rands = np.random.rand(len(data[:, 0]))
    data[:, 1] = rands > pmfs[data[:, 0]]
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
