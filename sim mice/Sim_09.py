"""
Generate data from a simulated mouse.

This mouse uses 10 states with random PMFs and transition matrix
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom
from scipy.linalg import eig

np.set_printoptions(suppress=True)
new_name = 'Sim_09'
seed = 10

print(new_name)
np.random.seed(seed)

pmfs = np.random.rand(10, 11)
plt.plot(pmfs.T)
plt.show()

transition_mat = np.random.dirichlet(np.ones(10) * 0.5, (10))
S, U = eig(transition_mat.T)
stationary = np.array(U[:, np.where(np.abs(S - 1.) < 1e-8)[0][0]].flat)
stationary = stationary / np.sum(stationary)

print(stationary)

info_dict = {}
info_dict['subject'] = new_name
info_dict['bias_start'] = 10
info_dict['n_sessions'] = 30
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
# Determine session numbers
till_session = info_dict['n_sessions']
from_session = 0
# Determine # of inputs
state_posterior = np.zeros((30, 10))


for j in range(from_session, till_session):

    n = int(500 + np.random.rand() * 500)
    data = np.zeros((n, 2), dtype=np.int)
    data[:, 0] = np.random.randint(11, size=n)

    rands = np.random.rand(n)
    state_plot = np.zeros(10)

    count = 0
    curr_state = np.random.randint(10)
    while count < n:
        dur = nbinom.rvs(21, 0.3)
        data[count:count + dur, 1] = rands[count:count + dur] > pmfs[curr_state, data[count:count + dur, 0]]
        state_plot[curr_state] += dur
        count += dur
        curr_state = np.random.choice(10, p=transition_mat[curr_state])
    state_posterior[j] = state_plot / len(data[:, 0])

    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))

plt.figure(figsize=(16, 9))
for s in range(10):
    plt.fill_between(range(till_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)
    plt.plot(np.linspace(till_session, till_session + 3, 11), s + pmfs[s] - 0.5)
plt.show()
