"""
Generate data from a simulated mouse.

This mouse uses 1 state before bias, then 2 biased states, which state is used depends on beliefs computed from Charles
these belief are thresholded
his priors is sometimes a little bit shorter than the actual data...
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
import seaborn as sns

pmfs = np.zeros((3, 11))
pmfs[2] = np.array([0.1, 0.22, 0.19, 0.19, 0.37, 0.47, 0.6, 0.8, 0.95, 0.87, 0.97])
pmfs[0] = np.array([0.07, 0.15, 0.06, 0.13, 0.16, 0.22, 0.38, 0.75, 0.8, 0.75, 0.82])
pmfs[1] = np.array([0.25, 0.32, 0.29, 0.27, 0.62, 0.65, 0.8, 0.95, 0.95, 0.87, 0.97])


subject = 'CSHL062'
new_name = 'Sim_04_inv'
seed = 5

print(new_name)
np.random.seed(seed)


all_cont_ticks = (np.arange(11), [-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])
state2col = dict(zip([0, 1, 2], ['k', 'r', 'b']))
plt.figure(figsize=(11, 9))

plt.plot(pmfs[2], label='normal')
plt.plot(pmfs[0], label='left')
plt.plot(pmfs[1], label='right')

plt.xticks(*all_cont_ticks, size=22-1)
plt.xlim(left=0)
plt.yticks(size=22-2)
plt.xlabel('Contrast', size=22)
plt.ylabel('P(answer rightward)', size=22)

plt.title("Recovery truth", size=22)
sns.despine()
plt.tight_layout()
plt.savefig("{} pmfs".format(subject))
plt.show()


info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
# Determine session numbers
till_session = info_dict['n_sessions']
from_session = 0
# Determine # of inputs

priors = pickle.load(open("../session_data/{}_priors_act.p".format(subject), "rb"))


plt.figure(figsize=(11, 9))
plt.plot(priors[0])

plt.xticks(size=22-1)
plt.xlim(left=0)
plt.yticks(size=22-2)
plt.xlabel('Trial', size=22)
plt.ylabel('P', size=22)
plt.xlim(right=750)

plt.title("Computed block prior", size=22)
sns.despine()
plt.tight_layout()
plt.savefig("{} prior sess 1".format(subject))
plt.show()

plt.figure(figsize=(11, 9))
plt.plot(priors[0] > 0.5)

plt.xticks(size=22-1)
plt.xlim(left=0)
plt.yticks(size=22-2)
plt.xlabel('Trial', size=22)
plt.ylabel('P', size=22)
plt.xlim(right=750)

plt.title("Computed block prior", size=22)
sns.despine()
plt.tight_layout()
plt.savefig("{} thresh prior sess 1".format(subject))
plt.show()


for j in range(from_session, till_session + 1):
    try:
        data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    except FileNotFoundError:
        continue

    data = data.astype(int)

    rands = np.random.rand(len(data[:, 0]))
    if j < info_dict['bias_start']:
        data[:, 1] = rands > pmfs[2, data[:, 0]]
    else:
        plt.plot(priors[j - info_dict['bias_start'], :len(data[:, 0])] > 0.5)
        plt.show()
        data[:, 1] = rands > pmfs[(priors[j - info_dict['bias_start'], :len(data[:, 0])] < 0.5).astype(int), data[:, 0]]
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
