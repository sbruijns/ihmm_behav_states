"""
Generate data from a simulated mouse.

This mouse uses 2 dynamic states throughout
They start a certain distance apart, states alternate during the session, duration is negative binomial
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from driftin_obs.pgmult_generation import noisy_mult
from scipy.stats import nbinom

dist = 0.6
var = 0.06

subject = 'CSHL059'
new_name = 'Sim_03_5_dist_{}_{}'.format(dist, var).replace('.', '_')
# original seed: 4
seed = 4

print(new_name)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['n_sessions']
from_session = 0

pmfs = [[noisy_mult(n=1, T=till_session+1, sdev=np.sqrt(var), start=np.array(dist)) for _ in range(11)],
        [noisy_mult(n=1, T=till_session+1, sdev=np.sqrt(var), start=np.array(-dist)) for _ in range(11)]]

pmfs_save = [[], []]
for j in range(from_session, till_session + 1):
    plt.plot([pmfs[0][0].natural2mean(pmfs[0][i].params[j])[0] for i in range(11)], c='b')
    plt.plot([pmfs[0][0].natural2mean(pmfs[1][i].params[j])[0] for i in range(11)], c='r')
    pmfs_save[0].append([pmfs[0][0].natural2mean(pmfs[0][i].params[j])[0] for i in range(11)])
    pmfs_save[1].append([pmfs[0][0].natural2mean(pmfs[1][i].params[j])[0] for i in range(11)])
pickle.dump(pmfs_save, open("../session_data/{}_pmfs.p".format(new_name), "wb"))
plt.ylim(bottom=0, top=1)
plt.show()

state_posterior = np.zeros((till_session + 1, 2))

for j in range(from_session, till_session + 1):
    try:
        data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    except FileNotFoundError:
        continue

    data = data.astype(int)

    rands = np.random.rand(len(data[:, 0]))
    state_plot = np.zeros(2)

    count = 0
    curr_state = int(np.random.rand() > 0.5)
    dur = nbinom.rvs(21, 0.3)
    for i, c in enumerate(data[:, 0]):
        data[i, 1] = rands[i] > pmfs[0][0].natural2mean(pmfs[curr_state][c].params[j])[0]
        state_plot[curr_state] += 1
        if dur == 0:
            curr_state = (curr_state + 1) % 2
            dur = nbinom.rvs(21, 0.3)
        else:
            dur -= 1
    state_posterior[j] = state_plot / len(data[:, 0])
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))

plt.figure(figsize=(16, 9))
for s in range(2):
    plt.fill_between(range(till_session+1), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)

plt.show()
