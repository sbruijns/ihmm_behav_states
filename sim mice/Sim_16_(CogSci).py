"""
Generate data from a simulated mouse.

Showing different state configurations for cogsci
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom

subject = 'ibl_witten_13'
new_name = 'Sim_16'
seed = 17

print(new_name)
np.random.seed(seed)

pmfs = np.array([[0.93, 0.92, 0.92, 0.93, 0.91, 0.92, 0.92, 0.91, 0.92, 0.93, 0.95],
                 [0.51, 0.5, 0.52, 0.53, 0.52, 0.5, 0.53, 0.49, 0.5, 0.51, 0.52],
                 [0.17, 0.18, 0.22, 0.27, 0.36, 0.4, 0.51, 0.52, 0.53, 0.51, 0.53],
                 [0.2, 0.21, 0.22, 0.27, 0.56, 0.78, 0.9, 0.92, 0.94, 0.96, 0.97],
                 [0.03, 0.04, 0.05, 0.07, 0.09, 0.2, 0.5, 0.67, 0.74, 0.77, 0.78]])
plt.plot(pmfs.T)
plt.show()


info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
info_dict['bias_start'] = info_dict['bias_start'] + 3
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
# Determine session numbers
till_session = info_dict['bias_start']
from_session = 0
# Determine # of inputs

print(till_session)
states = [[0], [0], [1], [1], [1, 2], [2], [2], [2], [2], [1], [1], [2], [4, 3], [4, 3], [4, 3], [4, 3], [4, 3], [4, 3]]
state_posterior = np.zeros((till_session, 6))

for j in range(from_session, till_session):
    try:
        data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
        if j > info_dict['bias_start'] - 3:
            # plt.plot(data[:, 0], 'k.')
            # plt.title('orig')
            # plt.show()
            data[:, 0] = np.random.choice([0, 2, 3, 4, 5, 6, 7, 8, 10], size=len(data))
            # plt.plot(data[:, 0], 'k.')
            # plt.show()
    except FileNotFoundError:
        continue

    data = data.astype(int)

    rands = np.random.rand(len(data[:, 0]))
    state_plot = np.zeros(6)

    count = 0
    curr_state = 0
    if len(states[j]) == 1:
        data[:, 1] = rands > pmfs[states[j][0], data[:, 0]]
        state_plot[states[j][0]] += len(data[:, 0])
    else:
        if states[j] == [1, 2]:
            data[:len(data[:, 0]) // 2, 1] = rands[:len(data[:, 0]) // 2] > pmfs[1, data[:len(data[:, 0]) // 2, 0]]
            state_plot[1] += len(data[:, 0]) // 2
            data[len(data[:, 0]) // 2:, 1] = rands[len(data[:, 0]) // 2:] > pmfs[2, data[len(data[:, 0]) // 2:, 0]]
            state_plot[2] += len(data[:, 0]) - len(data[:, 0]) // 2
        elif states[j] == [4, 3]:
            while count < len(data[:, 0]):
                dur = nbinom.rvs(21, 0.3) + 1 if states[j][curr_state % len(states[j])] == 3 else nbinom.rvs(15, 0.25) + 1
                data[count:count + dur, 1] = rands[count:count + dur] > pmfs[states[j][curr_state % len(states[j])], data[count:count + dur, 0]]
                state_plot[states[j][curr_state % len(states[j])]] += dur
                count += dur
                curr_state += 1
    state_posterior[j] = state_plot / len(data[:, 0])

    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
plt.figure(figsize=(16, 9))
for s in range(5):
    plt.fill_between(range(till_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)
plt.show()

pickle.dump((pmfs, state_posterior, ['inf', 'inf', 'inf', (21, 0.3), (15, 0.25)]), open("sim_16_truth.p", "wb"))
