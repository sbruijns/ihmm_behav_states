"""
Generate data from a simulated mouse.

6 states in a pretty wild mix, durations according to nbinom
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom

subject = 'CSH_ZAD_025'
new_name = 'Sim_07'
seed = 8

print(new_name)
np.random.seed(seed)

pmfs = np.random.rand(6, 11)
plt.plot(pmfs[[0, 1]].T)
plt.show()


info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
# Determine session numbers
till_session = info_dict['n_sessions']
from_session = 0
# Determine # of inputs

states = [[0, 1], [0, 1], [0, 2], [0, 2], [3, 4], [3, 4], [3, 4], [3, 5], [3, 5], [3, 5], [0, 2], [2, 5], [2, 5],
          [4, 1], [0, 1, 2], [3, 5], [4, 1], [4, 1], [0, 1, 2], [0, 1, 2], [3, 5, 2], [3, 5, 2], [3, 5, 2], [3, 5, 2],
          [3, 5, 2], [0, 1, 2], [0, 2], [3, 4], [3, 4], [3, 4], [4, 1], [0, 1], [0, 2], [0, 2], [2, 5], [4, 1]]
state_posterior = np.zeros((till_session + 1, 6))

for j in range(from_session, till_session + 1):
    try:
        data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    except FileNotFoundError:
        continue

    data = data.astype(int)

    rands = np.random.rand(len(data[:, 0]))
    state_plot = np.zeros(6)

    count = 0
    curr_state = 0
    while count < len(data[:, 0]):
        dur = nbinom.rvs(21, 0.3)
        data[count:count + dur, 1] = rands[count:count + dur] > pmfs[states[j][curr_state % len(states[j])], data[count:count + dur, 0]]
        state_plot[states[j][curr_state % len(states[j])]] += dur
        count += dur
        curr_state += 1
    state_posterior[j] = state_plot / len(data[:, 0])

    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
plt.figure(figsize=(16, 9))
for s in range(6):
    plt.fill_between(range(till_session + 1), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)
plt.show()
