"""
Generate data from a simulated mouse.

Mouses uses 3 states, 1 possible at start (slightly inattentive, then 2 states mainly active during
session (good and slightly biased))
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom
from scipy.linalg import eig


new_name = 'Sim_11'
seed = 12

print(new_name)
np.random.seed(seed)

pmfs = np.array([[0.15, 0.13, 0.24, 0.33, 0.51, 0.59, 0.78, 0.83, 0.86],
                 [0.03, 0.03, 0.05, 0.1, 0.5, 0.87, 0.92, 0.95, 0.98],
                 [0.12, 0.13, 0.15, 0.21, 0.62, 0.92, 0.94, 0.96, 0.99]])
plt.plot(pmfs.T)
plt.show()

transition_mat = np.array([[0, 0.5, 0.5],
                           [0, 0, 1],
                           [0, 1, 0]])
S, U = eig(transition_mat.T)
stationary = np.array(U[:, np.where(np.abs(S - 1.) < 1e-8)[0][0]].flat)
stationary = stationary / np.sum(stationary)

print(stationary)

info_dict = {}
info_dict['subject'] = new_name
info_dict['bias_start'] = 10
info_dict['n_sessions'] = 30
info_dict[2] = 0
info_dict[3] = 0
info_dict[4] = 0
info_dict[5] = 0
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))
# Determine session numbers
till_session = info_dict['n_sessions']
from_session = 0
# Determine # of inputs
state_posterior = np.zeros((30, pmfs.shape[0]))


for j in range(from_session, till_session):

    n = int(500 + np.random.rand() * 500)
    data = np.zeros((n, 2), dtype=np.int)
    data[:, 0] = np.random.randint(9, size=n)

    rands = np.random.rand(n)
    state_plot = np.zeros(pmfs.shape[0])

    count = 0
    curr_state = 0
    while count < n:
        dur = nbinom.rvs(21, 0.3)
        data[count:count + dur, 1] = rands[count:count + dur] > pmfs[curr_state, data[count:count + dur, 0]]
        state_plot[curr_state] += dur
        count += dur
        curr_state = np.random.choice(pmfs.shape[0], p=transition_mat[curr_state])
    state_posterior[j] = state_plot / len(data[:, 0])

    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))

plt.figure(figsize=(16, 9))
for s in range(pmfs.shape[0]):
    plt.fill_between(range(till_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)
    plt.plot(np.linspace(till_session, till_session + 3, 9), s + pmfs[s] - 0.5)
plt.show()
