import numpy as np
import matplotlib.pyplot as plt
import time


class noisy_mult:

    def __init__(self, T=40, n=100, n_words=2, sdev=0.3, start=None):

        self.T = T  # number of timesteps
        self.n = n  # number of observations in one timestep (needn't be constant actually)
        self.n_words = n_words  # number of different possible words
        self.sdev = sdev
        self.start = start

        self.obs = np.zeros((self.T, self.n_words), dtype=np.int32)
        self.params = np.zeros((self.T, self.n_words-1))
        self.produce_obs()

    def produce_obs(self):

        for t in range(self.T):
            if t == 0:
                if self.start is None:
                    self.params[0] = np.random.normal(0, self.sdev, self.n_words-1)
                else:
                    self.params[0] = self.start
            else:
                self.params[t] = self.params[t-1] + np.random.normal(0, self.sdev, self.n_words-1)

            self.obs[t] = np.random.multinomial(self.n, self.natural2mean(self.params[t]))
        return self.obs

    def natural2mean(self, p):
        if p.ndim == 1:
            temp = np.zeros(len(p) + 1)
            temp[:-1] = p
            return np.exp(temp) / np.sum(np.exp(temp))
        if p.ndim == 2:
            temp = np.zeros((p.shape[0], p.shape[1] + 1))
            temp[:, :-1] = p
            return np.exp(temp) / np.sum(np.exp(temp), axis=1)[:, None]

    def old_natural2mean(self, p):
        if p.ndim == 1:
            return np.exp(p) / np.sum(np.exp(p))
        if p.ndim == 2:
            return np.exp(p) / np.sum(np.exp(p), axis=1)[:, None]

    def indiv_obs(self):
        indiv_obs = np.zeros((self.T, self.n), dtype=np.int32)
        for i, sums in enumerate(self.obs):
            indiv_obs[i] = np.repeat(np.arange(self.n_words), sums)
        return indiv_obs

    def plot_natural(self):
        plt.plot(*self.params.T)
        plt.scatter(*self.params.T, c=np.linspace(0, 1, self.T))
        plt.show()

    def plot_mean(self):
        plt.plot(self.natural2mean(self.params)[:, 0], label='Truth', c='r')
        plt.ylim(0, 1)

if __name__ == '__main__':
    test = noisy_mult()
    print(test.produce_obs())
