import numpy as np
import matplotlib.pyplot as plt
from itertools import product
from scipy.stats import invgamma, chi2, norm


prior_alpha = 0.08
prior_beta = 0.08
prior_scale = 0.2
np.random.seed(7)

prior = ['inv_gamma', 'uniform', 'folded_noncentral_t', 'linear_grid', 'log_grid'][4]
ns = [80, 80, 80]
gen_vars = [0.00001, 0.1, 4]

n_samples = 350
burned_trace_n = 100

plt.figure(figsize=(16, 9))
for i, (n, gen_var) in enumerate(product(ns, gen_vars)):
    posterior_sigma = np.zeros((n_samples))
    np.random.seed(6 + i)
    data = np.random.normal(0, gen_var**(1/2), n)

    fraction = np.sum(data**2) / 2
    if prior == 'inv_gamma':
        posterior_sigma = invgamma.rvs(prior_alpha + n / 2, scale=prior_beta + fraction, size=n_samples)
    elif prior == 'uniform':
        posterior_sigma = 2 * fraction / chi2(data.shape[0] - 1).rvs(n_samples)
    elif prior == 'folded_noncentral_t':
        posterior_sigma = np.zeros(n_samples)
        xi = np.random.normal(0, 1)  # initialise from prior
        sigma_eta = invgamma.rvs(1 / 2, scale=(prior_scale ** 2) / 2)
        for j in range(n_samples):
            temp = data / xi
            temp_fraction = np.sum(temp**2) / 2
            sigma_eta = invgamma.rvs(1 / 2 + data.shape[0] / 2, scale=(prior_scale ** 2) / 2 + temp_fraction)
            posterior_sigma[j] = np.absolute(xi * np.sqrt(sigma_eta)) ** 2
            temp = data / np.sqrt(sigma_eta)  # messing with this seems to make no difference whatsoever
            xi = np.random.normal(1 / (1 + data.shape[0]) * np.sum(temp), (1 / (1 + data.shape[0]))**2)
    elif prior == 'linear_grid':
        vars = np.linspace(0, 1, 401)[1:]
        likes = np.zeros(400)
        for j, var in enumerate(vars):
            likes[j] = np.sum(norm.logpdf(data, 0, var))
        likes = np.exp(likes) / np.sum(np.exp(likes))
        posterior_sigma = np.random.choice(vars ** 2, n_samples, p=likes)
    elif prior == 'log_grid':
        vars = np.logspace(np.log(1e-20), np.log(1), 400)
        likes = np.zeros(400)
        for j, var in enumerate(vars):
            likes[j] = np.sum(norm.logpdf(data, 0, var))
        likes = np.exp(likes) / np.sum(np.exp(likes))
        posterior_sigma = np.random.choice(vars ** 2, n_samples, p=likes)

    burned_trace = posterior_sigma[-burned_trace_n:]

    plt.subplot(3, 3, i+1)
    plt.hist(burned_trace)
    plt.axvline(np.mean(burned_trace, axis=0), c='b', label='Posterior mean')
    plt.axvline(gen_var, c='r', label='Truth')

    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    if i == 0:
        plt.legend(frameon=False)
    if i < 6:
        # plt.xticks([])
        pass
    else:
        if i == 7:
            plt.xlabel('St devs', size=18)

    if i+1 == 2:
        plt.title("prior: {}".format(prior), size=22)
    if i % 3 != 0:
        # plt.yticks([])
        pass
    else:
        if i == 3:
            plt.ylabel('Occurences', size=18)

#plt.tight_layout()
plt.savefig("pure variance test, prior: {}".format(prior))
plt.show()
