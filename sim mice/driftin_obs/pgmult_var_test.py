import numpy as np
import matplotlib.pyplot as plt
from pgmult_generation import noisy_mult
from pgmult.lda import StickbreakingDynamicTopicsLDA
from scipy import sparse
from itertools import product
from scipy.stats import invgamma, chi2
np.set_printoptions(suppress=True)

prior_alpha = 0.08
prior_beta = 0.08
np.random.seed(7)

ns = [1000, 1000, 1000]
gen_sdevs = [0.01, 0.1, 0.4]
prior = ['inv_gamma', 'uniform'][1]

n_samples = 150
T = 100
burned_trace_n = 50


def natural2mean(p):
    if p.ndim == 1:
        temp = np.zeros(len(p) + 1)
        temp[:-1] = p
        return np.exp(temp) / np.sum(np.exp(temp))
    if p.ndim == 2:
        temp = np.zeros((p.shape[0], p.shape[1] + 1))
        temp[:, :-1] = p
        return np.exp(temp) / np.sum(np.exp(temp), axis=1)[:, None]

def old_natural2mean(p):
    if p.ndim == 1:
        return np.exp(p) / np.sum(np.exp(p))
    if p.ndim == 2:
        return np.exp(p) / np.sum(np.exp(p), axis=1)[:, None]


plt.figure(figsize=(16, 9))
for i, (n, gen_sdev) in enumerate(product(ns, gen_sdevs)):

    test = noisy_mult(n=n, T=T, sdev=gen_sdev)
    test.produce_obs()
    data = sparse.csr_matrix(test.obs)

    # def __init__(self, data, timestamps, K, alpha_theta)
    model = StickbreakingDynamicTopicsLDA(data, np.arange(T), K=1, alpha_theta=1, sigmasq_states=invgamma.rvs(prior_alpha, scale=prior_beta))

    lls = np.zeros(n_samples)
    posterior = np.zeros((n_samples, T))
    posterior_sigma = np.zeros((n_samples))
    for j in range(n_samples):
        model.resample()
        lls[j] = model.log_likelihood()
        posterior[j] = model.beta[:, 0, 0]
        posterior_sigma[j] = model.sigmasq_states

        assert T == model.psi.ravel().shape[0]
        fraction = np.sum(np.diff(model.psi.ravel())**2) / 2
        if prior == 'inv_gamma':
            model.sigmasq_states = invgamma.rvs(prior_alpha + (T - 1) / 2, scale=prior_beta + fraction)
        elif prior == 'uniform':
            model.sigmasq_states = 2 * fraction / chi2(model.psi.ravel().shape[0] - 1).rvs()

    burned_trace = posterior[-burned_trace_n:]

    temp = np.percentile(burned_trace, [2.5, 97.5], axis=0)

    plt.subplot(3, 3, i+1)
    plt.plot(np.mean(burned_trace, axis=0), c='b', label='Posterior')
    plt.fill_between(np.arange(T), temp[1], temp[0], color='b', alpha=0.2)
    # plt.plot(test.obs[:, 0] / n)
    test.plot_mean()
    plt.xlim(0, T)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    plt.title("est={:.3f}({:.3f}), true={:.3f}".format(np.mean(posterior_sigma[-burned_trace_n:]), np.median(posterior_sigma[-burned_trace_n:]), gen_sdev**2), size=18)
    if i == 0:
        plt.legend(frameon=False)
    if i < 6:
        plt.xticks([])
    else:
        if i == 7:
            plt.xlabel('Timepoints', size=18)

    if i % 3 != 0:
        plt.yticks([])
    else:
        if i == 3:
            plt.ylabel('Topic proportion/word proportion', size=18)

plt.tight_layout()
plt.savefig("extreme sampling variance test, prior {}".format(prior))
plt.show()
