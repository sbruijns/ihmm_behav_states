"""
Test to see how I can fix the problem of too few resamplings, by transferring some parameters.

This can be achieved by saving the psi variable of the model.
"""
import numpy as np
import matplotlib.pyplot as plt
from pgmult_generation import noisy_mult
from pgmult.lda import StickbreakingDynamicTopicsLDA
from scipy import sparse
from itertools import product
from pgmult.utils import psi_to_pi, pi_to_psi

n = 500
sdevs = [0.05, 0.2, 0.8]

meta_samples = 100
n_samples = [1, 15, 60]
T = 40
burned_trace_n = 0

save = 0  # dummy initialisation
plt.figure(figsize=(16, 9))
for i, (sdev, n_sample) in enumerate(product(sdevs, n_samples)):

    if i % 3 == 0:
        np.random.seed([47, 400, 46][i // 3])
        test = noisy_mult(n=n, T=T, sdev=0.2)
        test.produce_obs()
        data = sparse.csr_matrix(test.obs)

    # def __init__(self, data, timestamps, K, alpha_theta)
    lls = np.zeros(meta_samples)
    posterior = np.zeros((meta_samples, T))
    for j in range(meta_samples):
        model = StickbreakingDynamicTopicsLDA(data, np.arange(T), K=1, alpha_theta=1, sigmasq_states=sdev)
        for _ in range(n_sample):
            if j > 0:
                model.psi = save
            model.resample()
            save = model.psi
        lls[j] = model.log_likelihood()
        posterior[j] = model.beta[:, 0, 0]

    temp = np.percentile(posterior, [2.5, 97.5], axis=0)

    plt.subplot(3, 3, i+1)
    plt.plot(np.mean(posterior, axis=0), c='b', label='Posterior')
    plt.fill_between(np.arange(T), temp[1], temp[0], color='b', alpha=0.2)
    # plt.plot(test.obs[:, 0] / n)
    test.plot_mean()
    plt.xlim(0, T)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    plt.title("n_sample={}, dtm sdev={}".format(n_sample, sdev), size=18)
    if i == 0:
        plt.legend(frameon=False)
    if i < 6:
        plt.xticks([])
    else:
        if i == 7:
            plt.xlabel('Timepoints', size=18)

    if i % 3 != 0:
        plt.yticks([])
    else:
        if i == 3:
            plt.ylabel('Topic proportion/word proportion', size=18)

plt.tight_layout()
plt.savefig("n_resample fix summary")
plt.show()
