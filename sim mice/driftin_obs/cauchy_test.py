"""Understanding how the Cauchy distribution decomposes into Normal * root of inverse Gamma."""
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import cauchy, invgamma
np.set_printoptions(suppress=True)


scale = 0.00000000000000000002
n_bins = 100
plot_percentile = 0.95  # this is not exact for the half distribution
sample_size = 100000

rv = cauchy(scale=scale)
points = np.linspace(0, rv.ppf(plot_percentile))

xis = np.random.normal(0, 1, size=sample_size)
etas = invgamma.rvs(1/2, scale=(scale ** 2) / 2, size=sample_size)
alphas = np.absolute(xis * np.sqrt(etas))
cauchy_alphas = np.absolute(rv.rvs(size=sample_size))

bins = np.arange(0, rv.ppf(plot_percentile), rv.ppf(plot_percentile) / n_bins)
bins[-1] = 1e20

plt.figure(figsize=(16, 9))
plt.subplot(1, 2, 1)
plt.plot(points, 2 * rv.pdf(points))

plt.hist(alphas, bins=bins, density=True)
plt.ylim(bottom=0, top=2.2 * rv.pdf(0))
plt.xlim(left=0, right=rv.ppf(plot_percentile))
plt.title("Normal / Square root(Gamma)")

plt.subplot(1, 2, 2)
plt.plot(points, 2 * rv.pdf(points))
plt.hist(cauchy_alphas, bins=bins, density=True)
plt.ylim(bottom=0, top=2.2 * rv.pdf(0))
plt.xlim(left=0, right=rv.ppf(plot_percentile))
plt.title("Cauchy")

plt.show()
