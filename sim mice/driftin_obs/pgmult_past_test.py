"""
I want to test how pgmult deals with no past data.

Answer: It cannot
"""
import numpy as np
import matplotlib.pyplot as plt
from pgmult_generation import noisy_mult
from pgmult.lda import StickbreakingDynamicTopicsLDA
from scipy import sparse
from itertools import product


ns = [5, 50, 500]
sdevs = [0.2, 0.2, 0.2]

n_samples = 100
Ts = [4, 5, 6, 7, 8, 9]
jump_start, jump_end = 4, 9
T = 14
burned_trace_n = 50

plt.figure(figsize=(16, 9))
for i, (n, sdev) in enumerate(product(ns, sdevs)):

    test = noisy_mult(n=n, T=T, sdev=0.2)
    test.produce_obs()
    temp = np.zeros((len(Ts) + 1, 2))
    temp[:-1] = test.obs[Ts]
    data = sparse.csr_matrix(temp)

    # def __init__(self, data, timestamps, K, alpha_theta)
    model = StickbreakingDynamicTopicsLDA(data, Ts + [T], K=1, alpha_theta=1, sigmasq_states=sdev)

    lls = np.zeros(n_samples)
    posterior = np.zeros((n_samples, T))
    for j in range(n_samples):
        model.resample()
        lls[j] = model.log_likelihood()
        posterior[j] = model.beta[:, 0, 0]

    burned_trace = posterior[-burned_trace_n:]

    temp = np.percentile(burned_trace, [2.5, 97.5], axis=0)

    plt.subplot(3, 3, i+1)
    plt.plot(np.mean(burned_trace, axis=0), c='b', label='Posterior')
    plt.fill_between(np.arange(T), temp[1], temp[0], color='b', alpha=0.2)
    plt.axvline(jump_start+0.2, c='k')
    plt.axvline(jump_end-0.2, c='k')
    # plt.plot(test.obs[:, 0] / n)
    test.plot_mean()
    plt.xlim(0, T)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    plt.title("Jump simulation, n={}, dtm sdev={}".format(n, sdev), size=18)
    if i == 0:
        plt.legend(frameon=False)
    if i < 6:
        plt.xticks([])
    else:
        if i == 7:
            plt.xlabel('Timepoints', size=18)

    if i % 3 != 0:
        plt.yticks([])
    else:
        if i == 3:
            plt.ylabel('Topic proportion/word proportion', size=18)

plt.tight_layout()
plt.savefig("past test")
plt.show()
