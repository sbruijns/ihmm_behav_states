"""
Generate data from a simulated mouse.

This mouse uses 1 dynamic state throughout, can change the variance
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from driftin_obs.pgmult_generation import noisy_mult

var = 0.04
subject = 'CSHL059'
new_name = 'Sim_02_var_' + str(var).replace('.', '_')
seed = 3

print(subject)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['n_sessions']
from_session = 0

pmfs = [noisy_mult(n=1, T=till_session+1, sdev=np.sqrt(var)) for _ in range(11)]
pmfs_save = []
for j in range(from_session, till_session + 1):
    plt.plot([pmfs[0].natural2mean(pmfs[i].params[j])[0] for i in range(11)])
    pmfs_save.append([pmfs[0].natural2mean(pmfs[i].params[j])[0] for i in range(11)])
plt.ylim(bottom=0, top=1)
plt.show()
pickle.dump(pmfs_save, open("../session_data/{}_pmfs.p".format(new_name), "wb"))

for j in range(from_session, till_session + 1):
    try:
        data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    except FileNotFoundError:
        continue

    data = data.astype(int)

    rands = np.random.rand(len(data[:, 0]))
    for i, c in enumerate(data[:, 0]):
        data[i, 1] = rands[i] > pmfs[0].natural2mean(pmfs[c].params[j])[0]
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
