from pathlib import Path

import seaborn

from one.api import ONE
from one.remote import aws
from one.alf.files import add_uuid_string
# root_path = Path("/datadisk/FlatIron/aggregates")
#
# files_parquet = list(root_path.rglob('_ibl_subjectTrials.table.pqt'))

one.load(subject_id, content_type='subject', name='_ibl_subjectTrials.table.pqt')


if len(files_parquet) == 0:
    one = ONE()
    s3, bucket_name = aws.get_s3_from_alyx(alyx=one.alyx)
    datasets = one.alyx.rest('datasets', 'list', name='_ibl_subjectTrials.table.pqt')
    for dset in datasets:
        rel_path = dset['file_records'][0]['relative_path']
        aws_path = add_uuid_string('aggregates/' + rel_path, dset['url'][-36:])
        aws.s3_download_file(aws_path, root_path.joinpath(rel_path), s3=s3, bucket_name=bucket_name)


## %%
import dask.dataframe as dd
import pandas as pd
import numpy as np

time_bins = np.arange(0, 4000, 20)


trials = dd.read_parquet(files_parquet)
# the ITI is the start time of the next trial minus the end time of the current plus .5 secs
trials['iti'] = trials['intervals_0'].shift(-1) - trials['intervals_1'] + .5
trials = trials[trials['iti'] > 0]  # we need to remove the session jumps
# here we select only the ephys protocol trials
trials = trials[trials['task_protocol'].apply(lambda x: 'ephys' in x, meta=('task_protocol', 'bool'))]
# aggregate the iti per time bins
evolution = trials['iti'].groupby(trials['intervals_0'].map_partitions(pd.cut, time_bins)).agg(['mean', 'std'])



import time
# crunch crunch
now = time.time()
itis = trials['iti'].compute()
tev = evolution.compute()
print(time.time() - now)



## %%
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_theme('paper')
sns.set_palette('deep')
fig, axs = plt.subplots(1, 3, figsize=(16, 5))
axs[0].hist(itis, bins=1000, range=[0.5, 3], linewidth=0, alpha=0.8)
axs[0].set(title='ITI distribution for all trials', xlabel='ITI (s)', ylabel='Count')

x = time_bins[:-1] + 10
axs[1].fill_between(x, y1=tev['mean'].values - tev['std'].values, y2=tev['mean'].values + tev['std'].values, alpha=0.4)
axs[1].plot(x, tev['mean'].values, color='orange', linewidth=2)
axs[1].set(title='ITI duration', ylabel='ITI (s)', xlabel='Time elapsed in session (s)', ylim=[0.75, 2], xlim=[0, 3600])

trials_ = pd.read_parquet(files_parquet[0])
trials_['iti'] = trials_['intervals_0'].shift(-1) - trials_['intervals_1'] + .5
session0 = trials_[trials_['session'] == 'dc1b7422-cc16-4a37-b552-c31dccdddbce']
axs[2].plot(session0['intervals_0'], session0['iti'], label='dc1b7422', linewidth=0.5)
trials_ = pd.read_parquet(files_parquet[50])
trials_['iti'] = trials_['intervals_0'].shift(-1) - trials_['intervals_1'] + .5
session1 = trials_[trials_['session'] == 'd9d83a6a-8fb5-41eb-b4ce-7c6dd1716d72']
axs[2].plot(session1['intervals_0'], session1['iti'], label='d9d83a6a', linewidth=0.5)
axs[2].set(title='ITI duration', ylabel='ITI (s)', xlabel='Time elapsed in session (s)', ylim=[0.75, 2], xlim=[0, 3600])
axs[2].legend()
##
