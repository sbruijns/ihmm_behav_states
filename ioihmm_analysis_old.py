import numpy as np
import matplotlib.pyplot as plt
import pyhsmm
import pyhsmm.basic.distributions as distributions
import pickle
from itertools import permutations, product
import seaborn as sns

np.set_printoptions(suppress=True)

pos_colors = ['b', 'lime', 'm', 'g']
neg_colors = ['r', 'k', 'y', 'c']

fs = 16

class MCMC_result:

    def __init__(self, models, name, threshold=1000, seq_start=0):

        # TODO: save important numbers like num_samples, and put into functions

        self.models = models
        self.name = name
        self.seq_start = seq_start
        self.assign_counts = None  # Placeholder

        # TODO: I only check whether states have more than threshold trials assigned for the first sample
        flat_list = [item for sublist in self.models[0].stateseqs for item in sublist]
        a = np.unique(flat_list, return_counts=1)
        print(a)
        states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]
        self.proto_states = states[counts > threshold]

        self.state_map = dict(zip(self.proto_states, range(len(self.proto_states))))
        self.state_to_color = {}

        # for s in self.proto_states:
        #     print(self.models[0].obs_distns[s].weights[:, 0])
        #     print(np.sum(self.models[0].obs_distns[s].weights[:, 0]) / 9)
        #     self.state_to_color[s] = cmap(np.sum(self.models[0].obs_distns[s].weights[:, 0]) / 9)
        # return
        # attempt:
        tendency = []
        for s in self.proto_states:
            print()
            temp = np.sum(self.models[0].obs_distns[s].weights[:, 0] - 0.5)
            assert temp != 0.
            tendency.append(temp)

        ordered = [(x, y) for x, y in sorted(zip(tendency, self.proto_states))]
        pos = [y for x, y in ordered if x > 0]
        neg = [y for x, y in ordered if x < 0]

        for i, y in enumerate(pos):
            self.state_to_color[y] = pos_colors[i]
        for i, y in enumerate(reversed(neg)):
            self.state_to_color[y] = neg_colors[i]

        #self.state_to_color = dict(zip(self.proto_states, colors[:len(self.proto_states)]))


    def matrix(self, i):
        temp = self.models[i].trans_distn.trans_matrix[self.proto_states]
        temp = temp[:, self.proto_states]
        return temp

    def param_hist(self, param_func):
        for s in self.proto_states:
            durtaion_rs = np.zeros(len(self.models))
            for i, m in enumerate(self.models):
                durtaion_rs[i] = param_func(m, s)
            plt.hist(durtaion_rs)
            plt.title(s)
            plt.show()

    def transition_hists(self):
        for t in permutations(self.proto_states, 2):
            transitions = np.zeros(len(self.models))
            for i, m in enumerate(self.models):
                transitions[i] = m.trans_distn.trans_matrix[t]
            plt.hist(transitions)
            plt.xlim(left=0, right=1)
            plt.title(t)
            plt.show()

    def count_assigns(self):
        self.assign_counts = np.zeros((len(self.models), len(self.proto_states)))
        self.total_counts = np.zeros(len(self.models))
        for i, m in enumerate(self.models):
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a = np.unique(flat_list, return_counts=1)
            states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]

            self.total_counts[i] = counts.sum()
            for s, c in zip(states, counts):
                if s in self.proto_states:
                    self.assign_counts[i, self.state_map[s]] = c

    def percent_counts(self):
        if self.assign_counts is None:
            self.count_assigns()
        counts = np.zeros((self.models[0].num_states))
        trial_n = self.total_counts[0]

        for m in self.models:
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a, temp_counts = np.unique(flat_list, return_counts=1)
            counts[a] += temp_counts

        self.percent = counts / len(self.models) / trial_n

    def total_assign_evolution(self):
        if self.assign_counts is None:
            self.count_assigns()
        for i in range(len(self.models)):
            print(self.assign_counts[i].sum() / self.total_counts[i])

    def assign_hist(self):
        if self.assign_counts is None:
            self.count_assigns()
        for i in range(len(self.proto_states)):
            plt.hist(self.assign_counts[:, i])
        #plt.savefig("figures/convergence/count hists {}".format(fit))
        plt.show()

    def assign_evolution(self):
        plt.figure(figsize=(11, 8))
        if self.assign_counts is None:
            self.count_assigns()
        for i, s in enumerate(self.proto_states):
            plt.plot(self.assign_counts[:, i], c=self.state_to_color[s])

        plt.xlabel('Iteration', size=22)
        plt.ylabel('# assigned trials', size=22)
        plt.xticks(size=20-3)
        sns.despine()
        plt.tight_layout()
        plt.savefig("figures/convergence/count evolution {}".format(fit))
        plt.show()

    def pmf_plots(self):
        for i, m in enumerate(self.models):
            for s in self.proto_states:
                # TODO: Hack
                if s not in self.state_to_color:
                    continue
                w = m.obs_distns[s].weights
                label = s if i == 0 else None
                plt.plot(w[:, 0], self.state_to_color[s], label=label)

        plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
        plt.ylim(bottom=0, top=1)
        plt.savefig("figures/convergence/pmfs {}".format(fit))
        plt.show()

    def pmf_posterior_all(self):
        self.percent_counts()
        cm = plt.get_cmap('gist_rainbow')

        for s in range(self.models[0].num_states):
            if self.percent[s] < 0.01:
                continue
            pmfs = np.zeros((len(self.models), 9))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[:, 0]

            print(s)
            print(self.percent[s])
            print(min(1., self.percent[s] / 5 / (1 / self.models[0].num_states)))
            plt.plot(pmfs.mean(axis=0), color=cm(s / self.models[0].num_states), label=s, alpha=min(1., self.percent[s] / 5 / (1 / self.models[0].num_states)))
            plt.plot(np.percentile(pmfs, [2.5, 97.5], axis=0).T, color=cm(s / self.models[0].num_states), ls='--', alpha=min(1., self.percent[s] / 5 / (1 / self.models[0].num_states)))
        plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
        plt.ylim(bottom=0, top=1)
        plt.legend()
        plt.savefig("figures/convergence/pmfs with conf {}".format(fit))
        plt.show()

    def pmf_posterior(self):

        plt.figure(figsize=(11, 8))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), 9))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[:, 0]

            plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
            plt.plot(np.percentile(pmfs, [2.5, 97.5], axis=0).T, color=self.state_to_color[s], ls='--')
        plt.title("{}, {} sessions".format(self.name, len(self.models[0].stateseqs)), size=22)
        plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=22-3)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=8)
        sns.despine()

        plt.tight_layout()
        plt.savefig("figures/convergence/pmfs with conf {}".format(fit))
        plt.show()

    def sequence_posteriors(self, until=None, save=False, show=False):
        n = len(self.models[0].stateseqs) if until is None else until
        posteriors = []
        for seq_num in range(n):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(self.name, seq_num + self.seq_start), "rb"))
                #feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, seq_num + self.seq_start), "rb"))
                #wMode, std = pickle.load(open("./session_data/psytrack/result {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
                #trialnumbers = pickle.load(open("./session_data/psytrack/numbers {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
            except FileNotFoundError:
                continue
            posterior = np.zeros((len(self.proto_states), len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in self.proto_states:
                    posterior[self.state_map[s]] += m.stateseqs[seq_num] == s
            if len(blocks) < 140:
                continue

            plt.figure(figsize=(11, 6))
            for s in self.proto_states:
                plt.plot(posterior[self.state_map[s]] / len(self.models), color=self.state_to_color[s], label=s)
            assert len(blocks) == len(m.stateseqs[seq_num])

            # windowsize = 7
            # performance = np.zeros(len(feedback) - 2 * windowsize + 1)
            # for i, pos in enumerate(np.arange(windowsize, len(feedback) - windowsize + 1)):
            #     performance[i] = np.mean(feedback[pos - windowsize: pos])
            # #plt.plot(np.arange(windowsize, len(feedback) - windowsize + 1) - windowsize / 2, performance, 'k', lw=3)
            #
            # wMode[0] -= np.min(wMode[0])
            # wmax = np.max(wMode[0])
            # plt.plot(trialnumbers, wMode[0] / wmax, color='k', alpha=0.5)

            alpha = 0.25
            start = 0
            curr = 0
            block_to_color = {-1: 'r', 1: 'b'}
            for i, b in enumerate(blocks):
                if b != curr:
                    if curr != 0:
                        plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
                    curr = b
                    start = i
            plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
            sns.despine()

            plt.title("{}, session #{}".format(self.name, 1+seq_num), size=22)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(State)', size=22)
            plt.xlabel('Trial', size=22)

            plt.xlim(left=0, right=500)
            if save:
                plt.savefig("figures/state_seqs/posterior seq {} {}".format(self.name, seq_num))
            # if show:
            #     plt.show()
            # else:
            plt.show()
            posteriors.append(posterior / len(self.models))
        self.posteriors = posteriors

    def calc_posterior(self, until=None):
        n = len(self.models[0].stateseqs) if until is None else until
        posteriors = []
        for seq_num in range(n):

            posterior = np.zeros((len(self.proto_states), len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in self.proto_states:
                    posterior[self.state_map[s]] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / len(self.models))
        self.posteriors = posteriors

    def contrasts_plot(self, until=None, save=False):
        n = len(self.models[-1].stateseqs) if until is None else until
        contrasts_and_answers = self.models[-1].datas
        posteriors = []
        for seq_num in range(n):
            c_n_a = contrasts_and_answers[seq_num]
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(self.name, seq_num + self.seq_start), "rb"))
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue
            posterior = np.zeros((len(self.proto_states), len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in self.proto_states:
                    posterior[self.state_map[s]] += m.stateseqs[seq_num] == s

            plt.figure(figsize=(11, 6))
            for s in self.proto_states:
                plt.plot(8 * posterior[self.state_map[s]] / len(self.models), color=self.state_to_color[s], label=s)
            # plt.plot(posterior.sum(axis=0) / len(self.models), 'k')
            assert len(blocks) == len(m.stateseqs[seq_num])


            # points = []
            # choice_average = []
            # internal_count = 0
            # window_length = 4
            # window = np.zeros(window_length)
            # for i, ca in enumerate(c_n_a):
            #     if ca[0] == 4:
            #         points.append(i)
            #         window[internal_count % window_length] = ca[1] == 2
            #         choice_average.append(8 * np.mean(window))
            #         internal_count += 1
            # plt.plot(points, choice_average, 'k')

            windowsize = 7
            performance = np.zeros(len(feedback) - 2 * windowsize + 1)
            for i, pos in enumerate(np.arange(windowsize, len(feedback) - windowsize + 1)):
                performance[i] = np.mean(feedback[pos - windowsize: pos])
            plt.plot(np.arange(windowsize, len(feedback) - windowsize + 1), 8 * performance, 'k')


            mask = c_n_a[:, 1] == 0
            plt.plot(np.where(mask)[0], c_n_a[mask, 0], 'o', c='lime')
            mask = c_n_a[:, 1] == 1
            plt.plot(np.where(mask)[0], c_n_a[mask, 0], 'o', c='k')
            mask = c_n_a[:, 1] == 2
            plt.plot(np.where(mask)[0], c_n_a[mask, 0], 'o', c='k')

            plt.xlim(left=0, right=500)
            sns.despine()

            plt.title("{}, session #{}".format(self.name, 1+seq_num), size=22)
            plt.yticks(np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=22-2)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(State) and contrast', size=22)
            plt.xlabel('Trial', size=22)
            plt.tight_layout()
            plt.savefig("figures/convergence/posterior and contrasts {}, sess {}.png".format(self.name, seq_num))
            plt.show()
            posteriors.append(posterior / len(self.models))
        self.posteriors = posteriors


    def sequence_heatmap(self, normalization='adaptive', until=None):
        n = len(self.models[0].stateseqs) if until is None else until
        for seq_num in range(n):
            for s1, s2 in product(self.proto_states, repeat=2):
                heatmap = np.zeros((len(self.models[0].stateseqs[seq_num]), len(self.models[0].stateseqs[seq_num])))
                normalize_vector = np.zeros(len(self.models[0].stateseqs[seq_num]), dtype=np.int32)
                for m in self.models:
                    for t in range(len(m.stateseqs[seq_num])):
                        if m.stateseqs[seq_num][t] == s1:
                            normalize_vector[t] += 1
                            heatmap[t] += m.stateseqs[seq_num] == s2

                if normalization == 'adaptive':
                    mask = normalize_vector.nonzero()
                    heatmap[mask] = heatmap[mask] / normalize_vector[mask, None]
                    if s1 == s2:
                        heatmap -= np.diag(normalize_vector >= 1)  # empty diagonal if we look at autocorr
                    heatmap += np.diag(normalize_vector / len(self.models))
                elif normalization == 'total':
                    heatmap /= len(self.models)

                assert heatmap.max() <= 1.
                assert heatmap.min() >= 0.
                sns.heatmap(heatmap, vmin=0., vmax=1., square=True)
                title = "Sequence {}, states {} and {}".format(seq_num, s1, s2)
                plt.title(title)
                plt.savefig("figures/state_heatmaps/" + normalization + '_' + title)
                plt.close()


    def single_state_resample(self, n):
        model = self.models[-1]
        states = [np.zeros((len(self.proto_states), x.shape[0])) for x in model.datas]

        for i in range(n):
            model.resample_states()
            for j, seq in enumerate(model.stateseqs):
                for s in self.proto_states:
                    states[j][self.state_map[s]] += seq == s

        for seq_num in range(n):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue

            alpha = 0.25
            start = 0
            curr = 0
            block_to_color = {-1: 'r', 1: 'b'}
            for i, b in enumerate(blocks):
                if b != curr:
                    if curr != 0:
                        plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
                    curr = b
                    start = i
            plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)

            seq = states[seq_num]
            for s in self.proto_states:
                plt.plot(seq[self.state_map[s]] / n, color=self.state_to_color[s], label=s)

            plt.xlim(left=0, right=500)
            sns.despine()
            plt.legend()
            plt.show()
            quit()

    def state_development(self, save=False):
        states_by_session = np.zeros((len(self.proto_states), len(self.models[0].stateseqs)))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= len(self.models)
        plt.figure(figsize=(14, 6))
        for s in self.proto_states:
            plt.plot(states_by_session[self.state_map[s]], color=self.state_to_color[s], label=s)

        plt.title(self.name)

        perf = np.zeros(len(self.models[0].stateseqs))
        for i in range(len(self.models[0].stateseqs)):
            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                continue
            perf[i] = np.mean(feedback[self.models[-1].datas[i][:, 0] == 4])
        #plt.plot(perf)

        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('Porportion of trials', size=22)
        plt.xlabel('Session', size=22)

        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout()

        # if save:
        plt.savefig("figures/convergence/states_over_session {}".format(self.name))
        plt.close()

    def block_alignment(self):
        # throws errors when data is missing, e.g. ibl_witten_19
        n = len(self.models[0].stateseqs)
        states_by_session = np.zeros((2, len(self.proto_states), len(self.models[0].stateseqs)))
        for seq_num in range(n):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue
            if len(blocks) < 140:
                continue
            for m in self.models:
                seq = m.stateseqs[seq_num]
                for s in self.proto_states:
                    b1 = blocks == 1.
                    states_by_session[0, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b1)) / np.sum(b1)
                    b2 = blocks == -1.
                    states_by_session[1, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b2)) / np.sum(b2)
        states_by_session /= len(self.models)

        perf = np.zeros(len(self.models[0].stateseqs))
        for i in range(len(self.models[0].stateseqs)):
            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                continue
            perf[i] = np.mean(feedback)

        plt.figure(figsize=(14, 8))
        plt.suptitle("{}".format(self.name), size=fs)

        plt.subplot(211)
        plt.title('Right block', size=22)
        #plt.plot(perf)
        for s in self.proto_states:
            plt.plot(states_by_session[0, self.state_map[s]], color=self.state_to_color[s], label=s)

        sns.despine()
        plt.yticks(size=fs-2)
        plt.xticks([], size=fs-3)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.subplot(212)
        plt.title('Left block', size=22)
        for s in self.proto_states:
            plt.plot(states_by_session[1, self.state_map[s]], color=self.state_to_color[s])

        sns.despine()
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('Porportion of trials', size=22)
        plt.xlabel('Session', size=22)

        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        plt.savefig("figures/convergence/Block alginment {}".format(self.name))
        plt.close()

    def pmf_stationarity(self):
        n = len(self.models[0].stateseqs)
        # for every state, first half or second half, for every contrast, what was the answer?
        global answers
        answers = np.zeros((len(self.proto_states), 2, 9, 2))
        for i, datum in enumerate(self.models[-1].datas):
            for s in self.proto_states:
                for c in range(9):
                    mask1 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.5, datum[:, 0] == c), datum[:, 1] == 0)
                    mask2 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.5, datum[:, 0] == c), datum[:, 1] == 2)

                    answers[self.state_map[s], int(np.round(i / (n - 1))), c, 0] += np.sum(mask1)
                    answers[self.state_map[s], int(np.round(i / (n - 1))), c, 1] += np.sum(mask2)

        answers /= answers.sum(3)[..., None]
        plt.figure(figsize=(11, 8))
        for i, s in enumerate(self.proto_states):
            if i == 0:
                plt.plot(answers[self.state_map[s], 0, :, 0], color=self.state_to_color[s], label='First Half', ls='-')
                plt.plot(answers[self.state_map[s], 1, :, 0], color=self.state_to_color[s], label='Second Half', ls='--')
            else:
                plt.plot(answers[self.state_map[s], 0, :, 0], color=self.state_to_color[s], ls='-')
                plt.plot(answers[self.state_map[s], 1, :, 0], color=self.state_to_color[s], ls='--')
        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.yticks(size=22-3)
        plt.xticks(np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=22-2)
        plt.ylabel('P(answer right)', size=22)
        plt.xlabel('Contrast', size=22)

        plt.legend(fontsize=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=8)

        plt.tight_layout()
        plt.savefig("figures/convergence/pmf over halves {}".format(self.name))
        plt.show()


    def pmf_compare(self):

        # for s in self.proto_states:
        #     pmfs = np.zeros((len(self.models), 9))
        #     for i, m in enumerate(self.models):
        #         pmfs[i] = m.obs_distns[s].weights[:, 0]
        #
        #     plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
        #     plt.plot(np.percentile(pmfs, [2.5, 97.5], axis=0).T, color=self.state_to_color[s], ls='--')
        # plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
        # plt.ylim(bottom=0, top=1)
        # plt.legend()

        n = len(self.models[0].stateseqs)
        # for every state, first half or second half, for every contrast, what was the answer?
        answers = np.zeros((len(self.proto_states), 9, 2))
        for i, datum in enumerate(self.models[-1].datas):
            for s in self.proto_states:
                for c in range(9):
                    mask1 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.3, datum[:, 0] == c), datum[:, 1] == 0)
                    mask2 = np.logical_and(np.logical_and(self.posteriors[i][self.state_map[s]] > 0.3, datum[:, 0] == c), datum[:, 1] == 2)

                    answers[self.state_map[s], c, 0] += np.sum(mask1)
                    answers[self.state_map[s], c, 1] += np.sum(mask2)

        answers /= answers.sum(2)[..., None]

        for s in self.proto_states:
            plt.plot(answers[self.state_map[s], :, 0], color=self.state_to_color[s], label=s, ls=':')
        plt.show()

    def block_performance(self):
        c_n_a = self.models[-1].datas
        n = len(self.models[0].stateseqs)
        block_performance = np.zeros((2, n))
        for seq_num in range(n):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue
            if len(blocks) < 140:
                continue
            data = c_n_a[seq_num]
            block_performance[0, seq_num] = np.mean(data[np.logical_and(blocks == 1, data[:, 0] == 4)][:, 1] == 0)
            block_performance[1, seq_num] = np.mean(data[np.logical_and(blocks == -1, data[:, 0] == 4)][:, 1] == 0)

        plt.figure(figsize=(11, 8))
        plt.plot(block_performance[0], 'b', label='Right block')
        plt.plot(block_performance[1], 'r', label='Left block')
        plt.plot([0, n], [0.2, 0.2], 'r')
        plt.plot([0, n], [0.8, 0.8], 'b')
        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('P(answer right | Contrast = 0)', size=22)
        plt.xlabel('Session', size=22)

        plt.legend(fontsize=fs)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout()
        plt.savefig("figures/convergence/0 accuracy in blocks {}".format(self.name))
        plt.close()


    def switchiness(self):

        sess_switch = np.zeros(len(self.posteriors))
        sess_acc = np.zeros(len(self.posteriors))
        sess_acc_0 = np.zeros(len(self.posteriors))
        # sess_norm_switch = np.zeros(len(self.posteriors))

        for i, p in enumerate(self.posteriors):
            _, certain_states = np.where((p > 0.6).T)

            switches = 0
            current_state = -1
            counter = 0

            for s in certain_states:
                if s == current_state:
                    counter += 1
                else:
                    switches += counter > 5
                    current_state = s
                    counter = 1

            sess_switch[i] = switches / p.shape[1]

            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                print("warning {} {}".format(self.name, i))
                continue
            print(feedback.shape)
            print(p.shape[1])
            assert len(feedback) == p.shape[1]
            sess_acc[i] = np.mean(feedback)
            sess_acc_0[i] = np.mean(feedback[self.models[-1].datas[i][:, 0] == 4])
            # sess_norm_switch[i] = switches / np.sum(np.diff(blocks) != 0)

        return sess_switch, sess_acc, sess_acc_0

    def empirical_cmf(self):

        rts = np.zeros((len(self.proto_states), 9))
        counts = np.zeros((len(self.proto_states), 9))

        for i, p in enumerate(self.posteriors):
            certain_states = p > 0.6

            try:
                rt = pickle.load(open("./session_data/rt_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                print("warning {} {}".format(self.name, i))
                continue

            for s in self.proto_states:
                for c in range(9):
                    mask = np.logical_and(certain_states[self.state_map[s]], self.models[-1].datas[i][:, 0] == c)
                    if np.sum(mask) == 0:
                        continue
                    counts[self.state_map[s], c] += np.sum(mask) - np.sum(np.isnan(rt[mask]))
                    rts[self.state_map[s], c] += np.sum(np.nan_to_num(rt[mask]))

        print(rts)
        print(counts)
        for s in self.proto_states:
            plt.plot(rts[self.state_map[s]] / counts[self.state_map[s]], color=self.state_to_color[s], label=s)
        plt.legend()
        plt.show()


from_tills = [(0, 12), (0, 17), (0, 11), (0, 23), (0, 8), (0, 17), (0, 9), (0, 16), (0, 26), (0, 15), (0, 14), (0, 19), (0, 13), (0, 15), (0, 32), (0, 20)]
subjects = ['CSHL059', 'DY_008', 'DY_009', 'CSHL045', 'CSH_ZAD_024', 'ibl_witten_17', 'CSH_ZAD_017', 'CSHL060', 'CSH_ZAD_011', 'ZM_3003', 'CSH_ZAD_025', 'ibl_witten_18', 'ibl_witten_20', 'CSH_ZAD_026', 'CSH_ZAD_019', 'KS020']

from_tills = [(0, 23)]
subjects = ['CSHL045']

switches = []
accs = []
accs0 = []

for from_till, subject in zip(from_tills, subjects):
    fit = 'ioihmm fit {} {}'.format(subject, from_till)
    models = pickle.load(open("./iHMM_fits/{}.p".format(fit), "rb"))
    session_names = pickle.load(open("./session_data/{}_session_names.p".format(subject), "rb"))

    result = MCMC_result(models[1000:], threshold=500, name=subject, seq_start=52)
    result.assign_evolution()
    quit()

    print("./session_data/{}_df_{}_blocks.p".format(subject, 0))
    # for_messages = result.models[-1].states_list[0].messages_forwards()[0]
    #
    # for_messages -= np.max(for_messages, axis=1)[:, None]
    # probs2 = np.exp(for_messages)
    # probs2 /= np.sum(probs2, axis=1)[:, None]
    #
    # for s in result.proto_states:
    #     plt.plot(probs2[:, s], c=result.state_to_color[s])
    #
    # try:
    #     blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(subject, 0), "rb"))
    # except FileNotFoundError:
    #     continue
    #
    # print(blocks)
    # print(np.sum(blocks))
    #
    # alpha = 0.25
    # start = 0
    # curr = 0
    # block_to_color = {-1: 'r', 1: 'b'}
    # for i, b in enumerate(blocks):
    #     if b != curr:
    #         if curr != 0:
    #             plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
    #         curr = b
    #         start = i
    # plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
    # plt.xlim(left=0, right=500)
    #
    # plt.show()


    # plt.figure(figsize=(12, 9))
    # sns.heatmap(np.round(result.models[0].obs_distns[result.proto_states[1]].weights, 2), annot=True, cbar=0, annot_kws={'size': 20})
    # plt.ylabel('Contrast', size=28)
    # plt.yticks(np.arange(9) + 0.5, [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], rotation='horizontal', size=19)
    # plt.xlabel('Answer', size=28)
    # plt.xticks(np.arange(3) + 0.5, ['Right', 'Timeout', 'Left'], size=24)
    # plt.tight_layout()
    # plt.savefig("categoricals.png")
    # plt.show()
    #
    # plt.figure(figsize=(12, 9))
    # plt.plot(result.models[0].obs_distns[result.proto_states[1]].weights[:, 0])
    # plt.xticks(range(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1], size=19)
    # plt.yticks(size=19)
    # plt.xlabel('Contrast', size=28)
    # plt.ylabel('P(answer rightward)', size=28)
    # plt.ylim(bottom=0, top=1)
    # plt.xlim(left=0, right=8)
    # sns.despine()
    #
    # plt.tight_layout()
    # plt.savefig("simple_pmf")
    # plt.show()
    # quit()

    # result.calc_posterior()
    # result.empirical_cmf()
    # continue

    result.calc_posterior()
    result.pmf_posterior()
    # result.single_state_resample(500)
    result.sequence_posteriors()
    # a, b, c = result.switchiness()
    # switches.append(a)
    # accs.append(b)
    # accs0.append(c)
    # continue

    # result.sequence_posteriors()
    # result.switchiness()
    # #result.pmf_compare()
    # quit()
    # #
    #result.block_performance()
    #result.contrasts_plot(until=5)
    #result.sequence_posteriors(show=True, until=5, save=True)
    #result.state_development()
    #result.block_alignment()

    #result.single_state_resample(100)
    #result.assign_evolution()
    #result.transition_hists()
    # result.sequence_heatmap(normalization='total', until=5)
    #result.pmf_plots()
    #result.param_hist(lambda x, s: x.dur_distns[s].r)
    #result.param_hist(lambda x, s: x.dur_distns[s].p)
    #result.pmf_posterior()
    #result.sequence_posteriors(show=True, until=5, save=True)

quit()
plt.plot(np.concatenate(switches).ravel(), np.concatenate(accs).ravel(), 'ko')
plt.xlabel('Switchiness', size=18)
plt.ylabel('Accuracy', size=18)

plt.tight_layout()
plt.savefig('Switchiness and acc')
plt.show()

plt.plot(np.concatenate(switches).ravel(), np.concatenate(accs0).ravel(), 'ko')
plt.show()
