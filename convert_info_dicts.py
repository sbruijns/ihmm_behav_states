import pickle
import json

fails = ['KS022', 'ibl_witten_29', 'CSHL_015', 'UCLA017', 'KS091', 'KS023', 'KS055', 'CSHL_014', 'CSHL049', 'PL024', 'NYU-11']

for subject in fails:
	info_dict = pickle.load(open("./{}/{}_info_dict.p".format('session_data', subject), "rb"))
	info_dict['dates'] = [d.isoformat() for d in info_dict['dates']]
	del info_dict['date_and_session_num']
	json.dump(info_dict, open("./{}/{}_info_dict.json".format('session_data', subject), 'w'))
	# datetime.datetime.fromisoformat(info_dict['dates'][0].isoformat())