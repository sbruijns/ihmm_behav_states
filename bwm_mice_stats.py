from one.api import ONE
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


one = ONE()

# Zoes mice
# import json
# sessions = json.loads(open("./posterior_probs_for_seb/sessions.json").read())

contrast_to_num = {-1.: 0, -0.5: 1, -0.25: 2, -0.125: 3, -0.0625: 4, 0: 5, 0.0625: 6, 0.125: 7, 0.25: 8, 0.5: 9, 1.: 10}

# old:
# def get_df(eid):
#     choices, contrastL, contrastR, feedback, block, rt, gocue = one.load(eid, dataset_types=dataset_types)
#     if np.all(None == choices) or np.all(None == contrastL) or np.all(None == contrastR) or np.all(None == feedback) or np.all(None == block):
#         return None
#     d = {'response': choices, 'contrastL': contrastL, 'contrastR': contrastR, 'feedback': feedback}
#
#     df = pd.DataFrame(data=d, index=range(len(choices))).fillna(0)
#     df['feedback'] = df['feedback'].replace(-1, 0)
#     df['signed_contrast'] = df['contrastR'] - df['contrastL']
#     df['signed_contrast'] = df['signed_contrast'].map(contrast_to_num)
#     df['response'] += 1
#     df['block'] = block
#     df['rt'] = rt - gocue

dataset_types = ['choice', 'contrastLeft', 'contrastRight',
                 'feedbackType', 'probabilityLeft', 'response_times',
                 'goCue_times']
def get_df(trials):
    if np.all(None == trials['contrastLeft']) or np.all(None == trials['contrastRight']) or np.all(None == trials['feedbackType']) or np.all(None == trials['probabilityLeft']):  # or np.all(None == data_dict['response_times']):
        return None, None
    d = {'contrastL': trials['contrastLeft'], 'contrastR': trials['contrastRight'], 'feedback': trials['feedbackType']}

    df = pd.DataFrame(data=d, index=range(len(trials['contrastRight']))).fillna(0)
    df['feedback'] = df['feedback'].replace(-1, 0)
    df['signed_contrast'] = df['contrastR'] - df['contrastL']
    df['signed_contrast'] = df['signed_contrast'].map(contrast_to_num)
    df['block'] = trials['probabilityLeft']
    # df['rt'] = data_dict['response_times'] - data_dict['goCue_times']  # RTODO

    return df

misses = []
kick = ['NR_0027']
to_introduce = [2, 3, 4, 5]
subjects = ['NYU-11', 'NYU-12', 'NYU-21', 'NYU-27', 'NYU-30', 'NYU-37',
   	'NYU-39', 'NYU-40', 'NYU-45', 'NYU-46', 'NYU-47', 'NYU-48',
   	'CSHL045', 'CSHL047', 'CSHL049', 'CSHL051', 'CSHL052', 'CSHL053',
   	'CSHL054', 'CSHL055', 'CSHL058', 'CSHL059', 'CSHL060', 'UCLA005',
   	'UCLA006', 'UCLA011', 'UCLA012', 'UCLA014', 'UCLA015', 'UCLA017',
   	'UCLA033', 'UCLA034', 'UCLA035', 'UCLA036', 'UCLA037', 'KS014',
   	'KS016', 'KS022', 'KS023', 'KS042', 'KS043', 'KS044', 'KS045',
   	'KS046', 'KS051', 'KS052', 'KS055', 'KS084', 'KS086', 'KS091',
   	'KS094', 'KS096', 'DY_008', 'DY_009', 'DY_010', 'DY_011', 'DY_013',
   	'DY_014', 'DY_016', 'DY_018', 'DY_020', 'PL015', 'PL016', 'PL017',
   	'PL024', 'SWC_042', 'SWC_043', 'SWC_060', 'SWC_061', 'SWC_066',
   	'ZFM-01576', 'ZFM-01577', 'ZFM-01592', 'ZFM-01935', 'ZFM-01936',
   	'ZFM-01937', 'ZFM-02368', 'ZFM-02369', 'ZFM-02370', 'ZFM-02372',
   	'ZFM-02373', 'ZM_1897', 'ZM_1898', 'ZM_2240', 'ZM_2241', 'ZM_2245',
   	'ZM_3003', 'SWC_038', 'SWC_039', 'SWC_052', 'SWC_053', 'SWC_054',
   	'SWC_058', 'SWC_065', 'NR_0017', 'NR_0019', 'NR_0020', 'NR_0021',
   	'NR_0027', 'ibl_witten_13', 'ibl_witten_17', 'ibl_witten_18',
   	'ibl_witten_19', 'ibl_witten_20', 'ibl_witten_25', 'ibl_witten_26',
   	'ibl_witten_27', 'ibl_witten_29', 'CSH_ZAD_001', 'CSH_ZAD_011',
   	'CSH_ZAD_019', 'CSH_ZAD_022', 'CSH_ZAD_024', 'CSH_ZAD_025',
   	'CSH_ZAD_026', 'CSH_ZAD_029']
quit()
# why does CSHL058 not work?

old_style = False
if old_style:
    print("Warning, data can have splits")
    data_folder = 'session_data_old'
bias_eids = []

print("#########################################")
print("Waring, rt's removed, find with   # RTODO")
print("#########################################")

short_subjs = []
names = []

pre_bias = []
bias = []
ephys = []
other_protocols = []
trial_nums = []
for subject in subjects:
    if subject in kick:
        continue
    print('_____________________')
    print(subject)
    trial_nums.append([])

    eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2023-01-01'], details=True)

    start_times = [sess['date'] for sess in sess_info]
    protocols = [sess['task_protocol'] for sess in sess_info]
    nums = [sess['number'] for sess in sess_info]

    print("original # of eids {}".format(len(eids)))

    test = [(y, x) for y, x in sorted(zip(start_times, eids))]

    eids = [x for _, x in sorted(zip(start_times, eids))]
    dates = [x for x, _ in sorted(zip(start_times, eids))]
    nums = [x for _, x in sorted(zip(start_times, nums))]
    protocols = [x for _, x in sorted(zip(start_times, protocols))]

    prev_date = None
    prev_num = -1
    fixed_dates = []
    fixed_eids = []
    fixed_protocols = []
    additional_eids = []
    for d, e, n, p in zip(dates, eids, nums, protocols):
        print(d, e, n, p)
        if e in ['ae767daf-d391-491d-8dd3-8da74d4efca1', 'c461e710-d385-468c-ba11-3a10931a28a6', '2c8098cd-99df-4e4c-89a6-b412b4066aea', 'a3347c46-c080-4fcb-85eb-5ba80ab18d7a']:  # super hacky
            continue
        if d != prev_date:
            fixed_dates.append(d)
            fixed_eids.append(e)
            fixed_protocols.append(p)
            additional_eids.append([])
        else:
            assert n > prev_num
            if n == 1:
                additional_eids.append([e])
            elif n > 1:
                additional_eids[-1].append(e)
        prev_date = d
        prev_num = n

    pre_bias.append(0)
    bias.append(0)
    ephys.append(0)
    for p in fixed_protocols:
        if p.startswith('_iblrig_tasks_biasedChoiceWorld'):
            bias[-1] += 1
        elif p.startswith('_iblrig_tasks_trainingChoiceWorld'):
            pre_bias[-1] += 1
        elif p.startswith('_iblrig_tasks_ephysChoiceWorld'):
            ephys[-1] += 1
        else:
            if p not in other_protocols:
                other_protocols.append(p)

    # in case you want it
    if old_style:
        fixed_eids = eids
        fixed_dates = dates
        additional_eids = [[] for e in fixed_eids]
    print("remaining # of eids {}".format(len(fixed_eids)))

    performance = np.zeros(len(fixed_eids))
    easy_per = np.zeros(len(fixed_eids))
    hard_per = np.zeros(len(fixed_eids))
    bias_start = 0

    contrast_set = {0, 1, 9, 10}

    rel_count = -1
    for i, (eid, extra_eids) in enumerate(zip(fixed_eids, additional_eids)):

        try:
            trials = one.load_object(eid, 'trials')
        except Exception:
            print('skipped session')
            continue

        if 'probabilityLeft' not in trials or trials['probabilityLeft'] is None: # originally also "or df is None", if this gets reintroduced, probably need to download all data again
            if rel_count != -1:
                print('lost session, problem')
                misses.append((subject, i))
            continue
        rel_count += 1

        print("rel_count {}".format(rel_count))
        df = get_df(trials)
        # if there are some duplicate dates:
        for extra_eid in extra_eids:
            print('merging, orig size: {}'.format(len(df)))
            try:
                extra_trials = one.load_object(extra_eid, 'trials')
                if 'choice' not in extra_trials:
                    continue
            except Exception:
                print('skipped merging session')
                continue
            df2 = get_df(extra_trials)
            df = pd.concat([df, df2], ignore_index=1)
            print('new size: {}'.format(len(df)))

        trial_nums[-1].append(len(df))

        # fit_info[:, 2] = df['rt']  # RTODO

    if rel_count == -1:
        print("Something wrong here")
        quit()

    # print(bias_eids)


print(misses)
print(short_subjs)

print(pre_bias)
print(bias)
print(ephys)
print(trial_nums)
