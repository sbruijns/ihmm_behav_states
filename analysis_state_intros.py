"""
    Unused figures (kinda weird)
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle

fontsize = 16

def type_hist(data, title=''):
    highest = int(data.max())
    lowest = int(data.min())
    if (data % 1 == 0).all():
        bins = np.arange(lowest, highest + 2) - 0.5
    else:
        bins = np.histogram(data)[1]
    hist_max = 0
    for i in range(data.shape[1]):
        hist_max = max(hist_max, np.histogram(data[:, i], bins)[0].max())

    plt.subplot(3, 1, 1)
    assert np.histogram(data[:, 0])[0].sum() == np.histogram(data[:, 0], bins)[0].sum()
    plt.hist(data[:, 0], alpha=1/3, label="type 1", align='mid', bins=bins, color='grey')
    plt.xlim(lowest - 0.5, highest + 1)
    plt.ylim(0, hist_max + 1)
    ax2 = plt.gca().twinx()
    plt.ylabel("Type 1", size=fontsize)
    ax2.set_yticks([])

    plt.subplot(3, 1, 2)
    assert np.histogram(data[:, 1])[0].sum() == np.histogram(data[:, 1], bins)[0].sum()
    plt.hist(data[:, 1], alpha=1/3, label="type 2", align='mid', bins=bins, color='grey')
    plt.xlim(lowest - 0.5, highest + 1)
    plt.ylim(0, hist_max + 1)
    ax2 = plt.gca().twinx()
    plt.ylabel("Type 2", size=fontsize)
    ax2.set_yticks([])

    plt.subplot(3, 1, 3)
    assert np.histogram(data[:, 2])[0].sum() == np.histogram(data[:, 2], bins)[0].sum()
    plt.hist(data[:, 2], alpha=1/3, label="type 3", align='mid', bins=bins, color='grey')
    plt.xlim(lowest - 0.5, highest + 1)
    plt.ylim(0, hist_max + 1)
    plt.xlabel(title, size=fontsize)
    plt.ylabel("# of mice", size=fontsize)
    ax2 = plt.gca().twinx()
    plt.ylabel("Type 3", size=fontsize)
    ax2.set_yticks([])

    plt.savefig("./summary_figures/" + title)
    plt.show()


if __name__ == "__main__":
    all_intros = np.array(pickle.load(open("all_intros.p", 'rb')))
    all_intros_div = np.array(pickle.load(open("all_intros_div.p", 'rb')))
    all_states_per_type = np.array(pickle.load(open("all_states_per_type.p", 'rb')))

    # There are 5 mice with 0 type 2 intros, but only 3 mice with no type 2 stats.
    # That is because they come up, but don't explain the necessary 50% to start phase 2.
    type_hist(all_intros, title='State introductions')
    type_hist(all_intros_div, title='Session-normalised state introductions')
    type_hist(all_states_per_type, title='Average states used per session')
