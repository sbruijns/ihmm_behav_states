from pypolyagamma import PyPolyaGamma
import numpy as np
from scipy.linalg.lapack import dpotrs
from scipy.linalg import solve_triangular
import matplotlib.pyplot as plt

def resample_beta(X, Y, ws):
    """Don't know why this function doesn't work, it does what the paper says

    can't really compare because they do things weirdly...
    I actually get the correct V_w (as in, the same that they use) if I DON'T invert, but then m_w is pretty off..."""

    V_w = np.dot(X.T, np.dot(np.diag(ws), X)) + np.linalg.inv(B)
    m_w = np.dot(V_w, (np.dot(X.T, Y - 0.5)))  # B^{-1} * b goes away because b is 0 here
    return np.diag(np.random.normal(m_w, V_w))

def stupid_beta_resample(xs, yns, omegans):
    D = D_in
    prior_Sigma = B
    prior_J = np.linalg.inv(prior_Sigma)

    prior_h = prior_J.dot(np.zeros(D + 1))

    lkhd_h = np.zeros(D + 1)
    lkhd_J = np.zeros((D + 1, D + 1))

    augx = xs
    Jn = omegans
    hn = yns.flatten() - 0.5

    lkhd_J += (augx * Jn[:, None]).T.dot(augx)
    lkhd_h += hn.T.dot(augx)

    post_h = prior_h + lkhd_h
    post_J = prior_J + lkhd_J

    joint_sample = sample_gaussian(J=post_J, h=post_h)
    return joint_sample

def sample_gaussian(mu=None,Sigma=None,J=None,h=None):
    # Copied from pybasicbayes
    mean_params = mu is not None and Sigma is not None
    info_params = J is not None and h is not None
    assert mean_params or info_params

    if mu is not None and Sigma is not None:
        return np.random.multivariate_normal(mu, Sigma)
    else:
        L = np.linalg.cholesky(J)
        x = np.random.randn(h.shape[0])
        return solve_triangular(L, x, lower=True, trans='T') \
            + dpotrs(L, h, lower=True)[0]


ppgs = PyPolyaGamma(4)
np.random.seed(4)

n = 1500
n_samples = 250
burnin = 100

D_out = 1     # Output dimension
D_in = 2      # Input dimension
w0, w1, w2 = - 1, 2, - 4
b, B = np.zeros(3), np.diag([1, 1, 10])

# create predictors and their responses
X_no_bias = np.random.random((n, D_in))
X = np.ones((n, D_in + 1))
X[:, :-1] = X_no_bias
Y = ((1 / (1 + np.exp(- (X[:, 0] * w1 + X[:, 1] * w2 + w0)))) > np.random.random(n)).reshape(n, 1)

# draw beta from prior
beta = np.random.multivariate_normal(b, B)

ws = np.zeros(n)
samples = []
for _ in range(n_samples):
    print(np.ones(n), np.sum(beta * X, axis=1), ws)
    quit()
    ppgs.pgdrawv(np.ones(n), np.sum(beta * X, axis=1), ws)
    beta = stupid_beta_resample(X, Y, ws)
    # beta = resample_beta(X, Y, ws)
    samples.append(beta)

samples = samples[burnin:]

weight_0 = np.zeros(n_samples - burnin)
weight_1 = np.zeros(n_samples - burnin)
weight_2 = np.zeros(n_samples - burnin)
for i, sample in enumerate(samples):
    weight_0[i] = sample[2]
    weight_1[i] = sample[0]
    weight_2[i] = sample[1]

plt.figure(figsize=(18, 8))
plt.subplot(1, 3, 1)
plt.hist(weight_0)
plt.axvline(w0, c='r')
plt.ylabel("# of samples", size=28)
plt.gca().tick_params(axis='both', labelsize=20)
plt.title('w0', size=28)

plt.subplot(1, 3, 2)
plt.hist(weight_1)
plt.axvline(w1, c='r')
plt.xlabel("Beta", size=28)
plt.gca().tick_params(axis='both', labelsize=20)
plt.title('w1', size=28)

plt.subplot(1, 3, 3)
plt.hist(weight_2)
plt.axvline(w2, c='r')
plt.title('w2', size=28)
plt.gca().tick_params(axis='both', labelsize=20)
plt.tight_layout()
plt.show()
