import pickle
import numpy as np
import pyhsmm

temp = pickle.load(open('problem_data', 'rb'))

def my_messages_backwards_log(trans_mat, log_obs, log_durs, log_survivals):
    betal = np.zeros_like(log_obs) # 0's here are important because of += later
    betastarl = np.zeros_like(log_obs)
    betal[-1] = 1
    betastarl[-1] = np.exp(log_obs[-1]) # empirical fact, might be censoring term -> I get it myself to, comes from marginalisation of all durations since survival func
    S = betal.shape[1]
    T = betal.shape[0]


    #print(np.exp(log_durs[1]) + np.exp(log_durs[0]) + np.exp(log_survivals[1])) # this is how it works

    for t in range(T - 1, 0, -1):
        for i in range(S):
            temp = 0
            for d in range(1, T - t + 1):
                temp += betal[t + d - 1, i] * np.exp(log_durs[d - 1, i]) * np.prod(np.exp(log_obs[t:t + d, i])) # durs might start at 1

            temp += np.exp(log_survivals[T - t - 1, i]) * np.prod(np.exp(log_obs[t:, i])) # depends on coding of sf
            betastarl[t, i] = temp

        for i in range(S):
            for j in range(S):
                betal[t - 1, i] += betastarl[t, j] * trans_mat[i, j]

    # one last time, to fill last row o fbetastarl
    t = 0
    for i in range(S):
        temp = 0
        for d in range(1, T - t + 1):
            temp += betal[t + d - 1, i] * np.exp(log_durs[d - 1, i]) * np.prod(np.exp(log_obs[t:t + d, i])) # durs might start at 1

        temp += np.exp(log_survivals[T - t - 1, i]) * np.prod(np.exp(log_obs[t:, i])) # depends on coding of sf
        betastarl[t, i] = temp

    return betal, betastarl

a, b = my_messages_backwards_log(*temp)
