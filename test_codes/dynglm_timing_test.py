"""
Need to find out whether timepoints are mapped correctly.

We check this by whether uncertainty in the posterior gets reduced in the right spots.
"""
import numpy as np
import pyhsmm.basic.distributions as distributions
import matplotlib.pyplot as plt

# Testing of Dynamic_GLM implementation
np.set_printoptions(suppress=True)

seed = 215  # 215
np.random.seed(seed)

""" Time point test """
T = 20
n_inputs = 2
n_samples = 200
step_size = 0.5
Q = np.tile(np.eye(n_inputs), (T, 1, 1))
test = distributions.Dynamic_GLM(n_inputs=n_inputs, T=T, P_0=10 * np.eye(n_inputs), Q=Q * step_size, prior_mean=np.zeros(n_inputs))

test_points = [5, 6, 7, 8, 10, 18]
predictors = []
a, b = np.zeros(1000), np.zeros(1000)
a[250:500] = 1
a[750:1000] = 1
b[500:] = 1
for _ in range(T):
    if _ not in test_points:
        predictors.append(np.zeros(0))
    else:
        t = 1000
        pred = np.empty((t, n_inputs))
        pred[:, 0] = a
        pred[:, 1] = b
        predictors.append(pred)
sample = test.rvs(predictors, list(range(T)))

learn = distributions.Dynamic_GLM(n_inputs=n_inputs, T=T, P_0=np.eye(n_inputs), Q=Q * step_size, prior_mean=np.zeros(n_inputs))

samples = []
pseudo_samples = []
log_likes = []
test_obs = np.ones((1, 3))
for _ in range(n_samples):
    if _ % 10 == 0:
        print(_)
    temp = learn.resample(sample)
    pseudo_samples.append(temp)
    samples.append(learn.weights.copy())
    temp = np.zeros(T)
    for t in range(T):
        temp[t] = learn.log_likelihood(test_obs, t)
    log_likes.append(temp.copy())
samples = np.array(samples[100:])
logs = np.array(log_likes)

# T = 26
takefrom = [3, 7, 11, 15, 19, 20, 21, 25]

plt.figure(figsize=(16, 9))
for i in range(n_inputs + 1):
    plt.subplot(n_inputs + 1, 1, i+1)
    if i < n_inputs:
        label = 'Truth' if i == 0 else None
        plt.plot(np.arange(T), test.weights[:, i], label=label)
        sample_mean = np.mean(samples[:, :, i], axis=0)
        label = 'Posterior mean' if i == 0 else None
        plt.plot(np.arange(T), sample_mean, label=label, c='g')
        credible_interval = np.percentile(samples[:, :, i], [2.5, 97.5], axis=0)
        plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='g')
        # sns.despine()
        # compare with 0 variance
        # if i == 0:
        #     plt.ylim(bottom=-0.4, top=0)
        # if i == 1:
        #     plt.ylim(bottom=-3.4, top=-4.2)
    else:
        logs_mean = np.mean(logs, axis=0)
        label = 'logs mean' if i == 0 else None
        plt.plot(np.arange(T), logs_mean, label=label, c='b')
        credible_interval = np.percentile(logs, [2.5, 97.5], axis=0)
        plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='b')
        # sns.despine()
        # compare with 0 variance
        # plt.ylim(bottom=-4.4, top=-3.4)
    if i == 0:
        plt.legend(fontsize=18, frameon=False)
    plt.xlim(left=0, right=T)
    # for t in takefrom:
    #     plt.axvline(t)
plt.tight_layout()
plt.savefig("timepoint test")
plt.show()
