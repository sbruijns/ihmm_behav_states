"""
Try to fit the weird GLM which gives me trouble.

Solution: I defined the bias incorrectly in the Sim mice.
"""
import numpy as np
import matplotlib.pyplot as plt
import copy
import warnings
import pickle
from itertools import product
import pyhsmm.basic.distributions as distributions
import seaborn as sns


np.set_printoptions(suppress=True)

contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}
cont_mapping = np.vectorize(num_to_contrast.get)

subjects = ['GLM_Sim_06']
fit_type = ['prebias', 'bias', 'all', 'prebias_plus'][0]
with_time = False
fit_variance = [0.05, 0.01, 0.002, 0.0005, 'uniform'][3]

seed = 25  # 1842

n_samples = 200
sample = []

for subject in subjects:
    np.random.seed(seed)

    info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
    # Determine session numbers
    if fit_type == 'prebias':
        till_session = info_dict['bias_start']
    elif fit_type == 'bias' or fit_type == 'all':
        till_session = info_dict['n_sessions']
    elif fit_type == 'prebias_plus':
        till_session = min(info_dict['bias_start'] + 6, info_dict['n_sessions'])  # 6 here will actually turn into 7 later

    n_inputs = 5

    for j in [6, 7, 12]:
        try:
            data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
        except FileNotFoundError:
            continue
        if data.shape[0] == 0:
            print("meh, skipped session")
            continue

        for i in range(data.shape[0]):
            data[i, 0] = num_to_contrast[data[i, 0]]

        # previous answer
        prev_ans = data[:, 1].copy()
        prev_ans[1:] = prev_ans[:-1] - 1

        # previous reward
        side_info = pickle.load(open("../session_data/{}_side_info_{}.p".format(subject, j), "rb"))
        prev_reward = side_info[:, 1]
        prev_reward[1:] = prev_reward[:-1]

        bad_trials = data[:, 1] == 1
        bad_trials[0] = True
        mega_data = np.empty((np.sum(~bad_trials), n_inputs + 1))

        mega_data[:, 0] = np.maximum(data[~bad_trials, 0], 0)
        mega_data[:, 1] = np.abs(np.minimum(data[~bad_trials, 0], 0))
        mega_data[:, 2] = prev_ans[~bad_trials]
        mega_data[:, 3] = mega_data[:, 2]
        mega_data[prev_reward[~bad_trials] == 0, 3] *= -1
        if len(np.unique(mega_data[:, 0])) > 3:
            mega_data[:, 4] = 1
        else:
            mega_data[:, 4] = 0
        mega_data[:, 5] = data[~bad_trials, 1] - 1
        mega_data[:, 5] = (mega_data[:, 5] + 1) / 2

        print(np.unique(mega_data[:, 0], return_counts=1))
        print(np.unique(mega_data[:, 1], return_counts=1))
        sample.append(mega_data)

T = 3
Q = np.tile(np.eye(n_inputs), (3, 1, 1))
learn = distributions.Dynamic_GLM(n_inputs=n_inputs, T=3, P_0=np.eye(n_inputs), Q=Q * 0.01)

samples = []
log_likes = []
test_obs = np.ones((1, 3))
for _ in range(n_samples):
    if _ % 10 == 0:
        print(_)
    temp = learn.resample(sample)
    samples.append(learn.weights.copy())
samples = np.array(samples[100:])
logs = np.array(log_likes)

avg_weight = np.zeros(n_inputs)
plt.figure(figsize=(16, 9))
for i in range(n_inputs):
    plt.subplot(n_inputs, 1, i+1)
    label = 'Truth' if i == 0 else None
    # plt.plot(np.arange(T), test.weights[:, i], label=label)
    sample_mean = np.mean(samples[:, :, i], axis=0)
    avg_weight[i] = sample_mean[0]
    label = 'Posterior mean' if i == 0 else None
    plt.plot(np.arange(T), sample_mean, label=label, c='g')
    credible_interval = np.percentile(samples[:, :, i], [2.5, 97.5], axis=0)
    plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='g')
    sns.despine()

    if i == 0:
        plt.legend(fontsize=18, frameon=False)
    plt.xlim(left=0, right=T-1)
plt.tight_layout()
plt.savefig("timepoint test")
plt.show()

true_weight = np.array([-1, 1.2, 2.1, 0.1, 1])

learn.weights = np.tile(avg_weight, (3, 1))
print(learn.log_likelihood(sample[0], 0).sum())

learn.weights = np.tile(true_weight, (3, 1))
print(learn.log_likelihood(sample[0], 0).sum())
