from pybasicbayes.distributions import Input_Categorical_Normal
from pybasicbayes import distributions
import numpy as np


# ScalarGaussianNIX doesn't work for whatever reason

obs_dim = 1
obs_hypparams = {'mu_0': np.zeros(obs_dim),
                'sigma_0': np.eye(obs_dim)*0.2,
                'kappa_0': 0.3,
                'nu_0': obs_dim+5}


# test = distributions.Gaussian(**obs_hypparams)
# sample = test.rvs(1000)
# recover = distributions.Gaussian(**obs_hypparams)
#
# n = 1000
# mu = np.zeros(1)
# sig = np.zeros((1, 1))
# for _ in range(n):
#     recover.resample(sample)
#     mu += recover.mu
#     sig += recover.sigma
#
# print(test.mu)
# print(mu / n)
#
# print(test.sigma)
# print(sig / n)



test = Input_Categorical_Normal(9, 3, obs_hypparams)

#sample = [test.rvs(np.random.choice(9, 5000)), test.rvs(np.random.choice(9, 5000))]
sample = test.rvs(np.random.choice(9, 5000))
print(test.log_likelihood(sample))

recover = Input_Categorical_Normal(9, 3, obs_hypparams)

calc = np.zeros((9, 3))
calci = np.zeros((9, 2))
n = 1000
for _ in range(n):
    if _ % 25 == 0:
        print(_)
    recover.resample(sample)
    calc += recover.cats.weights
    for i in range(9):
        calci[i, 0] += recover.normals[i].mu
        calci[i, 1] += recover.normals[i].sigma

print("Producing weights:\n {}".format(test.cats.weights))
print("Sample averaged weights:\n {}".format(calc / n))

print('Producing vs sampled normal params:')
for i in range(9):
    print("Truth: mu: {}, sigmasq: {}".format(test.normals[i].mu, test.normals[i].sigma))
    print(calci[i] / n)
