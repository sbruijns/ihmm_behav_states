"""This seems to use the pdf of a standard normal to determine likelihood of arbitrary normal dists."""
import numpy as np
from scipy.stats import norm


# mu = 0.5
# sig = 0.9
#
#
# points = np.linspace(-2, 2)
#
# plt.plot(points, norm(mu, sig).pdf(points), label='True')
#
#
# plt.plot(points, norm(0, 1).pdf((points - mu) / sig) / sig, label='proxy')
# plt.legend()
# plt.show()


params = [(0, 1), (0.2, 1.9), (1.8, 0.4)]
producers = np.array([0, 0, 0, 1, 1, 2, 0, 2, 1, 1, 1, 2, 2, 0, 0, 2, 2])
vals = np.array([0.1, -0.3, 1.2, 0.2, -0.6, 2.3, 0.1, -1.1, 1.0, 0.9, 1.8, 0., 0., 0.18, -0.2, 2.3, 1.3])


def naive(params, zs, vals):
    lls = []
    for z, val in zip(zs, vals):
        lls.append(norm(*params[z]).pdf(val))

    return np.log(lls)

def smart(params, zs, vals):
    means = np.zeros_like(vals)
    stds = np.zeros_like(vals)
    for i, p in enumerate(params):
        mask = zs == i
        means[mask], stds[mask] = p

    likes = norm().pdf((vals - means) / stds) / stds

    return np.log(likes)

assert np.array_equal(naive(params, producers, vals), smart(params, producers, vals))
print('works')
