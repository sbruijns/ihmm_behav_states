import numpy as np
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from scipy.stats import pearsonr
from scipy.stats import spearmanr


# a = np.array(pickle.load(open('./../c.p', 'rb')))
# b = np.array(pickle.load(open('./../d.p', 'rb')))
#
# reg = LinearRegression(fit_intercept=True).fit(b.reshape(-1, 1), a)
# plt.scatter(b, a)
# points = np.linspace(min(b), max(b))
# plt.plot(points, reg.coef_ * points + reg.intercept_)
# plt.show()
#
# print(pearsonr(a, b))
# print(spearmanr(a, b))
#
#
# quit()
a = pickle.load(open('./../a.p', 'rb'))
b = pickle.load(open('./../b.p', 'rb'))


fs=24

f, (a0, a1) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1., 1.6180]}, figsize=(12, 6))

a0.hist(b, bins=range(1, 8), align='left', color='grey')

sns.despine()
a0.set_ylabel("# of sessions", size=fs)
a0.set_xlabel("# of states in session", size=fs)
a0.tick_params(axis='both', labelsize=fs -6)


a1.hist(a, color='grey')

sns.despine()
a1.set_ylabel("# state pairs", size=fs)
a1.set_xlabel("State correlations", size=fs)
a1.set_xlim(left=-1, right=1)
a1.tick_params(axis='both', labelsize=fs -6)

plt.tight_layout()
plt.show()
