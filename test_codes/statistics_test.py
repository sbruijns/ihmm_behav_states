# small test whether normal normal and mine have same statistics
# def local_multivariate_normal_draw(x, sigma, normal):
#     """
#     Cholesky doesn't like 0 cov matrix, but we want it.
#
#     TODO: This might need changing if in practice we see plently of 0 matrices
#     """
#     try:
#         return x + np.linalg.cholesky(sigma).dot(normal)
#     except np.linalg.LinAlgError:
#         if np.isclose(sigma, 0).all():
#             return x
#         else:
#             print("Weird covariance matrix")
#             quit()
#
#
# mean = np.array([1.2, -0.3, -3.7])
# cov = np.array([[0.08761913,  0.14207431, -0.03862822],
#                 [0.14207431,  0.27577677, -0.12762713],
#                 [-0.03862822, -0.12762713,  0.11092445]])
#
# from scipy.stats import kurtosis
# a = np.random.multivariate_normal(mean, cov, 10000)
# print(np.cov(a.T))
# print(np.mean(a, axis=0))
# print("aaaa {}".format(kurtosis(a)))
# b = np.random.standard_normal((10000, 3))
# for i in range(10000):
#     b[i] = local_multivariate_normal_draw(mean, cov, b[i])
#
# print(np.mean(b, axis=0))
# print(np.cov(b.T))
# print("aaaa {}".format(kurtosis(b)))
