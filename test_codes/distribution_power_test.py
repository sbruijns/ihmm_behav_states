import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def plot_normal_power():


    a = np.linspace(-2, 2)
    pdf = norm.pdf(a)

    power_1_5 = pdf ** 1.5
    power_1_5 /= np.sum(power_1_5)

    power_0_5 = pdf ** 0.5
    power_0_5 /= np.sum(power_0_5)

    plt.plot(a, pdf / np.sum(pdf), label='normal')
    plt.plot(a, power_1_5, label='1.5')
    plt.plot(a, power_0_5, label='0.5')
    plt.legend()
    plt.show()


plot_normal_power()
