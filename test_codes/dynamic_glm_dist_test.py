""" Test how well the parameters of PMFs are recovered by dynamic GLM."""
import numpy as np
import pyhsmm.basic.distributions as distributions
import time
import matplotlib.pyplot as plt
import pickle

# Testing of Dynamic_GLM implementation
np.set_printoptions(suppress=True)


seed = 215
np.random.seed(seed)

T = 30
n_regressors = 2
n_samples = 150
Q = np.tile(np.eye(n_regressors), (T, 1, 1))
test = distributions.Dynamic_GLM(n_regressors=n_regressors, T=T, P_0=np.eye(n_regressors), Q=Q * 0.01, prior_mean=np.zeros(n_regressors))

predictors = []
for _ in range(T):
    t = 30
    pred = np.empty((t, n_regressors))
    for i in range(3):
        pred[i*10:i*10 + 10, 0] = np.random.rand()
    pred[:, 1] = 1
    predictors.append(pred)
sample = test.rvs(predictors, list(range(T)))

learn = distributions.Dynamic_GLM(n_regressors=n_regressors, T=T, P_0=np.eye(n_regressors), Q=Q * 0.01, prior_mean=np.zeros(n_regressors))

pickle.dump((sample, test.weights, learn.weights), open("comparison_data", 'wb'))

np.random.seed(19)
samples = []
for _ in range(n_samples):
    if _ % 10 == 0:
        print(_)
    learn.resample(sample)
    samples.append(learn.weights.copy())
samples = np.array(samples[50:])

plt.figure(figsize=(16, 9))
for i in range(n_regressors):
    plt.subplot(n_regressors, 1, i+1)
    label = 'Truth' if i == 0 else None
    plt.plot(np.arange(T), test.weights[:, i], label=label)
    sample_mean = np.mean(samples[:, :, i], axis=0)
    label = 'Posterior mean' if i == 0 else None
    plt.plot(np.arange(T), sample_mean, label=label, c='g')
    credible_interval = np.percentile(samples[:, :, i], [2.5, 97.5], axis=0)
    plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='g')
    # sns.despine()
    if i == 0:
        plt.legend(fontsize=18, frameon=False)
    plt.xlim(left=0, right=T)
plt.tight_layout()
plt.savefig("GLM tracking compare")
plt.show()


""" Thorough test """

T = 30
n_regressors = 5
n_samples = 200
Q = np.tile(np.eye(n_regressors), (T, 1, 1))
test = distributions.Dynamic_GLM(n_regressors=n_regressors, T=T, P_0=np.eye(n_regressors), Q=Q * 0.01, prior_mean=np.zeros(n_regressors))

pi = 3.14159
offsets = [0, pi / 2, pi / 4, - pi / 2, 3 * pi / 4]
amplitude = [3, 3, 2, 1, 1.5]
speed = [1, 7, 0.5, 5, 1.5]

for i in range(n_regressors):
    for t in range(T):
        test.weights[t, i] = np.sin(t / speed[i] + offsets[i]) * amplitude[i]

predictors = []
for _ in range(T):
    t = int(np.random.rand() * 75) + 75
    pred = np.empty((t, n_regressors))
    pred[:, 0] = np.random.choice(7, t) / 6
    pred[:, 1] = np.random.choice(7, t) / 6
    pred[:, 2] = (np.random.choice(10, t) - ((10 - 1) / 2)) / ((10-1) / 2)
    pred[:, 3] = (np.random.choice(10, t) - ((10 - 1) / 2)) / ((10-1) / 2)
    pred[:, 4] = 1
    predictors.append(pred)
sample = test.rvs(predictors, list(range(T)))

learn = distributions.Dynamic_GLM(n_regressors=n_regressors, T=T, P_0=np.eye(n_regressors), Q=Q * 1, prior_mean=np.zeros(n_regressors))

samples = []
for _ in range(n_samples):
    if _ % 10 == 0:
        print(_)
    learn.resample(sample)
    samples.append(learn.weights.copy())
samples = np.array(samples[75:])

plt.figure(figsize=(16, 9))
for i in range(n_regressors):
    plt.subplot(n_regressors, 1, i+1)
    label = 'Truth' if i == 0 else None
    plt.plot(np.arange(T), test.weights[:, i], label=label)
    sample_mean = np.mean(samples[:, :, i], axis=0)
    label = 'Posterior mean' if i == 0 else None
    plt.plot(np.arange(T), sample_mean, label=label, c='g')
    credible_interval = np.percentile(samples[:, :, i], [2.5, 97.5], axis=0)
    plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='g')
    # sns.despine()
    if i == 0:
        plt.legend(fontsize=18, frameon=False)
    plt.xlim(left=0, right=T)
    plt.ylim(-3.5, 3.5)
plt.tight_layout()
plt.savefig("GLM tracking")
plt.show()


""" Time point test """
T = 42
n_regressors = 5
Q = np.tile(np.eye(n_regressors), (T, 1, 1))
test = distributions.Dynamic_GLM(n_regressors=n_regressors, T=T, P_0=np.eye(n_regressors), Q=Q * 0.01, prior_mean=np.zeros(n_regressors), jumplimit=3)

test_points = [5, 6, 7, 9, 10, 13, 14, 18, 23, 30]
predictors = []
for _ in range(T):
    if _ not in test_points:
        predictors.append(np.zeros(0))
    else:
        t = int(np.random.rand() * 1) + 2
        pred = np.empty((t, n_regressors))
        pred[:, 0] = np.random.choice(5, t)
        pred[:, 1] = np.random.choice(2, t)
        pred[:, 2] = np.random.choice(2, t)
        pred[:, 3] = np.random.choice(2, t)
        pred[:, 4] = 1
        predictors.append(pred)
sample = test.rvs(predictors, list(range(T)))

learn = distributions.Dynamic_GLM(n_regressors=n_regressors, T=T, P_0=np.eye(n_regressors), Q=Q * 0.01, prior_mean=np.zeros(n_regressors), jumplimit=3)

# print(test.log_likelihood(sample[3], 3))

solution = {5: 1, 6: 3, 7: 5, 8: 6, 9: 8, 10: 10, 11: 11, 12: 12, 13: 14, 14: 16, 15: 17, 16: 18, 17: 19, 18: 21, 19: 22, 20: 23, 21: 24, 22: 24, 23: 26, 24: 27, 25: 28, 26: 29, 27: 29, 28: 29, 29: 29, 30: 31}
timedict = test.resample(sample)
# assert timedict == solution
print('all good')
