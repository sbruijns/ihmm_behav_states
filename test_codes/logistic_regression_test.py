"""
Code to understand how Polyagamma regression works

2nd line so flake shuts up
"""
import matplotlib.pyplot as plt
import numpy as np
from pypolyagamma import BernoulliRegression
np.random.seed(4)

D_out = 1     # Output dimension
D_in = 2      # Input dimension
reg = BernoulliRegression(D_out, D_in)

n = 1500
n_samples = 250
burnin = 100

np.random.seed(4)
X = np.random.random((n, D_in))

w0, w1, w2 = - 1, 2, - 4

Y = ((1 / (1 + np.exp(- (X[:, 0] * w1 + X[:, 1] * w2 + w0)))) > np.random.random(n)).reshape(n, 1)
# Given X, an NxD_in array of real-valued inputs,
# and Y, and NxD_out array of binary observations. Fit
# the linear model y_n ~ Bern(sigma(A x_n + b)),
# where sigma is the logistic function.  A is D_out x D_in,
# b is D_out x 1, and the entries in y_n are cond. indep.
samples = []
for _ in range(n_samples):
    reg.resample((X, Y))
    samples.append((reg.A, reg.b))

samples = samples[burnin:]

weight_0 = np.zeros(n_samples - burnin)
weight_1 = np.zeros(n_samples - burnin)
weight_2 = np.zeros(n_samples - burnin)
for i, sample in enumerate(samples):
    weight_0[i] = sample[1][0, 0]
    weight_1[i] = sample[0][0, 0]
    weight_2[i] = sample[0][0, 1]

plt.hist(weight_0)
plt.axvline(w0, c='r')
plt.show()

plt.hist(weight_1)
plt.axvline(w1, c='r')
plt.show()

plt.hist(weight_2)
plt.axvline(w2, c='r')
plt.show()
