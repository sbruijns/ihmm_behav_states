import numpy as np
import pyhsmm.basic.distributions as distributions
import time

# Testing of Nested_Categorical implementation, now with mod of input
# This code doesn't quite work like this, I would need to generate from a normal 14 one,
# then mod down to seven and resample, compare with average over double pmfs from 14 one
np.set_printoptions(suppress=True)

test = distributions.Input_Categorical(14, 3)

sample_n = np.random.choice(14, 1000)
sample = test.rvs(sample_n)

learn = distributions.Input_Categorical(7, 3, modulate=7)

calc = np.zeros((7, 3))
n = 1000
a = time.time()
for i in range(n):
    learn.resample(sample)
    calc += learn.weights
print("Timing for {} resamplings: {}".format(n, time.time() - a))

print("Producing weights (transformed as necessary): {}".format((test.weights[:7] + test.weights[7:]) / 2))
print("Sample averaged weights: {}".format(calc / n))

print("Difference between samples and generative model: {}".format(np.abs((test.weights[:7] + test.weights[7:]) / 2 - calc / n)))

test.log_likelihood(sample)

quit()
for state in range(7):
    _, counts = np.unique(sample[sample[:, 0] == state][:, 1], return_counts=True)
    print(counts / len(sample[sample[:, 0] == state]))


test = distributions.Input_Categorical(3, 3)
test.weights = np.array([[0., 1., 0.], [1., 0., 0.], [0., 0.5, 0.5]])
inputs = np.random.choice(3, 25)
sample = test.rvs(inputs)
ll = test.log_likelihood(sample)

print(inputs)
print(sample)
print(ll)
