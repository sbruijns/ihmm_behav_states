"""
Code to understand how Matt's Gamma distribution works.

2nd line so flake shuts up
"""
from pybasicbayes.distributions import GammaCompoundDirichlet, MultinomialAndConcentration, CategoricalAndConcentration
import matplotlib.pyplot as plt
import numpy as np


n = 1000

# these all work the same
test = GammaCompoundDirichlet(a_0=100, b_0=0.01, K=15)
test = MultinomialAndConcentration(a_0=100, b_0=0.01, K=15)
test = CategoricalAndConcentration(a_0=100, b_0=0.01, K=15)

concs = np.zeros(n)
for i in range(n):
    test.resample()
    concs[i] = test.alpha_0  # or .concentration if GammaCompoundDirichlet

plt.hist(concs)
plt.show()
