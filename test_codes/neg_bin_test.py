import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import nbinom
from itertools import product

r_support = np.cumsum(np.arange(5, 100, 5))
r_support = np.arange(5, 705, 2)
# r_support = np.array([1, 2, 3, 5, 7, 10, 15, 21, 28, 36, 45, 55, 150])
# r_support = np.array([150])
label = "New prior" if np.array_equal(r_support, np.cumsum(np.arange(5, 100, 5))) else "Old prior"

lim = 1200
sample_n = 10000

prob = np.zeros(lim)
points = np.arange(lim)

ps = np.random.beta(1, 1, sample_n)
rs = np.random.choice(r_support, sample_n)
for i, (p, r) in enumerate(zip(ps, rs)):
    temp = nbinom.pmf(points, r, p)
    prob += temp
    if i < 125:
        plt.plot(temp)
        if i == 124:
            plt.show()

n_prob = prob / prob.sum()
print(r_support)
print(np.sum(np.cumsum(n_prob) < 0.5))
print(np.sum(np.cumsum(n_prob) < 0.8))

plt.plot(prob, label='PMF')
plt.title(label)
plt.axvline(np.sum(np.cumsum(n_prob) < 0.5), c='red', label="50% CDF")
plt.axvline(np.sum(np.cumsum(n_prob) < 0.8), c='black', label="80% CDF")
plt.legend()
plt.show()

def sample_neg_bins(r, n, points):
    ps = np.random.beta(1, 1, n)
    for i in range(n):
        plt.plot(points, nbinom.pmf(points, r, ps[i]))
    plt.show()

def scan_neg_bins(rs, points):
    for r, p in product(rs, np.arange(0.05, 1, 0.1)):
        plt.plot(points, nbinom.pmf(points, r, p))
    plt.show()
