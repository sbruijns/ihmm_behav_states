import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


cmap_dark = sns.diverging_palette(220, 20, center="dark", as_cmap=True)
cmap_light = sns.color_palette("coolwarm", as_cmap=True)
colors = np.zeros((100, 4))

dist = 8

for i in range(100):
    colors[i] = cmap_dark(i / 100) if 50 - dist <= i <= 50 + dist else cmap_light(i / 100)

for i in range(100):
    plt.plot([0, 1], [i, i], c=colors[i])
plt.show()

np.savetxt('colors.csv', colors, delimiter=',')
