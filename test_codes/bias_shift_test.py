"""
    Studying how much easier it is for small changes in the regressors to affect the pmf around 0 versus further from zero

"""
import numpy as np
import matplotlib.pyplot as plt


weights = list(np.zeros((17, 3)))

for i, weight in enumerate(weights):
    weight[-1] = i * 0.2 - 1.6

contrasts_L = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])
contrasts_R = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])[::-1]
bias = np.ones(11)

predictors = np.vstack((contrasts_L, contrasts_R, bias)).T

for weight in weights:
    plt.plot(1 / (1 + np.exp(- np.sum(weight * predictors, axis=1))))

plt.ylim(0, 1)
plt.show()



weights = list(np.zeros((17, 3)))

for i, weight in enumerate(weights):
    weight[-1] = i * 0.2 - 1.6
    weight[0] = 2

for weight in weights:
    plt.plot(1 / (1 + np.exp(- np.sum(weight * predictors, axis=1))))

plt.ylim(0, 1)
plt.show()