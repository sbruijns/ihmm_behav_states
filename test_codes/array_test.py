import numpy as np
import matplotlib.pyplot as plt



def softmax(x):
    return np.exp(x) / np.sum(np.exp(x))

x = np.linspace(-1, 1, 30)
y = np.linspace(-1, 1, 30)
xv, yv = np.meshgrid(x, y)

print(softmax([1, 0]))
print(softmax([np.log(2 * np.e), np.log(2)]))
