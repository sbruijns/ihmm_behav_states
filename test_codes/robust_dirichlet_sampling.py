import numpy as np


def d_samp():
    return np.random.dirichlet(np.array([.0001, .0001, .0001]))


def robust_d_samp():
    counter = 0
    while counter < 20:
        try:
            temp = np.random.dirichlet(np.array([.0001, .0001, .0001]))
        except ZeroDivisionError:
            counter += 1
        else:
            print(counter)
            if np.isnan(temp).any():
                print("happened and caught")
                continue
            return temp
    print("failed")
    return np.random.dirichlet(np.ones(3))
