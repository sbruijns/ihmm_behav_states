import numpy as np
import matplotlib.pyplot as plt
import pickle
import pyhsmm.basic.distributions as distributions

T = 16
n_inputs = 3

gibbs_burnin = 20000
pymc_burnin = 200000


samples, LL_weights = pickle.load(open('gibbs_posterior', 'rb'))
samples = np.array(samples)[gibbs_burnin:]
test = pickle.load(open('truth', 'rb'))
pymc_weights = pickle.load(open('pymc_posterior', 'rb'))


def temp(x):
    if x < 11:
        return x
    elif x == 11 or x == 12:
        return 10
    else:
        return x - 2


m = np.zeros((T, n_inputs))
u = np.zeros((T, n_inputs))
low = np.zeros((T, n_inputs))
for t in range(T):
    w = pymc_weights[temp(t), pymc_burnin:]
    for i in range(n_inputs):
        m[t, i] = np.mean(w[:, i])
        u[t, i] = np.percentile(w[:, i], 97.5)
        low[t, i] = np.percentile(w[:, i], 2.5)


plt.figure(figsize=(16, 9))
for i in range(n_inputs):
    plt.subplot(n_inputs, 1, i+1)
    plt.plot(np.arange(T), test.weights[:, i], label='Truth')
    plt.plot(np.arange(T), LL_weights[:, i], label='LL')
    sample_mean = np.mean(samples[:, :, i], axis=0)
    credible_interval = np.percentile(samples[:, :, i], [2.5, 97.5], axis=0)
    plt.plot(np.arange(T), sample_mean, label='Posterior mean', c='g')
    plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='g')

    plt.plot(np.arange(T), m[:, i], label='pymc mean', c='r')
    plt.fill_between(np.arange(T), u[:, i], low[:, i], alpha=0.2, color='r')

    # sns.despine()
    if i == 0:
        plt.legend(fontsize=18, frameon=False)
    plt.xlim(left=0, right=T)
plt.tight_layout()
plt.savefig("timepoint test")
plt.show()
