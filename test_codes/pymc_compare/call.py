"""Perform a pymc sampling of the test data."""
import pymc, bayes_fit  # load the model file
import numpy as np
import pickle

# Data params
T = 14
n_inputs = 3

# Sampling params
n_samples = 400000

R = pymc.MCMC(bayes_fit)  # build the model
R.sample(n_samples)  # populate and run it

# Extract weights
weights = np.zeros((T, n_samples, n_inputs))
for t in range(T):
    try:
        weights[t] = R.trace('w_{}'.format(t))[0:]
    except KeyError:
        weights[t] = R.trace('ws'.format(t))

# Save everything
pickle.dump(weights, open('pymc_posterior', 'wb'))
