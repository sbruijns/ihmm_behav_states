import numpy as np
import pymc
import pickle

np.random.seed(14)

x = pickle.load(open('test_data', 'rb'))
del x[11]
del x[11]
n_inputs = 3
step_size = 0.2
T = 16 - 2

def temp(x):
    if x <= 11:
        return x
    else:
        return x - 2

ans = np.zeros(T * 4)
for t in range(T):
    if t in [0, 1, 8, 9, 10, 14 - 2, 15 - 2]:
        continue
    ans[t*4] = np.sum(x[temp(t)][:250, -1])
    ans[t*4+1] = np.sum(x[temp(t)][250:500, -1])
    ans[t*4+2] = np.sum(x[temp(t)][500:750, -1])
    ans[t*4+3] = np.sum(x[temp(t)][750:, -1])

#priors
if step_size == 0.:
    ws = [pymc.MvNormal('ws', mu=np.zeros(n_inputs), tau=np.eye(n_inputs) / 4)]

    #model
    @pymc.deterministic(plot=False)
    def sigmoidal(ws=ws):
        ps = np.zeros(T * 4)
        for t in range(T):
            ps[t*4] = 1 / (1 + np.exp(- np.sum(np.array([0, 0, 1]) * ws)))
            ps[t*4+1] = 1 / (1 + np.exp(- np.sum(np.array([1, 0, 1]) * ws)))
            ps[t*4+2] = 1 / (1 + np.exp(- np.sum(np.array([0, 1, 1]) * ws)))
            ps[t*4+3] = 1 / (1 + np.exp(- np.sum(np.array([1, 1, 1]) * ws)))
        ps = np.clip(ps, np.spacing(1), 1 - np.spacing(1))
        return ps
else:
    ws = [pymc.MvNormal('w_0', mu=np.zeros(n_inputs), tau=np.eye(n_inputs) / 4)]
    for t in range(1, T):
        ws.append(pymc.MvNormal('w_{}'.format(t), mu=ws[-1], tau=np.eye(n_inputs) / step_size))
    ws = np.array(ws)

    #model
    @pymc.deterministic(plot=False)
    def sigmoidal(ws=ws):
        ps = np.zeros(T * 4)
        for t in range(T):
            ps[t*4] = 1 / (1 + np.exp(- np.sum(np.array([0, 0, 1]) * ws[t])))
            ps[t*4+1] = 1 / (1 + np.exp(- np.sum(np.array([1, 0, 1]) * ws[t])))
            ps[t*4+2] = 1 / (1 + np.exp(- np.sum(np.array([0, 1, 1]) * ws[t])))
            ps[t*4+3] = 1 / (1 + np.exp(- np.sum(np.array([1, 1, 1]) * ws[t])))
        ps = np.clip(ps, np.spacing(1), 1 - np.spacing(1))
        return ps

#likelihood
ns = np.zeros(T * 4)
ns[:] = 250
temp = ans.copy()
for tau in [0, 1, 8, 9, 10, 14 - 2, 15 - 2]:
    ns[tau * 4:(tau+1) * 4] = 0
y1 = pymc.Binomial('y1', n=ns, p=sigmoidal, value=ans, observed=True)
