"""
Code to understand how Matt's MultinomialAndConcentration works.

In particular, why does small prior not lead to few open tables?
"""
from pybasicbayes.distributions import MultinomialAndConcentration
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import digamma
from scipy.special import logsumexp


def crp_expec(n, theta):
    """
    Return expected number of tables after n customers, given concentration theta.

    From Wikipedia
    """
    return theta * (digamma(theta + n) - digamma(theta))

def exact_prob_for_just_one_table(n, theta):
    return np.exp(np.sum(np.log(np.arange(1, n) / (np.arange(1, n) + theta))))


N = 100


#np.random.seed(16)
# these all work the same
test = MultinomialAndConcentration(a_0=0.01, b_0=0.01, K=15)

# what weights did the prior pick
print(test.weights)

# what does a sample look like
print(test.rvs(3, N=N))

# what is current conc. parameter
print(test.alpha_0)

# what does expected number of tables have to say about this
print(crp_expec(N, test.alpha_0))

# what's prob that only one table is used
print(exact_prob_for_just_one_table(N, test.alpha_0))
