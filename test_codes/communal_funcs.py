import numpy as np

contrasts_L = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])
contrasts_R = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])[::-1]

def weights_to_pmf(weights, with_bias=None):
    if with_bias is not None:
        print("Bias is now always in PMF")
        quit()
    if weights.shape[0] == 3 or weights.shape[0] == 4 or weights.shape[0] == 5:
        psi = weights[0] * contrasts_L + weights[1] * contrasts_R + weights[-1]
        return 1 / (1 + np.exp(-psi))
    elif weights.shape[0] == 11:
        return weights[:, 0]
    else:
        print('new weight shape')
        quit()
