"""Can I average the output of two logistic function probs by just setting one value intermediate."""
import numpy as np


def logistic(w, x):
    sum = np.sum(w[:-1] * x) + w[-1]
    return 1 / (1 + np.exp(- sum))


w = np.random.rand(4) * 2 - 1
x_0 = np.array([0, 1, 0])
x_1 = np.array([0, 1, 1])
x_inbetween = np.array([0, 1, 0.5])

print("Manual average: {}, ({}, {})".format((logistic(w, x_0) + logistic(w, x_1)) / 2, logistic(w, x_0), logistic(w, x_1)))
print("Direct average: {}".format(logistic(w, x_inbetween)))
