import numpy as np
import pyhsmm.basic.distributions as distributions
import time

# Testing of Nested_Categorical implementation
np.set_printoptions(suppress=True)

test = distributions.Input_Categorical(7, 3)

sample_n = np.random.choice(7, 1000)
sample = test.rvs(sample_n)

learn = distributions.Input_Categorical(7, 3)

calc = np.zeros((7, 3))
n = 1000
a = time.time()
for i in range(n):
    learn.resample(sample)
    calc += learn.weights
print("Timing for {} resamplings: {}".format(n, time.time() - a))

print("Producing weights: {}".format(test.weights))
print("Sample averaged weights: {}".format(calc / n))

print("Difference between samples and generative model: {}".format(np.abs(test.weights - calc / n)))

test.log_likelihood(sample)

for state in range(7):
    _, counts = np.unique(sample[sample[:, 0] == state][:, 1], return_counts=True)
    print(counts / len(sample[sample[:, 0] == state]))


test = distributions.Input_Categorical(3, 3)
test.weights = np.array([[0., 1., 0.], [1., 0., 0.], [0., 0.5, 0.5]])
inputs = np.random.choice(3, 25)
sample = test.rvs(inputs)
ll = test.log_likelihood(sample)

print(inputs)
print(sample)
print(ll)
