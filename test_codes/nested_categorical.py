import numpy as np
import pyhsmm.basic.distributions as distributions
import time

# Testing of Nested_Categorical implementation

np.set_printoptions(suppress=True)

test = distributions.Nested_Categorical([7, 3])

sample_n = 1000
sample = test.rvs(sample_n)

learn = distributions.Nested_Categorical([7, 3])

calc = np.zeros((7, 3))
n = 1000
a = time.time()
for i in range(n):
    learn.resample(sample)
    calc += learn.weights
print("Timing for {} resamplings: {}".format(n, time.time() - a))

print("Producing weights: {}".format(test.weights))
print("Sample averaged weights: {}".format(calc / n))

print("Difference between samples and generative model: {}".format(np.abs(test.weights - calc / n)))

print("Marginals over second observation: {}".format(test.weights.sum(1)))
print("Marginals over samples: {}".format((calc / n).sum(1)))

_, counts = np.unique(sample[:, 0], return_counts=1)
print("Observation counts (sample averages should be close to this): {}".format(counts / sample_n))

test.log_likelihood(sample)
