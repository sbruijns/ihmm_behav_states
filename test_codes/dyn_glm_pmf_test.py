# import matplotlib
# matplotlib.use('Agg')
import numpy as np
import pyhsmm.basic.distributions as distributions
import matplotlib.pyplot as plt
from communal_funcs import weights_to_pmf
from itertools import product
from scipy.stats import norm

# Testing of Dynamic_GLM implementation
np.set_printoptions(suppress=True)


# The following code can serve to compare this implementation with the dynamic_glm_sampler.py implementation


T = 4
trials = 500
n_samples = 1000
n_inputs = 3
prior_means = [np.array([-1.5, 1.5, 0]), np.array([0, 0, 0])]
prior_vars = [np.eye(n_inputs) * np.array([4, 4, 4]), np.eye(n_inputs) * np.array([8, 8, 8])]

Q = np.tile(np.eye(n_inputs), (T, 1, 1))
contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}
cont_mapping = np.vectorize(num_to_contrast.get)

def recovery_test(weights, contrasts, prior_mean, prior_var, plot=False):

    print()
    print(weights)
    print(contrasts)

    true_pmf = weights_to_pmf(weights)
    # plt.plot(true_pmf)
    # plt.ylim(0, 1)
    # plt.show()
    data = []
    for _ in range(T):
        session = np.zeros((trials, 4))
        conts = np.random.choice(cont_mapping(contrasts), trials)
        session[:, 0] = np.maximum(conts, 0)
        session[:, 1] = np.abs(np.minimum(conts, 0))
        session[:, 2] = 1
        session[:, 3] = np.random.rand(trials) < 1 / (1 + np.exp(- np.sum(weights * session[:, :3], axis=1)))

        data.append(session.copy())

    # we have to flip the weights because we stupidly have a flip everywhere
    # (that is, the response coding is mixed up, but that is made up for by the pmf plotting code)
    # for c in contrasts:
    #     mask = np.logical_and(session[:, 0] == np.maximum(cont_mapping(c), 0), session[:, 1] == np.abs(np.minimum(cont_mapping(c), 0)))
    #     plt.plot(c, np.mean(session[mask, 3]), 'ko')
    # plt.ylim(0, 1)
    # plt.show()

    learn = distributions.Dynamic_GLM(n_inputs=n_inputs, T=T, prior_mean=prior_means[0], P_0=prior_vars[0], Q=Q * 0.03)
    learn2 = distributions.Dynamic_GLM(n_inputs=n_inputs, T=T, prior_mean=prior_means[1], P_0=prior_vars[1], Q=Q * 0.03)

    samples = []
    samples2 = []
    for _ in range(n_samples):
        learn.resample(data)
        learn2.resample(data)
        samples.append(learn.weights.copy())
        samples2.append(learn2.weights.copy())
    samples = np.array(samples[300:])
    samples2 = np.array(samples2[300:])

    pmfs = np.zeros((len(samples), 11))
    for i, s in enumerate(samples):
        pmfs[i] = weights_to_pmf(np.mean(s, 0))

    pmfs2 = np.zeros((len(samples2), 11))
    for i, s in enumerate(samples2):
        pmfs2[i] = weights_to_pmf(np.mean(s, 0))

    print("Total posterior error {:.4f}".format(np.sum(np.abs(pmfs[:, contrasts] - true_pmf[contrasts]))))
    print("Error dist {}".format(np.mean(pmfs[:, contrasts] - true_pmf[contrasts], 0)))
    print("Total posterior error {:.4f}".format(np.sum(np.abs(pmfs2[:, contrasts] - true_pmf[contrasts]))))
    print("Error dist {}".format(np.mean(pmfs2[:, contrasts] - true_pmf[contrasts], 0)))

    if plot:
        temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
        plt.plot(np.arange(11), pmfs.mean(axis=0), color='b')
        plt.plot(weights_to_pmf(weights), color='r')
        plt.fill_between(np.arange(11), temp[1], temp[0], alpha=0.2, color='b')
        plt.ylim(0, 1)
        plt.savefig("pmf compare")
        plt.show()

        plt.figure(figsize=(16, 9))
        for i in range(n_inputs):
            plt.subplot(n_inputs, 1, i+1)
            label = 'Truth' if i == 0 else None
            plt.plot(np.arange(T), np.ones(T) * weights[i], label=label)
            sample_mean = np.mean(samples[:, :, i], axis=0)
            label = 'Posterior mean' if i == 0 else None
            plt.plot(np.arange(T), sample_mean, label=label, c='g')
            credible_interval = np.percentile(samples[:, :, i], [2.5, 97.5], axis=0)
            plt.fill_between(np.arange(T), credible_interval[1], credible_interval[0], alpha=0.2, color='g')
            # sns.despine()
            if i == 0:
                plt.legend(fontsize=18, frameon=False)
            plt.xlim(left=0, right=T-1)
            plt.ylim(-6, 6)
        plt.tight_layout()
        plt.savefig("weights compare")
        plt.show()


pmf_weights = [np.array([0, 0, -3.8919]), np.array([0, 0, 3.8919]), np.array([0, 0, 0]), np.array([-5, 5, 0])]
for contrasts, weights in product([[0, 1, 9, 10], [0, 2, 3, 4, 5, 6, 7, 8, 10]], pmf_weights):
    recovery_test(weights, contrasts, prior_means, prior_vars, plot=True)
