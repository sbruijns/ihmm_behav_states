import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle


def plot_choices(df):
    sns.pointplot(x='signed_contrast', y='response', data=df)
    plt.ylim(0, 1)
    plt.show()


def plot_correct(df):
    correct_percent = df.groupby('signed_contrast')['feedback'].mean()

    plt.plot(correct_percent.index, correct_percent.values, 'ko')
    plt.ylim(0, 1)
    plt.show()


def plot_sessions(data):
    ms = 4
    mask = data[:, 1] == 0
    noise = np.random.rand(mask.sum()) * 0.4 - 0.2
    plt.plot(np.where(mask)[0], noise + data[mask, 0], 'o', c='b', ms=ms)
    mask = data[:, 1] == 1
    noise = np.random.rand(mask.sum()) * 0.4 - 0.2
    plt.plot(np.where(mask)[0], noise + data[mask, 0], 'o', c='k', ms=ms)
    mask = data[:, 1] == 2
    noise = np.random.rand(mask.sum()) * 0.4 - 0.2
    plt.plot(np.where(mask)[0], noise + data[mask, 0], 'o', c='r', ms=ms)

def plot_session_pmf(data):
    cont = data[:, 0]
    cs = np.unique(cont)
    accs = np.zeros(len(cs))
    for i, c in enumerate(cs):
        accs[i] = np.mean((data[:, 1] == 0)[cont == c])
    plt.ylim(bottom=0, top=1)
    plt.plot(accs)


subjects = ['CSH_ZAD_025', 'ZM_1897', 'KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06', 'SWC_023', 'CSHL051',
            'CSHL054', 'CSHL059', 'CSHL_007', 'CSHL_014', 'CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015',
            'CSH_ZAD_001', 'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011',
            'KS016', 'ibl_witten_16', 'KS019', 'CSH_ZAD_022', 'KS021', 'SWC_022', 'CSHL061', 'CSHL062'] # pre bias fits

session_count = 0
trials = 0
for subject in subjects:
    print(subject)
    info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))
    till_session = info_dict['bias_start']
    for j in range(0, till_session):
        try:
            data = pickle.load(open("./session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
        except FileNotFoundError:
            continue
        session_count += 1
        # plot_sessions(data)
        trials += data.shape[0]
        # plot_session_pmf(data)
        # plt.title("{} and bias start is {}".format(j, info_dict['bias_start']))
        # plt.show()
