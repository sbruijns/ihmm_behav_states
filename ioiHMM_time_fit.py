import numpy as np
import matplotlib.pyplot as plt
import pyhsmm
import pyhsmm.basic.distributions as distributions
import copy
import warnings
import pickle
import time
import faulthandler
faulthandler.enable()

np.set_printoptions(suppress=True)


subject = "CSHL045"
fit_type = ['pre_bias', 'bias', 'all'][0]
with_time = False
conditioned_on = ['nothing', 'reward', 'truth', 'answer'][0]

Nmax = 15
seeds = [11]
info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))

n = 1500
relevant_states = np.zeros((len(seeds), n))
likes = np.zeros((len(seeds), n))
models = []
for i, seed in enumerate(seeds):
    np.random.seed(seed)
    print(i+1)
    obs_dim = 1
    normal_params = {'mu_0': np.zeros(obs_dim),
                     'sigma_0': np.eye(obs_dim)*0.2,
                     'kappa_0': 0.3,
                     'nu_0': obs_dim+5}
    obs_hypparams = {'n_inputs': 9, 'n_outputs': 3, 'normal_hypparams': normal_params}
    dur_hypparams = dict(r_support=np.arange(1, 14), r_probs=np.ones(13)/13., alpha_0=1, beta_0=1)

    obs_distns = [distributions.Input_Categorical_Normal(**obs_hypparams) for state in range(Nmax)]
    dur_distns = [distributions.NegativeBinomialIntegerR2Duration(**dur_hypparams) for state in range(Nmax)]

    posteriormodel = pyhsmm.models.WeakLimitHDPHSMM(
            # https://math.stackexchange.com/questions/449234/vague-gamma-prior
            alpha_a_0=.5, alpha_b_0=20, # TODO: gamma vs alpha? gamma steers state number
            gamma_a_0=1, gamma_b_0=1,
            init_state_concentration=6.,
            obs_distns=obs_distns,
            dur_distns=dur_distns)

    for j in range(info_dict['n_sessions']):
        try:
            data = pickle.load(open("./session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
            print(data.shape)
        except FileNotFoundError:
            continue

        if data.shape[0] == 0:
            continue

        err = np.seterr(divide='ignore')
        data[:, 2] = np.log(np.clip(data[:, 2], 0, None))
        np.seterr(**err)

        posteriormodel.add_data(data, trunc=100)

    time_save = time.time()
    with warnings.catch_warnings(): # ignore the scipy warning
        warnings.simplefilter("ignore")
        for j in range(n):

            if j % 10 == 0:
                print(j)

            posteriormodel.resample_model()

            likes[i, j] = posteriormodel.log_likelihood()
            model_save = copy.deepcopy(posteriormodel)
            if j != n - 1:
                # To save on memory:
                model_save.delete_data()
            models.append(model_save)

            # save something in case of crash
            if j % 100 == 0:
                pickle.dump(models, open("{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), 'wb'))
    print(time.time() - time_save)


pickle.dump(models, open("{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), 'wb'))
plt.plot(likes.T)
plt.savefig("likelihoods")
plt.show()
