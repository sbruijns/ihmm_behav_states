from one.api import ONE
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

one = ONE()

# Zoes mice
# import json
# sessions = json.loads(open("./posterior_probs_for_seb/sessions.json").read())

contrast_to_num = {-1.: 0, -0.5: 1, -0.25: 2, -0.125: 3, -0.0625: 4, 0: 5, 0.0625: 6, 0.125: 7, 0.25: 8, 0.5: 9, 1.: 10}

# old:
# def get_df(eid):
#     choices, contrastL, contrastR, feedback, block, rt, gocue = one.load(eid, dataset_types=dataset_types)
#     if np.all(None == choices) or np.all(None == contrastL) or np.all(None == contrastR) or np.all(None == feedback) or np.all(None == block):
#         return None
#     d = {'response': choices, 'contrastL': contrastL, 'contrastR': contrastR, 'feedback': feedback}
#
#     df = pd.DataFrame(data=d, index=range(len(choices))).fillna(0)
#     df['feedback'] = df['feedback'].replace(-1, 0)
#     df['signed_contrast'] = df['contrastR'] - df['contrastL']
#     df['signed_contrast'] = df['signed_contrast'].map(contrast_to_num)
#     df['response'] += 1
#     df['block'] = block
#     df['rt'] = rt - gocue

dataset_types = ['choice', 'contrastLeft', 'contrastRight',
                 'feedbackType', 'probabilityLeft', 'response_times',
                 'goCue_times']
def get_df(trials):
    if np.all(None == trials['choice']) or np.all(None == trials['contrastLeft']) or np.all(None == trials['contrastRight']) or np.all(None == trials['feedbackType']) or np.all(None == trials['probabilityLeft']):  # or np.all(None == data_dict['response_times']):
        return None, None
    d = {'response': trials['choice'], 'contrastL': trials['contrastLeft'], 'contrastR': trials['contrastRight'], 'feedback': trials['feedbackType']}

    df = pd.DataFrame(data=d, index=range(len(trials['choice']))).fillna(0)
    df['feedback'] = df['feedback'].replace(-1, 0)
    df['signed_contrast'] = df['contrastR'] - df['contrastL']
    df['signed_contrast'] = df['signed_contrast'].map(contrast_to_num)
    df['response'] += 1
    df['block'] = trials['probabilityLeft']
    # df['rt'] = data_dict['response_times'] - data_dict['goCue_times']  # RTODO

    return df

misses = []
to_introduce = [2, 3, 4, 5]
subjects = ['ibl_witten_13', 'ibl_witten_17', 'ibl_witten_18',
            'ibl_witten_19', 'ibl_witten_20', 'ibl_witten_25', 'ibl_witten_26',
            'ibl_witten_27', 'ibl_witten_29', 'CSH_ZAD_001', 'CSH_ZAD_011',
            'CSH_ZAD_019', 'CSH_ZAD_022', 'CSH_ZAD_024', 'CSH_ZAD_025',
            'CSH_ZAD_026', 'CSH_ZAD_029']
subjects += ['CSHL045', 'CSHL046', 'CSHL049', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL054', 'CSHL055',
            'CSHL059', 'CSHL061', 'CSHL062', 'CSHL065', 'CSHL066']  # CSHL063 no good
subjects += ['CSHL_007', 'CSHL_014', 'CSHL_015', 'CSHL_018', 'CSHL_019', 'CSHL_020', 'CSH_ZAD_001', 'CSH_ZAD_011',
            'CSH_ZAD_017', 'CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022', 'CSH_ZAD_024', 'CSH_ZAD_025', 'CSH_ZAD_026']
subjects += ['KS002', 'KS003', 'KS005',
            'KS014', 'KS015', 'KS016', 'KS017', 'KS018', 'KS019', 'KS021', 'KS022', 'KS023', 'NYU-06']  # , 'KS020' no good
subjects += ['SWC_008',
            'SWC_009', 'SWC_018', 'SWC_021', 'SWC_022', 'SWC_023', 'ZM_1897', 'ZM_1898', 'ZM_3003', 'ibl_witten_07']
subjects += ['ibl_witten_13', 'ibl_witten_14', 'ibl_witten_15', 'ibl_witten_16', 'ibl_witten_17', 'ibl_witten_18',
            'ibl_witten_19', 'ibl_witten_20']  # downloaded already

# try still: ZM_1736, 25 sessions; SWC_060, 21 sess; ZM_1150 29 sess; DY_016 30 sess
# subjects = ['CSH_ZAD_029']  # 'DY_013']  #  35, 27, 20, 30
# 'CSHL060',  gives error
# can't find biased sessions of: 'NYU-17', 'NYU-18', 'CSHL050', 'CSHL021', 'KS013', 'SWC_026', 'SWC_031', 'ZM_2104', 'ZM_2407', 'ZM_2406', 'CSK_ZAD_013', 'CSHL048', 'CSHL056', 'CSHL057', 'CSH_ZAD_018', 'CSH_ZAD_020', 'CSH_ZAD_023', 'SWC_020', 'SWC_019'
# also: 'KS012', 'KS011', 'KS010', 'KS009', 'KS008', 'KS007', 'KS006', 'CSHL064',
# 'KS004', 'SWC_015', 'CSHL047' has none in gocue or rt
# weid problem: , 'CSHL058'
# subjects = ["CSHL_001", "CSHL_002", "CSHL_003", "CSHL_005", "CSHL_007", "CSHL_008", "CSHL_010", "CSHL_012",
#             "CSHL_014", "CSHL_015", "IBL-T1", "IBL-T2", "IBL-T3", "IBL-T4", "ibl_witten_04", "ibl_witten_05",
#             "ibl_witten_06", "ibl_witten_07", "ibl_witten_12", "ibl_witten_13", "ibl_witten_14", "ibl_witten_15",
#             "ibl_witten_16", "KS003", "KS005", "KS019", "NYU-01", "NYU-02", "NYU-04", "NYU-06", "ZM_1367", "ZM_1369",
#             "ZM_1371", "ZM_1372", "ZM_1743", "ZM_1745", "ZM_1746"]  # zoe's subjects
# subjects = ['CSHL_018']

data_folder = 'session_data'
# why does CSHL058 not work?

old_style = False
if old_style:
    print("Warning, data can have splits")
    data_folder = 'session_data_old'
bias_eids = []

print("#########################################")
print("Waring, rt's removed, find with   # RTODO")
print("#########################################")

short_subjs = []
names = []

prob_left = []
pre_bias = []
entire_training = []
for subject in subjects:
    print('_____________________')
    print(subject)

    eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2023-01-01'], details=True)

    start_times = [sess['date'] for sess in sess_info]
    protocols = [sess['task_protocol'] for sess in sess_info]
    nums = [sess['number'] for sess in sess_info]

    print("original # of eids {}".format(len(eids)))

    test = [(y, x) for y, x in sorted(zip(start_times, eids))]

    eids = [x for _, x in sorted(zip(start_times, eids))]
    dates = [x for x, _ in sorted(zip(start_times, eids))]
    nums = [x for _, x in sorted(zip(start_times, nums))]

    prev_date = None
    prev_num = -1
    fixed_dates = []
    fixed_eids = []
    additional_eids = []
    for d, e, n in zip(dates, eids, nums):
        if d != prev_date:
            fixed_dates.append(d)
            fixed_eids.append(e)
            additional_eids.append([])
        else:
            assert n > prev_num
            if n == 1:
                additional_eids.append([e])
            elif n > 1:
                additional_eids[-1].append(e)
        prev_date = d
        prev_num = n

    protocols = [x for _, x in sorted(zip(start_times, protocols))]

    # in case you want it
    if old_style:
        fixed_eids = eids
        fixed_dates = dates
        additional_eids = [[] for e in fixed_eids]
    print("remaining # of eids {}".format(len(fixed_eids)))

    performance = np.zeros(len(fixed_eids))
    easy_per = np.zeros(len(fixed_eids))
    hard_per = np.zeros(len(fixed_eids))
    bias_start = 0

    info_dict = {'subject': subject, 'dates': fixed_dates, 'eids': fixed_eids}
    contrast_set = {0, 1, 9, 10}

    rel_count = -1
    for i, (eid, extra_eids) in enumerate(zip(fixed_eids, additional_eids)):

        if rel_count == 5 or i >= 12:
            continue

        try:
            trials = one.load_object(eid, 'trials')
            if 'choice' not in trials:
                continue
        except Exception:
            print('skipped session')
            continue

        if trials['probabilityLeft'] is None: # originally also "or df is None", if this gets reintroduced, probably need to download all data again
            if rel_count != -1:
                print('lost session, problem')
                misses.append((subject, i))
            continue
        rel_count += 1

        print("rel_count {}".format(rel_count))
        prob_left.append(trials['probabilityLeft'])
        # if there are some duplicate dates:

plt.hist(np.concatenate(prob_left), bins=11)
plt.xlim(0, 1)
plt.ylabel("Occurences", fontsize=18)
plt.xlabel("Bias corrections", fontsize=18)
plt.savefig("bias corrections")
plt.show()
