import pickle
import numpy as np
import matplotlib.pyplot as plt


def fuse_states(test, cons_matrix):
    # this function takes a consistency matrix and fuses trials together into states
    # it does so by fusing trials which have high enough connection to any existing trial in the state
    state_sets = []
    accounted_for = np.zeros(test.results[0].n_datapoints)
    for i in range(test.results[0].n_datapoints):
        if accounted_for[i]:
            continue
        inds = np.where(cons_matrix[i])[0]
        old_len, new_len = 1, len(inds)
        while new_len > old_len:
            inds = np.where(np.any(cons_matrix[inds], axis=0))[0]
            old_len, new_len = new_len, len(inds)
        state_sets.append(inds)
        accounted_for[inds] = True

    new_mat = np.zeros((test.results[0].n_datapoints, test.results[0].n_datapoints))
    for i, state in enumerate(state_sets):
        new_mat[np.meshgrid(state, state)] = 1
    plt.imshow(new_mat)
    plt.savefig("tempolo")
    plt.close()

    return state_sets

class MCMC_result_list:

    def state_appearance_posterior(self, subject, save=False):
        all_state_starts = np.zeros(20)
        for res in self.results:
            session_bounds, state_starts = res.state_appearance_posterior()
            all_state_starts += state_starts
        plt.vlines(session_bounds, 0, 1, linestyles='dashed')
        plt.ylim(bottom=-0.01, top=1)
        plt.title(subject)
        if save:
            plt.savefig("temp")
        plt.close()
        return all_state_starts / len(self.results)

    def largest_states(self):
        # I think this function now exists as a more general version
        for res in self.results:
            plt.plot(res.assign_counts.max(axis=1))
        plt.show()

    def return_chains(self, func, model_for_loop, rank_norm=True, mode_indices=None):
        # Following Gelman page 284

        if mode_indices is None:
            if func.__name__ == "temp_state_glm_func":
                chains = np.zeros((self.m, self.n, 4))
            else:
                chains = np.zeros((self.m, self.n))
            if model_for_loop:
                for j, result in enumerate(self.results):
                    for i in range(self.n):
                        chains[j, i] = func(result.models[i])
            else:
                for i in range(self.m):
                    chains[i] = func(self.results[i])
        else:
            # this should probably be functioned up, exists elsewhere
            inds = []
            for lims in [range(i * self.n, (i + 1) * self.n) for i in range(self.m)]:
                inds.append([ind - lims[0] for ind in mode_indices if ind in lims])
            lens = [len(ind) for ind in inds]
            min_len = min([l for l in lens if l != 0])
            n_remaining_chains = len([l for l in lens if l != 0])
            print("{} chains left with a len of {}".format(n_remaining_chains, min_len))
            chains = np.zeros((n_remaining_chains, min_len))
            print(func.__name__)
            if func.__name__ == "temp_state_glm_func":
                chains = np.zeros((n_remaining_chains, min_len, 4))
            else:
                chains = np.zeros((n_remaining_chains, min_len))
            counter = -1
            for i, ind in enumerate(inds):
                if len(ind) == 0:
                    continue
                counter += 1
                step = len(ind) // min_len
                up_to = min_len * step
                chains[counter] = func(self.results[i], ind[:up_to:step])
        return chains

def state_appear_and_dur(m, n_sessions):
    # given a single model, find when states appear and how long they lasted
    observed_states = []
    n_sessions = len(m.stateseqs)

    state_appear = []
    session_dict = {}

    state_fraction = np.bincount(np.concatenate(m.stateseqs), minlength=15)
    state_fraction = state_fraction / state_fraction.sum()
    for i, seq in enumerate(m.stateseqs):
        sess_states = np.unique(seq)
        for s in sess_states:
            if s in observed_states:
                session_dict[s] = (session_dict[s][0], i)
                continue
            if state_fraction[s] < 0.05:
                continue
            state_appear.append(i / (n_sessions))
            observed_states.append(s)
            session_dict[s] = (i, i)

    return state_appear, [(1 + session_dict[s][1] - session_dict[s][0]) / n_sessions for s in observed_states]


class MCMC_result:

    def state_appearance_posterior(self):
        # posterior over when new states appear (ignoring the very first state)
        state_starts = np.zeros(self.n_datapoints)
        session_bounds = [0] + list(np.cumsum([len(s) for s in self.models[-1].stateseqs]))
        state_start_save = np.zeros(20)
        for m in self.models:
            observed_states = []
            state_fraction = np.bincount(np.concatenate(m.stateseqs), minlength=15)
            state_fraction = state_fraction / state_fraction.sum()
            for i, seq in enumerate(m.stateseqs):
                sess_states = np.unique(seq)
                for s in sess_states:
                    if s in observed_states:
                        continue
                    if state_fraction[s] < 0.05:
                        continue
                    # if len(observed_states) == 0:  # ignore the very first state
                    #     observed_states.append(s)
                    #     continue
                    index = np.where(seq == s)[0][0]
                    state_start_save[20 * index // len(seq)] += 1
                    state_starts[session_bounds[i] + index] += 1
                    observed_states.append(s)
        plt.plot(state_starts / self.n_samples)
        return session_bounds, state_start_save / self.n_samples

    def consistency_rsa_modified(self):
        # modification: put these consistency plots in all differenct matrices, to later dim reduce and cluster
        # resulting matrix is too big, especially when trying to apply pca later
        # analysis that Peter wanted:
        # take trial 30 and trial 3700 (say). You have 4000 iHMM samples - and an
        # assignment of each trial to a state in each sample.
        # I wanted an RSA matrix whose (30,3700) entry is the fraction of the 4000
        # samples for which those states are the same (or different).

        # currently takes entirely too long, roughly a second per sample, needs to be at least parallelised
        consistency_mat = np.zeros((self.n_samples, self.n_datapoints, self.n_datapoints), dtype=np.bool_)
        for i, m in enumerate(self.models):
            if i % 50 == 0:
                print(i)
            states = np.concatenate(m.stateseqs)
            for s in range(self.n_all_states):
                finds = np.where(states == s)[0]
                # n_finds = finds.shape[0]
                # a = np.tile(finds, n_finds)
                # b = np.repeat(finds, n_finds)
                # this next line might not work because of indexing, but generally it's the same
                consistency_mat[i, np.meshgrid(finds, finds)] = True
        return consistency_mat


def find_good_chains_unsplit_fast(chains1, chains2, chains3, chains4, reduce_to=8):
    delete_n = - reduce_to + chains1.shape[0]
    mins = np.zeros(delete_n + 1)
    n_chains = chains1.shape[0]
    chains = np.stack([chains1, chains2, chains3, chains4])

    print("Without removals: {}".format(eval_simple_r_hat(chains)))
    r_hat = eval_simple_r_hat(chains)

    mins[0] = r_hat

    l, m, n = chains.shape
    psi_dot_j = np.mean(chains, axis=2)
    s_j_squared = np.sum((chains - psi_dot_j[:, :, None]) ** 2, axis=2) / (n - 1)

    r_hat_min = 10
    sol = 0
    for x in combinations(range(n_chains), n_chains - delete_n):
        temp1 = chains[:, x]
        temp2 = psi_dot_j[:, x]
        temp3 = s_j_squared[:, x]
        r_hat = eval_amortized_r_hat(temp1, temp2, temp3, l, m - delete_n, n)
        if r_hat < r_hat_min:
            sol = x
        r_hat_min = min(r_hat, r_hat_min)
    print("Minimum is {} (removed {})".format(r_hat_min, delete_n))
    sol = [i for i in range(n_chains) if i not in sol]
    print("Removed: {}".format(sol))

    r_hat_local = eval_r_hat(np.delete(chains1, sol, axis=0), np.delete(chains2, sol, axis=0), np.delete(chains3, sol, axis=0), np.delete(chains4, sol, axis=0))
    print("Minimum over everything is {} (removed {})".format(r_hat_local, delete_n))

    return sol, r_hat_min


def find_good_chains_unsplit(chains1, chains2, chains3, chains4, reduce_to=8, simple=False):
    delete_n = - reduce_to + chains1.shape[0]
    mins = np.zeros(delete_n + 1)
    n_chains = chains1.shape[0]
    chains = np.stack([chains1, chains2, chains3, chains4])

    print("Without removals: {}".format(eval_simple_r_hat(chains)))
    if simple:
        r_hat = eval_simple_r_hat(chains)
    else:
        r_hat = eval_r_hat(chains1, chains2, chains3, chains4)
    mins[0] = r_hat

    for i in range(delete_n):
        print()
        r_hat_min = 10
        sol = 0
        for x in combinations(range(n_chains), n_chains - 1 - i):
            if simple:
                r_hat = eval_simple_r_hat(np.delete(chains, x, axis=1))
            else:
                r_hat = eval_r_hat(np.delete(chains1, x, axis=0), np.delete(chains2, x, axis=0), np.delete(chains3, x, axis=0), np.delete(chains4, x, axis=0))
            if r_hat < r_hat_min:
                sol = x
            r_hat_min = min(r_hat, r_hat_min)
        print("Minimum is {} (removed {})".format(r_hat_min, i + 1))
        sol = [i for i in range(32) if i not in sol]
        print("Removed: {}".format(sol))
        mins[i + 1] = r_hat_min

    return sol, r_hat_min


def state_glm_func_helper(t, mode_specific=False):
    if not mode_specific:
        def temp_state_glm_func(x):
            glms = np.zeros((x.n_samples, 4))
            args = np.argsort(x.assign_counts, 1)[:, -1 - t]
            for i, (m, s, n) in enumerate(zip(x.models, args, x.assign_counts[np.arange(x.assign_counts.shape[0]), args])):
                for j, seq in enumerate(m.stateseqs):
                    if s in seq:
                        glms[i] += np.sum(s == seq) * m.obs_distns[s].weights[j]
                glms[i] /= n
            return glms
    else:
        def temp_state_glm_func(x, ind):
            glms = np.zeros((x.n_samples, 4))
            np.argsort(x.assign_counts)[:-1 - n]
            for i, (m, s, n) in enumerate(zip(x.models, x.assign_counts.argmax(1), x.assign_counts.max(1))):
                for j, seq in enumerate(m.stateseqs):
                    if s in seq:
                        glms[i] += np.sum(s == seq) * m.obs_distns[s].weights[j]
                glms[i] /= n
            return glms[ind]
    return temp_state_glm_func


def params_to_pmf(params):
    return params[2] + (1 - params[2] - params[3]) / (1 + np.exp(- params[0] * (all_conts - params[1])))


def state_development_single_sample(test, indices, save=True, save_append='', show=True, dpi='figure', separate_pmf=False):

    for i, m in enumerate([item for sublist in test.results for item in sublist.models]):
        if i not in indices:
            continue
        sample = m
    pstates = np.unique(np.concatenate(sample.stateseqs))
    state_map = dict(zip(pstates, range(len(pstates))))
    states_by_session = np.zeros((len(pstates), test.results[0].n_sessions))
    for i, state_seq in enumerate(sample.stateseqs):
        states, counts = np.unique(state_seq, return_counts=True)
        for s, c in zip(states, counts):
            states_by_session[state_map[s], i] = c / len(state_seq)

    fig = plt.figure(figsize=(16, 9))
    spec = gridspec.GridSpec(ncols=100, nrows=3 * len(pstates)+5, figure=fig)
    spec.update(hspace=0.)  # set the spacing between axes.
    ax0 = fig.add_subplot(spec[:5, :79])  # performance line
    ax1 = fig.add_subplot(spec[5:, :79])  # state lines
    ax2 = fig.add_subplot(spec[5:, 86:])

    ax0.plot(1 + 0.25, 0.6, 'ko', ms=18)
    ax0.plot(1 + 0.25, 0.6, 'wo', ms=16.8)
    ax0.plot(1 + 0.25, 0.6, 'ko', ms=16.8, alpha=abs(num_to_cont[0]))

    ax0.plot(1 + 0.25, 0.6 + 0.2, 'ko', ms=18)
    ax0.plot(1 + 0.25, 0.6 + 0.2, 'wo', ms=16.8)
    ax0.plot(1 + 0.25, 0.6 + 0.2, 'ko', ms=16.8, alpha=abs(num_to_cont[1]))
    if test.results[0].type != 'bias' and test.results[0].type != 'all':
        current, counter = 0, 0
        for c in [2, 3, 4, 5]:
            if test.results[0].infos[c] == current:
                counter += 1
            else:
                counter = 0
            ax0.axvline(test.results[0].infos[c] + 1, color='gray', zorder=0)
            ax1.axvline(test.results[0].infos[c] + 1, color='gray', zorder=0)
            ax0.plot(test.results[0].infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'ko', ms=18)
            ax0.plot(test.results[0].infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'wo', ms=16.8)
            ax0.plot(test.results[0].infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
            current = test.results[0].infos[c]
    # if test.results[0].type == 'all' or test.results[0].type == 'prebias_plus':
    #     ax1.axvline(test.results[0].infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
    #     ax0.axvline(test.results[0].infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
    #     ax0.annotate('Bias', (test.results[0].infos['bias_start'] + 1 - 0.5, 0.68), fontsize=22)

    all_pmfs = []
    cmaps = ['Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds', 'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu']
    np.random.seed(8)
    np.random.shuffle(cmaps)

    for state in pstates:
        cmap = matplotlib.cm.get_cmap(cmaps[state_map[state]]) if state_map[state] < len(cmaps) else matplotlib.cm.get_cmap('Greys')

        session_js, pmfs = [], []
        for i, state_seq in enumerate(sample.stateseqs):
            if state in state_seq:
                session_js.append(i)
                pmfs.append(weights_to_pmf(sample.obs_distns[state].weights[i]))
                session_max = i

        defined_points = np.zeros(test.results[0].n_contrasts, dtype=bool)
        defined_points[test.results[0].session_contrasts[session_max]] = True

        n_points = 150
        points = np.linspace(1, test.results[0].n_sessions, n_points)
        interpolation = np.interp(points, np.arange(1, 1 + test.results[0].n_sessions), states_by_session[state_map[state]])

        for k in range(n_points-1):
            ax1.fill_between([points[k], points[k+1]],
                             state_map[state] - 0.5, [state_map[state] + interpolation[k] - 0.5, state_map[state] + interpolation[k+1] - 0.5], color=cmap(0.3 + 0.7 * k / n_points))

        alpha_level = 0.3
        ax2.axvline(0.5, c='grey', alpha=alpha_level, zorder=4)

        if test.results[0].type == 'bias':
            defined_points = np.ones(test.results[0].n_contrasts, dtype=bool)
        # else:
        #     if self.active_during_bias[self.state_map[s]]:
        #         defined_points = np.ones(test.results[0].n_contrasts, dtype=bool)
        #     else:
        #         defined_points = np.zeros(test.results[0].n_contrasts, dtype=bool)
        #         defined_points[[0, 1, -2, -1]] = True
        if separate_pmf:

            for j, pmf in zip(session_js, pmfs):
                ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmf[defined_points] - 0.5 + state_map[state], color=cmap(0.2 + 0.8 * j / test.results[0].n_sessions))
                ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmf[defined_points] - 0.5 + state_map[state], ls='', ms=7, marker='*', color=cmap(0.2 + 0.8 * j / test.results[0].n_sessions))
            all_pmfs.append((defined_points, [pmf[defined_points] for pmf in pmfs]))
        else:
            temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
            ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + state_map[state], color=state_color)
            ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + state_map[state], ls='', ms=7, marker='*', color=state_color)
            ax2.fill_between(np.where(defined_points)[0] / (len(defined_points)-1), temp[1, defined_points] - 0.5 + state_map[state], temp[0, defined_points] - 0.5 + state_map[state], alpha=0.2, color=state_color)
            all_pmfs.append((defined_points, pmfs[:, defined_points].mean(axis=0)))

        ax2.axhline(state_map[state] + 0.5, c='k')
        ax2.axhline(state_map[state], c='grey', alpha=alpha_level, zorder=4)
        ax1.axhline(state_map[state] + 0.5, c='grey', alpha=alpha_level, zorder=4)

    if not test.results[0].name.startswith('Sim_'):
        perf = np.zeros(test.results[0].n_sessions)
        found_files = 0
        counter = test.results[0].infos['bias_start'] - 1 if test.results[0].type == 'bias' else -1
        while found_files < test.results[0].n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(test.results[0].name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback[:, 1])
            # assert feedback.shape[0] >= self.full_posteriors[found_files].shape[1]
            found_files += 1
        ax0.axhline(-0.5, c='k')
        ax0.axhline(0.5, c='k')
        # print(perf)
        ax0.fill_between(range(1, 1 + test.results[0].n_sessions), perf - 0.5, -0.5, color='k')

    ax2.set_title('Psychometric\nfunction', size=16)

    ax1.set_ylabel('Proportion of trials', size=28)
    ax0.set_ylabel('% correct', size=18)
    ax2.set_ylabel('Probability', size=26)
    ax1.set_xlabel('Session', size=28)
    ax2.set_xlabel('Contrast', size=26)
    ax1.set_xlim(left=1, right=test.results[0].n_sessions)
    ax0.set_xlim(left=1, right=test.results[0].n_sessions)
    ax2.set_xlim(left=0, right=1)
    ax1.set_ylim(bottom=-0.5, top=len(pstates) - 0.5)
    ax0.set_ylim(bottom=-0.5)
    ax0.spines['top'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax2.set_ylim(bottom=-0.5, top=len(pstates) - 0.5)
    y_pos = (np.tile([0, 0.5], (len(pstates), 1)) + np.arange(len(pstates))[:, None]).flatten()
    ax1.set_yticks(y_pos)
    ax1.set_yticklabels(list(np.tile([0.5, 1], len(pstates))))
    ax0.set_yticks([0, 0.5])
    ax0.set_yticklabels([0.5, 1], size=fs)
    ax0.set_xticks([])
    y_pos = np.linspace(-0.5, len(pstates) - 0.5, 2 * len(pstates) + 1)
    ax2.set_yticks(y_pos)
    ax2.set_yticklabels([0] + list(np.tile([0.5, 1], len(pstates))), size=fs)

    # red session marker
    # ax1.axvline(12, color='red', zorder=0)
    # ax0.axvline(12, color='red', zorder=0)

    ax1.tick_params(axis='both', labelsize=fs)
    ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax2.set_xticks([0, 0.5, 1])
    ax2.set_xticklabels([-1, 0, 1], size=fs)

    plt.tight_layout()
    if save:
        print("saving with {} dpi".format(dpi))
        plt.savefig("dynamic_GLM_figures/meta state development single sample_{}_{}{}.png".format(test.results[0].name, separate_pmf, save_append), dpi=dpi)
    if show:
        plt.show()
    else:
        plt.close()

    return states_by_session, all_pmfs


def four_param_loss(params, pmf, offset=False):
    # if params[2] + params[3] > 1:
    #     return 10
    contrast_bools = len_to_bools[len(pmf)]
    fit = params_to_pmf(params)[contrast_bools]
    return np.sum((pmf - fit) ** 2) + offset * (np.abs(params[1]) * 0.0001 + np.clip(np.abs(params[0] - 1), 0, 0.35) * 0.0002)


def four_param_pmf(pmf, s1=1, s2=0, offset=False):
    res = minimize(lambda x: four_param_loss(x, pmf, offset=offset), method='Nelder-Mead', x0=np.array([s1, s2, pmf[0], pmf[-1]]))
    # can give [s1, s2, pmf.min(), 1 - pmf.max()] as starting points, but gives weird error
    return res

def compare_performance(cnas, contrasts=(1, 1), title=""):
    # compare the performance on a certain contrast
    # cnas is set of data for all sessions (from contrasts_plot)
    # contrasts is a tuple specifying the contrast of interest (first or second column, contrast strength)
    plt.figure(figsize=(16*0.75, 9*0.75))
    for i in range(len(cnas) - 1):
        if cnas[i][cnas[i][:, contrasts[0]] == contrasts[1], -1].shape[0] == 0 or cnas[i+1][cnas[i+1][:, contrasts[0]] == contrasts[1], -1].shape[0] == 0:
            continue
        perf1, perf2 = np.mean(cnas[i][cnas[i][:, contrasts[0]] == contrasts[1], -1]), np.mean(cnas[i+1][cnas[i+1][:, contrasts[0]] == contrasts[1], -1])
        p = two_sample_binom_test(cnas[i][cnas[i][:, contrasts[0]] == contrasts[1], -1], cnas[i+1][cnas[i+1][:, contrasts[0]] == contrasts[1], -1])[1]
        factor = (perf1 > perf2) * 2 - 1
        plt.annotate(xy=(i + 0.5, (perf1 + perf2) / 2 + factor * 0.025), text=("{:.3f}".format(p))[1:])
        color = 'red' if p < 0.05 else 'blue'
        plt.plot([i, i+1], [perf1, perf2], color=color)
    sns.despine()
    cont_string = "Contrast {} ".format('right' if contrasts[0] else 'left')
    plt.title(cont_string + str(contrasts[1]))
    plt.ylim(-0.07, 1.07)
    plt.tight_layout()
    if title != "":
        plt.savefig(title)
    plt.show()

def compare_params(test, session, state_sets, indices, states2compare, title=""):
    """
       Take a set of states, and plot a histogram of their regression parameters for a specific session.
       See how different they really are.

       Unfinished

       Takes states_by_session and all_pmfs as input from state_development
    """
    colors = ['blue', 'orange', 'green', 'black', 'red']
    assert len(states2compare) <= len(colors)
    # subtract 1 to get internal numbering
    states2compare = [s - 1 for s in states2compare]
    # transform desired states into the actual numbering, before ordering by bias
    states2compare = [key for key in test.state_mapping.keys() if test.state_mapping[key] in states2compare]

    for state, trials in enumerate(state_sets):
        if state not in states2compare:
            continue
        session_js, pmfs, pmf_weights = state_weights(test, trials, indices)
        return pmf_weights
    plt.tight_layout()
    plt.show()


def compare_weights(test, state_sets, indices, states2compare, states_by_session, title=""):
    """
        Take a set of states, and plot out their weights on all sessions on which they occur.
        See how different they really are.
        Similar to compare_pmfs

        difference to compare_params?
        Takes states_by_session as input from state_development
    """
    colors = ['blue', 'orange', 'green', 'black', 'red']
    assert len(states2compare) <= len(colors)
    # subtract 1 to get internal numbering
    states2compare = [s - 1 for s in states2compare]
    # transform desired states into the actual numbering, before ordering by bias
    states2compare = [key for key in test.state_mapping.keys() if test.state_mapping[key] in states2compare]

    sessions = np.where(states_by_session[states2compare].sum(0))[0]

    state_counter = -1
    for state, trials in enumerate(state_sets):
        if state not in states2compare:
            continue
        state_counter += 1
        _, weights = state_weights(test, trials, indices)
        counter = 0
        for j, session in enumerate(sessions):
            plt.subplot(1, len(sessions), j + 1)
            if state == 0:
                plt.title(session)
            if states_by_session[state, session] > 0:
                plt.plot(weights[counter], c=colors[state_counter])
                counter += 1
            plt.ylim(-6, 6)
            if j != 0:
                plt.gca().set_yticks([])
    # plt.tight_layout()
    if title != "":
        plt.savefig(title)
    plt.show()

def compare_pmfs(test, states2compare, states_by_session, all_pmfs, title=""):
    """
       Take a set of states, and plot out their PMFs on all sessions on which they occur.
       See how different they really are.

       Takes states_by_session and all_pmfs as input from state_development
    """
    colors = ['blue', 'orange', 'green', 'black', 'red']
    assert len(states2compare) <= len(colors)
    # subtract 1 to get internal numbering
    states2compare = [s - 1 for s in states2compare]
    # transform desired states into the actual numbering, before ordering by bias
    states2compare = [key for key in test.state_mapping.keys() if test.state_mapping[key] in states2compare]

    sessions = np.where(states_by_session[states2compare].sum(0))[0]

    for i, state in enumerate(states2compare):
        counter = 0
        for j, session in enumerate(sessions):
            plt.subplot(1, len(sessions), j + 1)
            if i == 0:
                plt.title(session)
            if states_by_session[state, session] > 0:
                plt.plot(np.where(all_pmfs[state][0])[0], (all_pmfs[state][1][counter])[all_pmfs[state][0]], c=colors[i])
                counter += 1
            plt.ylim(0, 1)
            if j != 0:
                plt.gca().set_yticks([])
    # plt.tight_layout()
    if title != "":
        plt.savefig(title)
    plt.show()

def state_cluster_interpolation(states, pmfs):
    #
    # Used to contain a first_type_count variable, which seemed to just count the number of states?
    pmf_examples = [[], [], []]
    state_trans = np.zeros((3, 3))
    state_types = np.zeros((3, states.shape[1]))
    for state, pmf in zip(states, pmfs):
        sessions = np.where(state)[0]
        for i, sess_pmf in enumerate(pmf[1]):
            if i == 0:
                state_type = pmf_type(sess_pmf[pmf[0]])
            if i > 0 and state_type != pmf_type(sess_pmf[pmf[0]]):
                state_trans[state_type, pmf_type(sess_pmf[pmf[0]])] += 1
                state_type = pmf_type(sess_pmf[pmf[0]])
            pmf_examples[pmf_type(sess_pmf[pmf[0]])].append((pmf[0], sess_pmf[pmf[0]]))
            state_types[pmf_type(sess_pmf[pmf[0]]), sessions[i]] += state[sessions[i]]

    return state_types, state_trans, pmf_examples

def bias_flips(states_by_session, pmfs, durs):
    state_counter = {}
    prev_bias = 0
    bias_flips = 0

    for sess in range(states_by_session.shape[1]):
        states = np.where(states_by_session[:, sess])[0]

        for s in states:
            if s not in state_counter:
                state_counter[s] = -1
            state_counter[s] += 1

            if s == np.argmax(states_by_session[:, sess]):
                bias = np.mean(pmfs[s][1][state_counter[s]][pmfs[s][0]])
                if bias < 0.45:
                    if prev_bias == 1:
                        print("sess {}".format(sess))
                        bias_flips += 1
                    prev_bias = -1
                elif bias > 0.55:
                    if prev_bias == -1:
                        print("sess {}".format(sess))
                        bias_flips += 1
                    prev_bias = 1
    return bias_flips

def plot_pmf_types(pmf_types, subject, fit_type, save=True, show=False):
    # Plot the different types of PMFs, all split up by their different types
    for i, pmfs in enumerate(pmf_types):
        plt.subplot(1, 3, i + 1)
        plt.plot([0, 11], [0.5, 0.5], 'grey', alpha=1/3)
        for def_points, pmf in pmfs:
            plt.plot(np.where(def_points)[0], pmf)
        plt.ylim(0, 1)
    plt.tight_layout()
    if save:
        plt.savefig("dynamic_GLM_figures/pmf_types_{}_{}.png".format(subject, fit_type))
    if show:
        plt.show()
    else:
        plt.close()

def lapse_sides(test, state_sets, indices):
    """Compute and plot a lapse differential across sessions.

    Takes a single mouse and plots (1 - lapse_left) - lapse_right across sessions, with sessions boundaries shown."""

    def func_init(): return {'lapse_side': np.zeros(test.results[0].n_datapoints) + 10, 'session_bounds': []}

    def first_for(test, results):
        results['pmf'] = np.zeros(test.results[0].n_contrasts)

    def second_for(m, j, counter, session_trials, trial_counter, results):
        states, counts = np.unique(m.stateseqs[j][session_trials - trial_counter], return_counts=True)
        for sub_state, c in zip(states, counts):
            results['pmf'] += weights_to_pmf(m.obs_distns[sub_state].weights[j]) * c / session_trials.shape[0]

    def end_first_for(results, indices, j, trial_counter, session_trials):
        pmf = results['pmf'] / len(indices)
        results['lapse_side'][session_trials] = (1 - pmf[0]) - pmf[-1]
        results['session_bounds'].append(trial_counter)

    session_bounds = set()
    lapse_side = np.zeros(test.results[0].n_datapoints)
    for trials in state_sets:
        results = control_flow(test, indices, trials, func_init, first_for, second_for, end_first_for)
        lapse_side[results['lapse_side'] != 10] = results['lapse_side'][results['lapse_side'] != 10]
        for sb in results['session_bounds']:
            session_bounds.add(sb)
    plt.vlines(list(session_bounds), -1, 1, linestyles='dashed', color='k', alpha=1/3)
    plt.axhline(0, color='grey')
    plt.plot(lapse_side)
    plt.savefig("cont lapse {}".format(test.results[0].name))
    plt.close()

if __name__ == "__main__":

    subjects = list(loading_info.keys())

    if 'analysis of transforming pmf into 4 param version and then using those':
        all_the_pmfs = pickle.load(open("multi_chain_saves/all_the_pmfs.p", 'rb'))
        err = 0
        flatness = 0
        incorr_counter = 0
        offset_errs = []
        non_offset_errs = []
        short_pmfs = []
        below_one = []
        above_one = []
        for pmf in all_the_pmfs:
            if len(pmf) not in len_to_bools:
                incorr_counter += 1
                continue
            res_offset = four_param_pmf(pmf, offset=True)
            offset_errs.append(res_offset.fun)
            res_non_offset = four_param_pmf(pmf)
            non_offset_errs.append(res_non_offset.fun)

            short_pmfs.append(res_offset.x)

            # if 1 - res_offset.x[2] - res_offset.x[3] < -0.1:
            #     quit()

            if np.abs(pmf[-1] - pmf[0]) < 0.15:
                if np.abs(1 - res_offset.x[0]) > 0.01:
                    plt.plot(pmf)
                    above_one.append(np.abs(pmf[-1] - pmf[0]))
                    if np.abs(pmf[-1] - pmf[0]) < 0.03:
                        weird_pmf = pmf
                else:
                    below_one.append(np.abs(pmf[-1] - pmf[0]))

            if np.abs(1 - res_offset.x[0]) < 0.01 and np.abs(pmf[-1] - pmf[0]) > flatness:
                flatness = np.abs(pmf[-1] - pmf[0])
                flattest_pmf = pmf
                flat_params_off = res_offset.x
                flat_params_nonoff = res_non_offset.x

            if np.abs(res_offset.fun - res_non_offset.fun) > err:
                err = np.abs(res_offset.fun - res_non_offset.fun)
                worst_pmf = pmf
                worst_params_off = res_offset.x
                worst_params_nonoff = res_non_offset.x
        print(incorr_counter)
        plt.savefig("slope=1 pmfs")
        plt.show()

        plt.plot(worst_pmf, label='pmfs')
        plt.plot(params_to_pmf(worst_params_off)[len_to_bools[len(worst_pmf)]], label='offset')
        plt.plot(params_to_pmf(worst_params_nonoff)[len_to_bools[len(worst_pmf)]], label='non-offset')
        plt.ylim(0, 1)
        plt.legend()
        plt.title(four_param_loss(worst_params_off, worst_pmf))
        plt.show()

        short_pmfs = np.array(short_pmfs)

        for pmf in short_pmfs:
            if np.abs(1 - pmf[0]) < 0.01:
                plt.plot(params_to_pmf(pmf))
        plt.savefig("slope=1 pmfs")
        plt.show()

        dim = 3

        function_range = 1 - short_pmfs[:, 2] - short_pmfs[:, 3]

        xy = np.vstack([short_pmfs[:, 0], short_pmfs[:, 2], short_pmfs[:, 3]])
        from scipy.stats import gaussian_kde
        z = gaussian_kde(xy)(xy)
        plt.figure(figsize=(24, 24 / 3))

        plt.subplot(1, 3, 1)
        plt.scatter(short_pmfs[:, 0], short_pmfs[:, 2], c=z)
        plt.xlabel("Slope")
        plt.ylabel("Lapse bottom")

        plt.subplot(1, 3, 2)
        plt.scatter(short_pmfs[:, 0], short_pmfs[:, 3], c=z)
        plt.xlabel("Slope")
        plt.ylabel("Lapse top")

        plt.subplot(1, 3, 3)
        plt.scatter(short_pmfs[:, 2], short_pmfs[:, 3], c=z)
        plt.xlabel("Lapse bottom")
        plt.ylabel("Lapse top")

        plt.colorbar()
        plt.tight_layout()
        plt.savefig("pmf fit scatter")
        plt.show()

        # New things
        xy = np.vstack([short_pmfs[:, 0], function_range, short_pmfs[:, 1]])
        z = gaussian_kde(xy)(xy)
        plt.figure(figsize=(24, 24 / 3))

        plt.subplot(1, 3, 1)
        plt.scatter(short_pmfs[:, 0], function_range, c=z)
        plt.xlabel("Slope")
        plt.ylabel("PMF range")

        plt.subplot(1, 3, 2)
        plt.scatter(short_pmfs[:, 0], short_pmfs[:, 1], c=z)
        plt.xlabel("Slope")
        plt.ylabel("Location")

        plt.subplot(1, 3, 3)
        plt.scatter(function_range, short_pmfs[:, 1], c=z)
        plt.xlabel("PMF range")
        plt.ylabel("Location")

        plt.colorbar()
        plt.tight_layout()
        plt.savefig("pmf fit scatter (lapse combined)")
        plt.show()

    for subject in subjects:

        test = pickle.load(open("multi_chain_saves/canonical_result_{}{}.p".format(subject, fit_type_string), 'rb'))

        # old type of PCA analysis
        temp = test.state_pca(subject, pca_vecs='glm_weights', dim=dim)

        # b_flips = bias_flips(states, pmfs, durs)
        # all_bias_flips.append(b_flips)

        # lapse differential
        # lapse_sides(test, [s for s in state_sets if len(s) > 40], mode_indices)

        # temp = contrasts_plot(test, [s for s in state_sets if len(s) > 40], dpi=300, subject=subject, save=False, show=False, consistencies=consistencies, CMF=False)
        # mice_exponentials.append(temp)

        # state comparisons, to check how different states are across sessions I think
        # a = compare_params(test, 26, [s for s in state_sets if len(s) > 40], mode_indices, [3, 5])
        # compare_pmfs(test, [3, 2, 4], states, pmfs, title="{} convergence pmf".format(subject))
        # compare_pmfs(test, [s for s in state_sets if len(s) > 40], mode_indices, [4, 5], states, pmfs, title="{} convergence pmf".format(subject))
        # compare_weights(test, [s for s in state_sets if len(s) > 40], mode_indices, [4, 5], states, title="{} convergence weights".format(subject))

        # single sample analysis
        # state_development_single_sample(test, [mode_indices[0]], show=True, separate_pmf=True, save=False)

        # xy, z = pickle.load(open("multi_chain_saves/xyz_{}.p".format(subject), 'rb'))
        #
        # single_sample = [np.argmax(z)]
        # for position, index in enumerate(np.where(np.logical_and(z > 2.7e-7, xy[0] < -500))[0]):
        #     print(position, index, index // test.n, index % test.n)
        #     states, pmfs = state_development_single_sample(test, [index], save_append='_{}_{}'.format('single_sample', position), show=True, separate_pmf=True)
        #     if position == 9:
        #         quit()

        # print("Deprecated")
        # all_state_starts = test.state_appearance_posterior(subject)
        # pop_state_starts += all_state_starts

        # a, b = np.where(states)
        # for i in range(states.shape[0]):
        #     state_appear.append(b[a == i][0] / (test.results[0].n_sessions - 1))
        #     state_dur.append(b[a == i].shape[0])

        # compute state type proportions and split the pmfs accordingly
        # ret, trans, pmf_types = state_cluster_interpolation(states, pmfs)
        # state_trans += trans  # matrix of how often the different types transition into one another
        # for i, r in enumerate(ret):
        #     state_trajs[i] += np.interp(np.linspace(1, test.results[0].n_sessions, n_points), np.arange(1, 1 + test.results[0].n_sessions), r)  # for interpolated population state trajectories
        # plot_pmf_types(pmf_types, subject=subject, fit_type=fit_type)
        # continue
        # plt.plot(ret.T, label=[0, 1, 2])
        # plt.legend()
        # plt.show()

        # all_state_starts = test.state_appearance_posterior(subject)
        # plt.plot(all_state_starts)
        # plt.savefig("temp")
        # plt.close()