import numpy as np
import matplotlib.pyplot as plt
from pybasicbayes.distributions.dynamic_multinomial import Dynamic_Input_Categorical


test = Dynamic_Input_Categorical(n_inputs=9, n_outputs=3, T=10, jumplimit=2)


input_a = [[[1, 2], [0, 0], [8, 2]],
           [],
           [],
           [[4, 1], [2, 2], [7, 0]],
           [],
           [],
           [],
           [],
           [],
           [[0, 2], [1, 2], [1, 2]]]

data1 = np.zeros((9, 3))
data1[1, 2] = 1
data1[0, 0] = 1
data1[8, 2] = 1

data2 = np.zeros((9, 3))
data2[4, 1] = 1
data2[2, 2] = 1
data2[7, 0] = 1

data3 = np.zeros((9, 3))
data3[0, 2] = 1
data3[1, 2] = 2

output_a1 = [data1, data2, data3]
output_a2 = [0, 3, 9]


data, times = test._get_statistics(input_a)
assert np.array_equal(data, output_a1)
assert times == output_a2

test.resample(input_a)

# TODO: test case where first few timepoints have no data
