"""Check whether our code stays internally consistent and time it to look for improvements"""
import numpy as np
import pyhsmm
import pyhsmm.basic.distributions as distributions
import copy
import warnings
import pickle
import time
import faulthandler
faulthandler.enable()

np.set_printoptions(suppress=True)

contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}


subjects = ['CSH_ZAD_022']
fit_type = 'prebias'
fit_variance = 0.002

seed = 177 # seed is 177

Nmax = 15
n = 15

for subject in subjects:

    save_title = "./consistency_data/data"
    print(save_title)
    np.random.seed(seed)

    info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))
    # Determine session numbers
    if fit_type == 'prebias':
        till_session = info_dict['bias_start']
    elif fit_type == 'bias' or fit_type == 'all':
        till_session = info_dict['n_sessions']
    elif fit_type == 'prebias_plus':
        till_session = min(info_dict['bias_start'] + 6, info_dict['n_sessions'])  # 6 here will actually turn into 7 later

    from_session = info_dict['bias_start'] if fit_type == 'bias' else 0

    models = []
    n_regressors = 5
    T = till_session - from_session + (fit_type != 'prebias')
    obs_hypparams = {'n_regressors': n_regressors, 'T': T, 'prior_mean': np.zeros(n_regressors), 'jumplimit': 3,
                     'P_0': 2 * np.eye(n_regressors), 'Q': fit_variance * np.tile(np.eye(n_regressors), (T, 1, 1))}
    dur_hypparams = dict(r_support=np.array([1, 2, 3, 5, 7, 10, 15, 21, 28, 36, 45, 55, 150]),
                         r_probs=np.ones(13)/13., alpha_0=1, beta_0=1)

    obs_distns = [distributions.Dynamic_GLM(**obs_hypparams) for state in range(Nmax)]
    dur_distns = [distributions.NegativeBinomialIntegerR2Duration(**dur_hypparams) for state in range(Nmax)]

    posteriormodel = pyhsmm.models.WeakLimitHDPHSMM(
            # https://math.stackexchange.com/questions/449234/vague-gamma-prior
            alpha_a_0=.5, alpha_b_0=.5,  # TODO: gamma vs alpha? gamma steers state number
            gamma_a_0=1., gamma_b_0=1,
            init_state_concentration=6.,
            obs_distns=obs_distns,
            dur_distns=dur_distns,
            var_prior=fit_variance)

    print(from_session, till_session + (fit_type != 'prebias'))
    for j in range(from_session, till_session + (fit_type != 'prebias')):
        data = pickle.load(open("./session_data/{}_fit_info_{}.p".format(subject, j), "rb"))

        data[:, 0] = np.vectorize(num_to_contrast.get)(data[:, 0])

        # previous answer
        prev_ans = data[:, 1].copy()
        prev_ans[1:] = prev_ans[:-1] - 1

        # previous reward
        side_info = pickle.load(open("./session_data/{}_side_info_{}.p".format(subject, j), "rb"))
        prev_reward = side_info[:, 1]
        prev_reward[1:] = prev_reward[:-1]

        bad_trials = data[:, 1] == 1
        bad_trials[0] = True
        mega_data = np.empty((np.sum(~bad_trials), n_regressors + 1))

        mega_data[:, 0] = np.maximum(data[~bad_trials, 0], 0)
        mega_data[:, 1] = np.abs(np.minimum(data[~bad_trials, 0], 0))
        mega_data[:, 2] = prev_ans[~bad_trials]
        mega_data[:, 3] = prev_reward[~bad_trials]
        mega_data[:, 4] = 1
        mega_data[:, 5] = data[~bad_trials, 1] - 1
        mega_data[:, 5] = (mega_data[:, 5] + 1) / 2
        print(mega_data.sum(0))
        posteriormodel.add_data(mega_data)

    import pyhsmm.util.profiling as prof
    from pyhsmm.internals.hsmm_states import sample_forwards_log_mypy
    prof_func = prof._prof(posteriormodel.resample_model)
    prof._prof.add_function(posteriormodel.states_list[0].resample)
    prof._prof.add_function(posteriormodel.states_list[0].messages_backwards)
    prof._prof.add_function(posteriormodel.states_list[0].sample_forwards)
    prof._prof.add_function(posteriormodel.obs_distns[0].log_likelihood)
    prof._prof.add_function(posteriormodel.obs_distns[0].resample)
    prof._prof.add_function(sample_forwards_log_mypy)

    time_save = time.time()
    with warnings.catch_warnings():  # ignore the scipy warning
        warnings.simplefilter("ignore")
        for j in range(n):

            prof_func()

            # for idx, od in enumerate(posteriormodel.obs_distns):
            #     if idx in [5, 8]:
            #         print(idx)
            #         print(od.weights)

            model_save = copy.deepcopy(posteriormodel)
            if j != n - 1 and j != 0:
                # To save on memory:
                model_save.delete_data()
            models.append(model_save)

    print(time.time() - time_save)

    # pickle.dump(models, open(save_title, 'wb'))

prev_res = pickle.load(open(save_title, 'rb'))

counter = 0
for p, m in zip(prev_res, models):
    print(counter)
    counter += 1
    for od, nd in zip(p.obs_distns, m.obs_distns):
        assert np.allclose(od.weights, nd.weights)
prof._prof.print_stats()
print('consistent')
