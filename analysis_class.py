import multiprocessing as mp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pyhsmm
import pyhsmm.basic.distributions as distributions
import pickle
from itertools import permutations, product
import seaborn as sns
from scipy.stats import nbinom
import sys
from matplotlib.patches import Rectangle
from scipy.special import logsumexp
import pyhsmm.util.profiling as prof
import time


colors = np.genfromtxt('colors.csv', delimiter=',')

# Todo: fix !!!'s
# use self.models[-1] not self.models[0]

np.set_printoptions(suppress=True)

fs = 16
# !!!
n_contrasts = 11
# !!!
num_to_cont = dict(zip(range(11), [-1., -0.5, -.25, -.125, -.062, 0., .062, .125, .25, 0.5, 1.]))

all_cont_ticks = (np.arange(11), [-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])
bias_cont_ticks = (np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
conts = np.array([-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])

cmap = cm.get_cmap('coolwarm')


def pmf_acc(x, w):
    """
    Given probabilities of answering right x, and relative contrast frequencies w, compute accuracy.

    A contrast is assumed to be equiprobable right or left (no bias here).
    But e.g. 0 contrast can appear more frequently than rest.
    """
    n = len(x)
    x[:int(n / 2)] = 1 - x[:int(n / 2)]
    x[int(n / 2)] = 0.5
    return np.sum(x * (w / w.sum()))


class MCMC_result:

    def __init__(self, models, name, infos, sessions, threshold=1000, seq_start=0, conditioning='nothing'):

        # TODO: get infos from info dict not manually

        self.models = models
        self.name = name
        self.seq_start = seq_start
        self.conditioning = conditioning
        self.type = sessions
        self.assign_counts = None  # Placeholder
        self.infos = infos

        # TODO: I only check whether states have more than threshold trials assigned for the first sample
        flat_list = [item for sublist in self.models[0].stateseqs for item in sublist]
        a = np.unique(flat_list, return_counts=1)
        print(a)
        states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]
        self.proto_states = states[counts > threshold]

        self.n_samples = len(self.models)
        self.n_sessions = len(self.models[-1].stateseqs)
        self.n_datapoints = sum([len(s) for s in self.models[-1].stateseqs])
        self.n_all_states = len(self.models[-1].obs_distns)
        self.threshold = threshold

        self.calc_full_state_posterior()
        total_post = np.zeros(self.n_all_states)
        for i, post in enumerate(self.full_posteriors):
            total_post += post.sum(axis=1)
        self.proto_states = np.where(total_post / self.n_datapoints > 0.02)[0]
        self.n_pstates = len(self.proto_states)
        print(self.proto_states)

        self.state_to_color = {}

        if sessions == 'bias':
            self.n_contrasts = 9
            self.cont_ticks = bias_cont_ticks
        else:
            self.n_contrasts = 11
            self.cont_ticks = all_cont_ticks

        tendencies = []
        for s in self.proto_states:
            temp = 1 - np.sum(self.models[0].obs_distns[s].weights[:, 0]) / (self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing'))
            # self.state_to_color[s] = cmap(temp)
            self.state_to_color[s] = colors[int(temp * 101)]
            tendencies.append(temp)

        ordered_states = [x for _, x in sorted(zip(tendencies, self.proto_states))]
        self.state_map = dict(zip(ordered_states, range(self.n_pstates)))
        self.count_assigns()
        self.calc_state_posterior()

        #     if type == 'conditioned':
        #         temp = np.sum(self.models[0].obs_distns[s].weights[:9, 0] - 0.5)
        #     elif type == 'rt':
        #         temp = np.sum(self.models[0].obs_distns[s].cats.weights[:, 0] - 0.5)
        #     else:
        #         temp = np.sum(self.models[0].obs_distns[s].weights[:, 0] - 0.5)

    def matrix(self, i):
        """Return transition matrix reduced to important states."""
        temp = self.models[i].trans_distn.trans_matrix[self.proto_states]
        temp = temp[:, self.proto_states]
        return temp

    def param_hist(self, param_func):
        """Histogram of whatever parameters for all samples, pick out parameter with lambda func."""
        for s in self.proto_states:
            params = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                params[i] = param_func(m, s)
            plt.hist(params)
            plt.title(s, color=self.state_to_color[s])
            plt.show()

    def duration_overview(self):
        """Multiple plots giving information about posterior of duration distributions."""
        for s in self.proto_states:
            r_params = np.zeros(self.n_samples)
            p_params = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                r_params[i] = m.dur_distns[s].r
                p_params[i] = m.dur_distns[s].p

            plt.hist(r_params)
            plt.title(s, color=self.state_to_color[s])
            plt.show()

            plt.scatter(r_params, p_params)
            plt.show()

            plt.plot(r_params, label='r')
            plt.plot(p_params, label='p')
            plt.plot(r_params * p_params / (1 - p_params), label='mean')
            plt.legend()
            plt.show()

    def transition_hists(self):
        """One histogram for each possible (important) transition probability for all samples."""
        for t in permutations(self.proto_states, 2):
            transitions = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                transitions[i] = m.trans_distn.trans_matrix[t]
            plt.hist(transitions)
            plt.xlim(left=0, right=1)
            plt.title(t)
            plt.show()

    def count_assigns(self):
        self.assign_counts = np.zeros((self.n_samples, self.n_pstates))
        self.total_counts = np.zeros(self.n_samples)
        for i, m in enumerate(self.models):
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a = np.unique(flat_list, return_counts=1)
            states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]

            self.total_counts[i] = counts.sum()  # this is only model dependent if I threshold...
            for s, c in zip(states, counts):
                if s in self.proto_states:
                    self.assign_counts[i, self.state_map[s]] = c

    def percent_counts(self):
        counts = np.zeros(self.models[0].num_states)
        trial_n = self.total_counts[0]

        for m in self.models:
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a, temp_counts = np.unique(flat_list, return_counts=1)
            counts[a] += temp_counts

        self.percent = counts / self.n_samples / trial_n

    def total_assign_evolution(self):
        for i in range(self.n_samples):
            print(self.assign_counts[i].sum() / self.total_counts[i])

    def assign_hist(self):
        for i in range(self.n_pstates):
            plt.hist(self.assign_counts[:, i])
        # plt.savefig("figures/convergence/count hists {}".format(fit))
        plt.show()

    def assign_evolution(self, show=True):
        plt.figure(figsize=(11, 8))
        for s in self.proto_states:
            # !!! self.state_map[s] was previously i, bad mistake
            plt.plot(self.assign_counts[:, self.state_map[s]], c=self.state_to_color[s])

        plt.xlabel('Iteration', size=22)
        plt.ylabel('# assigned trials', size=22)
        plt.xticks(size=20-3)
        sns.despine()
        plt.tight_layout()
        plt.savefig("figures/convergence/count evolution {} {} {}".format(self.name, self.type, self.conditioning))
        if show:
            plt.show()
        else:
            plt.close()

    def block_yielder(self, n):
        """Generator for block data."""
        # !!! make loading of all possible if n is None
        file_add = 0
        for seq_num in range(n):
            found_file = False
            while not found_file or file_add > 1000:
                try:
                    add_on = self.infos['bias_start'] if self.type == 'bias' else 0
                    blocks = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, seq_num + file_add + add_on), "rb"))[:, 0]
                    #print(self.infos['eids'][seq_num + file_add + add_on])

                except FileNotFoundError:
                    file_add += 1
                    continue
                found_file = True

            yield blocks

    def feedback_yielder(self, n):
        """Generator for feedback data."""
        # !!! make loading of all possible if n is None
        file_add = 0
        for seq_num in range(n):
            found_file = False
            while not found_file or file_add > 1000:
                try:
                    add_on = self.infos['bias_start'] if self.type == 'bias' else 0
                    feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, seq_num + file_add + add_on), "rb"))[:, 1]
                    #print(self.infos['eids'][seq_num + file_add + add_on])

                except FileNotFoundError:
                    file_add += 1
                    continue
                found_file = True

            yield feedback

    def sequence_posteriors(self, until=None, save=False, show=False, plot_blocks=False):
        n = self.n_sessions if until is None else until
        for seq_num, blocks in zip(range(n), self.block_yielder(n)):
            # try:
            #     feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, seq_num + self.seq_start), "rb"))
            #     wMode, std = pickle.load(open("./session_data/psytrack/result {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
            #     trialnumbers = pickle.load(open("./session_data/psytrack/numbers {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))


            if len(blocks) < 140:
                continue

            plt.figure(figsize=(11, 6))
            for s in self.proto_states:
                plt.plot(self.posteriors[seq_num][self.state_map[s]] / self.n_samples, color=self.state_to_color[s], label=s)
            assert len(blocks) == len(self.models[-1].stateseqs[seq_num])

            # windowsize = 7
            # performance = np.zeros(len(feedback) - 2 * windowsize + 1)
            # for i, pos in enumerate(np.arange(windowsize, len(feedback) - windowsize + 1)):
            #     performance[i] = np.mean(feedback[pos - windowsize: pos])
            # plt.plot(np.arange(windowsize, len(feedback) - windowsize + 1) - windowsize / 2, performance, 'k', lw=3)

            # wMode[0] -= np.min(wMode[0])
            # wmax = np.max(wMode[0])
            # plt.plot(trialnumbers, wMode[0] / wmax, color='k', alpha=0.5)

            self.plot_block_background(blocks)

            plt.title("{}, session #{}".format(self.name, 1 + seq_num + self.seq_start), size=22)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(State)', size=22)
            plt.xlabel('Trial', size=22)

            plt.xlim(left=0, right=500)
            if save:
                plt.savefig("figures/state_seqs/posterior seq {} {}".format(self.name, seq_num + self.seq_start))
            if show:
                plt.show()
            else:
                plt.show()


    def calc_state_posterior(self):
        """Calculate posterior over states by averaging over samples"""
        posteriors = []
        for seq_num in range(self.n_sessions):
            posterior = np.zeros((self.n_pstates, len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in self.proto_states:
                    posterior[self.state_map[s]] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / self.n_samples)
        self.posteriors = posteriors

    def calc_full_state_posterior(self):
        """Calculate posterior over ALL states by averaging over samples"""
        posteriors = []
        for seq_num in range(self.n_sessions):
            posterior = np.zeros((self.n_all_states, len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in range(self.n_all_states):
                    posterior[s] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / self.n_samples)
        self.full_posteriors = posteriors

    # !!! TODO
    def make_this_func(self):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples

    def contrasts_plot(self, until=None, save=False, show=False, plot_blocks=False):
        n = self.n_sessions if until is None else min(until, self.n_sessions)
        contrasts_and_answers = self.models[-1].datas
        for seq_num, blocks, feedback in zip(range(n), self.block_yielder(n), self.feedback_yielder(n)):
            c_n_a = contrasts_and_answers[seq_num]
            # if 1+seq_num + self.seq_start != 14:
            #     continue

            plt.figure(figsize=(11, 6))
            for s in self.proto_states:
                plt.plot((self.n_contrasts - 1) * self.posteriors[seq_num][self.state_map[s]], color=self.state_to_color[s], label=s, lw=4)
            # plt.plot(posterior.sum(axis=0) / self.n_samples, 'k')
            assert len(blocks) == len(self.models[-1].stateseqs[seq_num])

            # points = []
            # choice_average = []
            # internal_count = 0
            # window_length = 4
            # window = np.zeros(window_length)
            # for i, ca in enumerate(c_n_a):
            #     if ca[0] == 4:
            #         points.append(i)
            #         window[internal_count % window_length] = ca[1] == 2
            #         choice_average.append((self.n_contrasts - 1) * np.mean(window))
            #         internal_count += 1
            # plt.plot(points, choice_average, 'k')

            # windowsize = 7
            # performance = np.zeros(len(feedback) - 2 * windowsize + 1)
            # for i, pos in enumerate(np.arange(windowsize, len(feedback) - windowsize + 1)):
            #     performance[i] = np.mean(feedback[pos - windowsize: pos])
            # plt.plot(np.arange(windowsize, len(feedback) - windowsize + 1), (self.n_contrasts - 1) * performance, 'k')
            if plot_blocks or self.type == 'bias':
                self.plot_block_background(blocks)

            ms = 4
            noise = np.random.rand(len(feedback)) * 0.4 - 0.2
            mask = c_n_a[:, 1] == 0
            mask_reward = feedback == 1
            mask_unrew = feedback == 0
            plt.plot(np.where(np.logical_and(mask, mask_reward))[0]+0.15, noise[np.logical_and(mask, mask_reward)] + c_n_a[np.logical_and(mask, mask_reward), 0] % self.n_contrasts, 'o', c='g', ms=ms+1, marker='+')
            plt.plot(np.where(np.logical_and(mask, mask_unrew))[0]+0.15, noise[np.logical_and(mask, mask_unrew)] + c_n_a[np.logical_and(mask, mask_unrew), 0] % self.n_contrasts, 'o', c='k', ms=ms+1, marker='x')
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] % self.n_contrasts, 'o', c='b', ms=ms)
            mask = c_n_a[:, 1] == 1
            plt.plot(np.where(np.logical_and(mask, mask_reward))[0]+0.15, noise[np.logical_and(mask, mask_reward)] + c_n_a[np.logical_and(mask, mask_reward), 0] % self.n_contrasts, 'o', c='g', ms=ms+1, marker='+')
            plt.plot(np.where(np.logical_and(mask, mask_unrew))[0]+0.15, noise[np.logical_and(mask, mask_unrew)] + c_n_a[np.logical_and(mask, mask_unrew), 0] % self.n_contrasts, 'o', c='k', ms=ms+1, marker='x')
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] % self.n_contrasts, 'o', c='k', ms=ms)
            mask = c_n_a[:, 1] == 2
            plt.plot(np.where(np.logical_and(mask, mask_reward))[0]+0.15, noise[np.logical_and(mask, mask_reward)] + c_n_a[np.logical_and(mask, mask_reward), 0] % self.n_contrasts, 'o', c='g', ms=ms+1, marker='+')
            plt.plot(np.where(np.logical_and(mask, mask_unrew))[0]+0.15, noise[np.logical_and(mask, mask_unrew)] + c_n_a[np.logical_and(mask, mask_unrew), 0] % self.n_contrasts, 'o', c='k', ms=ms+1, marker='x')
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] % self.n_contrasts, 'o', c='r', ms=ms)

            sns.despine()

            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=22)
            plt.yticks(*self.cont_ticks, size=22-2)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(State) and contrast', size=22)
            plt.xlabel('Trial', size=22)
            plt.tight_layout()
            #plt.xlim(left=399, right=445)
            if save:
                plt.savefig("figures/all posterior and contrasts {} {} {}, sess {}.png".format(self.name, self.type, self.conditioning, seq_num))
            if show:
                plt.show()
            else:
                plt.close()


    def sequence_heatmap(self, normalization='adaptive', until=None):
        n = self.n_sessions if until is None else until
        for seq_num in range(n):
            for s1, s2 in product(self.proto_states, repeat=2):
                heatmap = np.zeros((len(self.models[0].stateseqs[seq_num]), len(self.models[0].stateseqs[seq_num])))
                normalize_vector = np.zeros(len(self.models[0].stateseqs[seq_num]), dtype=np.int32)
                for m in self.models:
                    for t in range(len(m.stateseqs[seq_num])):
                        if m.stateseqs[seq_num][t] == s1:
                            normalize_vector[t] += 1
                            heatmap[t] += m.stateseqs[seq_num] == s2

                if normalization == 'adaptive':
                    mask = normalize_vector.nonzero()
                    heatmap[mask] = heatmap[mask] / normalize_vector[mask, None]
                    if s1 == s2:
                        heatmap -= np.diag(normalize_vector >= 1)  # empty diagonal if we look at autocorr
                    heatmap += np.diag(normalize_vector / self.n_samples)
                elif normalization == 'total':
                    heatmap /= self.n_samples

                assert heatmap.max() <= 1.
                assert heatmap.min() >= 0.
                sns.heatmap(heatmap, vmin=0., vmax=1., square=True)
                title = "Sequence {}, states {} and {}".format(seq_num, s1, s2)
                plt.title(title)
                plt.savefig("figures/state_heatmaps/" + normalization + '_' + title)
                plt.close()

    def single_state_resample(self, n):
        model = self.models[-1]
        states = [np.zeros((self.n_pstates, x.shape[0])) for x in model.datas]

        for i in range(n):
            model.resample_states()
            for j, seq in enumerate(model.stateseqs):
                for s in self.proto_states:
                    states[j][self.state_map[s]] += seq == s

        for seq_num, blocks in zip(range(n), self.block_yielder(n)):  # how does n work here?

            self.plot_block_background(blocks)

            seq = states[seq_num]
            for s in self.proto_states:
                plt.plot(seq[self.state_map[s]] / n, color=self.state_to_color[s], label=s)

            sns.despine()
            plt.legend()
            plt.show()

    def plot_block_background(self, blocks):
        alpha = 0.1

        start = 0
        curr = 0.5
        block_to_color = {0.2: 'b', 0.8: 'r', 0.5: 'w'}
        try:
            for i, b in enumerate(blocks):
                if b != curr:
                    if curr != 0:
                        plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
                    curr = b
                    start = i
            plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
        except KeyError as ke:
            print(ke)

    def state_development(self, save=True, show=True):
        if self.type == 'bias':
            self.state_del_bias(save, show)
        else:
            self.state_del_train(save, show)

    def state_del_train(self, save, show):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples
        plt.figure(figsize=(16, 9))

        current, counter = 0, 0
        for c in [2, 3, 4, 5]:
            if self.infos[c] == current:
                counter += 1
            else:
                counter = 0
            plt.axvline(self.infos[c], color='gray', zorder=0)
            plt.plot(self.infos[c] - 0.28, self.n_pstates + 0.68 + counter * 0.28, 'ko', ms=18)
            plt.plot(self.infos[c] - 0.28, self.n_pstates + 0.68 + counter * 0.28, 'wo', ms=16.8)
            plt.plot(self.infos[c] - 0.28, self.n_pstates + 0.68 + counter * 0.28, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
            current = self.infos[c]

        for s in self.proto_states:
            plt.fill_between(range(1, 1 + self.n_sessions), self.state_map[s] - states_by_session[self.state_map[s]] / 2,
                             self.state_map[s] + states_by_session[self.state_map[s]] / 2, color=self.state_to_color[s])

            pmfs = np.zeros((len(self.models), 11 + 11 * (self.conditioning != 'nothing')))
            dur_max = 120
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[:, 0]
                posterior[:, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)

            alpha_level = 0.3
            plt.plot([self.n_sessions + 1]*2, [-0.5, self.n_pstates - 0.5], c='k')
            plt.plot([self.n_sessions]*2, [-0.5, self.n_pstates + 0.5], c='k')
            plt.plot([self.n_sessions + 0.5]*2, [-0.5, self.n_pstates - 0.5], c='grey', alpha=alpha_level, zorder=4)
            if self.conditioning == 'nothing':
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                defined_points[[0, 1, -2, -1]] = True
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if (~defined_points).sum() > 0:
                    gap = 0.02
                    plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                total_max = temp.max()
                plt.plot(points / dur_max + self.n_sessions + 1, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(points / dur_max + self.n_sessions + 1, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if defined_points.sum() > 0:
                    plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                plt.plot([self.n_sessions, self.n_sessions+2], [self.state_map[s] + 0.5] * 2, c='k')
                plt.plot([self.n_sessions, self.n_sessions+1], [self.state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)
            else:
                temp = np.percentile(pmfs[:, :11], [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                defined_points[[0, 1, -2, -1]] = True
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, 11:], [2.5, 97.5], axis=0)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, 11 + np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='-.')

                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if (~defined_points).sum() > 0:
                    gap = 0.02
                    plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                total_max = temp.max()
                plt.plot(points / dur_max + self.n_sessions + 1, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(points / dur_max + self.n_sessions + 1, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if defined_points.sum() > 0:
                    plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                plt.plot([self.n_sessions, self.n_sessions+2], [self.state_map[s] + 0.5] * 2, c='k')
                plt.plot([self.n_sessions, self.n_sessions+1], [self.state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)

        perf = np.zeros(self.n_sessions)
        found_files = 0
        counter = -1
        while found_files < self.n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback)
            # no assertions that file is correct
            found_files += 1

        plt.annotate('PMF', (self.n_sessions + 0.5, self.n_pstates - 0.45), size=12, ha='center')
        plt.annotate('Duration', (self.n_sessions + 1.5, self.n_pstates - 0.45), size=12, ha='center')
        plt.axhline(self.n_pstates + 0.5, c='k')
        plt.axhline(self.n_pstates - 0.5, c='k')
        plt.fill_between(range(1, 1 + self.n_sessions), self.n_pstates + perf / 2,
                         self.n_pstates - perf / 2, color='k')

        sns.despine()

        plt.title("{}".format(self.name), size=22, loc='left')
        plt.title("Sessions: {}, Conditioning: {}".format(self.type, self.conditioning), size=22)
        plt.yticks(range(self.n_pstates + 1), list(range(self.n_pstates)) + ['Acc.'])
        step_size = max(int(self.n_sessions / 7), 1)
        plt.xticks(list(range(0, self.n_sessions, step_size)) + [self.n_sessions+0.5, self.n_sessions+1.5], list(range(0, self.n_sessions, step_size)) + [str(0), str(int(dur_max / 2))])
        plt.xticks(size=fs-1)
        plt.yticks(size=fs-2)
        plt.ylabel('Proportion of trials', size=22)
        plt.xlabel('Session', size=22)

        plt.xlim(left=1, right=self.n_sessions + 2)
        plt.ylim(bottom=-0.5)

        plt.tight_layout()

        # if save:
        plt.savefig("figures/convergence/states_over_session {} {} {}".format(self.name, self.type, self.conditioning))
        if show:
            plt.show()
        else:
            plt.close()


    def state_del_bias(self, save, show):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples
        plt.figure(figsize=(16, 9))

        for s in self.proto_states:
            plt.fill_between(range(1, 1 + self.n_sessions), self.state_map[s] - states_by_session[self.state_map[s]] / 2,
                             self.state_map[s] + states_by_session[self.state_map[s]] / 2, color=self.state_to_color[s])

            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            dur_max = 120
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[:, 0]
                posterior[:, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)

            alpha_level = 0.3
            plt.plot([self.n_sessions + 1]*2, [-0.5, self.n_pstates - 0.5], c='k')
            plt.plot([self.n_sessions]*2, [-0.5, self.n_pstates + 0.5], c='k')
            plt.plot([self.n_sessions + 0.5]*2, [-0.5, self.n_pstates - 0.5], c='grey', alpha=alpha_level, zorder=4)
            if self.conditioning == 'nothing':
                # !!! remove defined_points
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                defined_points = np.ones(self.n_contrasts, dtype=bool)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                total_max = temp.max()
                plt.plot(points / dur_max + self.n_sessions + 1, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(points / dur_max + self.n_sessions + 1, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                plt.plot([self.n_sessions, self.n_sessions+2], [self.state_map[s] + 0.5] * 2, c='k')
                plt.plot([self.n_sessions, self.n_sessions+1], [self.state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)
            else:
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                defined_points = np.ones(self.n_contrasts, dtype=bool)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, self.n_contrasts + np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='-.')

                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                total_max = temp.max()
                plt.plot(points / dur_max + self.n_sessions + 1, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(points / dur_max + self.n_sessions + 1, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if defined_points.sum() > 0:
                    plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                plt.plot([self.n_sessions, self.n_sessions+2], [self.state_map[s] + 0.5] * 2, c='k')
                plt.plot([self.n_sessions, self.n_sessions+1], [self.state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)

        perf = np.zeros(self.n_sessions)
        found_files = 0
        counter = self.infos['bias_start'] - 1
        while found_files < self.n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback)
            # no assertions that file is correct
            found_files += 1

        plt.annotate('PMF', (self.n_sessions + 0.5, self.n_pstates - 0.45), size=12, ha='center')
        plt.annotate('Duration', (self.n_sessions + 1.5, self.n_pstates - 0.45), size=12, ha='center')
        plt.axhline(self.n_pstates + 0.5, c='k')
        plt.axhline(self.n_pstates - 0.5, c='k')
        plt.fill_between(range(1, 1 + self.n_sessions), self.n_pstates + perf / 2,
                         self.n_pstates - perf / 2, color='k')

        sns.despine()

        plt.title("{}".format(self.name), size=22, loc='left')
        plt.title("Sessions: {}, Conditioning: {}".format(self.type, self.conditioning), size=22)
        plt.yticks(range(self.n_pstates + 1), list(range(self.n_pstates)) + ['Acc.'])
        step_size = max(int(self.n_sessions / 7), 1)
        plt.xticks(list(range(0, self.n_sessions, step_size)) + [self.n_sessions+0.5, self.n_sessions+1.5], list(range(0, self.n_sessions, step_size)) + [str(0), str(int(dur_max / 2))])
        plt.xticks(size=fs-1)
        plt.yticks(size=fs-2)
        plt.ylabel('Porportion of trials', size=22)
        plt.xlabel('Session', size=22)

        plt.xlim(left=1, right=self.n_sessions + 2)
        plt.ylim(bottom=-0.5)

        plt.tight_layout()

        # if save:
        plt.savefig("figures/convergence/states_over_session {} {} {}".format(self.name, self.type, self.conditioning))
        if show:
            plt.show()
        else:
            plt.close()

    def block_alignment(self, show=False):
        # throws errors when data is missing, e.g. ibl_witten_19
        n = self.n_sessions
        states_by_session = np.zeros((2, self.n_pstates, self.n_sessions))
        for seq_num, blocks in zip(range(n), self.block_yielder(n)):

            if not (0.2 in blocks and 0.8 in blocks):
                continue
            for m in self.models:
                seq = m.stateseqs[seq_num]
                for s in self.proto_states:
                    b1 = blocks == 0.2
                    states_by_session[0, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b1)) / np.sum(b1)
                    b2 = blocks == 0.8
                    states_by_session[1, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b2)) / np.sum(b2)
        states_by_session /= self.n_samples

        perf = np.zeros(self.n_sessions)
        for i in range(self.n_sessions):
            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                continue
            perf[i] = np.mean(feedback)

        plt.figure(figsize=(14, 8))
        plt.suptitle("{}".format(self.name), size=fs)

        plt.subplot(211)
        plt.title('Right block', size=22)
        #plt.plot(perf)
        for s in self.proto_states:
            plt.plot(states_by_session[0, self.state_map[s]], color=self.state_to_color[s], label=s)

        sns.despine()
        plt.yticks(size=fs-2)
        plt.xticks([], size=fs-3)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.subplot(212)
        plt.title('Left block', size=22)
        for s in self.proto_states:
            plt.plot(states_by_session[1, self.state_map[s]], color=self.state_to_color[s])

        sns.despine()
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('Porportion of trials', size=22)
        plt.xlabel('Session', size=22)

        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        #plt.savefig("figures/convergence/Block alginment {}".format(self.name))
        if show:
            plt.show()
        else:
            plt.close()

    def block_performance(self):
        c_n_a = self.models[-1].datas
        block_performance = np.zeros((2, self.n_sessions))
        for seq_num in range(self.n_sessions):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(
                                     self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue
            if len(blocks) < 140:
                continue
            data = c_n_a[seq_num]
            block_performance[0, seq_num] = np.mean(data[np.logical_and(blocks == 1, data[:, 0] == 4)][:, 1] == 0)
            block_performance[1, seq_num] = np.mean(data[np.logical_and(blocks == -1, data[:, 0] == 4)][:, 1] == 0)

        plt.figure(figsize=(11, 8))
        plt.plot(block_performance[0], 'b', label='Right block')
        plt.plot(block_performance[1], 'r', label='Left block')
        plt.plot([0, self.n_sessions], [0.2, 0.2], 'r')
        plt.plot([0, self.n_sessions], [0.8, 0.8], 'b')
        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('P(answer right | Contrast = 0)', size=22)
        plt.xlabel('Session', size=22)

        plt.legend(fontsize=fs)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout()
        plt.savefig("figures/convergence/0 accuracy in blocks {}".format(self.name))
        plt.close()

    # !!!
    def pmf_posterior(self):
        plt.figure(figsize=(11, 8))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[:, 0]

            if self.conditioning == 'nothing':
                plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
            else:
                plt.plot(pmfs[:, :self.n_contrasts].mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                #plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])

                plt.plot(pmfs[:, self.n_contrasts:].mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                #plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
        plt.title("{}, {} sessions".format(self.name, len(self.models[0].stateseqs)), size=22)
        plt.xticks(*self.cont_ticks, size=22-3)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=10)
        sns.despine()

        plt.tight_layout()
        plt.savefig("figures/convergence/pmfs with conf {} {} {}".format(self.name, self.type, self.conditioning))
        plt.show()

    # !!!
    def single_pmf_posterior(self):
        for s in self.proto_states:
            plt.figure(figsize=(9, 8))
            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[:, 0]

            if self.conditioning == 'nothing':
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                plt.plot(range(defined_points.sum()), pmfs[:, defined_points].mean(axis=0), color=self.state_to_color[s])
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])
            else:
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                plt.plot(range(defined_points.sum()), pmfs[:, np.where(defined_points)[0]].mean(axis=0), color=self.state_to_color[s], label='previous right')
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                plt.plot(range(defined_points.sum()), pmfs[:, self.n_contrasts + np.where(defined_points)[0]].mean(axis=0), color=self.state_to_color[s], label='previous left', ls='-.')
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])
            plt.title("{}, state {}".format(self.name, self.state_map[s]), size=22)
            plt.xticks(range(defined_points.sum()), conts[np.where(defined_points)[0]], size=22-3)
            plt.yticks(size=22-2)
            plt.xlabel('Contrast', size=22)
            plt.ylabel('P(answer rightward)', size=22)
            plt.ylim(bottom=0, top=1)
            plt.xlim(left=0, right=defined_points.sum() - 1)
            plt.legend()
            sns.despine()

            plt.tight_layout()
            plt.savefig("figures/convergence/single pmf with conf {} {} {} state {}".format(self.name, self.type, self.conditioning, self.state_map[s]))
            plt.show()

    def switchiness(self):
        """Stupid function for seeing how often the posterior switches states."""
        sess_switch = np.zeros(len(self.posteriors))
        sess_acc = np.zeros(len(self.posteriors))
        sess_acc_0 = np.zeros(len(self.posteriors))
        # sess_norm_switch = np.zeros(len(self.posteriors))

        for i, p in enumerate(self.posteriors):
            _, certain_states = np.where((p > 0.6).T)

            switches = 0
            current_state = -1
            counter = 0

            for s in certain_states:
                if s == current_state:
                    counter += 1
                else:
                    switches += counter > 5
                    current_state = s
                    counter = 1

            sess_switch[i] = switches / p.shape[1]

            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                print("warning {} {}".format(self.name, i))
                continue
            print(feedback.shape)
            print(p.shape[1])
            assert len(feedback) == p.shape[1]
            sess_acc[i] = np.mean(feedback)
            sess_acc_0[i] = np.mean(feedback[self.models[-1].datas[i][:, 0] == 4])
            # sess_norm_switch[i] = switches / np.sum(np.diff(blocks) != 0)

        return sess_switch, sess_acc, sess_acc_0

    def duration_posterior(self):
        for s in self.proto_states:
            plt.figure(figsize=(11, 8))
            params = np.zeros((2, self.n_samples))
            for i, m in enumerate(self.models):
                params[0, i] = m.dur_distns[s].r
                params[1, i] = m.dur_distns[s].p

            dur_max = 150
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i in range(self.n_samples):
                posterior[:, i] = nbinom.pmf(points, params[0, i], 1 - params[1, i])
            plt.plot(points+1, posterior.mean(axis=1), color=self.state_to_color[s], label=s)
            temp = np.percentile(posterior, [2.5, 97.5], axis=1)
            plt.fill_between(points+1, temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])

            plt.title("State {}".format(self.state_map[s]), size=22)
            plt.xlabel('Trial duration', size=22)
            plt.ylabel('Probability', size=22)
            plt.xticks(size=20-3)
            plt.yticks(size=20-4)
            plt.xlim(left=1, right=dur_max + 1)
            plt.ylim(bottom=0)
            sns.despine()
            plt.tight_layout()

            plt.savefig("figures/convergence/duration dist {} {} {} state {}".format(self.name, self.type, self.conditioning, self.state_map[s]))
            plt.show()

    def waic(self):
        data = self.models[-1].datas
        size = int(self.n_samples / 5)
        arg_list = [(self.models[i*size:(i+1)*size], data, self.n_sessions) for i in range(5)]
        pool = mp.Pool(5)
        log_pred_dens = pool.starmap(posterior_helper, arg_list)
        pool.close()
        log_pred_dens = np.concatenate(log_pred_dens, axis=0)
        assert (log_pred_dens == 0).sum() == 0

        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)
        pickle.dump(log_pred_dens, open("WAIC_{}_{}_condition_{}.p".format(self.name, self.type, self.conditioning), 'wb'))
        return log_pred_dens

    def waic_one_core(self):
        # predictive density
        log_pred_dens = np.zeros((self.n_samples, self.n_sessions))
        data = self.models[-1].datas

        for i, m in enumerate(self.models):
            if i % 50 == 0:
                print(i)
            for j, (s, d) in enumerate(zip(m.states_list, data)):
                s.data = d
                log_pred_dens[i, j] = s.log_likelihood()

        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)
        pickle.dump(log_pred_dens, open("WAIC_{}_{}_condition_{}_2.p".format(self.name, self.type, self.conditioning), 'wb'))
        return log_pred_dens

    def load_waic(self):
        log_pred_dens = pickle.load(open("WAIC_{}_{}_condition_{}.p".format(self.name, self.type, self.conditioning), 'rb'))
        assert (log_pred_dens == 0).sum() == 0
        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)

        return(clppd - cpwaic2)

    def generate_data(self):
        # need to subtract confounds from contrasts (from models.datas)
        np.random.seed(4)
        fake_data = []
        state_data = []
        for contrasts, sseq in zip(self.models[-1].datas, self.models[-1].stateseqs):
            c_n_a, states = self.models[-1].my_generate(contrasts[:, 0], np.unique(sseq))
            fake_data.append(c_n_a)
            state_data.append(states)

        for i, fd in enumerate(fake_data):
            print(np.unique(fd[:, 0]))
            pickle.dump(fd, open("./session_data/{}_recovery_info_{}.p".format(self.name, i), 'wb'))

        self.truth_diagram(state_data)
        return c_n_a

    def truth_diagram(self, state_data):
        flat_list = [item for sublist in state_data for item in sublist]
        a = np.unique(flat_list, return_counts=1)
        states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]
        proto_states = states[counts > self.threshold]
        n_pstates = len(proto_states)
        tendencies = []
        state_to_color = {}
        model = self.models[-1]
        for s in proto_states:
            temp = 1 - np.sum(model.obs_distns[s].weights[:, 0]) / (self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing'))
            state_to_color[s] = colors[int(temp * 101)]
            tendencies.append(temp)

        ordered_states = [x for _, x in sorted(zip(tendencies, proto_states))]
        state_map = dict(zip(ordered_states, range(n_pstates)))

        states_by_session = np.zeros((n_pstates, self.n_sessions))
        for i, seq in enumerate(state_data):
            for s in proto_states:
                states_by_session[state_map[s], i] += np.sum(seq == s) / len(seq)
        plt.figure(figsize=(16, 9))

        current, counter = 0, 0
        for c in [2, 3, 4, 5]:
            if self.infos[c] == current:
                counter += 1
            else:
                counter = 0
            plt.axvline(self.infos[c], color='gray', zorder=0)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'ko', ms=18)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'wo', ms=16.8)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
            current = self.infos[c]

        for s in proto_states:
            plt.fill_between(range(1, 1 + self.n_sessions), state_map[s] - states_by_session[state_map[s]] / 2,
                             state_map[s] + states_by_session[state_map[s]] / 2, color=state_to_color[s])

            pmfs = np.zeros((1, 11 + 11 * (self.conditioning != 'nothing')))
            dur_max = 120
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, 1))
            pmfs = model.obs_distns[s].weights[:, 0]
            posterior = nbinom.pmf(points, model.dur_distns[s].r, 1 - model.dur_distns[s].p)

            alpha_level = 0.3
            plt.plot([self.n_sessions + 1]*2, [-0.5, n_pstates - 0.5], c='k')
            plt.plot([self.n_sessions]*2, [-0.5, n_pstates + 0.5], c='k')
            plt.plot([self.n_sessions + 0.5]*2, [-0.5, n_pstates - 0.5], c='grey', alpha=alpha_level, zorder=4)
            #plt.annotate(str(abs(num_to_cont[c])), ((self.infos[c] - 0.75 + 0.55 * counter) / self.n_sessions, 0.9), color='black', xycoords='figure fraction', size=16)
            if self.conditioning == 'nothing':
                defined_points = np.ones(11, dtype=np.bool)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[defined_points] - 0.5 + state_map[s], color=state_to_color[s])

                total_max = posterior.max()
                plt.plot(points / dur_max + self.n_sessions + 1, posterior / total_max - 0.5 + state_map[s], color=state_to_color[s])

                plt.plot([self.n_sessions, self.n_sessions+2], [state_map[s] + 0.5] * 2, c='k')
                plt.plot([self.n_sessions, self.n_sessions+1], [state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)

        perf = np.zeros(self.n_sessions)
        found_files = 0
        counter = -1
        while found_files < self.n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback)
            # no assertions that file is correct
            found_files += 1

        plt.annotate('PMF', (self.n_sessions + 0.5, n_pstates - 0.45), size=12, ha='center')
        plt.annotate('Duration', (self.n_sessions + 1.5, n_pstates - 0.45), size=12, ha='center')
        plt.axhline(n_pstates + 0.5, c='k')
        plt.axhline(n_pstates - 0.5, c='k')
        plt.fill_between(range(1, 1 + self.n_sessions), n_pstates + perf / 2,
                         n_pstates - perf / 2, color='k')

        sns.despine()
        plt.title("{}".format(self.name), size=22, loc='left')
        plt.title("Sessions: {}, Conditioning: {} recovery truth".format(self.type, self.conditioning), size=22)
        plt.yticks(range(n_pstates + 1), list(range(n_pstates)) + ['Acc.'])
        step_size = max(int(self.n_sessions / 7), 1)
        plt.xticks(list(range(0, self.n_sessions, step_size)) + [self.n_sessions+0.5, self.n_sessions+1.5], list(range(0, self.n_sessions, step_size)) + [str(0), str(int(dur_max / 2))])
        plt.xticks(size=fs-1)
        plt.yticks(size=fs-2)
        plt.ylabel('Proportion of trials', size=22)
        plt.xlabel('Session', size=22)
        plt.xlim(left=1, right=self.n_sessions + 2)
        plt.ylim(bottom=-0.5)
        plt.tight_layout()

        plt.savefig("figures/convergence/recovery_truth_{} {} {}".format(self.name, self.type, self.conditioning))
        plt.show()


    def pmfs(self, states):
        for s in states:
            plt.plot(self.models[-1].obs_distns[s].weights[11:, 0])
            plt.plot(self.models[-1].obs_distns[s].weights[:11, 0], '--')
            plt.title(s)
            plt.show()

    def session_entropy(self):
        entropies = np.zeros(self.n_sessions)
        for i, ful_post in enumerate(self.full_posteriors):
            p = ful_post.sum(1) / ful_post.shape[1]
            zero_mask = p != 0.
            entropies[i] = - np.sum(p * np.log(p, where=zero_mask))
        return entropies

    def state_session_pref(self, time_slots=15):
        times = np.zeros((self.n_all_states, time_slots))
        for post in self.full_posteriors:
            trials = post.shape[1]

            slot_sizes = np.ones(time_slots, dtype=np.int16) * int(trials / time_slots)
            slot_sizes[:int(trials - (slot_sizes[1] * time_slots))] += 1  # add enough so as to fill whole session

            so_far = 0
            for i, step in enumerate(slot_sizes):  # could maybe be solved with cumsum, but it sucks
                times[:, i] += post[:, so_far:so_far + step].sum(axis=1)
                so_far += step

        for i in range(self.n_all_states):
            if times[i].sum() < 20:
                continue
            label = "PState {}".format(self.state_map[i]) if i in self.proto_states else i
            line_style = '--' if i in self.proto_states else '-'
            plt.plot(times[i] / times[i].sum(), label=label, ls=line_style)
        plt.legend()
        plt.show()

    def state_correlations(self):
        state_activity = np.zeros((self.n_sessions, self.n_pstates))
        for i, post in enumerate(self.posteriors):
            state_activity[i] = post.sum(axis=1)
        correlations = np.corrcoef(state_activity.T)
        sns.heatmap(correlations, square=True, vmin=-1, vmax=1)
        print(correlations[np.triu_indices(correlations.shape[1], 1)])
        plt.show()

        return correlations[np.triu_indices(correlations.shape[1], 1)]

    def thresholded_state_correlations(self):
        state_activity = np.zeros((self.n_sessions, self.n_pstates))
        for i, post in enumerate(self.posteriors):
            state_activity[i] = post.mean(axis=1) > 0.05
        state_activity = state_activity[:, state_activity.sum(0) != 0]
        correlations = np.corrcoef(state_activity.T)
        #sns.heatmap(correlations, square=True, vmin=-1, vmax=1)
        print(correlations[np.triu_indices(correlations.shape[1], 1)])
        #plt.show()

        return correlations[np.triu_indices(correlations.shape[1], 1)]

    def states_per_sess(self):
        state_counts = np.zeros(self.n_sessions)
        for i, post in enumerate(self.full_posteriors):
            state_counts[i] = np.sum(np.mean(post, axis=1) > 0.05)
        print(state_counts)
        return state_counts

    def parallel_relevant_states(self):
        parallel_states = np.zeros((self.n_all_states, self.n_all_states))
        for i, post in enumerate(self.full_posteriors):
            active = post.mean(axis=1) > 0.05
            parallel_states[active] += active

        print(parallel_states)

        return parallel_states


def posterior_helper(models, data, n_sessions):
    log_pred_dens = np.zeros((len(models), n_sessions))

    for i, m in enumerate(models):
        for j, (s, d) in enumerate(zip(m.states_list, data)):
            s.data = d
            log_pred_dens[i, j] = s.log_likelihood()

    return log_pred_dens


def waic_plots(subjects):
    waic = np.zeros((2, len(subjects)))
    for i, cond in enumerate(['nothing', 'answer']):
        for j, s in enumerate(subjects):
            mcmc_samples = pickle.load(open("./iHMM_fits/{}_{}_withtime_{}_condition_{}.p".format(s, 'bias', False, cond), "rb"))
            info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(s), "rb"))
            result = MCMC_result(mcmc_samples[-1000:], threshold=200, name=s, infos=info_dict, sessions='bias', conditioning=cond)
            waic[i, j] = result.load_waic()

    norm_waic = waic - waic[0]
    plt.figure(figsize=(12, 8))
    plt.plot(norm_waic)

    plt.xticks([0, 1], ['nothing', 'answer'], size=20)
    plt.xlabel('Conditioning', size=20)
    plt.ylabel('Delta WAIC', size=20)
    sns.despine()
    plt.tight_layout()
    plt.savefig("waic_overview")
    plt.show()
    return waic


subjects = ['CSH_ZAD_025', 'ZM_1897', 'KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06', 'SWC_023', 'CSHL051',
            'CSHL054', 'CSHL059', 'CSHL_007', 'CSHL_014', 'CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015',
            'CSH_ZAD_001', 'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011',
            'KS016', 'ibl_witten_16', 'KS019', 'CSH_ZAD_022', 'KS021', 'SWC_022', 'CSHL061', 'CSHL062'] # pre bias fits
subjects = ['CSHL045', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL055', 'CSHL059', 'CSHL061', 'CSHL062', 'CSHL065',
            'CSHL066', 'CSH_ZAD_011', 'CSH_ZAD_017', 'CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022', 'CSH_ZAD_024',
            'CSH_ZAD_026', 'KS002', 'KS014', 'KS016', 'KS022', 'KS023', 'SWC_022', 'ZM_1897'] # bias fits
subjects = ['CSH_ZAD_026']
fit_type = ['prebias', 'bias', 'all'][1]
with_time = False
conditioned_on = ['nothing', 'reward', 'truth', 'answer'][0]

# hm = waic_plots(subjects)
#
# quit()

fit_num = None
if len(sys.argv) == 2:
    fit_num = '_{}'.format(sys.argv[1])

n_states = []
n_sess = []
correlations = []
state_n_per_sess = []
parallel_n = []
threshold_corrs = []
c = np.zeros((len(subjects), 50))
d = np.zeros((len(subjects), 50))
for i, subject in enumerate(subjects):
    print(subject)
    print(conditioned_on)
    info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))
    if fit_num is None:
        mcmc_samples = pickle.load(open("./iHMM_fits/{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), "rb"))
    else:
        mcmc_samples = pickle.load(open("./iHMM_fits/{}_{}_withtime_{}_condition_{}{}.p".format(subject, fit_type, with_time, conditioned_on, fit_num), "rb"))

    result = MCMC_result(mcmc_samples[-1000:], threshold=200, name=subject, infos=info_dict, sessions=fit_type, conditioning=conditioned_on)
    n_states.append(result.n_pstates)
    n_sess.append(result.n_sessions)

    # result.waic()
    #threshold_corrs += list(result.thresholded_state_correlations())
    # parallel_n += list(result.parallel_relevant_states())
    #state_n_per_sess += list(result.states_per_sess())
    # correlations.append(list(result.state_correlations()))
    #result.assign_evolution(show=1)
    # result.contrasts_plot(until=33, show=1)
    #result.state_development(show=1)
    #result.block_alignment(show=1)
    result.contrasts_plot(until=33, show=1)
    quit()
    #result.load_waic()
    continue

    # result.contrasts_plot(show=True)
    continue

    a = result.session_entropy()
    b = np.interp(np.linspace(0, len(a)-1), np.arange(len(a)), a)
    c[i] = b
    b = b - b.mean()  # try relative entropy
    d[i] = b

    #result.load_waic()
    continue
    result.duration_overview()
    quit()
    result.pmf_posterior()
    continue

fs = 24
plt.figure(figsize=(8, 9))
plt.hist(n_states, bins=range(1, 8), align='left', color='grey')
plt.xticks([1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6])
a0 = plt.gca()
a0.set_ylabel("# of mice", size=fs)
a0.set_xlabel("# of states during biased training", size=fs)
a0.tick_params(axis='both', labelsize=fs -6)
sns.despine()
plt.tight_layout()
plt.show()

plt.plot(c.mean(0))
temp = np.percentile(c, [2.5, 97.5], axis=0)
plt.fill_between(np.arange(50), *temp, alpha=0.2)
plt.show()

plt.plot(d.mean(0))
temp = np.percentile(d, [2.5, 97.5], axis=0)
plt.fill_between(np.arange(50), *temp, alpha=0.2)
plt.show()
