# 'SWC_054', 'ZFM-01592', 'SWC_060', 'SWC_043', 'CSHL059', 'ZM_2241', 'CSHL049', 'CSHL051',
#             'DY_018', 'DY_013', 'DY_009', 'SWC_052', 'CSHL045', 'DY_020', 'CSHL052', 'SWC_058', 'ZFM-01936',
#             'ZFM-01576', 'DY_016'
# subjects = ["DY_013", "CSHL051", "CSHL059", "SWC_060"]  # CSHL058 missing
# subjects = ['CSHL045', 'CSHL051', 'CSHL052']
# subjects = ['CSHL053', 'CSHL055', 'CSHL059']
# subjects = ['CSHL061', 'CSHL062', 'CSHL065']
# subjects = ['CSHL066', 'CSH_ZAD_011'j, 'CSH_ZAD_017']
# subjects = ['CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022']
# subjects = ['CSH_ZAD_024', 'CSH_ZAD_026', 'KS002']
# subjects = ['KS014', 'KS016', 'KS022']
# subjects = ['KS023', 'SWC_022', 'ZM_1897']
# do:  'Sim_02_var_0_05', 'Sim_02_var_0_2', 'Sim_01_biased', 'Sim_01'
# subjects = ['Sim_12', 'Sim_13', 'Sim_14', 'Sim_15']  # ,'CSH_ZAD_029', ]  # do 'SWC_061' 'DY_013', 'NYU-20', 'CSH_ZAD_022', 'SWC_061'
# var fitted: 'Sim_03_dist_0_6_0_06', 'Sim_01', 'Sim_03_dist_0_6', 'Sim_04_inv'
subjects = ['CSH_ZAD_022', 'CSHL049']
subjects = ['CSHL045']#, 'SWC_054']
subjects = ['CSHL062']#, 'CSHL059']
subjects = ['ZM_1897']#, 'DY_013']
subjects = [['KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06',
             'SWC_023', 'CSHL051', 'CSHL054', 'CSHL_007', 'CSHL_014'][5]]
# subjects = [['CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015', 'CSH_ZAD_001',
#              'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003'][9]]
# subjects = [['CSH_ZAD_022', 'KS014', 'ibl_witten_17', 'CSHL045', 'CSHL052', 'DY_016', 'KS022'][4]]
# subjects = [['CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897'][0]]
# subjects = [['ibl_witten_14', 'CSHL_018', 'KS015', 'CSH_ZAD_001', 'CSH_ZAD_026', 'KS003', 'ibl_witten_13'][6]]
# subjects = [["CSHL_001", "CSHL_002", "CSHL_003", "CSHL_005", "CSHL_007", "CSHL_008", "CSHL_010",
#              "CSHL_014", "CSHL_015", "IBL-T2", "IBL-T3", "IBL-T4", "ibl_witten_04", "ibl_witten_05",
#              "ibl_witten_06", "ibl_witten_07", "ibl_witten_12", "ibl_witten_13", "ibl_witten_14", "ibl_witten_15",
#              "ibl_witten_16", "KS003", "KS005", "KS019", "NYU-01", "NYU-02", "NYU-04", "NYU-06", "ZM_1367", "ZM_1369",
#              "ZM_1371", "ZM_1372", "ZM_1743", "ZM_1745", "ZM_1746"][7]]  # zoe's subjects
subjects = ['NYU-06', 'KS022', 'CSH_ZAD_022', 'KS014', 'SWC_021', 'CSHL051', 'SWC_023',
            'CSH_ZAD_025', 'ZM_1897', 'ibl_witten_17', 'CSHL054', 'CSHL059', 'CSHL_007',
            'CSHL_014', 'CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015', 'CSH_ZAD_001',
            'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017',
            'CSH_ZAD_011', 'KS016', 'ibl_witten_16']
subjects = ['SWC_021', 'SWC_023', 'CSHL054', 'CSHL_007', 'CSH_ZAD_026', 'KS003',
             'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011',
             'KS016', 'ibl_witten_16'][10:12]
subjects = [['SWC_021', 'CSHL051', 'SWC_023'][0]] # bias
