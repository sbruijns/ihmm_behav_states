from simplex_plot import plotSimplex
import numpy as np
import pickle
from analysis_pmf import type2color
import math
import matplotlib.pyplot as plt
import copy
import string


all_state_types = pickle.load(open("all_state_types.p", 'rb'))


# create a list of fixed offsets, to apply to mice when they are in a corner
offsets = [(0, 0)]
hex_size = 0
angle_add = math.pi / 3
while len(offsets) < len(all_state_types):
    hex_size += 1
    corner = (hex_size, 0)
    offsets.append(corner)
    local_angle = 4 * math.pi / 3
    for i in range(6):
        next_corner = (corner[0] + hex_size * math.cos(local_angle), corner[1] + hex_size * math.sin(local_angle))
        local_angle -= angle_add
        for j in range(hex_size - 1):
            offsets.append((corner[0] * (j + 1) / hex_size + next_corner[0] * (hex_size - j - 1) / hex_size,
                            corner[1] * (j + 1) / hex_size + next_corner[1] * (hex_size - j - 1) / hex_size))
        corner = next_corner
        if i != 5:
            offsets.append(copy.copy(corner))


_, test_count = np.unique(offsets, return_counts=1, axis=0)
assert (test_count == 1).all()

# for i, o in enumerate(offsets):
#     plt.scatter(o[0], o[1])
# plt.show()
# quit()

session_counter = -1
string_list = []
alphabet = string.ascii_lowercase

for char1 in alphabet:
    for char2 in alphabet:
        string_list.append(char1 + char2)
    if len(string_list) == 100:
        break

# do as many sessions as it takes
while True:

    session_counter += 1

    not_ended = 0
    type_proportions = np.zeros((3, len(all_state_types)))
    x_offset, y_offset = np.zeros(len(all_state_types)), np.zeros(len(all_state_types))
    # iterate through all mice, count how many are in each corner
    for i, sts in enumerate(all_state_types):

        # check whether this mouse still has sessions left
        if sts.shape[1] > session_counter:
            not_ended += 1
            temp_counter = session_counter
        else:
            temp_counter = -1

        assert np.sum(sts[:, temp_counter]) <= 1.000000000000001

        if (sts[:, temp_counter] == 1).any():
            x_offset[i] = offsets[i][0] * 0.01
            y_offset[i] = offsets[i][1] * 0.01

        type_proportions[:, i] = sts[:, temp_counter]

    plotSimplex(type_proportions.T * 10, x_offset=x_offset, y_offset=y_offset, c=np.arange(len(all_state_types)), show=False, title="Session {}".format(1 + session_counter),
                vertexcolors=[type2color[i] for i in range(3)], vertexlabels=['Type 1', 'Type 2', 'Type 3'], save_title="simplex_{}.png".format(string_list[session_counter]))

    if not_ended == 0:
        break
