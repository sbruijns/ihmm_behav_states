import numpy as np
import matplotlib.pyplot as plt

data = np.random.normal(np.zeros(2), np.array([[1, 2], [2, 1]]))
plt.scatter(data)
plt.show()
