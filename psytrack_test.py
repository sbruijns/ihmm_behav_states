from psytrack.hyperOpt import hyperOpt
import numpy as np
import pandas as pd
from oneibl import one
from export_funs import trialinfo_to_df
import os
import pickle


def fit_sess_psytrack(session, maxlength=2.5, normfac=5., as_df=False):
    '''
    Use Nick's Psytrack code to fit a session. Very slow to run.
    Parameters
    ----------
    session : str
        UUID of the session to fit. Only fits single sessions.
    Returns
    -------
    wMode : np.ndarray
        5 x N_trials array, in which each row is a set of weights per trial for a given type.
        Row 0: Bias
        Row 1: Left contrast
        Row 2: Previous value of left contrast
        Row 3: Right contrast
        Row 4: Previous value of right contrast
    W_std : np.ndarray
        5 x N_trials array. Elementwise corresponds to stdev of each element of wMode.
    '''
    trialdf = trialinfo_to_df(session, maxlen=maxlength, ret_wheel=False)
    choices = trialdf.choice
    # Remap choice values from -1, 1 to 1, 2 (because psytrack demands it)
    newmap = {-1: 1, 1: 2}
    choices.replace(newmap, inplace=True)
    choices = choices.values[1:]
    tmp = trialdf[['contrastLeft', 'contrastRight']]
    sL = tmp.contrastLeft.apply(np.nan_to_num).values[1:].reshape(-1, 1)
    # sL = np.vstack((sL[1:], sL[:-1])).T  # Add a history term going back 1 trial
    sR = tmp.contrastRight.apply(np.nan_to_num).values[1:].reshape(-1, 1)
    # sR = np.vstack((sR[1:], sR[:-1])).T  # History term is prior value of contr on that side
    if normfac is not None:
        sL = np.tanh(normfac * sL) / np.tanh(normfac)
        sR = np.tanh(normfac * sR) / np.tanh(normfac)

    data = {'inputs': {'sL': sL, 'sR': sR},
            'y': choices, }
    weights = {'bias': 1, 'sL': 1, 'sR': 1}
    # K = np.sum(list(weights.values()))
    hyper_guess = {'sigma': 2**-5,
                   'sigInit': 2**5,
                   'sigDay': None}
    opt_list = ['sigma']
    hyp, evd, wMode, hess = hyperOpt(data, hyper_guess, weights, opt_list)
    if as_df:
        wMode = pd.DataFrame(wMode.T, index=trialdf.index[1:], columns=['bias', 'left', 'right'])
    return wMode, hess['W_std']


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    one = one.ONE()
    subject = 'CSHL059'
    eids = [['1c6173cb-1ea0-4045-a2b9-4ae49e5f9275', '484390a0-8139-4475-a69f-da237c17125b',
             '321555ff-a667-4f30-a5b1-9b5c6a3967d5', '32364bd4-286a-4ca7-a281-bfbdbb15eec3',
             'db072900-7135-4759-ac8a-65c0dffe243a', 'ea9f637f-10bb-4cb1-8c6d-10bba5cf79a1',
             '7025e23d-54d3-4917-b429-d83e9bc1de17', '8de7f3d5-adcb-4dcb-a96f-778726d757a8',
             '227f8deb-9b82-4e05-9505-b6445fb11ee0', 'bd051464-8de3-43f3-980c-2615682022ca',
             '760a2b34-1536-42d6-852c-60b57329eceb', '43c8d641-7cb1-4875-8b3e-9864f9c2f9ac']]
    for i, s in enumerate(eids):

        wMode, std = fit_sess_psytrack(s)
        pickle.dump((wMode, std), open("./session_data/psytrack/result {} {}.p".format(subject, i), 'wb'))
        trialnumbers = np.arange(1, wMode.shape[1] + 1)
        pickle.dump(trialnumbers, open("./session_data/psytrack/numbers {} {}.p".format(subject, i), 'wb'))

        trialdf = trialinfo_to_df(s, maxlen=58, wheel=False)
        plt.figure(figsize=(9, 9))
        plt.plot(trialnumbers, wMode[0], label='Bias', c='b')
        plt.plot(range(len(trialdf)), 3 * (trialdf.probabilityLeft - 0.5), color='k')
        plt.fill_between(trialnumbers, wMode[0] - std[0], wMode[0] + std[0],
                         color='b', alpha=0.5)
        plt.plot(trialnumbers, wMode[1], label='Weight contrast Left',
                 color='orange')
        plt.fill_between(trialnumbers, wMode[1] - std[1], wMode[1] + std[1],
                         color='orange', alpha=0.5)
        plt.plot(trialnumbers, wMode[2], label='Weight contrast Right',
                 color='purple')
        plt.fill_between(trialnumbers, wMode[2] - std[2], wMode[2] + std[2],
                         color='purple', alpha=0.5)
        plt.xlabel('Trial number', fontsize=18)
        plt.ylabel('Weight value', fontsize=18)
        plt.title(f'Psytrack fit'.format(subject), fontsize=24)
        plt.legend(fontsize=18)
        #plt.savefig(plotfile)
        plt.show()
