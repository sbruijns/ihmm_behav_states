"""Start a (series) of iHMM fit(s)."""
import numpy as np
import matplotlib.pyplot as plt
import pyhsmm
import pyhsmm.basic.distributions as distributions
import copy
import warnings
import pickle
import time
from scipy.special import digamma
import faulthandler
from itertools import product
faulthandler.enable()

np.set_printoptions(suppress=True)


def crp_expec(n, theta):
    """
    Return expected number of tables after n customers, given concentration theta.

    From Wikipedia
    """
    return theta * (digamma(theta + n) - digamma(theta))


def fill_timeouts(xs, skip=1):
    """Just iterate over array, replacing invalid elements."""
    if xs[0] == skip:
        print('sucks')
        xs[0] = np.random.rand() > 0.5

    curr = xs[0]
    ret = np.copy(xs)
    for i, x in enumerate(xs):
        if x == skip:
            ret[i] = curr
        curr = ret[i]

    return ret


def eleven2nine(x):
    """Map from 11 possible contrasts to 9, for the non-training phases.

    1 and 9 can't appear there, make other elements consider this.

    E.g.:
    [2, 0, 4, 8, 10] -> [1, 0, 3, 7, 8]
    """
    assert 1 not in x and 9 not in x
    x[x > 9] -= 1
    x[x > 1] -= 1
    return x


# Quick test
assert np.array_equal(eleven2nine(np.array([2, 0, 4, 8, 10])), np.array([1, 0, 3, 7, 8]))



subjects = ['Sim_16']#, 'KS003', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011']
# subjects = ['CSHL045', 'CSHL051', 'CSHL052']
# subjects = ['CSHL053', 'CSHL055', 'CSHL059']
# subjects = ['CSHL061', 'CSHL062', 'CSHL065']
# subjects = ['CSHL066', 'CSH_ZAD_011'j, 'CSH_ZAD_017']
# subjects = ['CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022']
# subjects = ['CSH_ZAD_024', 'CSH_ZAD_026', 'KS002']
# subjects = ['KS014', 'KS016', 'KS022']
# subjects = ['KS023', 'SWC_022', 'ZM_1897']
# do:  'Sim_02_var_0_05', 'Sim_02_var_0_2', 'Sim_01_biased', 'Sim_01'
# subjects = ['Sim_12', 'Sim_13', 'Sim_14', 'Sim_15']  # ,'CSH_ZAD_029', ]  # do 'SWC_061' 'DY_013', 'NYU-20', 'CSH_ZAD_022', 'SWC_061'
# var fitted: 'Sim_03_dist_0_6_0_06', 'Sim_01', 'Sim_03_dist_0_6', 'Sim_04_inv'
fit_type = ['prebias', 'bias', 'all', 'prebias_plus'][2]
with_time = False
conditioned_on = ['nothing', 'reward', 'truth', 'answer'][0]

redo_n = 0
seed = 1845  # 1842
# seed = 1848


Nmax = 15
n = 1000

# for subject in subjects:
for subject, gamma_prior in product(subjects, [(0.001, 10000)]):#product([0.1, 1, 10, 100], [0.1, 1, 10, 100])):
    likes = np.zeros(n)
    # title endings: _uniform_start, _solved
    save_title = "./dynamic_iHMM_fits/{}_{}_withtime_{}_condition_{}_var_{}_gamma_{}_5.p".format(subject, fit_type, with_time, conditioned_on, 0, str(gamma_prior).replace('.', '_'))
    print(save_title)
    np.random.seed(seed)

    print(redo_n)
    if redo_n == 0:
        print('New sample')
        info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))
        # Determine session numbers
        if fit_type == 'prebias':
            till_session = info_dict['bias_start']
        elif fit_type == 'bias' or fit_type == 'all':
            till_session = info_dict['n_sessions']
        elif fit_type == 'prebias_plus':
            till_session = min(info_dict['bias_start'] + 6, info_dict['n_sessions'])  # 6 here will actually turn into 7 later
        from_session = info_dict['bias_start'] if fit_type == 'bias' else 0
        # Determine # of inputs
        n_inputs = 9 if fit_type == 'bias' else 11

        models = []

        obs_hypparams = {'n_inputs': n_inputs * (1 + (conditioned_on != 'nothing')), 'n_outputs': 3}
        dur_hypparams = dict(r_support=np.array([1, 2, 3, 5, 7, 10, 15, 21, 28, 36, 45, 55, 150]),
                             r_probs=np.ones(13)/13., alpha_0=1, beta_0=1)

        obs_distns = [distributions.Input_Categorical(**obs_hypparams) for state in range(Nmax)]
        dur_distns = [distributions.NegativeBinomialIntegerR2Duration(**dur_hypparams) for state in range(Nmax)]

        posteriormodel = pyhsmm.models.WeakLimitHDPHSMM(
                # https://math.stackexchange.com/questions/449234/vague-gamma-prior
                alpha_a_0=.5, alpha_b_0=.5,  # TODO: gamma vs alpha? gamma steers state number
                gamma_a_0=gamma_prior[0], gamma_b_0=gamma_prior[1],
                init_state_concentration=6.,
                obs_distns=obs_distns,
                dur_distns=dur_distns)

        for j in range(from_session, till_session + (fit_type != 'prebias')):
            try:
                data = pickle.load(open("./session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
                if n_inputs == 9:
                    data[:, 0] = eleven2nine(data[:, 0])
            except FileNotFoundError:
                continue

            if data.shape[0] == 0:
                continue
            if conditioned_on == 'answer':
                prev_ans = fill_timeouts(data[:, 1])
                prev_ans[1:] = prev_ans[:-1]
                data[:, 0] += (prev_ans == 2) * n_inputs
            elif conditioned_on == 'reward':
                side_info = pickle.load(open("./session_data/{}_side_info_{}.p".format(subject, j), "rb"))
                prev_reward = side_info[:, 1]
                prev_reward[1:] = prev_reward[:-1]
                prev_reward[0] = np.random.rand() > 0.5
                data[:, 0] += (prev_reward == 1) * n_inputs
            elif conditioned_on == 'truth':
                side_info = pickle.load(open("./session_data/{}_side_info_{}.p".format(subject, j), "rb"))
                prev_reward = side_info[:, 1]
                prev_reward[1:] = prev_reward[:-1]

                prev_ans = fill_timeouts(data[:, 1])
                prev_ans[1:] = prev_ans[:-1]

                truth = prev_reward == (prev_ans == 2)
                truth[0] = np.random.rand() > 0.5

                data[:, 0] += (truth == 1) * n_inputs

            data = data[:, [0, 1]]
            data = data.astype(int)

            posteriormodel.add_data(data)
    else:
        print("Adding to previous fit")
        models = pickle.load(open(save_title, 'rb'))
        posteriormodel = models[-1]
        n = redo_n

    # import pyhsmm.util.profiling as prof
    # from pybasicbayes.util.stats import sample_crp_tablecounts
    # prof_func = prof._prof(posteriormodel.resample_model)
    # prof._prof.add_function(sample_crp_tablecounts)
    # later
    # !!! call prof_func, not the original func
    # later
    # prof._prof.print_stats()
    # print("WARNING, SETTING STATES")
    # states_solution = pickle.load(open("states_{}_{}_condition_{}_{}.p".format('DY_013', 'all', 'nothing', '0_01'), 'rb'))  # todo: remove!

    time_save = time.time()
    with warnings.catch_warnings():  # ignore the scipy warning
        warnings.simplefilter("ignore")
        for j in range(n):

            if j % 50 == 0:
                print(j)

            posteriormodel.resample_model()
            # if j == 0:
            #     print(posteriormodel.stateseqs[0])
            #
            #     for i, seq in enumerate(posteriormodel.stateseqs):
            #         seq[:] = states_solution[i]
            #     print(posteriormodel.stateseqs[0])

            likes[j] = posteriormodel.log_likelihood()
            model_save = copy.deepcopy(posteriormodel)
            if j != n - 1 and j != 0:
                # To save on memory:
                model_save.delete_data()
            models.append(model_save)

            # save something in case of crash
            if j % 400 == 0 and j > 0:
                pickle.dump(models, open(save_title, 'wb'))
    print(time.time() - time_save)

    pickle.dump(models, open(save_title, 'wb'))
