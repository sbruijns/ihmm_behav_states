"""Compare the two different samplings I got from the forward sampling code"""
import numpy as np
import matplotlib.pyplot as plt
import pyhsmm
import pyhsmm.basic.distributions as distributions
import pickle

orig, mine = pickle.load(open("./state comparisons", "rb"))

orig = np.array(orig)
mine = np.array(mine)

N = orig.shape[0]
T = orig.shape[1]

for t in range(T):
    plt.hist(orig[:, t])
    plt.hist(mine[:, t], alpha=0.3)
    print(np.unique(orig[:, t], return_counts=1))
    print(np.unique(mine[:, t], return_counts=1))
    print()
    plt.show()
