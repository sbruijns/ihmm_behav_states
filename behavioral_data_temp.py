"""
    This is code for figuring out which training criteria is fulfilled at which session.
""""

from one.api import ONE
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import pickle
import json
import os
import re
import psychofit
pd.options.mode.chained_assignment = None  # default='warn'
np.set_printoptions(suppress=True)
pd.set_option('display.float_format', lambda x: '%.3f' % x)
one = ONE()

contrast_to_num = {-1.: 0, -0.5: 1, -0.25: 2, -0.125: 3, -0.0625: 4, 0: 5, 0.0625: 6, 0.125: 7, 0.25: 8, 0.5: 9, 1.: 10}

dataset_types = ['choice', 'contrastLeft', 'contrastRight',
                 'feedbackType', 'probabilityLeft', 'response_times',
                 'goCue_times']

def get_df(trials):
    if np.all(None == trials['choice']) or np.all(None == trials['contrastLeft']) or np.all(None == trials['contrastRight']) or np.all(None == trials['feedbackType']) or np.all(None == trials['probabilityLeft']):  # or np.all(None == data_dict['response_times']):
        return None, None
    d = {'response': trials['choice'], 'contrastL': trials['contrastLeft'], 'contrastR': trials['contrastRight'], 'feedback': trials['feedbackType']}

    df = pd.DataFrame(data=d, index=range(len(trials['choice']))).fillna(0)
    df['feedback'] = df['feedback'].replace(-1, 0)
    df['signed_contrast_clear'] = df['contrastR'] - df['contrastL']
    df['signed_contrast'] = df['signed_contrast_clear'].map(contrast_to_num)
    df['response'] += 1  # this is coded most unintuitely, 0 is rightwards, and 1 is leftwards (which is why I not this variable in other programs)
    df['block'] = trials['probabilityLeft']
    # df['rt'] = data_dict['response_times'] - data_dict['goCue_times']  # RTODO

    return df


def criterion_check(dfs, printer=False):
    for df in dfs:
        if type(df) == int:
            print("No 3 sessions")
            return

    uber_df = pd.concat(dfs)
    df_psych = uber_df.groupby('signed_contrast_clear').agg(
        n_trials=pd.NamedAgg(column='signed_contrast', aggfunc='count'),
        p_left=pd.NamedAgg(column='choice', aggfunc=lambda x: np.sum(x == 0) / np.sum(x != 1)))
    psych_params, _ = psychofit.mle_fit_psycho(
        np.c_[df_psych.index.values * 100, df_psych['n_trials'], df_psych['p_left']].T,
        P_model='erf_psycho_2gammas',
        parstart=np.array([0., 20., 0.05, 0.05]),
        parmin=np.array([-100, 0, 0., 0.]),
        parmax=np.array([100, 100., 1, 1]),
        nfits=10
    )

    # plt.plot(df_psych.index.values * 100, df_psych['p_left'], 'bo', mfc='b')
    # plt.plot(np.arange(-100, 100), psychofit.erf_psycho_2gammas(psych_params, np.arange(-100, 100)), '-b')
    # plt.ylim(0, 1)
    # plt.show()

    median_rt_on_0 = False
    contrast_0_introduced = True
    trials_200 = True
    trials_400 = True
    easy_perf_80 = True
    easy_perf_90 = True
    for df in dfs:
        easy_perf_80 = easy_perf_80 and np.mean(df.feedbackType[np.abs(df.signed_contrast_clear) >= 0.5]) > 0.8
        easy_perf_90 = easy_perf_90 and np.mean(df.feedbackType[np.abs(df.signed_contrast_clear) >= 0.5]) > 0.9
        trials_200 = trials_200 and df.shape[0] > 200
        trials_400 = trials_400 and df.shape[0] > 400
    abs_bias_16 = np.abs(psych_params[0]) < 16
    abs_bias_10 = np.abs(psych_params[0]) < 10
    thresh_19 = psych_params[1] < 19
    thresh_20 = psych_params[1] < 20
    lapse_2 = psych_params[2] < 0.2 and psych_params[3] < 0.2
    lapse_1 = psych_params[2] < 0.1 and psych_params[3] < 0.1
    if np.any(uber_df.signed_contrast_clear == 0):
        median_rt_on_0 = np.median((uber_df.response_times - uber_df.goCue_times)[uber_df.signed_contrast_clear == 0]) < 2
    else:
        contrast_0_introduced = False
    
    crit_1a = trials_200 and easy_perf_80 and abs_bias_16 and thresh_19 and lapse_2 and contrast_0_introduced
    crit_1b = trials_400 and easy_perf_90 and abs_bias_10 and thresh_20 and lapse_1 and contrast_0_introduced and median_rt_on_0

    if printer:
        print("1a is {}, 1b is {}".format(crit_1a, crit_1b))

    problems = np.zeros(12)
    problems = trials_200, easy_perf_80, abs_bias_16, thresh_19, lapse_2, contrast_0_introduced, trials_400, easy_perf_90, abs_bias_10, thresh_20, lapse_1, median_rt_on_0

    return crit_1a, crit_1b, problems

misses = []
to_introduce = [2, 3, 4, 5]

amiss = ['UCLA034', 'UCLA036', 'UCLA037', 'PL015', 'PL016', 'PL017', 'PL024', 'NR_0017', 'NR_0019', 'NR_0020', 'NR_0021', 'NR_0027']
fit_type = ['prebias', 'bias', 'all', 'prebias_plus', 'zoe_style'][0]
if fit_type == 'bias':
    loading_info = json.load(open("canonical_infos_bias.json", 'r'))
elif fit_type == 'prebias':
    loading_info = json.load(open("canonical_infos.json", 'r'))
bwm = ['NYU-11', 'NYU-12', 'NYU-21', 'NYU-27', 'NYU-30', 'NYU-37',
    'NYU-39', 'NYU-40', 'NYU-45', 'NYU-46', 'NYU-47', 'NYU-48',
    'CSHL045', 'CSHL047', 'CSHL049', 'CSHL051', 'CSHL052', 'CSHL053',
    'CSHL054', 'CSHL055', 'CSHL058', 'CSHL059', 'CSHL060', 'UCLA005',
    'UCLA006', 'UCLA011', 'UCLA012', 'UCLA014', 'UCLA015', 'UCLA017',
    'UCLA033', 'UCLA034', 'UCLA035', 'UCLA036', 'UCLA037', 'KS014',
    'KS016', 'KS022', 'KS023', 'KS042', 'KS043', 'KS044', 'KS045',
    'KS046', 'KS051', 'KS052', 'KS055', 'KS084', 'KS086', 'KS091',
    'KS094', 'KS096', 'DY_008', 'DY_009', 'DY_010', 'DY_011', 'DY_013',
    'DY_014', 'DY_016', 'DY_018', 'DY_020', 'PL015', 'PL016', 'PL017',
    'PL024', 'SWC_042', 'SWC_043', 'SWC_060', 'SWC_061', 'SWC_066',
    'ZFM-01576', 'ZFM-01577', 'ZFM-01592', 'ZFM-01935', 'ZFM-01936',
    'ZFM-01937', 'ZFM-02368', 'ZFM-02369', 'ZFM-02370', 'ZFM-02372',
    'ZFM-02373', 'ZM_1897', 'ZM_1898', 'ZM_2240', 'ZM_2241', 'ZM_2245',
    'ZM_3003', 'SWC_038', 'SWC_039', 'SWC_052', 'SWC_053', 'SWC_054',
    'SWC_058', 'SWC_065', 'NR_0017', 'NR_0019', 'NR_0020', 'NR_0021',
    'NR_0027', 'ibl_witten_13', 'ibl_witten_17', 'ibl_witten_18',
    'ibl_witten_19', 'ibl_witten_20', 'ibl_witten_25', 'ibl_witten_26',
    'ibl_witten_27', 'ibl_witten_29', 'CSH_ZAD_001', 'CSH_ZAD_011',
    'CSH_ZAD_019', 'CSH_ZAD_022', 'CSH_ZAD_024', 'CSH_ZAD_025',
    'CSH_ZAD_026', 'CSH_ZAD_029']
regexp = re.compile(r'canonical_result_((\w|-)+)_prebias.p')
subjects = []
for filename in os.listdir("./multi_chain_saves/"):
    if not (filename.startswith('canonical_result_') and filename.endswith('.p')):
        continue
    result = regexp.search(filename)
    if result is None:
        continue
    subject = result.group(1)
    subjects.append(subject)
already_fit = list(loading_info.keys())


# remaining_subs = [s for s in subjects if s not in amiss and s not in already_fit]
# print(remaining_subs)

data_folder = 'session_data'

old_style = False
if old_style:
    print("Warning, data can have splits")
    data_folder = 'session_data_old'
bias_eids = []

print("#########################################")
print("Waring, rt's removed, find with   # RTODO")
print("#########################################")

short_subjs = []
names = []

pre_bias = []
entire_training = []
training_status_reached = []
actually_existing = []

all_problems = []
count, count_1a, count_1b, count_any_1a, count_any_1b = 0, 0, 0, 0, 0
not_good_enough = []
problem_lists = []
criterion_lists = []
for subject in subjects:
    break
    print('_____________________')
    print(subject)

    if subject in ['NYU-12']:
        # NYU-12 starts bias almost immediately
        continue

    # if subject in already_fit or subject in amiss:
    #     continue
    try:
        trials = one.load_aggregate('subjects', subject, '_ibl_subjectTrials.table')

    # Load training status and join to trials table
    
        training = one.load_aggregate('subjects', subject, '_ibl_subjectTraining.table')

        trials = (trials
                  .set_index('session')
                  .join(training.set_index('session'))
                  .sort_values(by='session_start_time', kind='stable'))
        actually_existing.append(subject)

        start_times, indices = np.unique(trials.session_start_time, return_index=True)
        start_times = [trials.session_start_time[index] for index in sorted(indices)]
        task_protocol, indices = np.unique(trials.task_protocol, return_index=True)
        task_protocol = [trials.task_protocol[index] for index in sorted(indices)]
        nums, indices = np.unique(trials.session_number, return_index=True)
        nums = [trials.session_number[index] for index in sorted(indices)]
        eids, indices = np.unique(trials.index, return_index=True)
        eids = [trials.index[index] for index in sorted(indices)]
    except:
        print("Not working {}".format(subject))
        continue

    print("original # of eids {}".format(len(eids)))

    test = [(y, x) for y, x in sorted(zip(start_times, eids))]
    pickle.dump(test, open("./{}/{}_session_names.p".format(data_folder, subject), "wb"))

    performance = np.zeros(len(eids))
    easy_per = np.zeros(len(eids))
    hard_per = np.zeros(len(eids))
    bias_start = 0
    ephys_start = 0

    info_dict = {'subject': subject, 'dates': [st.to_pydatetime() for st in start_times], 'eids': eids, 'date_and_session_num': {}}
    contrast_set = {0, 1, 9, 10}

    rel_count = -1

    last_3_dfs = [0, 0, 0]
    any_1a, any_1b = False, False
    problem_list = [np.zeros(12), np.zeros(12), np.zeros(12), np.zeros(12), np.zeros(12), np.zeros(12)]
    criterion_list = [np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2)]

    for i, start_time in enumerate(start_times):

        rel_count += 1

        assert rel_count == i

        df = trials[trials.session_start_time == start_time]
        df.loc[:, 'contrastRight'] = df.loc[:, 'contrastRight'].fillna(0)
        df.loc[:, 'contrastLeft'] = df.loc[:, 'contrastLeft'].fillna(0)
        df.loc[:, 'feedbackType'] = df.loc[:, 'feedbackType'].replace(-1, 0)
        df.loc[:, 'signed_contrast_clear'] = df.loc[:, 'contrastRight'] - df.loc[:, 'contrastLeft']
        df.loc[:, 'signed_contrast'] = df.loc[:, 'signed_contrast_clear'].map(contrast_to_num)
        df.loc[:, 'choice'] = df.loc[:, 'choice'] + 1

        if any([df[x].isnull().any() for x in ['signed_contrast', 'choice', 'feedbackType', 'probabilityLeft']]):
            quit()

        assert len(np.unique(df['session_start_time'])) == 1

        current_contrasts = set(df['signed_contrast'])
        diff = current_contrasts.difference(contrast_set)
        for c in to_introduce:
            if c in diff:
                info_dict[c] = rel_count
        contrast_set.update(diff)

        performance[i] = np.mean(df['feedbackType'])
        easy_per[i] = np.mean(df['feedbackType'][np.logical_or(df['signed_contrast'] == 0, df['signed_contrast'] == 10)])
        hard_per[i] = np.mean(df['feedbackType'][df['signed_contrast'] == 5])

        if bias_start == 0 and df.task_protocol[0].startswith('_iblrig_tasks_biasedChoiceWorld'):
            bias_start = i
            print("bias start {}".format(rel_count))
            info_dict['bias_start'] = rel_count
            training_status_reached.append(set(df.training_status))
            if bias_start < 33:
                short_subjs.append(subject)

        if i > 3:
            crit1a, crit1b, problems = criterion_check(last_3_dfs)
            any_1a = any_1a or crit1a
            any_1b = any_1b or crit1b
            problem_list.append(problems)
            problem_list.pop(0)
            criterion_list.append(np.array([any_1a, any_1b]))
            criterion_list.pop(0)
        if bias_start > 0:
            print(subject)
            crit1a, crit1b, problems = criterion_check(last_3_dfs, printer=True)
            count += 1
            count_1a += crit1a
            count_1b += crit1b
            count_any_1a += any_1a
            count_any_1b += any_1b
            all_problems.append(problems)
            if not any_1a:
                not_good_enough.append(subject)
            problem_lists.append(problem_list)
            criterion_lists.append(criterion_list)
            break

        if ephys_start == 0 and df.task_protocol[0].startswith('_iblrig_tasks_ephysChoiceWorld'):
            ephys_start = i
            print("ephys start {}".format(rel_count))
            info_dict['ephys_start'] = rel_count

        last_3_dfs[rel_count % 3] = df

names = ["200 trials", "easy 80", "bias 16", "thresh 19", "lapse 20", "all conts", "trials 400", "easy 90", "bias 10", "thresh 20", "lapse 10", "median 0 rt"]
criterion_array = pickle.load(open("criterion_array", 'rb'))
reached_array = pickle.load(open('reached_array', 'rb'))

plt.figure(figsize=(16, 9))
ls = ['-'] * 6 + ['--'] * 6
for i in range(criterion_array.shape[-1]):
    plt.plot(range(-6, 0), criterion_array[..., i].mean(0), ls=ls[i], label=names[i])

criteria_names = ["1a reached", "1b reached"]
colours = ['green', 'black']
for i in range(reached_array.shape[-1]):
    plt.plot(range(-6, 0), reached_array[..., i].mean(0), c=colours[i], label=criteria_names[i], lw=4)

plt.ylim(0, 1)

plt.ylabel("% of population fulfilling criterion", size=28)
plt.xlabel("Sessions till start of bias training", size=28)

plt.legend(frameon=False, fontsize=14, ncols=2)
plt.show()