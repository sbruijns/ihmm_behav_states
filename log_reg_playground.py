import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize


contrasts_L = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])
contrasts_R = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])[::-1]

contrasts_mixed = contrasts_R - contrasts_L

pmf_to_fit = np.array([0.05, 0.053, 0.07, 0.12, 0.21, 0.4, 0.6, 0.7, 0.81, 0.83, 0.84])


def weights_to_pmf(weights):
    psi = weights[0] * contrasts_R + weights[1] * contrasts_L + weights[-1]
    return 1 / (1 + np.exp(psi))


def weights_to_pmf_mixed(weights):
    psi = weights[0] * contrasts_mixed + weights[-1]
    return 1 / (1 + np.exp(psi))


fit_1 = minimize(lambda x: np.sum((pmf_to_fit - weights_to_pmf(x)) ** 2), x0=np.zeros(3))


fit_2 = minimize(lambda x: np.sum((pmf_to_fit - weights_to_pmf_mixed(x)) ** 2), x0=np.zeros(2))


plt.plot(pmf_to_fit, 'k', lw=4, label='target')
plt.plot(weights_to_pmf(fit_1.x), 'g', label='fit')
plt.plot(weights_to_pmf_mixed(fit_2.x), 'b', label='fit one sens.')

plt.ylim(0, 1)
plt.legend()
plt.show()