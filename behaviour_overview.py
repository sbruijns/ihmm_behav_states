import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from one.api import ONE
import pickle


show = 1

def progression(data, contrasts, progression_variable='feedback', windowsize=6, upper_bound=None, title=None):
    # looks somewhat irregular, red dots are not in middle of bump they cause, this is because distribution of specific contrasts is not uniform
    plt.figure(figsize=(9, 6))
    feedback = data[data['signed_contrast'].isin(contrasts)]['feedback']
    rights = data[(data['signed_contrast'].isin(contrasts)) & (data['feedback'] == 1)]['rt']
    wrongs = data[(data['signed_contrast'].isin(contrasts)) & (data['feedback'] == 0)]['rt']
    mean_value = np.mean(data[data['signed_contrast'].isin(contrasts)][progression_variable])
    progression = [np.mean(data[data['signed_contrast'].isin(contrasts)][progression_variable].values[i - windowsize : i + windowsize + 1]) for i in range(windowsize, len(feedback)-windowsize)]
    plt.gca().axhline(mean_value, c='k', label='mean ' + progression_variable + ' over session')
    plt.plot(rights.index, rights, 'g.', label='correct answers')
    plt.plot(wrongs.index, wrongs, 'r.', label='wrong answers')
    plt.plot(feedback.index[windowsize:-windowsize], progression, 'b', label='average ' + progression_variable + ' over windowsize ' + str(2*windowsize+1))
    plt.ylim(0, upper_bound)
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    # if title == "KS014, RT session 12 / 15" or title == "KS014, performance session 12 / 15":
    #     plt.plot([599, 603], [1, 1], color='m', lw=6)
    #     plt.plot([658, 662], [1, 1], color='m', lw=6)
    #     plt.plot([703, 708], [1, 1], color='m', lw=6)

    plt.title(title, size=22)
    plt.legend(fontsize=13)
    plt.ylabel(progression_variable, size=20)
    plt.xlabel('trial', size=20)
    # if title:
    #     plt.savefig('./overview_figures/' + title.replace('/', '_') + '.png')

    if show:
        plt.show()
    else:
        plt.close()

    # if progression_variable == 'feedback':
    #     means = data.groupby('signed_contrast').mean()['response']
    #     stds = data.groupby('signed_contrast').std()['response']
    #     plt.errorbar(means.index, means.values, stds.values)
    #     plt.ylim(-0.2, 1.2)
    #     plt.title(title, size=22)
    #     plt.savefig("temp {}".format(title).replace('/', '_'))
    #     plt.close()
    if progression_variable == 'feedback':
        data_local = data[:113]
        means = data_local.groupby('signed_contrast').mean()['response']
        stds = data_local.groupby('signed_contrast').sem()['response']
        plt.errorbar(means.index, means.values, stds.values, label='1')

        data_local = data[113:226]
        means = data_local.groupby('signed_contrast').mean()['response']
        stds = data_local.groupby('signed_contrast').sem()['response']
        plt.errorbar(means.index, means.values, stds.values, label='2')

        data_local = data[226:339]
        means = data_local.groupby('signed_contrast').mean()['response']
        stds = data_local.groupby('signed_contrast').sem()['response']
        plt.errorbar(means.index, means.values, stds.values, label='3')

        data_local = data[339:]
        means = data_local.groupby('signed_contrast').mean()['response']
        stds = data_local.groupby('signed_contrast').sem()['response']
        plt.errorbar(means.index, means.values, stds.values, label='last')
        plt.title(title, size=22)
        plt.ylim(0, 1)
        plt.legend()
        # plt.savefig("temp {}".format(title).replace('/', '_'))
        plt.show()
    if progression_variable == 'rt':
        means = data.groupby('signed_contrast').mean()['rt']
        stds = data.groupby('signed_contrast').sem()['rt']
        plt.errorbar(means.index, means.values, stds.values)
        plt.title(title, size=22)
        # plt.savefig("temp {}".format(title).replace('/', '_'))
        plt.close()


dataset_types = ['choice', 'contrastLeft', 'contrastRight', \
                 'feedbackType', 'probabilityLeft', 'response_times', \
                 'stimOn_times', 'goCue_times']
def get_df(eid):
    # dangerously many changes, check again
    try:
        data_dict = one.load_object(eid, 'trials') # download_only=True messes with my code, returns path for whatever reason
    except:
        print('lol')
        return None, None
    if np.all(None == data_dict['choice']) or np.all(None == data_dict['contrastLeft']) or np.all(None == data_dict['contrastRight']) or np.all(None == data_dict['feedbackType']) or np.all(None == data_dict['probabilityLeft']) or np.all(None == data_dict['response_times']) or np.all(None == data_dict['stimOn_times']):
        return None, None
    d = {'response': data_dict['choice'], 'contrastL': data_dict['contrastLeft'], 'contrastR': data_dict['contrastRight'], 'feedback': data_dict['feedbackType']}
    df = pd.DataFrame(data=d, index=range(len(data_dict['choice']))).fillna(0)
    df['feedback'] = df['feedback'].replace(-1, 0)
    df['response'] = df['response'].replace(-1, 0) # 0's actually have meaning
    df['signed_contrast'] = df['contrastR'] - df['contrastL']
    df['contrast'] = np.abs(df['signed_contrast'])
    df['response'] = np.logical_not(df['response'])
    df['rt'] = data_dict['response_times'] - data_dict['stimOn_times']
    df['correct_answer'] = np.logical_not(df['response'] ^ df['feedback']) # calculation of what the actualy correct answer is, basically boolean of contrast (but doesnt work for 0)
    return df, data_dict['probabilityLeft']

one = ONE()
exclude_eids = ['a66f1593-dafd-4982-9b66-f9554b6c86b5', 'ee40aece-cffd-4edb-a4b6-155f158c666a',
                'ebe090af-5922-4fcd-8fc6-17b8ba7bad6d', '266a0360-ea0a-4580-8f6a-fe5bad9ed17c',
                '61e11a11-ab65-48fb-ae08-3cb80662e5d6', '064a7252-8e10-4ad6-b3fd-7a88a2db5463',
                'b01df337-2d31-4bcc-a1fe-7112afd50c50', 'c7248e09-8c0d-40f2-9eb4-700a8973d8c8',
                '28cd1b10-a722-459c-9539-8aac60f3da16', '46f6bf36-6d5a-42f7-b627-51f3357fbf03',
                '2e43faf9-2aba-488f-b7b7-e5193032b25b', 'dda5fc59-f09a-4256-9fb5-66c67667a466',
                '03cf52f6-fba6-4743-a42e-dd1ac3072343', 'f312aaec-3b6f-44b3-86b4-3a0c119c0438',
                '7f6b86f9-879a-4ea2-8531-294a221af5d0']

# repeated site
# traj = one.alyx.rest('trajectories', 'list', provenance='Planned',
#                      x=-2243, y=-2000,  # repeated site coordinate
#                      project='ibl_neuropixel_brainwide_01')
# traj.reverse()

subject = "UCLA015" #'KS022'
eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2024-01-01'], details=True)

start_times = [sess['date'] for sess in sess_info]
protocols = [sess['task_protocol'] for sess in sess_info]

eids = [x for _, x in sorted(zip(start_times, eids))]
protocols = [x for _, x in sorted(zip(start_times, protocols))]

# for t in traj:
counti = 0
for i, (prot, eid) in enumerate(zip(protocols, eids)):
    print("Nice demonstration that the PMF jump is actually quite sudden!")
    print(i)
    # if not prot.startswith('_iblrig_tasks_trainingChoiceWorld'):
    #     continue
    if 'habituation' in prot:
        continue
    # eid = t['session']['id']
    df, _ = get_df(eid)

    if df is None:
        continue

    print(prot)
    print(eid)

    counti += 1

    # if counti != 7:
    #     continue

    # rt_data = np.zeros((len(df), 3))
    # rt_data[:, 0] = df['signed_contrast']
    # rt_data[:, 1] = df['rt']
    # rt_data[:, 2] = df['response']
    # pickle.dump(rt_data, open("./session_data/{} rt info {}".format(subject, counti), 'wb'))

    progression(df, df['signed_contrast'].unique(), progression_variable='feedback', upper_bound=2, title="{} PMF {}".format(subject, counti))
    # progression(df, df['signed_contrast'].unique(), progression_variable='rt', upper_bound=4, title="{} CMF {}".format(subject, counti))
