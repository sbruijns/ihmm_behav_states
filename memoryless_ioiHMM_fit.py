import numpy as np
import matplotlib.pyplot as plt
import pyhsmm
import pyhsmm.basic.distributions as distributions
import copy
import warnings
import pickle
import time
import faulthandler
faulthandler.enable()

np.set_printoptions(suppress=True)


subject = "CSHL045"
fit_type = ['pre_bias', 'bias', 'all'][0]
with_time = False
conditioned_on = ['nothing', 'reward', 'truth', 'answer'][0]

Nmax = 15
seeds = [11]
info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))

n = 1500
relevant_states = np.zeros((len(seeds), n))
likes = np.zeros((len(seeds), n))
models = []
for i, seed in enumerate(seeds):
    np.random.seed(seed)
    print(i+1)
    obs_hypparams = {'n_inputs': 11, 'n_outputs': 3}

    obs_distns = [distributions.Input_Categorical(**obs_hypparams) for state in range(Nmax)]
    posteriormodel = pyhsmm.models.WeakLimitHDPHMM(
            # https://math.stackexchange.com/questions/449234/vague-gamma-prior
            alpha_a_0=.5, alpha_b_0=20, # TODO: gamma vs alpha? gamma steers state number
            gamma_a_0=1, gamma_b_0=1,
            init_state_concentration=6.,
            obs_distns=obs_distns)

    for j in range(info_dict['n_sessions']):
        try:
            data = pickle.load(open("./session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
            print(data.shape)
        except FileNotFoundError:
            continue

        if data.shape[0] == 0:
            continue

        data = data[:, [0, 1]]
        data = data.astype(int)
        posteriormodel.add_data(data)

    time_save = time.time()
    with warnings.catch_warnings(): # ignore the scipy warning
        warnings.simplefilter("ignore")
        for j in range(n):

            if j % 10 == 0:
                print(j)

            posteriormodel.resample_model()

            likes[i, j] = posteriormodel.log_likelihood()
            model_save = copy.deepcopy(posteriormodel)
            if j != n - 1:
                # To save on memory:
                model_save.delete_data()
            models.append(model_save)

            # save something in case of crash
            if j % 100 == 0:
                pickle.dump(models, open("{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), 'wb'))
    print(time.time() - time_save)


pickle.dump(models, open("{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), 'wb'))
plt.plot(likes.T)
plt.savefig("likelihoods")
plt.show()
