import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.cm as cm


bias_cont_ticks = (np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
cmap = cm.get_cmap('magma')

start = np.array([0.04, 0.06, 0.1, 0.21, 0.5, 0.79, 0.9, 0.94, 0.96])
end = np.array([0.27, 0.33, 0.47, 0.65, 0.82, 0.88, 0.95, 0.97, 0.99])

plt.figure(figsize=(11, 9))
for w in range(10):
    plt.plot((1 - (w/10)) * start + (w/10) * end + np.random.normal(0, 0.008, size=9), c=cmap(w / 10))

sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=1, vmax=10))
clb = plt.colorbar(sm)
clb.ax.set_title('Session', size=23, pad=14)
clb.ax.tick_params(labelsize=22-3)

plt.xticks(*bias_cont_ticks, size=22-1)
plt.xlim(left=0)
plt.yticks(size=22-2)
plt.xlabel('Contrast', size=22)
plt.ylabel('P(answer rightward)', size=22)

# plt.title("Zoe psychometric functions {}".format(subject), size=22)
sns.despine()
plt.tight_layout()
plt.savefig("dynamic_pmf_plot")
plt.show()
