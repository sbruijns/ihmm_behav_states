from one.api import ONE
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import pickle
import datetime


one = ONE()

# Zoes mice
# import json
# sessions = json.loads(open("./posterior_probs_for_seb/sessions.json").read())

contrast_to_num = {-1.: 0, -0.5: 1, -0.25: 2, -0.125: 3, -0.0625: 4, 0: 5, 0.0625: 6, 0.125: 7, 0.25: 8, 0.5: 9, 1.: 10}

# old:
# def get_df(eid):
#     choices, contrastL, contrastR, feedback, block, rt, gocue = one.load(eid, dataset_types=dataset_types)
#     if np.all(None == choices) or np.all(None == contrastL) or np.all(None == contrastR) or np.all(None == feedback) or np.all(None == block):
#         return None
#     d = {'response': choices, 'contrastL': contrastL, 'contrastR': contrastR, 'feedback': feedback}
#
#     df = pd.DataFrame(data=d, index=range(len(choices))).fillna(0)
#     df['feedback'] = df['feedback'].replace(-1, 0)
#     df['signed_contrast'] = df['contrastR'] - df['contrastL']
#     df['signed_contrast'] = df['signed_contrast'].map(contrast_to_num)
#     df['response'] += 1
#     df['block'] = block
#     df['rt'] = rt - gocue

dataset_types = ['choice', 'contrastLeft', 'contrastRight',
                 'feedbackType', 'probabilityLeft', 'response_times',
                 'goCue_times']

def get_df(trials):
    if np.all(trials['choice'] is None) or np.all(None == trials['contrastLeft']) or np.all(None == trials['contrastRight']) or np.all(None == trials['feedbackType']) or np.all(None == trials['probabilityLeft']):  # or np.all(None == data_dict['response_times']):
        return None, None
    d = {'response': trials['choice'], 'contrastL': trials['contrastLeft'], 'contrastR': trials['contrastRight'], 'feedback': trials['feedbackType']}

    df = pd.DataFrame(data=d, index=range(len(trials['choice']))).fillna(0)
    df['feedback'] = df['feedback'].replace(-1, 0)
    df['signed_contrast'] = df['contrastR'] - df['contrastL']
    df['signed_contrast'] = df['signed_contrast'].map(contrast_to_num)
    df['response'] += 1  # this is coded most unintuitely, 0 is rightwards, and 1 is leftwards (which is why I not this variable in other programs)
    df['block'] = trials['probabilityLeft']
    # df['rt'] = data_dict['response_times'] - data_dict['goCue_times']  # RTODO

    return df

misses = []
to_introduce = [2, 3, 4, 5]
# subjects = ['ZFM-05236', 'ZFM-05245']
# subjects = ['CSHL045', 'CSHL046', 'CSHL049', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL054', 'CSHL055',
#             'CSHL059', 'CSHL061', 'CSHL062', 'CSHL065', 'CSHL066']  # CSHL063 no good
# subjects = ['CSHL_007', 'CSHL_014', 'CSHL_015', 'CSHL_018', 'CSHL_019', 'CSHL_020', 'CSH_ZAD_001', 'CSH_ZAD_011',
#             'CSH_ZAD_017', 'CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022', 'CSH_ZAD_024', 'CSH_ZAD_025', 'CSH_ZAD_026']
# subjects = ['KS002', 'KS003', 'KS005',
#             'KS014', 'KS015', 'KS016', 'KS017', 'KS018', 'KS019', 'KS021', 'KS022', 'KS023', 'NYU-06']  # , 'KS020' no good
# subjects = ['SWC_008',
#             'SWC_009', 'SWC_018', 'SWC_021', 'SWC_022', 'SWC_023', 'ZM_1897', 'ZM_1898', 'ZM_3003', 'ibl_witten_07']
# subjects = ['ibl_witten_13', 'ibl_witten_14', 'ibl_witten_15', 'ibl_witten_16', 'ibl_witten_17', 'ibl_witten_18',
#             'ibl_witten_19', 'ibl_witten_20']  # downloaded already

# try still: ZM_1736, 25 sessions; SWC_060, 21 sess; ZM_1150 29 sess; DY_016 30 sess
# subjects = ['CSH_ZAD_029']  # 'DY_013']  #  35, 27, 20, 30
# 'CSHL060',  gives error
# can't find biased sessions of: 'NYU-17', 'NYU-18', 'CSHL050', 'CSHL021', 'KS013', 'SWC_026', 'SWC_031', 'ZM_2104', 'ZM_2407', 'ZM_2406', 'CSK_ZAD_013', 'CSHL048', 'CSHL056', 'CSHL057', 'CSH_ZAD_018', 'CSH_ZAD_020', 'CSH_ZAD_023', 'SWC_020', 'SWC_019'
# also: 'KS012', 'KS011', 'KS010', 'KS009', 'KS008', 'KS007', 'KS006', 'CSHL064',
# 'KS004', 'SWC_015', 'CSHL047' has none in gocue or rt
# weid problem: , 'CSHL058'
# subjects = ["CSHL_001", "CSHL_002", "CSHL_003", "CSHL_005", "CSHL_007", "CSHL_008", "CSHL_010", "CSHL_012",
#             "CSHL_014", "CSHL_015", "IBL-T1", "IBL-T2", "IBL-T3", "IBL-T4", "ibl_witten_04", "ibl_witten_05",
#             "ibl_witten_06", "ibl_witten_07", "ibl_witten_12", "ibl_witten_13", "ibl_witten_14", "ibl_witten_15",
#             "ibl_witten_16", "KS003", "KS005", "KS019", "NYU-01", "NYU-02", "NYU-04", "NYU-06", "ZM_1367", "ZM_1369",
#             "ZM_1371", "ZM_1372", "ZM_1743", "ZM_1745", "ZM_1746"]  # zoe's subjects
subjects = ["fip_{}".format(i) for i in list(range(13, 17)) + list(range(26, 43))]

data_folder = 'session_data'
# why does CSHL058 not work?

old_style = False
if old_style:
    print("Warning, data can have splits")
    data_folder = 'session_data_old'
bias_eids = []

print("#########################################")
print("Waring, rt's removed, find with   # RTODO")
print("#########################################")

short_subjs = []
names = []

pre_bias = []
entire_training = []
for subject in subjects:
    print('_____________________')
    print(subject)

    eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2025-01-01'], details=True)

    start_times = [sess['date'] for sess in sess_info]
    nums = [sess['number'] for sess in sess_info]

    print("original # of eids {}".format(len(eids)))

    test = [(y, x) for y, x in sorted(zip(start_times, eids))]
    pickle.dump(test, open("./{}/{}_session_names.p".format(data_folder, subject), "wb"))

    eids = [x for _, x in sorted(zip(start_times, eids))]
    dates = [x for x, _ in sorted(zip(start_times, eids))]
    nums = [x for _, x in sorted(zip(start_times, nums))]
    start_times = sorted(start_times)

    prev_date = None
    prev_num = -1
    fixed_dates = []
    fixed_eids = []
    fixed_start_times = []
    additional_eids = []
    for d, e, n, st in zip(dates, eids, nums, start_times):
        if d != prev_date:
            fixed_dates.append(d)
            fixed_eids.append(e)
            additional_eids.append([])
            fixed_start_times.append(st)
        else:
            assert n > prev_num
            if n == 1:
                additional_eids.append([e])
            elif n > 1:
                additional_eids[-1].append(e)
        prev_date = d
        prev_num = n

    # in case you want it
    if old_style:
        fixed_eids = eids
        fixed_dates = dates
        additional_eids = [[] for e in fixed_eids]
    print("remaining # of eids {}".format(len(fixed_eids)))

    performance = np.zeros(len(fixed_eids))
    easy_per = np.zeros(len(fixed_eids))
    hard_per = np.zeros(len(fixed_eids))
    bias_start = 0

    info_dict = {'subject': subject, 'dates': fixed_dates, 'eids': fixed_eids}
    info_dict = {'subject': subject, 'dates': [st for st in sorted(fixed_start_times)], 'eids': eids, 'date_and_session_num': {}}
    contrast_set = {0, 1, 9, 10}

    rel_count = -1

    assert len(fixed_eids) == len(additional_eids) == len(fixed_start_times)
    for i, (eid, extra_eids, start_time) in enumerate(zip(fixed_eids, additional_eids, fixed_start_times)):

        try:
            trials = one.load_object(eid, 'trials')
            if 'choice' not in trials:
                continue
        except Exception:
            print('skipped session')
            continue

        if trials['probabilityLeft'] is None: # originally also "or df is None", if this gets reintroduced, probably need to download all data again
            if rel_count != -1:
                print('lost session, problem')
                misses.append((subject, i))
            continue
        rel_count += 1

        print("rel_count {}".format(rel_count))
        df = get_df(trials)
        # if there are some duplicate dates:
        for extra_eid in extra_eids:
            print('merging, orig size: {}'.format(len(df)))
            try:
                extra_trials = one.load_object(extra_eid, 'trials')
                if 'choice' not in trials:
                    continue
            except Exception:
                print('skipped merging session')
                continue
            df2 = get_df(extra_trials)
            df = pd.concat([df, df2], ignore_index=1)
            print('new size: {}'.format(len(df)))

        current_contrasts = set(df['signed_contrast'])
        diff = current_contrasts.difference(contrast_set)
        for c in to_introduce:
            if c in diff:
                info_dict[c] = rel_count
        contrast_set.update(diff)

        performance[i] = np.mean(df['feedback'])
        easy_per[i] = np.mean(df['feedback'][np.logical_or(df['signed_contrast'] == 0, df['signed_contrast'] == 10)])
        hard_per[i] = np.mean(df['feedback'][df['signed_contrast'] == 5])

        if bias_start == 0 and np.array_equal(np.unique(df['block']), [0.2, 0.5, 0.8]):
            bias_start = i
            print("bias start {}".format(rel_count))
            info_dict['bias_start'] = rel_count
            if bias_start < 33:
                short_subjs.append(subject)

        if bias_start:
            bias_eids.append(eid)

        print(start_time)
        if start_time == datetime.date(2022, 12, 30):
            quit()
        pickle.dump(df, open("./{}/{}_df_{}.p".format(data_folder, subject, rel_count), "wb"))
        info_dict['date_and_session_num'][rel_count] = start_time
        info_dict['date_and_session_num'][start_time] = rel_count

        side_info = np.zeros((len(df), 2))
        side_info[:, 0] = df['block']
        side_info[:, 1] = df['feedback']
        pickle.dump(side_info, open("./{}/{}_side_info_{}.p".format(data_folder, subject, rel_count), "wb"))

        fit_info = np.zeros((len(df), 3))
        fit_info[:, 0] = df['signed_contrast']
        fit_info[:, 1] = df['response']
        print(len(df))
        # fit_info[:, 2] = df['rt']  # RTODO
        pickle.dump(fit_info, open("./{}/{}_fit_info_{}.p".format(data_folder, subject, rel_count), "wb"))

    if rel_count == -1:
        continue
    plt.figure(figsize=(11, 8))
    print(performance)
    plt.plot(performance, label='Overall')
    plt.plot(easy_per, label='100% contrasts')
    plt.plot(hard_per, label='0% contrasts')
    plt.axvline(bias_start - 0.5)
    skip_count = 0
    for p in performance:
        if p == 0.:
            skip_count += 1
        else:
            break
    for c in to_introduce:
        if c == 5 and subject.startswith("fip"):
            continue
        plt.axvline(info_dict[c] + skip_count, ymax=0.85, c='grey')
    if not subject.startswith("fip"):
        plt.annotate('Pre-bias', (bias_start / 2, 1.), size=20, ha='center')
        plt.annotate('Bias', (bias_start + (i - bias_start) / 2, 1.), size=20, ha='center')
    plt.title(subject, size=22)
    plt.ylabel('Performance', size=22)
    plt.xlabel('Session', size=22)
    plt.xticks(size=16)
    plt.xticks(size=16)
    plt.ylim(bottom=0)
    plt.xlim(left=0)

    sns.despine()
    plt.tight_layout()
    if not old_style:
        plt.savefig('./figures/behavior/all_of_trainig_{}'.format(subject))
    plt.close()

    # print(bias_eids)
    if not subject.startswith("fip"):
        pre_bias.append(info_dict['bias_start'])
    entire_training.append(rel_count + 1)

    info_dict['n_sessions'] = rel_count
    pickle.dump(info_dict, open("./{}/{}_info_dict.p".format(data_folder, subject), "wb"))
print(misses)
print(short_subjs)

print(pre_bias)
print(entire_training)
