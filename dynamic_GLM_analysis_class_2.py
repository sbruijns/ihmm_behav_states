import matplotlib
matplotlib.use('Agg')
import multiprocessing as mp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pyhsmm
import pyhsmm.basic.distributions as distributions
import pickle
from itertools import permutations, product
import seaborn as sns
from scipy.stats import nbinom
import sys
from matplotlib.patches import Rectangle
from scipy.special import logsumexp
import pyhsmm.util.profiling as prof
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MaxNLocator
import warnings
from communal_funcs import contrasts_L, contrasts_R, weights_to_pmf

colors = np.genfromtxt('colors.csv', delimiter=',')

# Todo: fix !!!'s
# use self.models[-1] not self.models[0]

np.set_printoptions(suppress=True)

fs = 16
num_to_cont = dict(zip(range(11), [-1., -0.5, -.25, -.125, -.062, 0., .062, .125, .25, 0.5, 1.]))
contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}

all_cont_ticks = (np.arange(11), [-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])
bias_cont_ticks = (np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
conts = np.array([-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])

cmap = cm.get_cmap('coolwarm')

def state_sess_interpol(states, interpol_n=None):
    interpol_n = min([len(s) for s in states_per_sess]) if interpol_n is None else interpol_n
    state_array = np.zeros((10, interpol_n))
    for s in states:
        temp = np.interp(np.linspace(0, 1, interpol_n) * len(s),
                         np.arange(len(s)), s)
        for i, t in enumerate(temp):
            if t % 1 != 0:
                state_array[int(t), i] += 1 - (t % 1)
                state_array[int(t) + 1, i] += t % 1
            else:
                state_array[int(t), i] += 1
    state_array = state_array[state_array.sum(1) != 0]
    return state_array[::-1]

def pmf_acc(x, w):
    """
    Given probabilities of answering right x, and relative contrast frequencies w, compute accuracy.

    A contrast is assumed to be equiprobable right or left (no bias here).
    But e.g. 0 contrast can appear more frequently than rest.
    """
    n = len(x)
    x[:int(n / 2)] = 1 - x[:int(n / 2)]
    x[int(n / 2)] = 0.5
    return np.sum(x * (w / w.sum()))

def extended_pmf_acc(pmf, conts):
    # compute accuracy of given pmf given the current contrasts (session-level accuracy)
    acc = 0
    for cont in conts:
        if cont < 0:
            acc += 1 - pmf[contrast_to_num[cont]]
        elif cont > 0:
            acc += pmf[contrast_to_num[cont]]
        else:
            acc += 0.5
    return acc / len(conts)

# remove threshold from class !!!
class MCMC_result:

    def __init__(self, models, infos, data, sessions, fit_variance, save_id, threshold=0.01, seq_start=0, dur='yes'):

        self.models = models
        self.name = infos['subject']
        self.seq_start = seq_start
        self.type = sessions
        self.fit_variance = str(fit_variance).replace('.', '_')
        self.save_id = save_id
        self.assign_counts = None  # Placeholder
        self.infos = infos
        self.data = data

        self.n_samples = len(self.models)
        self.n_sessions = len(self.models[-1].stateseqs)
        self.n_datapoints = sum([len(s) for s in self.models[-1].stateseqs])
        self.n_all_states = self.models[-1].num_states
        self.threshold = threshold

        self.calc_full_state_posterior()
        total_post = np.zeros(self.n_all_states)
        for i, post in enumerate(self.full_posteriors):
            total_post += post.sum(axis=1)
        self.proto_states = np.where(total_post / self.n_datapoints > self.threshold)[0]
        self.n_pstates = len(self.proto_states)
        # print(total_post / self.n_datapoints)
        # print(self.proto_states)

        self.n_contrasts = 11
        self.cont_ticks = all_cont_ticks

        self.state_to_color = {}
        self.state_to_ls = {}
        linestyles = ['-', '--', '-.', ':', '-', '-', '-', '-', '-', '-', '-', '-', '-']
        ls_counter = 0
        self.state_tendency = {}
        self.pmf_means = {}
        self.dur = dur
        tendencies = []
        for s in self.proto_states:
            pmf = np.zeros((len(self.models), self.n_contrasts))
            for i, m in enumerate(self.models):
                if self.fit_variance == '0':
                    pmf[i] = weights_to_pmf(m.obs_distns[s].weights[:, 0])
                else:
                    pmf[i] = weights_to_pmf(np.mean(m.obs_distns[s].weights, axis=0))
            # !!! only do this once: also needed for state_development
            # maybe use a better we to exclude contrasts than uncertainty?
            percentiles = np.percentile(pmf, [2.5, 97.5], axis=0)
            if self.type == 'bias':
                defined_points = np.ones(self.n_contrasts, dtype=bool)
            else:
                uncertainty = np.abs(percentiles[0] - percentiles[1])
                defined_points = uncertainty < 0.5
                defined_points[[0, 1, -2, -1]] = True
            self.pmf_means[s] = pmf[:, defined_points].mean(axis=0)
            temp = np.sum(pmf[:, defined_points].mean(axis=0)) / (np.sum(defined_points))
            #self.state_to_color[s] = colors[int((temp + 2 * (temp - 0.5)) * 101)]
            self.state_tendency[s] = int(temp * 101 - 1)
            self.state_to_color[s] = colors[int(temp * 101 - 1)]
            if 45 <= int(temp * 101 - 1) <= 55:
                self.state_to_ls[s] = linestyles[ls_counter]
                ls_counter += 1
            else:
                self.state_to_ls[s] = '-'
            tendencies.append(temp)

        ordered_states = [x for _, x in sorted(zip(tendencies, self.proto_states))]
        self.state_map = dict(zip(ordered_states, range(self.n_pstates)))
        print(self.state_map)
        self.count_assigns()
        self.calc_state_posterior()

        if self.type == 'prebias':
            active_sessions = np.array([p.mean(axis=1) for p in self.posteriors]) > 0.05
            num_sessions = np.sum(active_sessions, axis=0)
            active_sessions[-1, num_sessions == 0] = True
            self.bias_weight_present = [np.max(np.where(s)) >= self.infos[2] for s in active_sessions.T]
        else:
            self.bias_weight_present = np.ones(self.n_pstates)
        # self.bias_weight_present[2] = False
        # self.bias_weight_present[1] = False
        # print('making a mess, confirm with input')
        # input()


    def matrix(self, i):
        """Return transition matrix reduced to important states."""
        temp = self.models[i].trans_distn.trans_matrix[self.proto_states]
        temp = temp[:, self.proto_states]
        return temp

    def param_hist(self, param_func, truth=None, save=False, xlabel=''):
        """Histogram of whatever parameters for all samples, pick out parameter with lambda func."""
        for s in self.proto_states:
            params = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                params[i] = param_func(m, s)
            plt.hist(params, label='Posterior samples')
            if truth is not None:
                plt.axvline(truth, color='red', label='Truth')
                plt.legend(frameon=False, fontsize=15)
            sns.despine()
            plt.title(title.format(s), size=18)
            plt.ylabel('Occurences', size=16)
            plt.xlabel(xlabel, size=16)
            plt.tight_layout()
            if save:
                plt.savefig("dynamic_GLM_figures/param hist {} {} {}".format(self.save_id, s, param_func))
            plt.show()

    def param_hist_nonstate(self, param_func, truth=None, title='{}', save=False, xlabel='', show=True):
        """Histogram of whatever parameters for all samples, pick out parameter with lambda func."""
        plt.figure(figsize=(16,9))
        params = np.zeros(self.n_samples)
        for i, m in enumerate(self.models):
            params[i] = param_func(m)
        plt.hist(params, label='Posterior samples')
        if truth is not None:
            plt.axvline(truth, color='red', label='Truth')
            plt.legend(frameon=False, fontsize=15)
        sns.despine()
        plt.title(title, size=18)
        plt.ylabel('Occurences', size=16)
        plt.xlabel(xlabel, size=16)
        plt.tight_layout()
        if save:
            print("dynamic_GLM_figures/" + title)
            plt.savefig("dynamic_GLM_figures/" + title)
        if show:
            plt.show()
        else:
            plt.close()

    def duration_overview(self):
        """Multiple plots giving information about posterior of duration distributions."""
        for s in self.proto_states:
            r_params = np.zeros(self.n_samples)
            p_params = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                r_params[i] = m.dur_distns[s].r
                p_params[i] = m.dur_distns[s].p

            plt.hist(r_params)
            plt.title(s, color=self.state_to_color[s])
            plt.show()

            plt.scatter(r_params, p_params)
            plt.show()

            plt.plot(r_params, label='r')
            plt.plot(p_params, label='p')
            plt.plot(r_params * p_params / (1 - p_params), label='mean')
            plt.legend()
            plt.show()

    def transition_hists(self):
        """One histogram for each possible (important) transition probability for all samples."""
        for t in permutations(self.proto_states, 2):
            transitions = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                transitions[i] = m.trans_distn.trans_matrix[t]
            plt.hist(transitions)
            plt.xlim(left=0, right=1)
            plt.title(t)
            plt.show()

    def count_assigns(self):
        self.assign_counts = np.zeros((self.n_samples, self.n_pstates))
        self.total_counts = np.zeros(self.n_samples)
        for i, m in enumerate(self.models):
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a = np.unique(flat_list, return_counts=1)
            states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]

            self.total_counts[i] = counts.sum()  # this is only model dependent if I threshold...
            for s, c in zip(states, counts):
                if s in self.proto_states:
                    self.assign_counts[i, self.state_map[s]] = c

    def percent_counts(self):
        """
        Seems to calculate the overall percentage of states.

        Could probably be computed faster/better
        """
        counts = np.zeros(self.n_all_states)
        trial_n = self.total_counts[0]

        for m in self.models:
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a, temp_counts = np.unique(flat_list, return_counts=1)
            counts[a] += temp_counts

        self.percent = counts / self.n_samples / trial_n

    def assign_hist(self):
        """Make simple histogram over how many trials are assigned to proto_states."""
        for i in range(self.n_pstates):
            plt.hist(self.assign_counts[:, i])
        plt.show()

    def assign_evolution(self, show=True, show_like=False, unmasked_data=None):
        """Plot how the number of trials assigned to proto_states changes during sampling (check convergence)."""
        plt.figure(figsize=(11, 8))
        if show_like:
            plt.subplot(2, 1, 1)
        for s in self.proto_states:
            plt.plot(self.assign_counts[:, self.state_map[s]], c=self.state_to_color[s], ls=self.state_to_ls[s])

        plt.xlabel('Iteration', size=22)
        plt.ylabel('# assigned trials', size=22)
        plt.xticks(size=20-3)
        plt.title(self.save_id)

        if show_like:
            plt.subplot(2, 1, 2)
            lls_within = self.eval_pred_perf(show=False, type=['only_obs', 'll', 'pred_ll'][2])
            if unmasked_data is not None:
                lls_out_of = self.eval_cross_val(unmasked_data=unmasked_data, show=False)
                print("mean on cv: {}".format(lls_out_of.mean()))
                plt.plot(lls_out_of, label="cross_val ll")
            plt.plot(lls_within, label="within sample ll")
            opt_ll = np.argmax(lls_within)
            plt.axvline(np.argmax(lls_within), color='r')
            plt.axhline(-0.38766323073000963, color='r')
            plt.legend()

        sns.despine()
        plt.tight_layout()
        plt.savefig("dynamic_GLM_figures/convergence/count evolution {}".format(self.save_id))
        if show:
            plt.show()
        else:
            plt.close()

        if show_like:
            plt.scatter(lls_within, lls_out_of)
            plt.xlabel('training ll')
            plt.ylabel('testing ll')

            m, b = np.polyfit(lls_within, lls_out_of, 1)
            plt.title("m: {}, b: {}".format(m, b))
            plt.plot([lls_within.min() - 0.01, lls_within.max() + 0.01], [m*(lls_within.min() - 0.01) + b, m*(lls_within.max() + 0.01) + b], 'k')
            # plt.gca().set_aspect('equal', adjustable='box')
            # plt.xlim(left=0.74, right=0.78)
            # plt.ylim(bottom=0.73, top=0.77)
            # plt.savefig('temp')
            plt.show()
        if unmasked_data is not None:
            return lls_within, lls_out_of

    def block_yielder(self, n=1000):
        """Yield block data."""
        add_on = self.infos['bias_start'] if self.type == 'bias' or self.type == 'prebias_plus' else 0
        for seq_num in range(n):
            name = "CSHL062" if self.name == "Sim_04" else self.name
            blocks = pickle.load(open("./session_data/{}_side_info_{}.p".format(name, seq_num + add_on), "rb"))[:, 0]
            yield blocks

    def feedback_yielder(self, n=1000):
        """Yield feedback data."""
        for seq_num in range(n):
            add_on = self.infos['bias_start'] if self.type == 'bias' else 0
            feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, seq_num + add_on), "rb"))[:, 1]
            #print(self.infos['eids'][seq_num + file_add + add_on])
            yield feedback

    def calc_state_posterior(self):
        """Calculate posterior over states by averaging over samples"""
        posteriors = []
        for seq_num in range(self.n_sessions):
            posterior = np.zeros((self.n_pstates, len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in self.proto_states:
                    posterior[self.state_map[s]] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / self.n_samples)
        self.posteriors = posteriors

    def calc_full_state_posterior(self):
        """Calculate posterior over ALL states by averaging over samples"""
        posteriors = []
        for seq_num in range(self.n_sessions):
            posterior = np.zeros((self.n_all_states, len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in range(self.n_all_states):
                    posterior[s] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / self.n_samples)
        self.full_posteriors = posteriors

    # !!! TODO
    def make_this_func(self):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples

    def performance_interpolation(self, windowsize):
        # returns an interpolation of performance, to compare with the best state interpolation
        fy = self.feedback_yielder(self.n_sessions)
        interpolation = np.zeros(100)
        for seq_num in range(self.n_sessions):
            feedback = next(fy)
            filtered_fb = np.convolve(feedback, np.ones(windowsize), mode='valid') / windowsize
            fb_interpolation = np.interp(np.linspace(0, 1, 100) * filtered_fb.shape[0],
                                         np.arange(filtered_fb.shape[0]), filtered_fb)
            interpolation += fb_interpolation
        return interpolation / self.n_sessions


    def contrasts_plot(self, until=None, save=False, show=False, plot_blocks=True, plot_0s=False, plot_perf=False, dpi='figure'):
        n = self.n_sessions if until is None else min(until, self.n_sessions)
        # priors = pickle.load(open("./session_data/{}_priors_act.p".format('CSHL062'), "rb"))
        by = self.block_yielder(n)
        fy = self.feedback_yielder(n)
        for seq_num in range(n):
            if seq_num + 1 != 11:
                continue
            c_n_a = self.data[seq_num]

            plt.figure(figsize=(18, 9))
            for s in self.proto_states:
                prevalent = np.mean(self.posteriors[seq_num][self.state_map[s]]) > 0.02
                if prevalent:
                    pmfs = np.zeros((self.n_samples, self.n_contrasts))
                    for i, m in enumerate(self.models):
                        pmfs[i] = weights_to_pmf(m.obs_distns[s].weights[seq_num], with_bias=self.bias_weight_present[self.state_map[s]])
                    acc = np.round(pmf_acc(np.mean(pmfs, axis=0), np.ones(self.n_contrasts) * 100), 2)

                label = "State {}".format(self.state_map[s], acc) if prevalent else None
                plt.plot(-1 + 2 * self.posteriors[seq_num][self.state_map[s]], color=self.state_to_color[s], lw=4, label=label, ls=self.state_to_ls[s])
            # plt.plot(posterior.sum(axis=0) / self.n_samples, 'k')

            # try:
            #     wMode, std = pickle.load(open("./session_data/psytrack/result {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
            #     trialnumbers = pickle.load(open("./session_data/psytrack/numbers {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
            # wMode[0] -= np.min(wMode[0])
            # wmax = np.max(wMode[0])
            # plt.plot(trialnumbers, wMode[0] / wmax, color='k', alpha=0.5)

            if plot_0s:
                self.plot_0_choices(contrasts_and_answers=c_n_a, window_length=5)

            if plot_perf:
                feedback = next(fy)
                assert len(feedback) == len(self.models[-1].stateseqs[seq_num]), "Block length not aligned"
                self.plot_performance(feedback)

            if (plot_blocks and self.type == 'bias') or (plot_blocks and seq_num >= self.infos['bias_start']):
                blocks = next(by)
                print("Block length assertion don\'t work no more")
                # assert len(blocks) == len(self.models[-1].stateseqs[seq_num]), "Block length not aligned"
                self.plot_block_background(blocks)

            ms = 4
            noise = np.zeros(len(c_n_a))# np.random.rand(len(c_n_a)) * 0.4 - 0.2
            kernel_len = 10
            kernel = np.flip(np.exp(-np.arange(kernel_len) * 0.45), axis=0)
            smoothed_action_prob = np.zeros(c_n_a.shape[0])
            for i in range(c_n_a.shape[0]):
                start = max(0, i - kernel_len)
                current_len = i - start
                if i == 0:
                    smoothed_action_prob[i] = 0.5
                else:
                    smoothed_action_prob[i] = np.sum(c_n_a[start:i, 1] / 2 * kernel[-current_len:] / np.sum(kernel[-current_len:]))
            # plt.plot((self.n_contrasts - 1) * smoothed_action_prob, 'k', label="Smoothed choices")

            mask = c_n_a[:, -1] == 0
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] - c_n_a[mask, 1], 'o', c='b', ms=ms, label='Leftward')

            mask = c_n_a[:, -1] == 1
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] - c_n_a[mask, 1], 'o', c='r', ms=ms, label='Rightward')

            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=26)
            # plt.yticks(*self.cont_ticks, size=22-2)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(answer rightwards) and contrast', size=26)
            plt.xlabel('Trial', size=28)
            sns.despine()
            plt.xlim(left=600, right=850)
            plt.legend(frameon=False, fontsize=22, bbox_to_anchor=(1.1, 0.5))
            plt.tight_layout()
            if save:
                plt.savefig("dynamic_GLM_figures/all posterior and contrasts {}, sess {}.png".format(self.save_id, seq_num), dpi=dpi)#, bbox_inches='tight')
            if show:
                plt.show()
            else:
                plt.close()

    def cv_trial_plot(self, unmasked_data, show=True, plot_perf=False, plot_cv_perf=True, plot_pred=None):
        # plot_pred takes one single model number as input, and plots the predictions of that model on top of the contrasts
        n = self.n_sessions
        fy = self.feedback_yielder(n)
        for seq_num in range(n):
            c_n_a = self.data[seq_num]

            plt.figure(figsize=(18, 9))

            if plot_perf:
                feedback = next(fy)
                self.plot_performance(feedback)

            ms = 8
            noise = np.zeros(len(c_n_a))  # np.random.rand(len(c_n_a)) * 0.4 - 0.2

            mask = c_n_a[:, -1] == 0
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] - c_n_a[mask, 1], 'o', c='g', ms=ms, label='Leftward')

            mask = c_n_a[:, -1] == 1
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] - c_n_a[mask, 1], 'o', c='r', ms=ms, label='Rightward')

            mask = np.isnan(c_n_a[:, -1])
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] - c_n_a[mask, 1], 'o', c='k', ms=ms, label='withheld')
            super_mask = np.logical_and(mask, unmasked_data[seq_num][:, -1] == 1)
            plt.plot(np.where(super_mask)[0], noise[super_mask] + c_n_a[super_mask, 0] - c_n_a[super_mask, 1], 'o', c='r', ms=ms - 3)
            super_mask = np.logical_and(mask, unmasked_data[seq_num][:, -1] == 0)
            plt.plot(np.where(super_mask)[0], noise[super_mask] + c_n_a[super_mask, 0] - c_n_a[super_mask, 1], 'o', c='g', ms=ms - 3)

            if plot_pred is not None:
                temp = unmasked_data[seq_num].copy()
                m = self.models[plot_pred]
                temp[:, -1] = 1
                lls = np.zeros(temp.shape[0])
                for s in range(self.n_all_states):
                    mask = m.stateseqs[seq_num] == s
                    if mask.sum() > 0:
                        ll = m.obs_distns[s].log_likelihood(temp[mask], seq_num)
                        lls[mask] = ll
                plt.plot((1 - lls) * 2 - 1)

            if plot_cv_perf:
                lls = np.zeros((self.n_samples, np.sum(mask)))
                for i, m in enumerate(self.models):
                    for s in range(self.n_all_states):
                        super_mask = m.stateseqs[seq_num][mask] == s
                        if super_mask.sum() > 0:
                            ll = m.obs_distns[s].log_likelihood(unmasked_data[seq_num][mask][super_mask], seq_num)
                            lls[i, np.where(super_mask)[0]] = ll

                plt.errorbar(np.where(mask)[0], -1 + 2 * np.mean(lls, axis=0), np.std(lls, axis=0))

            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=22)
            # plt.yticks(*self.cont_ticks, size=22-2)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(answer rightwards) and contrast', size=22)
            plt.xlabel('Trial', size=22)
            sns.despine()
            # plt.xlim(left=250, right=500)
            plt.legend(frameon=False, fontsize=22, bbox_to_anchor=(1.01, 0.5))
            plt.tight_layout()

            if show:
                plt.show()
            else:
                plt.close()

    def plot_0_choices(self, contrasts_and_answers, window_length=4):
        """Plot the tendency towards one side on 0 contrasts, given array of contrasts and choices."""
        points = []
        choice_average = []
        internal_count = 0
        window = np.zeros(window_length)
        for i, ca in enumerate(contrasts_and_answers):
            if ca[0] == 0. and ca[1] == 0.:
                points.append(i)
                window[internal_count % window_length] = ca[-1] == 0
                choice_average.append(2 * np.mean(window) - 1)
                internal_count += 1
        plt.plot(points[:-window_length // 2], choice_average[window_length // 2 + 1:], 'k', label="Strong left\ncontrast answers")

    def plot_performance(self, feedback, windowsize=7):
        """Plot local performance."""
        performance = np.zeros(len(feedback) - 2 * windowsize + 1)
        for i, pos in enumerate(np.arange(windowsize, len(feedback) - windowsize + 1)):
            performance[i] = np.mean(feedback[pos - windowsize: pos])
        plt.plot(np.arange(windowsize, len(feedback) - windowsize + 1), (self.n_contrasts - 1) * performance, 'k')

    def sequence_heatmap(self, normalization='adaptive', until=None):
        n = self.n_sessions if until is None else until
        for seq_num in range(n):
            for s1, s2 in product(self.proto_states, repeat=2):
                heatmap = np.zeros((len(self.models[0].stateseqs[seq_num]), len(self.models[0].stateseqs[seq_num])))
                normalize_vector = np.zeros(len(self.models[0].stateseqs[seq_num]), dtype=np.int32)
                for m in self.models:
                    for t in range(len(m.stateseqs[seq_num])):
                        if m.stateseqs[seq_num][t] == s1:
                            normalize_vector[t] += 1
                            heatmap[t] += m.stateseqs[seq_num] == s2

                if normalization == 'adaptive':
                    mask = normalize_vector.nonzero()
                    heatmap[mask] = heatmap[mask] / normalize_vector[mask, None]
                    if s1 == s2:
                        heatmap -= np.diag(normalize_vector >= 1)  # empty diagonal if we look at autocorr
                    heatmap += np.diag(normalize_vector / self.n_samples)
                elif normalization == 'total':
                    heatmap /= self.n_samples

                assert heatmap.max() <= 1.
                assert heatmap.min() >= 0.
                sns.heatmap(heatmap, vmin=0., vmax=1., square=True)
                title = "Sequence {}, states {} and {}".format(seq_num, s1, s2)
                plt.title(title)
                plt.savefig("dynamic_GLM_figures/state_heatmaps/" + normalization + '_' + title)
                plt.close()

    def single_state_resample(self, n):
        model = self.models[-1]
        states = [np.zeros((self.n_pstates, x.shape[0])) for x in model.datas]

        for i in range(n):
            model.resample_states()
            for j, seq in enumerate(model.stateseqs):
                for s in self.proto_states:
                    states[j][self.state_map[s]] += seq == s

        for seq_num, blocks in zip(range(n), self.block_yielder(n)):  # how does n work here?

            self.plot_block_background(blocks)

            seq = states[seq_num]
            for s in self.proto_states:
                plt.plot(seq[self.state_map[s]] / n, color=self.state_to_color[s], label=s)

            sns.despine()
            plt.legend()
            plt.show()

    def plot_block_background(self, blocks, block_to_color=None):
        """Plot the blocks in background for other plots."""
        alpha = 0.1
        start = 0
        curr = blocks[0]
        block_to_color = {0.2: 'b', 0.8: 'r', 0.5: 'w'} if block_to_color is None else block_to_color
        try:
            for i, b in enumerate(blocks):
                if b != curr:
                    plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
                    curr = b
                    start = i
            plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
        except KeyError as ke:
            print(ke)

    def plot_glm_weights(self):
        # GLM_weights = [np.array([-4.5, 4.3, 0, 0., 1.2, -0.7]),
        #                np.array([-4.5, 3.2, 0, 1.3, 0.3, -1.5])]
        # GLM_weights = [np.array([-4.5, 3.2, 1.3, 0., 0.3, -1.5]),
        #                np.array([-1, 1.2, 2.1, 0.5, 0.1, 1]),
        #                np.array([-4.5, 4.3, 0, 0., 1.2, -0.7]),
        #                np.array([-0.1, 0.2, 0, 0.1, 3.9, -1.])]
        names = {0: 'contR', 1: 'contL', 2: 'prevAns', 3: 'prevTruth', 4: 'bias'}
        for j, s in enumerate(self.proto_states):
            all_weights = np.empty((self.n_samples, self.n_sessions, 5))
            for i, m in enumerate(self.models):
                all_weights[i] = m.obs_distns[s].weights
            for i in range(5):
                # plt.axhline(GLM_weights[j][i], color='r')
                p = plt.plot(np.mean(all_weights[:, :, i], axis=0), label=names[i])
                temp = np.percentile(all_weights[:, :, i], [2.5, 97.5], axis=0)
                plt.fill_between(np.arange(self.n_sessions), temp[0], temp[1], alpha=0.2, color=p[0].get_color())
            plt.title("{}, state {}".format(self.name, self.state_map[s]))
            plt.legend()
            plt.savefig("dynamic_GLM_figures/convergence/glm weights {} {}".format(self.save_id, self.state_map[s]))
            plt.show()


    def state_development(self, save=True, show=True, dpi='figure'):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples
        fig = plt.figure(figsize=(16, 9))
        spec = gridspec.GridSpec(ncols=100, nrows=3 * self.n_pstates+5, figure=fig)
        spec.update(hspace=0.) # set the spacing between axes.
        ax0 = fig.add_subplot(spec[:5, :69])  # performance line
        ax1 = fig.add_subplot(spec[5:, :69])  # state lines
        ax2 = fig.add_subplot(spec[5:, 76:86])
        ax3 = fig.add_subplot(spec[5:, 90:])

        if not self.name.startswith('Sim_'):
            ax0.plot(1 + 0.25, 0.6, 'ko', ms=18)
            ax0.plot(1 + 0.25, 0.6, 'wo', ms=16.8)
            ax0.plot(1 + 0.25, 0.6, 'ko', ms=16.8, alpha=abs(num_to_cont[0]))

            ax0.plot(1 + 0.25, 0.6 + 0.2, 'ko', ms=18)
            ax0.plot(1 + 0.25, 0.6 + 0.2, 'wo', ms=16.8)
            ax0.plot(1 + 0.25, 0.6 + 0.2, 'ko', ms=16.8, alpha=abs(num_to_cont[1]))
            if self.type != 'bias':
                current, counter = 0, 0
                for c in [2, 3, 4, 5]:
                    if self.infos[c] == current:
                        counter += 1
                    else:
                        counter = 0
                    ax0.axvline(self.infos[c] + 1, color='gray', zorder=0)
                    ax1.axvline(self.infos[c] + 1, color='gray', zorder=0)
                    ax0.plot(self.infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'ko', ms=18)
                    ax0.plot(self.infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'wo', ms=16.8)
                    ax0.plot(self.infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
                    current = self.infos[c]
        if self.type == 'all' or self.type == 'prebias_plus':
            ax1.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.annotate('Bias', (self.infos['bias_start'] + 1 - 0.5, 0.68), fontsize=22)

        dur_max = 600  # TODO: dont change params just once, also below
        posterior = np.zeros((self.n_pstates, dur_max, self.n_samples))
        for j, s in enumerate(self.proto_states):
            points = np.arange(dur_max)
            for i, m in enumerate(self.models):
                if self.dur == 'yes':
                    posterior[j, :, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!
        total_max = posterior.mean(axis=2).max() * 1.05
        dur_probs = np.zeros(2 * self.n_pstates)
        for s in self.proto_states:
            ax1.fill_between(range(1, 1 + self.n_sessions), self.state_map[s] - 0.5,
                             self.state_map[s] + states_by_session[self.state_map[s]] -0.5, color=self.state_to_color[s])

            pmfs = np.zeros((self.n_samples, self.n_contrasts))
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i, m in enumerate(self.models):
                if self.fit_variance == '0':
                    pmfs[i] = weights_to_pmf(m.obs_distns[s].weights, with_bias=self.bias_weight_present[self.state_map[s]])
                else:
                    pmfs[i] = weights_to_pmf(np.mean(m.obs_distns[s].weights, axis=0), with_bias=self.bias_weight_present[self.state_map[s]])
                if self.dur == 'yes':
                    posterior[:, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!

            alpha_level = 0.3
            ax2.axvline(0.5, c='grey', alpha=alpha_level, zorder=4)
            temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
            if self.type == 'bias':
                defined_points = np.ones(self.n_contrasts, dtype=bool)
            else:
                if self.bias_weight_present[self.state_map[s]]:
                    defined_points = np.ones(self.n_contrasts, dtype=bool)
                else:
                    defined_points = np.zeros(self.n_contrasts, dtype=bool)
                    defined_points[[0, 1, -2, -1]] = True
            ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
            ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='', ms=7, marker='*')
            ax2.fill_between(np.where(defined_points)[0] / (len(defined_points)-1), temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

            temp = np.percentile(posterior, [2.5, 97.5], axis=1)
            # ax3.plot(points / dur_max, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
            # ax3.fill_between(points / dur_max, np.clip(temp[1] / total_max, a_min=None, a_max=1) - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])
            total_max = np.max(temp[1])
            dur_probs[2 * self.state_map[s]] = np.round(total_max / 2, 3)
            dur_probs[2 * self.state_map[s] + 1] = np.round(total_max, 3)
            ax3.plot(points / dur_max, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
            ax3.fill_between(points / dur_max, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

            # if (~defined_points).sum() > 0:
            #     gap = 0.02
            #     plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

            temp = np.percentile(posterior, [2.5, 97.5], axis=1)

            # if defined_points.sum() > 0:
            #     plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
            ax3.axhline(self.state_map[s] + 0.5, c='k')
            ax2.axhline(self.state_map[s] + 0.5, c='k')
            ax2.axhline(self.state_map[s], c='grey', alpha=alpha_level, zorder=4)
            ax1.axhline(self.state_map[s] + 0.5, c='grey', alpha=alpha_level, zorder=4)


        if not self.name.startswith('Sim_'):
            perf = np.zeros(self.n_sessions)
            found_files = 0
            counter = self.infos['bias_start'] - 1 if self.type == 'bias' else -1
            while found_files < self.n_sessions:
                counter += 1
                try:
                    feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
                except FileNotFoundError:
                    continue
                perf[found_files] = np.mean(feedback[:, 1])
                assert feedback.shape[0] >= self.full_posteriors[found_files].shape[1]
                found_files += 1
            ax0.axhline(-0.5, c='k')
            ax0.axhline(0.5, c='k')
            print(perf)
            ax0.fill_between(range(1, 1 + self.n_sessions), perf -0.5, -0.5, color='k')

        #sns.despine()
        #ax0.set_title("{}".format(self.name), size=22, loc='left')
        #ax0.set_title("Sessions: {}".format(self.type), size=22)
        ax2.set_title('Psychometric\nfunction', size=16)
        ax3.set_title('Duration\ndistribution', size=16)

        ax1.set_ylabel('Proportion of trials', size=28)
        ax0.set_ylabel('% correct', size=18)
        ax2.set_ylabel('Probability', size=26)
        ax1.set_xlabel('Session', size=28)
        ax2.set_xlabel('Contrast', size=26)
        ax1.set_xlim(left=1, right=self.n_sessions)
        ax0.set_xlim(left=1, right=self.n_sessions)
        ax2.set_xlim(left=0, right=1)
        ax3.set_xlabel('Trials', size=26)
        ax1.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax0.set_ylim(bottom=-0.5)
        ax0.spines['top'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax2.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax3.set_xlim(left=0, right=1)
        ax3.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        y_pos = (np.tile([0, 0.5], (self.n_pstates, 1)) + np.arange(self.n_pstates)[:, None]).flatten()
        ax1.set_yticks(y_pos)
        ax1.set_yticklabels(list(np.tile([0.5, 1], self.n_pstates)))
        ax0.set_yticks([0, 0.5])
        ax0.set_yticklabels([0.5, 1], size=fs)
        ax0.set_xticks([])
        y_pos = np.linspace(-0.5, self.n_pstates - 0.5, 2 * self.n_pstates + 1)
        ax2.set_yticks(y_pos)
        ax2.set_yticklabels([0] + list(np.tile([0.5, 1], self.n_pstates)), size=fs)
        ax3.set_yticks(y_pos)
        # ax3.set_yticklabels([0] + list(np.tile(["{:.2f}".format(total_max / 2), "{:.2f}".format(total_max)], self.n_pstates)), size=fs)
        ax3.set_yticklabels([0] + [str(d)[1:] for d in dur_probs], size=fs)

        ax1.axvline(11, color='red', zorder=0)
        ax0.axvline(11, color='red', zorder=0)

        ax1.tick_params(axis='both', labelsize=fs)
        ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax2.set_xticks([0, 0.5, 1])
        ax2.set_xticklabels([-1, 0, 1], size=fs)
        ax3.set_xticks([0, 0.5, 1])
        ax3.set_xticklabels([0, dur_max // 2, dur_max], size=fs)


        plt.tight_layout()
        if save:
            print("saving with {} dpi".format(dpi))
            plt.savefig("dynamic_GLM_figures/convergence/states_over_session {}".format(self.save_id), dpi=dpi)
        if show:
            plt.show()
        else:
            plt.close()

    def state_development_truth(self, save=True, show=True):
        true_pmf, true_states, true_dur = pickle.load(open("./sim mice/sim_16_truth.p", "rb"))
        truth_map = dict(zip(list(self.proto_states), [0, 2, 3, 1, 4]))
        truth_map2 = dict(zip(list(self.proto_states), [0, 1, 2, 3, 4]))
        print(truth_map)
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples
        fig = plt.figure(figsize=(16, 9))
        spec = gridspec.GridSpec(ncols=100, nrows=self.n_pstates+1, figure=fig)
        spec.update(hspace=0.) # set the spacing between axes.
        ax0 = fig.add_subplot(spec[:1, :69])  # performance line
        ax1 = fig.add_subplot(spec[1:, :69])  # state lines
        ax2 = fig.add_subplot(spec[1:, 76:86])
        ax3 = fig.add_subplot(spec[1:, 90:])

        if self.type != 'bias':
            current, counter = 0, 0
            for c in [2, 3, 4, 5]:
                if self.infos[c] == current:
                    counter += 1
                else:
                    counter = 0
                ax0.axvline(self.infos[c] + 1, color='gray', zorder=0)
                ax1.axvline(self.infos[c] + 1, color='gray', zorder=0)
                ax0.plot(self.infos[c] + 1 - 0.2, counter * 0.15 - 0.4, 'ko', ms=18)
                ax0.plot(self.infos[c] + 1 - 0.2, counter * 0.15 - 0.4, 'wo', ms=16.8)
                ax0.plot(self.infos[c] + 1 - 0.2, counter * 0.15 - 0.4, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
                current = self.infos[c]
        if self.type == 'all' or self.type == 'prebias_plus':
            ax1.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.annotate('Bias', (self.infos['bias_start'] + 1 - 0.5, 0.68), fontsize=22)

        dur_max = 120  # TODO: dont change params just once, also below
        posterior = np.zeros((self.n_pstates, dur_max, self.n_samples))
        for j, s in enumerate(self.proto_states):
            points = np.arange(dur_max)
            for i, m in enumerate(self.models):
                posterior[j, :, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!
        total_max = posterior.max()
        for s in self.proto_states:
            ax1.fill_between(range(1, 1 + self.n_sessions), self.state_map[s] - states_by_session[self.state_map[s]] / 2,
                             self.state_map[s] + states_by_session[self.state_map[s]] / 2, color=self.state_to_color[s])
            ax1.plot(range(1, 1 + self.n_sessions), truth_map[s] - true_states[:, truth_map2[s]] / 2, color='r')
            ax1.plot(range(1, 1 + self.n_sessions), truth_map[s] + true_states[:, truth_map2[s]] / 2, color='r')

            pmfs = np.zeros((len(self.models), self.n_contrasts))
            dur_max = 120  # TODO: dont change params just once, also above
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i, m in enumerate(self.models):
                if self.fit_variance == '0':
                    pmfs[i] = m.obs_distns[s].weights[:, 0]
                else:
                    pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]
                posterior[:, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!

            alpha_level = 0.3
            ax2.axvline(0.5, c='grey', alpha=alpha_level, zorder=4)

            temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
            if self.type == 'bias':
                defined_points = np.ones(self.n_contrasts, dtype=bool)
            else:
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                defined_points[[0, -1]] = True
            ax2.plot(np.arange(self.n_contrasts) / (self.n_contrasts-1), true_pmf[truth_map2[s]] - 0.5 + truth_map[s], color='r')
            ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
            ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='', ms=5, marker='*')
            ax2.fill_between(np.where(defined_points)[0] / (len(defined_points)-1), temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

            # if (~defined_points).sum() > 0:
            #     gap = 0.02
            #     plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

            temp = np.percentile(posterior, [2.5, 97.5], axis=1)
            if true_dur[truth_map2[s]] != 'inf':
                ax3.plot(points / dur_max, nbinom.pmf(points, true_dur[truth_map2[s]][0], true_dur[truth_map2[s]][1]) / total_max - 0.5 + truth_map[s], color='r')
            ax3.plot(points / dur_max, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
            ax3.fill_between(points / dur_max, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

            # if defined_points.sum() > 0:
            #     plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
            ax2.axhline(self.state_map[s] + 0.5, c='k')
            ax3.axhline(self.state_map[s] + 0.5, c='k')
            ax2.axhline(self.state_map[s], c='grey', alpha=alpha_level, zorder=4)
            ax1.axhline(self.state_map[s] + 0.5, c='grey', alpha=alpha_level, zorder=4)


        #sns.despine()
        ax0.set_title("{}".format(self.name), size=22, loc='left')
        ax0.set_title("Sessions: {}".format(self.type), size=22)
        ax2.set_title('Psychometric\nfunction', size=12)
        ax3.set_title('Duration\ndistribution', size=12)
        ax1.set_ylabel('Proportion of trials', size=22)
        ax2.set_ylabel('Probability', size=22)
        ax1.set_xlabel('Session', size=22)
        ax2.set_xlabel('Contrast', size=22)
        ax3.set_xlabel('Trials', size=22)
        ax1.set_xlim(left=1, right=self.n_sessions)
        ax0.set_xlim(left=1, right=self.n_sessions)
        ax2.set_xlim(left=0, right=1)
        ax3.set_xlim(left=0, right=1)
        ax1.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax0.set_ylim(bottom=-0.5, top=0.)
        ax0.spines['top'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax2.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax3.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        y_pos = (np.tile([0, 0.25, 0.5], (self.n_pstates, 1)) + np.arange(self.n_pstates)[:, None]).flatten()
        ax1.set_yticks(y_pos)
        ax1.set_yticklabels(list(np.tile([0, 0.5, 1], self.n_pstates)))
        ax0.set_xticks([])
        ax0.set_yticks([])
        y_pos = np.linspace(-0.5, self.n_pstates - 0.5, 2 * self.n_pstates + 1)
        ax2.set_yticks(y_pos)
        ax2.set_yticklabels([0] + list(np.tile([0.5, 1], self.n_pstates)), size=fs)
        ax3.set_yticks(y_pos)
        ax3.set_yticklabels([0] + list(np.tile(["{:.2f}".format(total_max / 2), "{:.2f}".format(total_max)], self.n_pstates)), size=fs)

        ax1.tick_params(axis='both', labelsize=fs)
        ax2.set_xticks([0, 0.5, 1])
        ax2.set_xticklabels([-1, 0, 1], size=fs)
        ax3.set_xticks([0, 0.5, 1])
        ax3.set_xticklabels([0, dur_max // 2, dur_max], size=fs)

        plt.tight_layout()
        if save:
            plt.savefig("dynamic_GLM_figures/convergence/true_states_over_session {}".format(self.save_id))
        if show:
            plt.show()
        else:
            plt.close()



    def block_alignment(self, show=False, save=False):
        # throws errors when data is missing, e.g. ibl_witten_19
        n = self.n_sessions
        states_by_session = np.zeros((2, self.n_pstates, self.n_sessions))
        for seq_num, blocks in zip(range(n), self.block_yielder(n)): # !!! not working for all fit types

            if self.type != 'bias':
                seq_num += self.infos['bias_start']
            if not (0.2 in blocks and 0.8 in blocks):
                continue
            for m in self.models:
                seq = m.stateseqs[seq_num]
                for s in self.proto_states:
                    b1 = blocks == 0.2
                    states_by_session[0, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b1)) / np.sum(b1)
                    b2 = blocks == 0.8
                    states_by_session[1, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b2)) / np.sum(b2)
        states_by_session /= self.n_samples

        perf = np.zeros(self.n_sessions)
        for i in range(self.n_sessions):
            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                continue
            perf[i] = np.mean(feedback)

        plt.figure(figsize=(14, 8))
        plt.suptitle("{}".format(self.name), size=fs)

        plt.subplot(211)
        plt.title('Right block', size=22)
        #plt.plot(perf)
        for s in self.proto_states:
            plt.plot(states_by_session[0, self.state_map[s]], color=self.state_to_color[s], label=s)

        sns.despine()
        plt.yticks(size=fs-2)
        plt.xticks([], size=fs-3)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.subplot(212)
        plt.title('Left block', size=22)
        for s in self.proto_states:
            plt.plot(states_by_session[1, self.state_map[s]], color=self.state_to_color[s])

        sns.despine()
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('Porportion of trials', size=22)
        plt.xlabel('Session', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        if save:
            plt.savefig("dynamic_GLM_figures/convergence/Block alginment {}".format(self.save_id))
        if show:
            plt.show()
        else:
            plt.close()

    def block_performance(self):
        c_n_a = self.data
        block_performance = np.zeros((2, self.n_sessions))
        for seq_num in range(self.n_sessions):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(
                                     self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue
            if len(blocks) < 140:
                continue
            data = c_n_a[seq_num]
            block_performance[0, seq_num] = np.mean(data[np.logical_and(blocks == 1, data[:, 0] == 4)][:, 1] == 0)
            block_performance[1, seq_num] = np.mean(data[np.logical_and(blocks == -1, data[:, 0] == 4)][:, 1] == 0)

        plt.figure(figsize=(11, 8))
        plt.plot(block_performance[0], 'b', label='Right block')
        plt.plot(block_performance[1], 'r', label='Left block')
        plt.plot([0, self.n_sessions], [0.2, 0.2], 'r')
        plt.plot([0, self.n_sessions], [0.8, 0.8], 'b')
        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('P(answer right | Contrast = 0)', size=22)
        plt.xlabel('Session', size=22)

        plt.legend(fontsize=fs)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout()
        plt.savefig("dynamic_GLM_figures/convergence/0 accuracy in blocks {}".format(self.save_id))
        plt.close()

    def pmf_posterior(self):
        plt.figure(figsize=(11, 9))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_contrasts))
            for i, m in enumerate(self.models):
                pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]

            print('TODO')
            if True:# old condition == 'nothing':
                plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
            else:
                plt.plot(pmfs[:, :self.n_contrasts].mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                #plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])

                plt.plot(pmfs[:, self.n_contrasts:].mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                #plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
        plt.title("{}, {} sessions".format(self.name, len(self.models[0].stateseqs)), size=22)
        plt.xticks(*self.cont_ticks, size=22-1)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        #plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)
        sns.despine()

        plt.tight_layout()
        plt.savefig("dynamic_GLM_figures/convergence/pmfs with conf {}".format(self.save_id))
        plt.show()

    def dynamic_pmf_posterior(self, show=0):
        # pmfs_save = pickle.load(open("./session_data/{}_pmfs.p".format(self.name), "rb"))
        for ii, s in enumerate(self.proto_states):
            plt.figure(figsize=(16, 9))
            pmfs = np.zeros((len(self.models), self.n_sessions, self.n_contrasts))
            for i, m in enumerate(self.models):
                for j in range(self.n_sessions):
                    pmfs[i, j] = m.obs_distns[s].weights[j, :, 0]

            for j in range(self.n_sessions):
                plt.subplot(self.n_sessions // 6 + 1, 6, j+1)
                conts = np.unique(self.data[j][:, 0])
                plt.plot(pmfs[:, j].mean(axis=0)[conts], color=self.state_to_color[s], label="State {}".format(self.state_map[s]))
                # plt.plot(np.array(pmfs_save[j])[conts], label='Truth', c='r')
                temp = np.percentile(pmfs[:, j], [2.5, 97.5], axis=0)[:, conts]
                plt.fill_between(range(len(conts)), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
                plt.ylim(bottom=0, top=1)
                plt.title("Session " + str(j+1))
                if not j % 6 == 0:
                    plt.yticks([], [])
                else:
                    plt.ylabel('P(rightwards)')
                if j >= self.n_sessions - 6:
                    plt.xlabel('Contrasts')
                plt.xticks(range(len(conts)), [])
                if j == 0:
                    plt.legend()
                plt.xlim(left=0, right=len(conts)-1)
                sns.despine()

            plt.tight_layout()
            plt.savefig("dynamic_GLM_figures/convergence/pmfs with conf {} {}".format(self.save_id, s))
            if show:
                plt.show()
            else:
                plt.close()

    def dynamic_pmf_posterior_one_sess(self, show=1, sess=0):
        pmfs_save = pickle.load(open("./session_data/{}_pmfs.p".format(self.name), "rb"))
        for ii, s in enumerate(self.proto_states):
            plt.figure(figsize=(16, 9))
            pmfs = np.zeros((len(self.models), self.n_contrasts))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[sess, :, 0]

            conts = np.unique(self.data[sess][:, 0])
            plt.plot(pmfs.mean(axis=0)[conts], color=self.state_to_color[s], label="State {}".format(self.state_map[s]))
            plt.plot(np.array(pmfs_save[sess])[conts], label='Truth', c='r')
            temp = np.percentile(pmfs, [2.5, 97.5], axis=0)[:, conts]
            plt.fill_between(range(len(conts)), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
            plt.ylim(bottom=0, top=1)
            plt.title("Session " + str(sess+1), size=22)

            plt.ylabel('P(rightwards)', size=22)
            plt.xlabel('Contrasts', size=22)
            plt.xticks(*bias_cont_ticks, size=22-2)
            plt.yticks(size=22-2)
            plt.legend(frameon=False, fontsize=22)
            plt.xlim(left=0, right=8)
            sns.despine()

            plt.tight_layout()
            plt.savefig("dynamic_GLM_figures/convergence/single pmf with conf {} {} {}".format(self.save_id, sess, s))
            if show:
                plt.show()
            else:
                plt.close()

    def dynamic_pmf_posterior_single_frame(self, show=0):
        #pmfs_save = pickle.load(open("./session_data/{}_pmfs.p".format(self.name), "rb"))
        for ii, s in enumerate(self.proto_states):
            plt.figure(figsize=(11, 9))
            cmap = cm.get_cmap('magma')
            pmfs = np.zeros((len(self.models), self.n_sessions, self.n_contrasts))
            for i, m in enumerate(self.models):
                for j in range(self.n_sessions):
                    pmfs[i, j] = weights_to_pmf(m.obs_distns[s].weights[j])

            for j in range(self.n_sessions):
                plt.plot(pmfs[:, j].mean(axis=0), label=self.state_map[s], c=cmap(j / self.n_sessions))
                plt.plot(pmfs[:, j].mean(axis=0), label=self.state_map[s], c=cmap(j / self.n_sessions))
            plt.ylim(bottom=0, top=1)
            plt.ylabel('P(rightwards)', size=28)
            plt.xlabel('Contrasts', size=28)
            plt.xticks(*self.cont_ticks, size=20)
            plt.yticks(size=20)
            plt.xlim(left=0, right=self.n_contrasts)
            plt.title("State {}".format(self.state_map[s]))
            # plt.colorbar(cmap)
            sns.despine()

            plt.tight_layout()
            plt.savefig("dynamic_GLM_figures/evolving pmf {} {}".format(self.save_id, s))
            if show:
                plt.show()
            else:
                plt.close()

    def pmf_diffs(self, show=0):
        plt.figure(figsize=(16, 9))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_sessions, self.n_contrasts))
            for i, m in enumerate(self.models):
                for j in range(self.n_sessions):
                    pmfs[i, j] = m.obs_distns[s].weights[j, :, 0]

            diffs = np.zeros(self.n_sessions - 1)
            for j in range(self.n_sessions - 1):
                diffs[j] = np.sum(np.abs(pmfs[:, j].mean(axis=0) - pmfs[:, j+1].mean(axis=0)))

            plt.plot(diffs, color=self.state_to_color[s], label=self.state_map[s])
        plt.legend()
        plt.tight_layout()
        plt.savefig("dynamic_GLM_figures/convergence/pmf drift {} {}".format(self.save_id, s))
        if show:
            plt.show()
        else:
            plt.close()

    def single_pmf_posterior(self):
        plt.figure(figsize=(9, 8))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_contrasts))
            for i, m in enumerate(self.models):
                pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]

            # TODO old condition
            print('TODO')
            if True:
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                if s == 5:
                    plt.plot(range(defined_points.sum()), pmfs[:, defined_points].mean(axis=0), color=self.state_to_color[s], lw=3)
                else:
                    plt.plot(range(defined_points.sum()), pmfs[:, defined_points].mean(axis=0), color=self.state_to_color[s], lw=3, alpha=0.3)
                # plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])
            else:
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                plt.plot(range(defined_points.sum()), pmfs[:, np.where(defined_points)[0]].mean(axis=0), color=self.state_to_color[s], label='previous right')
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                plt.plot(range(defined_points.sum()), pmfs[:, self.n_contrasts + np.where(defined_points)[0]].mean(axis=0), color=self.state_to_color[s], label='previous left', ls='-.')
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])
        # plt.title("{}, state {}".format(self.name, self.state_map[s]), size=22)
        plt.xticks(*self.cont_ticks, size=22-3)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=defined_points.sum() - 1)
        # plt.legend()
        sns.despine()

        plt.tight_layout()
        plt.savefig("dynamic_GLM_figures/convergence/single pmf with conf {} state {}".format(self.save_id, self.state_map[s]))
        plt.show()

    def switchiness(self):
        """Stupid function for seeing how often the posterior switches states."""
        sess_switch = np.zeros(len(self.posteriors))
        sess_acc = np.zeros(len(self.posteriors))
        sess_acc_0 = np.zeros(len(self.posteriors))
        # sess_norm_switch = np.zeros(len(self.posteriors))

        for i, p in enumerate(self.posteriors):
            _, certain_states = np.where((p > 0.6).T)

            switches = 0
            current_state = -1
            counter = 0

            for s in certain_states:
                if s == current_state:
                    counter += 1
                else:
                    switches += counter > 5
                    current_state = s
                    counter = 1

            sess_switch[i] = switches / p.shape[1]

            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                print("warning {} {}".format(self.name, i))
                continue
            print(feedback.shape)
            print(p.shape[1])
            assert len(feedback) == p.shape[1]
            sess_acc[i] = np.mean(feedback)
            sess_acc_0[i] = np.mean(feedback[self.data[i][:, 0] == 4])
            # sess_norm_switch[i] = switches / np.sum(np.diff(blocks) != 0)

        return sess_switch, sess_acc, sess_acc_0

    def duration_posterior(self):
        for s in self.proto_states:
            plt.figure(figsize=(11, 8))
            params = np.zeros((2, self.n_samples))
            for i, m in enumerate(self.models):
                params[0, i] = m.dur_distns[s].r
                params[1, i] = m.dur_distns[s].p

            dur_max = 150
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i in range(self.n_samples):
                posterior[:, i] = nbinom.pmf(points, params[0, i], 1 - params[1, i])
            plt.plot(points+1, posterior.mean(axis=1), color=self.state_to_color[s], label=s)
            temp = np.percentile(posterior, [2.5, 97.5], axis=1)
            plt.fill_between(points+1, temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])

            plt.title("State {}".format(self.state_map[s]), size=22)
            plt.xlabel('Trial duration', size=22)
            plt.ylabel('Probability', size=22)
            plt.xticks(size=20-3)
            plt.yticks(size=20-4)
            plt.xlim(left=1, right=dur_max + 1)
            plt.ylim(bottom=0)
            sns.despine()
            plt.tight_layout()

            plt.savefig("dynamic_GLM_figures/convergence/duration dist {} state {}".format(self.save_id, self.state_map[s]))
            plt.show()

    def waic(self):
        """Compute WAIC for a fitted model."""
        data = self.data
        size = int(self.n_samples / 5)
        arg_list = [(self.models[i*size:(i+1)*size], data, self.n_sessions) for i in range(5)]
        pool = mp.Pool(5)
        log_pred_dens = pool.starmap(waic_helper, arg_list)
        pool.close()
        log_pred_dens = np.concatenate(log_pred_dens, axis=0)
        assert (log_pred_dens == 0).sum() == 0

        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print("Posterior predictive density {}".format(clppd))
        print("Posterior variance {}".format(cpwaic2))
        print("WAIC {}".format(clppd - cpwaic2))
        pickle.dump(log_pred_dens, open("./WAIC/{}_{}.p".format(self.name, self.type), 'wb'))
        return log_pred_dens

    def log_like_plot(self):
        """Plot log_likelihood for chain of samples."""
        data = self.data
        size = int(self.n_samples / 5)
        arg_list = [(self.models[i*size:(i+1)*size], data, self.n_sessions) for i in range(5)]
        pool = mp.Pool(5)
        ll = pool.starmap(ll_helper, arg_list)
        pool.close()
        ll = np.concatenate(ll, axis=0)
        assert (ll == 0).sum() == 0

        plt.plot(ll)
        plt.show()

    def goodness_of_fit(self):
        """Compute WAIC for a fitted model."""
        data = self.data
        size = int(self.n_samples / 5)
        arg_list = [(self.models[i*size:(i+1)*size], data, self.n_sessions) for i in range(5)]
        pool = mp.Pool(5)
        log_pred_dens = pool.starmap(goodness_of_fit_helper, arg_list)
        pool.close()
        log_pred_dens = np.concatenate(log_pred_dens, axis=0)
        assert (log_pred_dens == 0).sum() == 0

        gof = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        print("Goodness of fit {}".format(gof))
        pickle.dump(log_pred_dens, open("./WAIC/{}_{}.p".format(self.name, self.type), 'wb'))
        return log_pred_dens

    def waic_one_core(self):
        # predictive density
        log_pred_dens = np.zeros((self.n_samples, self.n_sessions))
        data = self.data

        for i, m in enumerate(self.models):
            if i % 50 == 0:
                print(i)
            for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, data)):
                s.data = d
                log_pred_dens[i, j] = np.sum(s.aBl[np.arange(s.aBl.shape[0]), ss])

        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)
        pickle.dump(log_pred_dens, open("./WAIC/{}_{}_one_core.p".format(self.name, self.type), 'wb'))
        return log_pred_dens

    def load_waic(self):
        log_pred_dens = pickle.load(open("./WAIC/{}_{}.p".format(self.name, self.type), 'rb'))
        assert (log_pred_dens == 0).sum() == 0
        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)

        return(clppd - cpwaic2)

    def generate_data(self):
        # need to subtract confounds from contrasts (from models.datas)
        np.random.seed(4)
        fake_data = []
        state_data = []
        for contrasts, sseq in zip(self.data, self.models[-1].stateseqs):
            c_n_a, states = self.models[-1].my_generate(contrasts[:, 0], np.unique(sseq))
            fake_data.append(c_n_a)
            state_data.append(states)

        for i, fd in enumerate(fake_data):
            print(np.unique(fd[:, 0]))
            pickle.dump(fd, open("./session_data/{}_recovery_info_{}.p".format(self.name, i), 'wb'))

        self.truth_diagram(state_data)
        return c_n_a

    def truth_diagram(self, state_data):
        flat_list = [item for sublist in state_data for item in sublist]
        a = np.unique(flat_list, return_counts=1)
        states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]
        warnings.warn('This code is not maintained, thresholding doesn\' work anymore')
        proto_states = states[counts > self.threshold]
        n_pstates = len(proto_states)
        tendencies = []
        state_to_color = {}
        model = self.models[-1]
        for s in proto_states:
            temp = 1 - np.sum(np.mean(model.obs_distns[s].weights, axis=0)[:, 0]) / (self.n_contrasts)
            state_to_color[s] = colors[int(temp * 101)]
            tendencies.append(temp)

        ordered_states = [x for _, x in sorted(zip(tendencies, proto_states))]
        state_map = dict(zip(ordered_states, range(n_pstates)))

        states_by_session = np.zeros((n_pstates, self.n_sessions))
        for i, seq in enumerate(state_data):
            for s in proto_states:
                states_by_session[state_map[s], i] += np.sum(seq == s) / len(seq)
        plt.figure(figsize=(16, 9))

        current, counter = 0, 0
        for c in [2, 3, 4, 5]:
            if self.infos[c] == current:
                counter += 1
            else:
                counter = 0
            plt.axvline(self.infos[c], color='gray', zorder=0)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'ko', ms=18)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'wo', ms=16.8)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
            current = self.infos[c]

        for s in proto_states:
            plt.fill_between(range(1, 1 + self.n_sessions), state_map[s] - states_by_session[state_map[s]] / 2,
                             state_map[s] + states_by_session[state_map[s]] / 2, color=state_to_color[s])

            pmfs = np.zeros((1, self.n_contrasts))
            dur_max = 120
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, 1))
            pmfs = np.mean(model.obs_distns[s].weights, axis=0)[:, 0]
            posterior = nbinom.pmf(points, model.dur_distns[s].r, 1 - model.dur_distns[s].p)

            alpha_level = 0.3
            plt.plot([self.n_sessions + 1]*2, [-0.5, n_pstates - 0.5], c='k')
            plt.plot([self.n_sessions]*2, [-0.5, n_pstates + 0.5], c='k')
            plt.plot([self.n_sessions + 0.5]*2, [-0.5, n_pstates - 0.5], c='grey', alpha=alpha_level, zorder=4)
            #plt.annotate(str(abs(num_to_cont[c])), ((self.infos[c] - 0.75 + 0.55 * counter) / self.n_sessions, 0.9), color='black', xycoords='figure fraction', size=16)

            defined_points = np.ones(11, dtype=np.bool)
            plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[defined_points] - 0.5 + state_map[s], color=state_to_color[s])

            total_max = max(temp.max(), posterior.mean(axis=1).max())
            plt.plot(points / dur_max + self.n_sessions + 1, posterior / total_max - 0.5 + state_map[s], color=state_to_color[s])

            plt.plot([self.n_sessions, self.n_sessions+2], [state_map[s] + 0.5] * 2, c='k')
            plt.plot([self.n_sessions, self.n_sessions+1], [state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)

        perf = np.zeros(self.n_sessions)
        found_files = 0
        counter = -1
        while found_files < self.n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback)
            # no assertions that file is correct
            found_files += 1

        plt.annotate('PMF', (self.n_sessions + 0.5, n_pstates - 0.45), size=12, ha='center')
        plt.annotate('Duration', (self.n_sessions + 1.5, n_pstates - 0.45), size=12, ha='center')
        plt.axhline(n_pstates + 0.5, c='k')
        plt.axhline(n_pstates - 0.5, c='k')
        plt.fill_between(range(1, 1 + self.n_sessions), n_pstates + perf / 2,
                         n_pstates - perf / 2, color='k')

        sns.despine()
        plt.title("{}".format(self.name), size=22, loc='left')
        plt.title("Sessions: {}, recovery truth".format(self.type), size=22)
        plt.yticks(range(n_pstates + 1), list(range(n_pstates)) + ['Acc.'])
        step_size = max(int(self.n_sessions / 7), 1)
        plt.xticks(list(range(0, self.n_sessions, step_size)) + [self.n_sessions+0.5, self.n_sessions+1.5], list(range(0, self.n_sessions, step_size)) + [str(0), str(int(dur_max / 2))])
        plt.xticks(size=fs-1)
        plt.yticks(size=fs-2)
        plt.ylabel('Proportion of trials', size=22)
        plt.xlabel('Session', size=22)
        plt.xlim(left=1, right=self.n_sessions + 2)
        plt.ylim(bottom=-0.5)
        plt.tight_layout()

        plt.savefig("dynamic_GLM_figures/convergence/recovery_truth_{}".format(self.save_id))
        plt.show()

    def pmfs(self, states):
        for s in states:
            plt.plot(np.mean(self.models[-1].obs_distns[s].weights, axis=0)[11:, 0])
            plt.plot(np.mean(self.models[-1].obs_distns[s].weights, axis=0)[:11, 0], '--')
            plt.title(s)
            plt.show()

    def session_entropy(self):
        entropies = np.zeros(self.n_sessions)
        for i, ful_post in enumerate(self.full_posteriors):
            p = ful_post.sum(1) / ful_post.shape[1]
            zero_mask = p != 0.
            entropies[i] = - np.sum(p * np.log(p, where=zero_mask))
        return entropies

    def state_session_pref(self, time_slots=15):
        times = np.zeros((self.n_all_states, time_slots))
        for post in self.full_posteriors:
            trials = post.shape[1]

            slot_sizes = np.ones(time_slots, dtype=np.int16) * int(trials / time_slots)
            slot_sizes[:int(trials - (slot_sizes[1] * time_slots))] += 1  # add enough so as to fill whole session

            so_far = 0
            for i, step in enumerate(slot_sizes):  # could maybe be solved with cumsum, but it sucks
                times[:, i] += post[:, so_far:so_far + step].sum(axis=1)
                so_far += step

        for i in range(self.n_all_states):
            if times[i].sum() < 20:
                continue
            label = "PState {}".format(self.state_map[i]) if i in self.proto_states else i
            line_style = '--' if i in self.proto_states else '-'
            plt.plot(times[i] / times[i].sum(), label=label, ls=line_style)
        plt.legend()
        plt.show()

    def state_correlations(self):
        state_activity = np.zeros((self.n_sessions, self.n_pstates))
        for i, post in enumerate(self.posteriors):
            state_activity[i] = post.sum(axis=1)
        correlations = np.corrcoef(state_activity.T)
        sns.heatmap(correlations, square=True, vmin=-1, vmax=1)
        plt.show()

        return correlations[np.triu_indices(correlations.shape[1], 1)]

    def thresholded_state_correlations(self):
        state_activity = np.zeros((self.n_sessions, self.n_pstates))
        for i, post in enumerate(self.posteriors):
            state_activity[i] = post.mean(axis=1) > 0.05
        state_activity = state_activity[:, state_activity.sum(0) != 0]
        state_activity = state_activity[:, state_activity.sum(0) != self.n_sessions]
        correlations = np.corrcoef(state_activity.T)
        #sns.heatmap(correlations, square=True, vmin=-1, vmax=1)
        #plt.show()
        if correlations.ndim == 0:
            return []
        return list(correlations[np.triu_indices(correlations.shape[1], 1)])

    def states_per_sess(self):
        state_counts = np.zeros(self.n_sessions)
        for i, post in enumerate(self.full_posteriors):
            state_counts[i] = np.sum(np.mean(post, axis=1) > 0.05)
        return state_counts

    def states_per_mouse(self):
        return self.n_pstates

    def pmf_return(self):
        if self.n_pstates == 1:
            return None, None
        prevalences = []
        for s in self.proto_states:
            state_trials = 0
            for post in self.posteriors:
                state_trials += np.sum(post[self.state_map[s]])
            prevalences.append(state_trials)
        print(self.proto_states, prevalences)
        most_prevalent = [x for _, x in sorted(zip(prevalences, self.proto_states))]
        print(most_prevalent)

        if self.state_tendency[most_prevalent[-1]] > self.state_tendency[most_prevalent[-2]]:
            return self.pmf_means[most_prevalent[-1]], self.pmf_means[most_prevalent[-2]]
        else:
            return self.pmf_means[most_prevalent[-2]], self.pmf_means[most_prevalent[-1]]

    def state_appearances(self):
        appearences = []
        for s in self.proto_states:
            for i in range(self.n_sessions):
                activity = np.sum(self.posteriors[i][self.state_map[s]]) / self.posteriors[i].shape[1]
                if activity > 0.1:
                    appearences.append(i)
                    break
        ret = np.array(appearences) / (self.n_sessions - 1)  # is -1 here good?
        return list(ret)

    def sessions_per_state(self):
        sess_list = []
        for s in self.proto_states:
            sess_count = 0
            for i in range(self.n_sessions):
                activity = np.sum(self.posteriors[i][self.state_map[s]]) / self.posteriors[i].shape[1]
                sess_count += activity > 0.1
            sess_list.append(sess_count)
        ret = np.array(sess_list) / self.n_sessions
        return list(ret)

    def parallel_relevant_states(self):
        parallel_states = np.zeros((self.n_all_states, self.n_all_states))
        for i, post in enumerate(self.full_posteriors):
            active = post.mean(axis=1) > 0.05
            parallel_states[active] += active

        print(parallel_states)

        return list(parallel_states)

    def save_states(self):
        all_states = []
        bstart = self.infos['bias_start'] + 1

        for i in range(self.n_sessions):
            if i < bstart:
                states = np.zeros(self.posteriors[i].shape[1])
                states[:] = 2
                all_states.append(states)
                print(len(states))
            else:
                states = np.zeros(self.posteriors[i].shape[1])
                states[self.posteriors[i][1] > self.posteriors[i][2]] = 1
                print(np.mean(states))
                print(len(states))
                all_states.append(states)
            plt.plot(states)
            plt.title(i)
            plt.show()
        pickle.dump(all_states, open("states_{}_{}_{}.p".format(self.name, self.type, self.fit_variance), 'wb'))

    def pmf_compare(self, show=1, save=0):
        contrasts_and_answers = self.data
        for seq_num in range(self.n_sessions):
            c_n_a = contrasts_and_answers[seq_num]
            true_pmf = np.zeros(self.n_contrasts)
            curr_cont = []
            for c in range(self.n_contrasts):
                mask = c_n_a[:, 0] == c
                if mask.sum() == 0:
                    continue
                curr_cont.append(c)
                true_pmf[c] = (c_n_a[mask, 1] == 0).mean()

            inv_map = {v: k for k, v in self.state_map.items()}
            pmf_mix = np.zeros(self.n_contrasts)
            rel_states, counts = np.unique(np.argmax(self.posteriors[seq_num], axis=0), return_counts=True)
            tot_counts = counts.sum()
            for s, c in zip(rel_states, counts):
                pmfs = np.zeros((len(self.models), self.n_contrasts))
                for i, m in enumerate(self.models):
                    pmfs[i] = m.obs_distns[inv_map[s]].weights[seq_num, :, 0]
                pmf_mix += pmfs.mean(axis=0) * c / tot_counts

            plt.plot(true_pmf[curr_cont], label='True')
            plt.plot(pmf_mix[curr_cont], label='predicted')
            plt.ylim(bottom=0, top=1)
            plt.legend()
            plt.ylabel('% Rightwards', size=20)
            plt.xlabel('Contrasts', size=20)
            sns.despine()
            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=22)
            if save:
                plt.savefig("dynamic_GLM_figures/pmf true vs predicted {}, sess {}.png".format(self.save_id, seq_num))
            if show:
                plt.show()
            else:
                plt.close()

    def belief_csv(self):
        for seq_num in range(self.n_sessions):
            np.save("./beliefs/{}_{}".format(self.name, self.infos['dates'][seq_num]), self.posteriors[seq_num])

    def belief_hist(self):
        if self.n_pstates == 2:
            plt.hist(np.hstack(self.posteriors)[0], bins=20)
            plt.show()
        else:
            beliefs = np.hstack(self.posteriors)
            from scipy.stats import gaussian_kde
            z = gaussian_kde(beliefs)(beliefs)
            plt.scatter(beliefs[0], beliefs[1], c=z)
            plt.show()

    def glm_plot(self):
        for s in self.proto_states:
            weights = np.empty((self.n_samples, self.n_sessions, 5))
            for i, m in enumerate(self.models):
                weights[i] = m.obs_distns[s].weights
            for w in range(5):
                plt.subplot(5, 1, w + 1)
                if w == 0:
                    plt.title(self.state_map[s])
                plt.plot(np.mean(weights[:, :, w], axis=0), color=self.state_to_color[s])
                percentiles = np.percentile(weights[:, :, w], [2.5, 97.5], axis=0)
                plt.fill_between(np.arange(self.n_sessions), percentiles[1], percentiles[0],
                                 color=self.state_to_color[s], alpha=0.15)
            plt.show()

    def glm_session_weights(self, sessions=None, show=False, ret_differetiate=False, models=None):
        """Plot overall averaged GLM weights for every trial"""
        sessions = range(self.n_sessions) if sessions is None else sessions
        self.weights_per_session = []
        local_n_samples = self.n_samples if models is None else len(models)
        local_models = self.models if models is None else [self.models[m] for m in models]
        names = ['contL', 'contR', 'prevA', 'bias']  # 'prevA', 'WSLS'
        for seq_num in sessions:
            weights = np.empty((local_n_samples, self.data[seq_num].shape[0], len(names)))
            for i, m in enumerate(local_models):
                for s in range(self.n_all_states):
                    mask = m.stateseqs[seq_num] == s
                    if np.sum(mask) > 0:
                        weights[i, mask] = m.obs_distns[s].weights[seq_num]
            self.weights_per_session.append(np.zeros((len(names), self.data[seq_num].shape[0])))
            for w, w_name in enumerate(names):
                p = plt.plot(np.mean(weights[:, :, w], axis=0), label=w_name)
                self.weights_per_session[-1][w] = np.mean(weights[:, :, w], axis=0)
                percentiles = np.percentile(weights[:, :, w], [2.5, 97.5], axis=0)
                # if w in [2, 3]:
                #     sig_trials = np.sum(np.logical_or(percentiles[1] < -0.2, percentiles[0] > 0.2))
                #     if sig_trials > 0:
                #         print("Session {}, weight {}, {} trials".format(seq_num + 1, w, sig_trials))
                plt.fill_between(np.arange(self.data[seq_num].shape[0]), percentiles[1], percentiles[0],
                                 color=p[0].get_color(), alpha=0.15)
                if ret_differetiate:
                    plt.close()
                    if w == 0:
                        temp_top = percentiles[1]
                    if w == 1:
                        if np.sum(temp_top < percentiles[0]) > 20:
                            print(self.name, seq_num + 1, self.n_sessions)
                            return seq_num + 1, self.n_sessions

            if ret_differetiate:
                continue
            plt.legend(fontsize=16)
            plt.xlabel('Trial', size=18)
            plt.ylabel('GLM weight', size=18)
            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=20)
            plt.tight_layout()
            sns.despine()
            # plt.savefig("dynamic_GLM_figures/convergence/session_weights posterior {} {}".format(self.save_id, seq_num + 1))
            if show:
                plt.show()
            else:
                plt.close()

    def perserve_wsls(self, show=False):
        """Plot averaged component of perseveration and win-stay lose-shift across sessions"""
        sessions = range(self.n_sessions)
        weights = np.zeros((2, self.n_sessions))
        upper = np.zeros((2, self.n_sessions))
        lower = np.zeros((2, self.n_sessions))
        for seq_num in sessions:
            posterior = result.posteriors[seq_num].sum(1)
            for s in self.proto_states:
                if posterior[self.state_map[s]] >= 1:
                    comp = np.zeros((2, self.n_samples))
                    for i, m in enumerate(self.models):
                        comp[:, i] += m.obs_distns[s].weights[seq_num][[0, 1]]
                    temp = np.mean(comp, axis=1)
                    weights[:, seq_num] += temp * posterior[self.state_map[s]] / posterior.sum()
                    upper[:, seq_num] += (np.percentile(comp, 97.5, axis=1)-temp) * posterior[self.state_map[s]] / posterior.sum()
                    lower[:, seq_num] += (np.percentile(comp, 2.5, axis=1)-temp) * posterior[self.state_map[s]] / posterior.sum()

        p = plt.plot(weights[0], label='Cont L')
        plt.fill_between(np.arange(self.n_sessions), weights[0] + upper[0], weights[0] + lower[0],
                         color=p[0].get_color(), alpha=0.15)
        p = plt.plot(weights[1], label='Cont R')
        plt.fill_between(np.arange(self.n_sessions), weights[1] + upper[1], weights[1] + lower[1],
                         color=p[0].get_color(), alpha=0.15)
        diff_achieved = np.min(np.where(weights[0] + upper[0] + 0.2 < weights[1] + lower[1]))
        plt.axvline(diff_achieved, color='r')
        print(weights[0, diff_achieved], weights[1, diff_achieved])
        print(weights[0, -1], weights[1, -1])
        print((np.abs(weights[0, diff_achieved]) > np.abs(weights[1, diff_achieved])) == (np.abs(weights[0, -1]) > np.abs(weights[1, -1])))
        plt.legend(fontsize=16)
        plt.xlabel('Session', size=18)
        plt.ylabel('GLM weight', size=18)
        plt.title("{}".format(self.name), size=20)
        plt.tight_layout()
        sns.despine()
        plt.savefig("dynamic_GLM_figures/contrasts {}".format(self.save_id))
        if show:
            plt.show()
        else:
            plt.close()
        return diff_achieved, self.n_sessions

    def glm_weights_clusters(self, sessions=None, show=False):
        sessions = range(self.n_sessions) if sessions is None else sessions
        self.weights_per_session = []
        for seq_num in sessions:
            weights = np.empty((self.n_samples, self.data[seq_num].shape[0], 5))
            for i, m in enumerate(self.models):
                for s in range(self.n_all_states):
                    mask = m.stateseqs[seq_num] == s
                    if np.sum(mask) > 0:
                        weights[i, mask] = m.obs_distns[s].weights[seq_num]
            self.weights_per_session.append(np.mean(weights, axis=0).T)
            # plt.plot(self.weights_per_session[-1].T)
            # plt.show()

        beliefs = np.hstack(self.weights_per_session)
        covariance_matrix = np.cov(beliefs)
        eigen_values, eigen_vectors = np.linalg.eig(covariance_matrix)

        variance_explained = []
        for i in eigen_values:
            variance_explained.append((i/sum(eigen_values))*100)

        print(variance_explained)

        projection_matrix = (eigen_vectors.T[:][:2]).T
        X_pca = beliefs.T.dot(projection_matrix)

        counter = 0
        cmap = cm.get_cmap('magma')
        for i in range(self.n_sessions):
            # print(counter, counter + self.data[i].shape[0])
            # if i + 1 == 5:
            #     np.set_printoptions(threshold=np.inf)
            #     print(beliefs.T[counter:counter + self.data[i].shape[0]])
            from scipy.stats import gaussian_kde
            z = gaussian_kde(X_pca[counter:counter + self.data[i].shape[0]].T)(X_pca[counter:counter + self.data[i].shape[0]].T)
            plt.scatter(X_pca[counter:counter + self.data[i].shape[0], 0],
                        X_pca[counter:counter + self.data[i].shape[0], 1],
                        # c=cmap((i+1) / self.n_sessions))
                        c=z)
            counter += self.data[i].shape[0]
            plt.title("{}/{} {} dots".format(i + 1, self.n_sessions, self.data[i].shape[0]))
            plt.show()

        return (eigen_vectors.T[:][:2]).T

    def compare_glm_weights(self, states, session):
        """Plot the weights of the GLM for specific states during a specific session, to compare them."""
        for s in states:
            weights = np.empty((self.n_samples, 5))
            for i, m in enumerate(self.models):
                weights[i] = m.obs_distns[s].weights[session]
            p = plt.plot(np.mean(weights, axis=0))
            print(np.mean(weights, axis=0))
            percentiles = np.percentile(weights, [2.5, 97.5], axis=0)
            plt.fill_between(np.arange(5), percentiles[1], percentiles[0],
                             color=p[0].get_color(), alpha=0.15)
        plt.show()

    def compare_states(self, states, sessions):
        """Plot various infos about states on specific sessions, to compare them."""
        plt.figure(figsize=(16, 9))
        for i, sess in enumerate(sessions):
            pmfs = np.zeros((len(states), self.n_samples, self.n_contrasts))
            weights = np.zeros((len(states), self.n_samples, 5))
            for j, m in enumerate(self.models):
                for k, s in enumerate(states):
                    pmfs[k, j] = weights_to_pmf(m.obs_distns[s].weights[sess], with_bias=self.bias_weight_present[self.state_map[s]])
                    weights[k, j] = m.obs_distns[s].weights[sess]

            plt.subplot(len(sessions), 2, 2*i+1)
            for k, s in enumerate(states):
                if self.bias_weight_present[self.state_map[s]]:
                    defined_points = np.ones(self.n_contrasts, dtype=bool)
                else:
                    defined_points = np.zeros(self.n_contrasts, dtype=bool)
                    defined_points[[0, 1, -2, -1]] = True
                print(pmfs[k].shape)
                print(pmfs[k, :, defined_points].mean(axis=0).shape)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[k, :, defined_points].mean(axis=1), color=self.state_to_color[s], ls=self.state_to_ls[s])
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[k, :, defined_points].mean(axis=1), color=self.state_to_color[s], ls='', ms=5, marker='*')
                temp = np.percentile(pmfs[k], [2.5, 97.5], axis=0)
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])

            plt.subplot(len(sessions), 2, 2*i+2)
            for k, s in enumerate(states):
                plt.plot(np.mean(weights[k], axis=0), ls=self.state_to_ls[s], color=self.state_to_color[s])
                percentiles = np.percentile(weights[k], [2.5, 97.5], axis=0)
                plt.fill_between(np.arange(5), percentiles[1], percentiles[0],
                                 color=self.state_to_color[s], alpha=0.15)
        plt.show()


    def loglike_evolution(self, states, sessions):
        """Plot how the log_likelihood of a state for an entire session evolves across samples (specific)."""
        for j, sess in enumerate(sessions):
            loglike = np.zeros((len(states), self.n_samples))
            for i, m in enumerate(self.models):
                for k, s in enumerate(states):
                    loglike[k, i] = m.obs_distns[s].log_likelihood(self.data[sess], sess).sum()
            plt.subplot(len(sessions), 1, j + 1)
            higher = np.max(np.mean(loglike, axis=1))
            plt.ylim(top=higher+20, bottom=higher-20)
            plt.plot(loglike.T)
        plt.show()

    def init_dist_compare(self, states):
        """Plot how the probability for states evolves in the initial state dist across samples."""
        p_0s = np.zeros((len(states), self.n_samples))
        for i, m in enumerate(self.models):
            for j, s in enumerate(states):
                p_0s[j, i] = m.init_state_distn.pi_0[s]
        plt.plot(p_0s.T)
        plt.show()

    def glm_weights_chain(self):
        for s in self.proto_states:
            weights = np.empty((self.n_samples, self.n_sessions, 5))
            for i, m in enumerate(self.models):
                weights[i] = m.obs_distns[s].weights
            for w in range(5):
                plt.subplot(5, 1, w + 1)
                if w == 0:
                    plt.title(self.state_map[s])
                plt.plot(np.mean(weights[:, :, w], axis=1), color=self.state_to_color[s])
            plt.show()

    def prediction_vs_reward(self, zoe_way=False):
        perf = np.zeros(self.n_sessions)
        found_files = 0
        counter = self.infos['bias_start'] - 1 if self.type == 'bias' else -1
        while found_files < self.n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback[:, 1])
            assert feedback.shape[0] >= self.full_posteriors[found_files].shape[1]
            found_files += 1

        pred_p = np.zeros((self.n_samples, self.n_sessions))
        zero_p = np.zeros((self.n_samples, self.n_sessions))
        for i, m in enumerate(self.models):
            for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, self.data)):
                s.data = d
                preds = np.exp(s.aBl[np.arange(s.aBl.shape[0]), ss])
                if not zoe_way:
                    pred_p[i, j] = np.mean(preds)
                else:
                    pred_p[i, j] = np.mean(preds > 0.5)
                zeros = np.logical_and(d[:, 0] == 0, d[:, 1] == 0)
                if zeros.sum() > 0:
                    if not zoe_way:
                        zero_p[i, j] = np.mean(preds[zeros])
                    else:
                        zero_p[i, j] = np.mean(preds[zeros] > 0.5)
        plt.plot(perf, label="Performance")
        plt.plot(np.mean(pred_p, axis=0), label="Prediction")
        plt.plot(np.mean(zero_p, axis=0), label="Prediction on 0")
        plt.axhline(0.5, color='r')
        plt.legend(frameon=False, fontsize=16)
        plt.ylim(bottom=0, top=1)
        plt.ylabel('Performance', size=18)
        plt.xlabel('Session', size=18)
        plt.title(self.name, size=18)
        sns.despine()

        save_title = "dynamic_GLM_figures/predictive performance {}".format(self.save_id) if not zoe_way else "dynamic_GLM_figures/predictive performance zoe {}".format(self.save_id)
        plt.savefig(save_title)
        plt.close()

    def eval_cross_val(self, unmasked_data, show=False):
        lls = np.zeros(self.n_samples)
        cross_val_n = 0
        for time, (d_real, full_data) in enumerate(zip(self.data, unmasked_data)):
            d = d_real.copy()
            start_from = 0  # correction because initially I didn't cut off first trial for categorical trials
            if d.shape[0] == full_data.shape[0] + 1:
                start_from = 1
            held_out = np.isnan(d[start_from:, -1])
            cross_val_n += held_out.sum()
            d[start_from:, -1][held_out] = full_data[:, -1][held_out]
            # fake_d = d.copy()
            # fake_d[:, -1] = (fake_d[:, -1] + 1) % 2
            for i, m in enumerate(self.models):
                for s in range(self.n_all_states):
                    mask = np.logical_and(held_out, m.stateseqs[time][start_from:] == s)
                    if mask.sum() > 0:
                        ll = m.obs_distns[s].log_likelihood(d[start_from:][mask], time)
                        lls[i] += np.sum(ll)
                        # fake_ll = m.obs_distns[s].log_likelihood(fake_d[start_from:][mask], time)
                        # assert np.allclose(np.exp(ll), 1 - np.exp(fake_ll)), "logls no good"
        lls /= cross_val_n
        print(lls)
        if show:
            plt.hist(lls)
            plt.title(self.save_id + " mean: {}".format(lls.mean()))
            plt.savefig("dynamic_GLM_figures/cross_val/cross_val perf {}".format(self.save_id))
            plt.show()

        return lls

    def comp_sample_to_opt(self, unmasked_data, sample):
        # I compare the CV pred performance of the best sample, to the known correct solution
        cross_val_n = 0
        for time, (d_real, full_data) in enumerate(zip(self.data, unmasked_data)):
            if time != 6:
                continue
            d = d_real.copy()
            start_from = 0  # correction because initially I didn't cut off first trial for categorical trials
            if d.shape[0] == full_data.shape[0] + 1:
                start_from = 1
            held_out = np.isnan(d[start_from:, -1])
            cross_val_n += held_out.sum()
            d[start_from:, -1][held_out] = full_data[:, -1][held_out]
            m = self.models[sample]
            lls = np.zeros(held_out.sum())
            for i, t in enumerate(np.where(held_out)[0]):
                print(m.obs_distns[m.stateseqs[time][t]].weights[time])
                lls[i] = m.obs_distns[m.stateseqs[time][t]].log_likelihood(d[[t]], time)

            print()
            m.obs_distns[0].weights[:] = np.array([-3.5, 1.7, -1.5])
            opt_ll = m.obs_distns[0].log_likelihood(d[held_out], time)

            print(lls)
            print(opt_ll)
            print(lls.sum(), opt_ll.sum())
            print("we quit now because we messed with the weigths")
            quit()


    def eval_pred_perf(self, show=False, type='ll'):
        """Do same as above, but not cross validated, just check on all data."""
        import time as time_measure

        a = time_measure.time()
        lls = np.zeros(self.n_samples)
        n_without_masks = 0
        if type == 'only_obs':
            for time, d in enumerate(self.data):
                not_nans = ~np.isnan(d[:, -1])
                n_without_masks += not_nans.sum()
                for i, m in enumerate(self.models):
                    for s in range(self.n_all_states):
                        mask = np.logical_and(m.stateseqs[time] == s, not_nans)
                        if mask.sum() > 0:
                            ll = m.obs_distns[s].log_likelihood(d[mask], time)
                            lls[i] += np.sum(ll)
            lls /= n_without_masks
        else:
            data = self.data
            n_cores = 2
            size = int(self.n_samples / n_cores)
            arg_list = [(self.models[i*size:(i+1)*size], data) for i in range(n_cores)]
            pool = mp.Pool(n_cores)
            if type == 'll':
                func = ll_helper
            elif type == 'pred_ll':
                func = ll_pred_helper
            lls = pool.starmap(func, arg_list)
            pool.close()
            lls = np.concatenate(lls, axis=0)
        print(time_measure.time() - a)

        # a = time_measure.time()
        # log_pred_dens = np.zeros((self.n_samples, self.n_sessions))
        # for i, m in enumerate(self.models):
        #     for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, self.data)):
        #         s.data = d
        #         log_pred_dens[i, j] = np.sum(np.exp(s.aBl[np.arange(s.aBl.shape[0]), ss]))
        # print(time_measure.time() - a)
        # assert np.allclose(log_pred_dens.sum(1), lls)

        print('why did using self.n_datapoints here make the result so weird?')
        if show:
            plt.hist(lls)
            plt.title(self.save_id + " fully fitted mean: {}".format(lls.mean()))
            # plt.savefig("dynamic_GLM_figures/cross_val/cross_val perf {}".format(self.save_id))
            plt.show()

        return lls

    def generalised_state_occurences(self):
        # when are states used across sessions? Particularly, when is best state used, when are non-best states used?
        so_far_used_states = {}
        interpolated_best_state_occurence = np.zeros(100)
        # inv_map = {v: k for k, v in self.state_map.items()}  # delete if no need
        for seq_num in range(self.n_sessions):
            post_prop = self.posteriors[seq_num].sum(axis=1) / self.posteriors[seq_num].shape[1]
            with_bias = self.data[seq_num][:, -2].max()
            for s in self.proto_states:
                if post_prop[self.state_map[s]] < 0.05:
                    continue
                pmf_weights = np.zeros(self.models[0].obs_distns[0].weights.shape[1])
                for m in self.models:
                    pmf_weights += m.obs_distns[s].weights[seq_num]
                pmf_weights /= self.n_samples
                pmf = weights_to_pmf(pmf_weights, with_bias=with_bias)

                so_far_used_states[s] = pmf

            used_states, performances = [], []
            for s in so_far_used_states:
                used_states.append(s)
                performances.append(extended_pmf_acc(so_far_used_states[s], np.unique(self.data[seq_num][:, 0] - self.data[seq_num][:, 1])))

            # used_states = [x for _, x in sorted(zip(performances, used_states))]
            best_state = self.state_map[used_states[np.argmax(performances)]]
            interpolation = np.interp(np.linspace(0, 1, 100) * self.posteriors[seq_num].shape[1],
                                      np.arange(self.posteriors[seq_num].shape[1]), self.posteriors[seq_num][best_state])
            interpolated_best_state_occurence += interpolation
        interpolated_best_state_occurence /= self.n_sessions
        plt.plot(interpolated_best_state_occurence)
        plt.savefig("dynamic_GLM_figures/besto_stateo_interpolation_{}".format(self.name))
        plt.close()

        return interpolated_best_state_occurence

    def weight_distances(self):
        # how big are jumps in state weights, from noise and from new states?
        states_pmfs = {}
        noise_deltas = []
        jump_deltas = []
        encountered_states = {}
        for seq_num in range(self.n_sessions):
            post_prop = self.full_posteriors[seq_num].sum(axis=1) / self.full_posteriors[seq_num].shape[1]
            eligible_states = np.where(post_prop >= 0.05)
            states, pos = np.where(result.full_posteriors[seq_num][eligible_states[0]] > 0.05)  # bit low of a threshold, but otherwise it happens that state doesn't exist at all
            state_list = []
            occurence_trial = []
            for i, s in enumerate(eligible_states[0]):
                state_list.append(s)
                occurence_trial.append(np.min(pos[states == i]))
            ordered_states = [x for _, x in sorted(zip(occurence_trial, state_list))]
            for s in ordered_states:
                pmf_weights = np.zeros(self.models[0].obs_distns[0].weights.shape[1])
                for m in self.models:
                    pmf_weights += m.obs_distns[s].weights[seq_num]
                pmf_weights /= self.n_samples

                if s in encountered_states:
                    if encountered_states[s] == seq_num - 1:
                        noise_deltas.append(np.sum(np.abs(pmf_weights - states_pmfs[s])))
                else:
                    diff_list = []
                    if len(states_pmfs) != 0:
                        for ss in states_pmfs:
                            diff_list.append(np.sum(np.abs(pmf_weights - states_pmfs[ss])))
                        jump_deltas.append(min(diff_list))
                states_pmfs[s] = pmf_weights
                encountered_states[s] = seq_num
        return noise_deltas, jump_deltas


def waic_helper(models, data, n_sessions):
    log_pred_dens = np.zeros((len(models), n_sessions))
    # TODO: this is done much faster in eval_pred_perf
    for i, m in enumerate(models):
        for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, data)):
            s.data = d
            log_pred_dens[i, j] = np.sum(s.aBl[np.arange(s.aBl.shape[0]), ss])

    return log_pred_dens

def ll_helper(models, data):
    ll = np.zeros(len(models))

    for i, m in enumerate(models):
        for s, d in zip(m.states_list, data):
            s.data = d
        ll[i] = m.log_likelihood()

    return ll

def ll_pred_helper(models, data):
    ll_pred = np.zeros(len(models))
    print('in')
    print("maybe something can be done with result.models[0].states_list[0].messages_forwards()")
    print("but currently, this kind of predictive ll code is not ready")
    print('not ready')
    quit()
    for i, m in enumerate(models):
        for d in data:
            ll_pred[i] += m.predictive_likelihoods(d, [1])

    return ll_pred

def goodness_of_fit_helper(models, data, n_sessions):
    log_pred_dens = np.zeros((len(models), n_sessions))

    for i, m in enumerate(models):
        for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, data)):
            s.data = d
            log_pred_dens[i, j] = s.log_likelihood()

    return log_pred_dens



def waic_plots(subjects):
    waic = np.zeros((2, len(subjects)))
    for i, cond in enumerate(['nothing', 'answer']):
        for j, s in enumerate(subjects):
            mcmc_samples = pickle.load(open("./iHMM_fits/{}_{}_withtime_{}_condition_{}.p".format(s, 'bias', False, cond), "rb"))
            info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(s), "rb"))
            result = MCMC_result(mcmc_samples[-1000:], threshold=0.02, infos=info_dict, sessions='bias', conditioning=cond)
            waic[i, j] = result.load_waic()

    norm_waic = waic - waic[0]
    plt.figure(figsize=(12, 8))
    plt.plot(norm_waic)

    plt.xticks([0, 1], ['nothing', 'answer'], size=20)
    plt.xlabel('Conditioning', size=20)
    plt.ylabel('Delta WAIC', size=20)
    sns.despine()
    plt.tight_layout()
    plt.savefig("waic_overview")
    plt.show()
    return waic


subjects = ['CSH_ZAD_025', 'ZM_1897', 'KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06', 'SWC_023', 'CSHL051',
            'CSHL054', 'CSHL059', 'CSHL_007', 'CSHL_014', 'CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015',
            'CSH_ZAD_001', 'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011',
            'KS016', 'ibl_witten_16', 'KS019', 'CSH_ZAD_022', 'KS021', 'SWC_022', 'CSHL061', 'CSHL062'] # pre bias fits
subjects = ['CSHL045', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL055', 'CSHL059', 'CSHL061', 'CSHL062', 'CSHL065',
            'CSHL066', 'CSH_ZAD_011', 'CSH_ZAD_017', 'CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022', 'CSH_ZAD_024',
            'CSH_ZAD_026', 'KS002', 'KS014', 'KS016', 'KS022', 'KS023', 'SWC_022', 'ZM_1897'] # bias fits
# all : ['CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013'] # 'ZM_1897' last var is missing
# prebias = ['NYU-06', 'KS014', 'CSHL061', 'ibl_witten_13', 'KS022', 'SWC_021', 'CSH_ZAD_025', 'CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013']
# bias = ['CSHL053', 'CSH_ZAD_029', 'CSHL066', 'CSH_ZAD_019', 'CSHL061', 'CSHL055', 'CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013', 'CSHL066', 'CSHL060'] CSHL055 last var missing
subjects = ['Sim_01', 'Sim_02_var_0_2', 'Sim_02_var_0_04', 'Sim_03_5_dist_0_6_0_06', 'Sim_03_dist_0_6', 'Sim_03_dist_0_3']
subjects = ['SWC_054', 'ZFM-01592', 'SWC_060', 'SWC_043', 'CSHL059', 'ZM_2241', 'CSHL049', 'CSHL051',
            'DY_018', 'DY_013', 'DY_009', 'SWC_052', 'CSHL045', 'DY_020', 'CSHL052', 'SWC_058', 'ZFM-01936',
            'ZFM-01576', 'DY_016']  # CSH_ZAD_029 is missing 0.00005
subjects = ['CSHL062', 'SWC_054', 'DY_013', 'CSH_ZAD_022', 'ZM_1897']
subjects = ['KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06',
            'SWC_023', 'CSHL051', 'CSHL054', 'CSHL_007', 'CSHL_014']
# 'KS014', GLM_Sim_08
print(len(subjects))
fit_type = ['prebias', 'bias', 'all', 'prebias_plus', 'zoe_style'][0]
fit_variance = [0.01, 0.002, 0.0005, 'uniform', 0][3]
gammab = [1 / 10, 10, 100, 1000, 10000][3]
gammaa = 0.001
dur = 'yes'

cross_val = True
cross_val_num = 1
# hm = waic_plots(subjects)

# prebias_var_0.008_gamma_0.001_1000_33_1
# plain bad: CSHL059, ZM_3003, CSHL_020, CSHL_020
# rather meh: SWC_021
slices = {'CSHL_014':       (4500, 5700, 'good', 33),
          'ibl_witten_14':  (5830, 7480, 'looks good, but strong reverberations at start', 33),
          'KS015':          (3620, 6850, 'awesome', 33),
          'NYU-06':         (3000, 5000, 'okayish, could also take later one (but reverberations), 6900, None', 31),
          'KS022':          (2010, 2940, 'ok', 33),
          'KS014':          (6750, None, 'good', 33),
          'CSH_ZAD_025':    (7010, None, 'just about acceptable (but neat fit in the end)', 33),
          # 'CSHL051':        (7000, None, 'okayish', 33),
          'CSHL051':        (6600, None, 'good', 37),
          'CSHL_018':       (5000, 6750, 'good', 33),
          'CSH_ZAD_001':    (6000, None, 'great', 33),
          'ZM_1897':        (5925, 6945, 'probs good', 33),
          # 'CSH_ZAD_022':    (6355, 7200, 'ok, stopped too early', 33),
          'CSH_ZAD_022':    (4625, 6650, 'good', 34),
          # 'CSH_ZAD_022':    (6800, None, 'ok', 35),
          # 'CSH_ZAD_022':    (6500, None, 'ok', 36),
          # 'CSHL049':        (3800, 4800, 'good', 39),
          'CSHL049':        (6000, None, 'perfect, could take more', 38),
          # 'CSHL049':        (5000, 7000, 'Almost perfect, could take more', 37),
          # 'CSHL049':        (6000, 7000, 'Meh', 36),
          'ibl_witten_17':  (3600, 4850, 'reasonable', 33),
          'CSHL_015':       (3400, 5600, 'awesome, could take more', 33),
          'CSHL054':        (3460, 4820, 'ok', 34),
          'ibl_witten_13':  (4150, 6050, 'good, could take later one, but weak non-stationarity', 34),
          'CSH_ZAD_011':    (4000, 5850, 'good, could also take later posterior (slight non-stationarity there, 6450, None)', 34),
          'CSH_ZAD_026':    (4600, 5600, 'ok', 34),
          'ibl_witten_16':  (6730, None, 'meh, non-stationarity included', 34),
          'KS021':          (5650, None, 'almost perfect, could take more', 34),
          'KS016':          (4900, 7200, 'almost perfect, could take more (again)', 34),
          'SWC_023':        (5880, 6880, 'ok', 34),
          'KS019':          (4440, 5700, 'good', 34),
          # 'SWC_021':        (5000, 6000, 'good (other seeds suck though)', 37),
          'SWC_021':        (4600, 5600, 'good (other seeds suck though)', 38),
          'SWC_022':        (6050, None, 'great', 34),
          'CSHL052':        (-5000, None, 'hasty, redo', 34)  # bias
          # 'CSHL053':        (5320, 7530, 'great', 34),  # bias, so far only good fit
          # 'CSHL052':        (4650, 6320, 'ok, tough case', 34)  # bias
          }


slices = {'CSH_ZAD_022':    [(6000, None, 'great, could take more', 40),
                             (6600, 7700, 'good', 40),
                             (5054, 5692, 'bad, too few samples', 40),
                             (7000, None, 'ok', 40),
                             (6900, None, 'good', 40),
                             (5600, 6700, 'good', 40),
                             (5440, 6440, 'meh', 40),
                             (3000, 4000, 'good', 40)]}
subjects = ['CSHL_018', 'CSH_ZAD_011', 'KS003', 'NYU-06', 'KS014', 'CSHL054', 'CSH_ZAD_001',
            'CSHL_007', 'ibl_witten_13', 'ibl_witten_17', 'KS021', 'CSHL059', 'ibl_witten_16',
            'KS017', 'SWC_022', 'CSH_ZAD_022', 'KS019', 'KS016', 'CSHL062', 'SWC_021',
            'CSHL051', 'CSHL061', 'CSHL_015', 'ZM_1897', 'KS015'] # missed:
# no good: , 'ibl_witten_14'

slices = {'CSHL_007':        (420, 1200, 'good', 880, '_1', 80),
          'ibl_witten_13':   (1320, 2320, 'ok', 47, '', 80),
          # 'ibl_witten_13':   (0, 1000, 'ok', 47, '_2', 80)
          'ibl_witten_17':   (900, 1900, 'good', 780, '', 80),
          # 'KS021':           (1600, 2600, 'good (two states do same job)', 413, '', 80),
          'KS021':           (1100, 2100, 'quite good', 413, '_2', 80),
          'CSH_ZAD_001':     (1610, 2610, 'good', 407, '', 80),
          'CSHL059':         (2600, 3600, 'super stable (even across saves)', 718, '', 80),
          'ibl_witten_16':   (1900, 2900, 'meh', 853, '', 80),
          # 'KS017':           (3000, 3600, 'nice states, but few samples...', 744, ''),
          'KS017':           (850, 1850, 'okayish, really unsure whether this or other chain', 744, '_2', 80),
          # 'ibl_witten_14':   (None, None, 'all too changey', 361, '_1', 80)
          'SWC_022':         (2000, 3000, 'super stable (slight reverberations at start)', 967, '', 80),
          'CSH_ZAD_022':     (200, 1200, 'okayish', 773, '', 80),
          'KS019':           (1500, 2500, 'reasonable, but reverberations', 371, '', 80),
          'KS016':           (3000, None, 'good', 373, '', 80),
          # 'KS016':           (None, 1000, 'also good', 373, '_2', 80)
          'CSHL062':         (2750, None, 'reverberations prime example', 42, '', 80),
          'SWC_021':         (1000, 2000, 'very nice', 531, '', 80),
          # 'SWC_021':         (2000, 3000, 'nice', 531, '_2', 80)
          'CSHL051':         (2000, 3000, 'micro and macro reverberations', 839, '', 80),
          'CSHL061':         (825, 1300, 'super noisy, this is as good as it gets (nice garbage state though)', 863, '_2', 80),
          'CSHL_015':        (1500, 2500, 'nice (some of smaller states are noisy)', 536, '', 80),
          'ZM_1897':         (450, 1700, 'good, one state is small, but excluding it hurts sample size', 817, '_1', 80),
          'KS015':           (650, 1650, 'good, mini reverberations', 866, '', 80),
          'CSHL054':         (3000, None, 'good', 303, '_1', 80),
          'KS014':           (3000, None, 'mind-blowing stability', 744, '', 80),
          'NYU-06':          (2800, 3770, 'meh', 99, '_2', 80),
          'KS003':           (500, 1500, 'quite stable overall', 103, '', 80),
          'CSH_ZAD_011':     (920, 1920, 'okayish, generally quite switchy', 956, '', 80), # I think there happen to be another fit of this, 956, 755
          # 'CSHL_018':        (3000, None, '', 753, '_2', 80),  # also a double with seed 90 and num 965
          'CSHL_018':        (3000, None, '', 965, '_2', 90),
          }
# None
seed = 90
fit_num = None
if len(sys.argv) == 2:
    fit_num = '{}'.format(sys.argv[1])


threshold = 0.01
sess_count = 0
state_sesss = []
state_mouse = []
sess_mouse = []
state_appear = []
state_corrs = []
sess_state = []
up_states, down_states = [], []
side_diff, n_sess = [], []
best_state_interpolations = np.zeros(100)
performance_interpolations = np.zeros(100)
states_per_sess = []
noise_deltas = []
jump_deltas = []
#for subject, fit_variance in product(subjects, [0.05, 0.01, 0.002, 0.0005, 'uniform']):
# for subject, fit_variance in product(subjects, [0.008]):
for subject, fit_variance, cross_val_num in product(subjects, [0.03], [0]):
#for subject in subjects:
#for subject, gamma_prior in product(subjects, [(0.1, 100)]):
    print()
    info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))
    # if subject in slices and len(slices[subject]) > cross_val_num:
    if subject in slices:
        left, right, comment, fit_num, chain_num, seed = slices[subject]
        print("{} prepicked, comment: {}".format(subject, comment))
    else:
        left, right = -20, None
    if fit_num is None:
        load_file = "./dynamic_GLMiHMM_fits2/{}_{}_var_{}.p".format(subject, fit_type, fit_variance)
        # load_file = "./dynamic_iHMM_fits/{}_{}_withtime_{}_condition_{}_var_{}_gamma_{}_1.p".format(subject, fit_type, with_time, conditioned_on, fit_variance, str(gamma_prior).replace('.', '_'))
        print(load_file)
        mcmc_samples = pickle.load(open(load_file, "rb"))
        #mcmc_samples = pickle.load(open("./dynamic_iHMM_fits/{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), "rb"))
        print(len(mcmc_samples))
    else:
        if not cross_val:
            file = "./dynamic_GLMiHMM_crossvals/{}_{}_var_{}_gamma_{}_{}_{}_1.p".format(subject, fit_type, fit_variance, gammaa, gammab, fit_num)
            save_id = "{} {} {} gamma {} {}".format(subject, fit_type, fit_variance, (gammaa, gammab), fit_num).replace('.', '_')
        else:
            file = "./dynamic_GLMiHMM_crossvals/{}_crossval_{}_{}_var_{}_{}_{}.p".format(subject, cross_val_num, fit_type, fit_variance, seed, fit_num)
            file = "./dynamic_GLMiHMM_crossvals/{}_fittype_{}_var_{}_{}_{}{}.p".format(subject, fit_type, fit_variance, seed, fit_num, chain_num)

            # unmasked_data = pickle.load(open('./session_data/data_save_{}.p'.format(subject), 'rb'))
            # actual cross-val:
            save_id = "{}_crossval_{}_{}_var_{}_{}_{}.p".format(subject, cross_val_num, fit_type, fit_variance, seed, fit_num).replace('.', '_')
            # save_id = "{}_fittype_{}_var_{}_{}_{}.p".format(subject, fit_type, fit_variance, seed, fit_num).replace('.', '_')
        print(file)
        mcmc_samples = pickle.load(open(file, "rb"))
        print(len(mcmc_samples))

    result = MCMC_result(mcmc_samples[left:right], threshold=threshold, infos=info_dict,
                         data=mcmc_samples[0].datas, sessions=fit_type, fit_variance=fit_variance,
                         dur=dur, save_id=save_id)
    # argh
    # result.state_development(save=1, show=1)
    # result.contrasts_plot(until=45, show=1, save=1, dpi=300)
    # quit()
    a, b = result.weight_distances()
    noise_deltas += a
    jump_deltas += b

    best_state_interpolations += result.generalised_state_occurences()
    # result.dynamic_pmf_posterior_single_frame(show=1)
    performance_interpolations += result.performance_interpolation(windowsize=10)
    states_per_sess.append(result.states_per_sess())

    sess_count += result.n_sessions
    state_mouse.append(result.states_per_mouse())
    sess_mouse.append(result.n_sessions)
    state_appear += result.state_appearances()
    continue
    # continue
    # result.glm_plot()
    # result.compare_states([0, 6, 14], [-2, -1])
    # a, b = result.perserve_wsls(show=False)
    # side_diff.append(a)
    # n_sess.append(b)

    # result.glm_session_weights(show=1, ret_differetiate=True)

    # result.param_hist_nonstate((lambda m: m.trans_distn.gamma), title="{}, gamma_a_0: {}, gamma_b_0: {}".format(subject, gammaa, gammab).replace('.', '_'), xlabel='Gamma', save=True)

    # result.glm_session_weights(show=1)

    # result.prediction_vs_reward(zoe_way=True)

    # result.compare_states([4, 10], [21, 22, 23])
    ll = result.assign_evolution(show=1) # , show_like=True, unmasked_data=unmasked_data)
    result.contrasts_plot(until=45, show=1, save=1) #, dpi=300)
    quit()
    # result.cv_trial_plot(unmasked_data=unmasked_data, plot_cv_perf=False, plot_pred=opt)
    continue
    # result.comp_sample_to_opt(unmasked_data, sample=opt)
    # result.glm_session_weights(models=[opt], show=True)
    # a = result.eval_pred_perf()
    # quit()
    # lls = result.eval_cross_val(unmasked_data, show=1)
    quit()
    # quit()


    # result.log_like_plot()
    # result.glm_plot()
    # result.init_dist_compare([13, 5])
    # result.loglike_evolution([13, 5], [10, 11, 12, 13, 14])
    # result.glm_weights_chain()
    # a = result.glm_weights_clusters()
    # quit()

    #result.pmf_compare(show=1, save=1)
    # result.block_alignment(show=1)
    # state_counts += list(result.states_per_sess())

    # a, b = result.pmf_return()
    # if a is None:
    #     continue
    # up_states.append(a)
    # down_states.append(b)
    # sess_state += result.sessions_per_state()
    # state_corrs += result.thresholded_state_correlations()
    # result.param_hist_nonstate((lambda m: m.trans_distn.gamma), title="{}_{}_withtime_{}_condition_{}_var_{}_gamma_{}_3".format(subject, fit_type, with_time, conditioned_on, fit_variance, str(gamma_prior).replace('.', '_')), xlabel='Gamma', save=True)
    # result.glm_session_weights(show=True)
    #result.plot_glm_weights()
    #result.state_development_truth(save=1, show=1)
    # result.dynamic_pmf_posterior_one_sess(show=1, sess=19)
    # result.dynamic_pmf_posterior(show=1)
    #result.single_pmf_posterior()
    #result.param_hist((lambda m, s: m.obs_distns[s].sigmasq_states), truth=0.0, title="{} dynamic variance".format(subject), xlabel='Dynamic variance', save=False)

    continue
    result.belief_hist()
    result.waic()


    #result.param_hist((lambda m, s: m.dur_distns[s].p), xlabel='p')
    #result.param_hist((lambda m, s: m.dur_distns[s].r), xlabel='r')
    # result.waic()
    continue
    # result.pmf_posterior()
    #result.belief_csv()
    # result.pmf_diffs(show=1)
prof._prof.print_stats()
print(sess_count)

j_min = np.min(jump_deltas)
j_max = np.max(jump_deltas)
n_min = np.min(noise_deltas)
n_max = np.max(noise_deltas)

plt.hist(noise_deltas, bins=10 ** np.linspace(np.log10(n_min), np.log10(n_max), 10), label="State dynamics", color='grey')
a = plt.hist(jump_deltas, bins=10 ** np.linspace(np.log10(j_min), np.log10(j_max), 10), label="New state", color='black')
plt.close()

plt.hist(noise_deltas, bins=10 ** np.linspace(np.log10(n_min), np.log10(n_max), 10), label="State dynamics", color='grey')
plt.hist(jump_deltas, bins=10 ** np.linspace(np.log10(j_min), np.log10(j_max), 10), label="New state", color='black', bottom=a[1:])

# plt.hist(jump_deltas, label="New state", color='black')
# plt.hist(noise_deltas, label="State dynamics", color='grey')

plt.gca().set_xscale("log")
plt.ylabel('Occurences', fontsize=22)
plt.xlabel('Log weight change', fontsize=22)
plt.legend(frameon=False, fontsize=18, bbox_to_anchor=(0.6, 0.9))
plt.gca().tick_params(axis='both', labelsize=14)
sns.despine()
plt.tight_layout()
plt.savefig("weight changes", dpi=300)
plt.close()

quit()
np.random.seed(4)
plt.scatter(np.array(sess_mouse) + 0.3 * np.random.rand(25), np.array(state_mouse) + 0.3 * np.random.rand(25), color='grey')
plt.ylabel('# of states', fontsize=28)
plt.xlabel('# of sessions', fontsize=28)
plt.gca().tick_params(axis='both', labelsize=18)
sns.despine()
plt.tight_layout()
plt.savefig("states_n_sessions_scatter", dpi=300)
plt.close()


plt.figure(figsize=(8, 9))
plt.title('Occurence of best current state', fontsize=30)
plt.plot(np.linspace(0, 1, 100), best_state_interpolations / len(list(subjects)), color='grey', lw=4, label='Best state')
# plt.plot(np.linspace(0, 1, 100), temp, color='grey', lw=4, label='Best state')
plt.plot(np.linspace(0, 1, 100), performance_interpolations / len(list(subjects)), color='b', lw=4, label='Average performance')
plt.ylabel('Probability', fontsize=28)
plt.xlabel('Interpolated time in session', fontsize=28)
plt.ylim(bottom=0, top=0.75)
plt.xlim(left=0, right=1)
plt.legend(frameon=False, fontsize=28)
plt.gca().tick_params(axis='both', labelsize=18)
sns.despine()
plt.tight_layout()
plt.savefig("best current state occurence", dpi=300)
plt.close()

plt.figure(figsize=(8, 9))
plt.hist(state_mouse, color='grey', bins=0.5 + np.arange(8))
plt.title('State threshold {}'.format(threshold), fontsize=30)
plt.ylabel('# of mice', fontsize=30)
plt.gca().set_xticks([2, 4, 6])
plt.xlabel('Total # of states', fontsize=30)
plt.ylim(bottom=0, top=13)
plt.gca().tick_params(axis='both', labelsize=20)
sns.despine()
plt.tight_layout()
plt.close()



# a2.hist(sess_state, color='grey')
# a2.set_ylabel('# of states', fontsize=30)
# a2.set_xlabel('Proportion of sessions', fontsize=30)
# a2.tick_params(axis='both', labelsize=20)
# sns.despine()

#
# quit()

# plt.figure(figsize=(12, 9))
# for u, d in zip(up_states, down_states):
#     plt.plot(u, 'r')
#     plt.plot(d, 'b')
#
# sns.despine()
# plt.xlabel("Contrast", fontsize=28)
# plt.ylabel("Probability rightward", fontsize=28)
# plt.tight_layout()
# plt.show()
# quit()

f, (a0, a1) = plt.subplots(1, 2, figsize=(18,9), gridspec_kw={'width_ratios': [1, 1.4]})
a1.hist(state_appear, color='grey')
a1.set_xlim(left=0, right=1)
# plt.title('First appearence of ', fontsize=22)
a1.set_ylabel('# of states', fontsize=30)
a1.set_xlabel('Training proportion at appearance', fontsize=30)
a1.tick_params(axis='both', labelsize=20)
sns.despine()

a0.hist(state_mouse, color='grey', bins=3.5 + np.arange(8))
# plt.title('First appearence of ', fontsize=22)
a0.set_ylabel('# of mice', fontsize=30)
a0.set_xticks([4, 7, 10])
a0.set_xlabel('Total # of states', fontsize=30)
a0.tick_params(axis='both', labelsize=20)
sns.despine()
plt.tight_layout()
plt.savefig('states in sessions', dpi=300)
plt.close()

for i in range(10, 14):
    sns.heatmap(state_sess_interpol(states_per_sess, interpol_n=i))
    plt.ylabel('# of states', fontsize=30)
    plt.yticks([4.5, 2.5, 0.5], [2, 4, 6])
    plt.xticks([0.5, 4.5, 7.5, 11.5], [1, 4, 8, 12])
    plt.xlabel('Interpolated session number', fontsize=30)
    plt.tick_params(axis='both', labelsize=20)
    plt.tight_layout()
    plt.savefig("sps interpol {}".format(i), dpi=300)
    plt.close()
