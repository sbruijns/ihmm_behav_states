from one.api import ONE
import psytrack as psy
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from scipy.stats import sem
import copy
import warnings

one = ONE(mode='local')
SPATH = './'
subject = 'CSH_ZAD_022'
subject = ['KS016', 'CSHL061', 'SWC_023', 'ibl_witten_17'][0]
subject = ['CSHL051', 'CSHL054']
subject = ['CSHL_007'][0]

# 'SWC_023': -0.47250523058448435           my best: -0.505, -0.504, -0.52
# 'CSH_ZAD_022': -0.38766323073000963       my best: -0.403, -0.425, -0.427
# 'CSHL061': -0.44454087212417803           my best: -0.454, -0.447, -0.445
# 'KS019': -0.4853980051509297              my best: missing
# 'ibl_witten_14': -0.49230661954769467     my best: -0.511, -0.487, -0.584
# 'CSHL_007': -0.5162144295020293           my best: -0.454, -0.56, -0.511

plt.plot([0, 1], [-0.47250523058448435, -0.47250523058448435], 'r', label='Nick full CV')
plt.scatter([0.5, 0.2, 0.8], [-0.505, -0.504, -0.52], color='b', label='1 fold of 10-fold CV')

plt.plot([2, 3], [-0.38766323073000963, -0.38766323073000963], 'r')
plt.scatter([2.5, 2.2, 2.8], [-0.403, -0.425, -0.427], color='b')

plt.plot([4, 5], [-0.44454087212417803, -0.44454087212417803], 'r')
plt.scatter([4.5, 4.2, 4.8], [-0.454, -0.447, -0.445], color='b')

plt.plot([6, 7], [-0.49230661954769467, -0.49230661954769467], 'r')
plt.scatter([6.5, 6.2, 6.8], [-0.511, -0.487, -0.584], color='b')

plt.plot([8, 9], [-0.5162144295020293, -0.5162144295020293], 'r')
plt.scatter([8.5, 8.2, 8.8], [-0.454, -0.56, -0.511], color='b')

plt.xticks([0.5, 2.5, 4.5, 6.5, 8.5], ['SWC_023!', 'CSH_ZAD_022.', 'CSHL061!', 'ibl_witten_14', 'CSHL_007'])

plt.legend()
plt.show()
quit()

colors = psy.COLORS
zorder = psy.ZORDER
plt.rcParams['figure.dpi'] = 140
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['savefig.facecolor'] = (1, 1, 1, 0)
plt.rcParams['savefig.bbox'] = "tight"
plt.rcParams['font.size'] = 10
# plt.rcParams['font.family'] = 'sans-serif'     # not available in Colab
# plt.rcParams['font.sans-serif'] = 'Helvetica'  # not available in Colab
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['axes.labelsize'] = 12

# Nick style
# required_vars = ['_ibl_trials.choice', '_ibl_trials.contrastLeft',
#                  '_ibl_trials.contrastRight','_ibl_trials.feedbackType']
# eids = one.search(required_vars)

redo = True
if redo:
    # my style
    eids, sess_info = one.search(subject=subject, date_range=['2015-01-01', '2022-01-01'], details=True)
    start_times = [sess['date'] for sess in sess_info]
    protocols = [sess['task_protocol'] for sess in sess_info]

    protocols = [x for _, x in sorted(zip(start_times, protocols), key=lambda pair: pair[0])]
    sess_info = [x for _, x in sorted(zip(start_times, sess_info), key=lambda pair: pair[0])]
    eids = [x for _, x in sorted(zip(start_times, eids), key=lambda pair: pair[0])]

    sess_info = [si for p, si in zip(protocols, sess_info) if p.startswith('_iblrig_tasks_trainingChoiceWorld')]
    eids = [eid for p, eid in zip(protocols, eids) if p.startswith('_iblrig_tasks_trainingChoiceWorld')]

    if subject == 'CSH_ZAD_022':
        # for some reason one session way later also has trainingchoiceworld
        eids = eids[:-1]
        sess_info = sess_info[:-1]

    mouseData = pd.DataFrame()
    for i, (eid, si) in enumerate(zip(eids, sess_info)):
        sess_vars = {
            "eid": eid,
            "lab": si.lab,
            "subject": si.subject,
            "date": si.date,
            "session": i,  # speculation that this is right here?
        }
        mouseData = mouseData.append(sess_vars, sort=True, ignore_index=True)


    # BOX 3)
    all_vars = ["contrastLeft", "contrastRight", "choice", "feedbackType", "probabilityLeft"]
    df = pd.DataFrame()

    all_mice = []
    for j, s in enumerate(mouseData["subject"].unique()):
        print("\rProcessing " + str(j+1) + " of " + str(len(mouseData["subject"].unique())), end="")
        mouse = mouseData[mouseData["subject"] == s].sort_values(['date', 'session']).reset_index()
        for i, row in mouse.iterrows():
            myVars = {}

            data_dict = one.load_object(row.eid, 'trials')
            for v in all_vars:
                myVars[v] = list(data_dict[v].flatten())

            num_trials = len(myVars[v])
            myVars['lab'] = [row.lab]*num_trials
            myVars['subject'] = [row.subject]*num_trials
            myVars['date'] = [row.date]*num_trials
            myVars['session'] = [row.session]*num_trials

            all_mice += [pd.DataFrame(myVars, columns=myVars.keys())]

    df = pd.concat(all_mice, ignore_index=True)

    df = df[df['choice'] != 0]        # dump mistrials
    df = df[df['feedbackType'] != 0]  # 3 anomalous trials from ZM_1084, omit
    df.loc[np.isnan(df['contrastLeft']), "contrastLeft"] = 0
    df.loc[np.isnan(df['contrastRight']), "contrastRight"] = 0
    df.loc[df["contrastRight"] < 0, "contrastLeft"] = np.abs(df.loc[df["contrastRight"] < 0, "contrastRight"])
    df.loc[df["contrastRight"] < 0, "contrastRight"] = 0  # 81 anomalous trials in CSHL_002, correct
    df["answer"] = df["feedbackType"] * df["choice"]      # new column to indicate correct answer
    df.loc[df["answer"] == 1, "answer"] = 0
    df.loc[df["answer"] == -1, "answer"] = 1
    df.loc[df["feedbackType"] == -1, "feedbackType"] = 0
    df.loc[df["choice"] == 1, "choice"] = 0
    df.loc[df["choice"] == -1, "choice"] = 1
    df.to_csv('./' + "ibl_processed_{}.csv".format(subject), index=False)

    print()
    print("contrastLeft: ", np.unique(df['contrastLeft']))    # [0, 0.0625, 0.125, 0.25, 0.5, 1.0] and [0.04, 0.08]
    print("contrastRight: ", np.unique(df['contrastRight']))  # [0, 0.0625, 0.125, 0.25, 0.5, 1.0] and [0.04, 0.08]
    print("choice: ", np.unique(df['choice']))                # [0, 1]
    print("feedbackType: ", np.unique(df['feedbackType']))    # [0, 1]
    print("answer: ", np.unique(df['answer']))                # [0, 1]

# It gets serious again
ibl_mouse_data_path = './' + "ibl_processed_{}.csv".format(subject)
MOUSE_DF = pd.read_csv(ibl_mouse_data_path)


def getMouse(subject, p=5):
    df = MOUSE_DF[MOUSE_DF['subject'] == subject]   # Restrict data to the subject specified

    cL = np.tanh(p*df['contrastLeft'])/np.tanh(p)   # tanh transformation of left contrasts
    cR = np.tanh(p*df['contrastRight'])/np.tanh(p)  # tanh transformation of right contrasts
    inputs = dict(cL=np.array(cL)[:, None], cR=np.array(cR)[:, None])

    dat = dict(
        subject=subject,
        lab=np.unique(df["lab"])[0],
        contrastLeft=np.array(df['contrastLeft']),
        contrastRight=np.array(df['contrastRight']),
        date=np.array(df['date']),
        dayLength=np.array(df.groupby(['date', 'session']).size()),
        correct=np.array(df['feedbackType']),
        answer=np.array(df['answer']),
        probL=np.array(df['probabilityLeft']),
        inputs=inputs,
        y=np.array(df['choice']))

    return dat


FOLDS = 10  # number of cross-validation folds
SEED = 42   # controls random divide of trials into FOLDS bins

outData = getMouse(subject, 5)
outData['contrast'] = outData['contrastRight'] - outData['contrastLeft']
counter = 0
bias_start = 0
for dl in outData['dayLength']:
    elem_l = np.unique(outData['contrastLeft'][counter:counter + dl])
    elem_r = np.unique(outData['contrastRight'][counter:counter + dl])
    if elem_l.shape[0] == 4 or elem_r.shape[0] == 4:
        first_l = np.where(outData['contrastLeft'][counter:counter + dl] == 0.25)[0][0]
        first_r = np.where(outData['contrastRight'][counter:counter + dl] == 0.25)[0][0]
        first = min(first_l, first_r)
        if 1 - first / dl > 0.5:
            bias_start = counter
        else:
            bias_start = counter + dl
        break
    counter += dl


bias_dat = psy.trim(outData, START=bias_start, END=outData['date'].shape[0] - (outData['date'].shape[0] - bias_start) % 10)
no_bias_dat = psy.trim(outData, END=bias_start - bias_start % 10)


optList = ['sigma', 'sigDay']
type = 'unaltered'

fit = False
if fit:
    weights = {'bias': 1, 'cL': 1, 'cR': 1}
    K = np.sum([weights[i] for i in weights.keys()])
    hyper_guess = {'sigma': [2**-5]*K, 'sigInit': 2**5, 'sigDay': [2**-4]*K}
    hyp, evd, wMode, hess_info = psy.hyperOpt(copy.deepcopy(bias_dat), hyper_guess, weights, optList)
    dat = {'hyp': hyp, 'evd': evd, 'wMode': wMode, 'W_std': hess_info['W_std'],
           'weights': weights, 'new_dat': bias_dat}
    np.savez_compressed(SPATH+'fig3b_data_mine_{}_{}_{}.npz'.format(subject, type, weights['bias']), dat=dat)
    plt.plot(wMode.T)
    plt.show()

    weights = {'bias': 0, 'cL': 1, 'cR': 1}
    K = np.sum([weights[i] for i in weights.keys()])
    hyper_guess = {'sigma': [2**-5]*K, 'sigInit': 2**5, 'sigDay': [2**-4]*K}
    hyp, evd, wMode, hess_info = psy.hyperOpt(copy.deepcopy(no_bias_dat), hyper_guess, weights, optList)
    dat = {'hyp': hyp, 'evd': evd, 'wMode': wMode, 'W_std': hess_info['W_std'],
           'weights': weights, 'new_dat': no_bias_dat}
    np.savez_compressed(SPATH+'fig3b_data_mine_{}_{}_{}.npz'.format(subject, type, weights['bias']), dat=dat)
    plt.plot(wMode.T)
    plt.show()

new = True
if new:
    weights = {'bias': 1, 'cL': 1, 'cR': 1}
    K = np.sum([weights[i] for i in weights.keys()])
    hyper_guess = {'sigma': [2**-5]*K, 'sigInit': 2**5, 'sigDay': [2**-4]*K}
    xval_logli_post, xval_pL = psy.crossValidate(copy.deepcopy(bias_dat), hyper_guess, weights, optList,
                                                F=FOLDS, seed=SEED)
    np.savez_compressed(SPATH+'my_cross_val_data_{}_{}_bias_{}.npz'.format(subject, type, weights['bias']),
                        new_dat=bias_dat, xval_pL=xval_pL)

    weights = {'bias': 0, 'cL': 1, 'cR': 1}
    K = np.sum([weights[i] for i in weights.keys()])
    hyper_guess = {'sigma': [2**-5]*K, 'sigInit': 2**5, 'sigDay': [2**-4]*K}
    xval_logli_pre, xval_pL = psy.crossValidate(copy.deepcopy(no_bias_dat), hyper_guess, weights, optList,
                                                F=FOLDS, seed=SEED)
    np.savez_compressed(SPATH+'my_cross_val_data_{}_{}_bias_{}.npz'.format(subject, type, weights['bias']),
                        new_dat=no_bias_dat, xval_pL=xval_pL)

# >>> xval_logli_post
# -6068.075062397276
# >>> xval_logli_pre
# 535.5756790442708  # wth dude?

# unclear what's correct
# >>> (xval_logli_post + xval_logli_pre) / (b['xval_pL'].shape[0] + no_b['xval_pL'].shape[0])
# -0.47776333189576903
# >>> (np.log(np.abs(no_bias_dat['y'] - xval_pL)).sum() + np.log(np.abs(bias_dat['y'] - b['xval_pL'])).sum()) / (b['xval_pL'].shape[0] + no_b['xval_pL'].shape[0])
# -0.38766323073000963

# previously didn't save name (subject)
no_b = np.load('my_cross_val_data_{}_{}_bias_{}.npz'.format(subject, type, 0), allow_pickle=1)
b = np.load('my_cross_val_data_{}_{}_bias_{}.npz'.format(subject, type, 1), allow_pickle=1)

print("Nick's perf")
print((np.log(np.abs(no_bias_dat['y'] - xval_pL)).sum() + np.log(np.abs(bias_dat['y'] - b['xval_pL'])).sum()) / (b['xval_pL'].shape[0] + no_b['xval_pL'].shape[0]))


quit()
print('Nicks performance:')
# this used to be new_dat['answer'], but it needs to be 'y', which encodes choice (answer is correct side)
print("{}, bias weights: {}".format(type, 1))
print(np.mean(np.abs(bias_dat['y'] - b['xval_pL'])))
print("{}, bias weights: {}".format(type, 0))
print(np.mean(np.abs(no_bias_dat['y'] - no_b['xval_pL'])))

temp = np.zeros(no_bias_dat['y'].shape[0] + bias_dat['y'].shape[0])
temp[:no_bias_dat['y'].shape[0]] = np.abs(no_bias_dat['y'] - no_b['xval_pL'])
temp[no_bias_dat['y'].shape[0]:] = np.abs(bias_dat['y'] - b['xval_pL'])

quit()
# predictions per day
# count = 0
# for d in new_dat['dayLength']:
#     cont, counti = np.unique(new_dat['contrast'][count:count + d], return_counts=1)
#     l_occ, r_occ = counti[cont < 0].sum(), counti[cont > 0].sum()
#     print(l_occ, r_occ, 0.5 + np.abs(0.5 - l_occ / (l_occ + r_occ)))
#     print(np.mean(np.abs(new_dat['y'] - a['xval_pL'])[count:count + d]))
#     print()
#     count += d


# averaging functions
# def averages(seq, win=10):
#     ret = np.zeros(seq.shape[0] // win)
#     for i in range(seq.shape[0] // win):
#         ret[i] = np.mean(seq[i * win: (i + 1) * win])
#     return ret

# plt.plot(averages(np.abs(new_dat['y'] - a['xval_pL']), win=30))
# plt.show()

type = ['random_noise', 'unaltered', 'random_bias_0_6', 'random_bias_0_8'][1]

if type == 'random_noise':
    new_dat['y'][:] = np.random.rand(new_dat['y'].shape[0]) > 0.5
elif type == 'random_bias_0_6':
    new_dat['y'][:] = np.random.rand(new_dat['y'].shape[0]) > 0.6
elif type == 'random_bias_0_8':
    new_dat['y'][:] = np.random.rand(new_dat['y'].shape[0]) > 0.8


# unaltered, bias weights: 1
# 0.7521587292155238

# unaltered, bias weights: 0
# 0.7476410794093039

# random_noise, bias weights: 0
# this is not updated, but that's probs ok
# 0.500070266338031

# random_noise, bias weights: 1
# 0.5003634176423873


# random_bias_0_6, bias weights: 1
# 0.5192005131311875
# ____ why is this so awful? ____
# duh, 0.6 ** 2 + 0.4 ** 2

# random_bias_0_6, bias weights: 0
# 0.5136408756821639
# this on the other hand is too good...

# random_bias_0_8, bias weights: 0
# 0.659012541941315
# this is not that surprising, one only every gets one contrast, so they can take the role of a bias, only problem is that it will
# be a bias scaled by contrast (which the real one is not)

# random_bias_0_8, bias weights: 1
# 0.6795571384307016
