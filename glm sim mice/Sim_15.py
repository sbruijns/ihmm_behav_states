"""
Generate data from a simulated mouse-GLM.

This mouse uses 6 states all throughout, but duration prior is misspecified
They start a certain distance apart, states alternate during the session, duration is negative binomial
"""
import numpy as np
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom
from scipy.linalg import eig
from communal_funcs import weights_to_pmf

subject = 'CSH_ZAD_022'
new_name = 'GLM_Sim_15'
seed = 20

print(new_name)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['bias_start']
from_session = 0


GLM_weights = [np.array([0., 0., 0.]),
               np.array([-2.8, 2.8, 0.]),
               np.array([2.8, -2.8, 0.]),
               np.array([-0.85, 0.85, 0.]),
               np.array([-2.5, -2.5, -2.5]),
               np.array([2.5, 2.5, 2.5])]

plt.subplot(2, 1, 1)
for gw in GLM_weights:
    plt.plot(weights_to_pmf(gw))

plt.subplot(2, 1, 2)
for gw in GLM_weights:
    plt.plot(weights_to_pmf(gw))
plt.close()
neg_bin_params = [(23, 0.2), (91, 0.38), (211, 0.42), (10, 0.2), (159, 0.52), (7, 0.15)]
# [5, 15, 30, 50, 75, 105, 140, 180, 225, 275, 330, 390, 455, 525, 600, 680, 765, 855, 950]


transition_mat = np.random.dirichlet(np.ones(6) * 0.5, (6))
np.fill_diagonal(transition_mat, 0)
transition_mat = transition_mat / transition_mat.sum(1)[:, None]
print(transition_mat)
print(transition_mat.sum(1))

# this computation doesn't work for some reason
eigenvals, eigenvects = eig(transition_mat.T)
close_to_1_idx = np.isclose(eigenvals, 1)
target_eigenvect = eigenvects[:, close_to_1_idx]
target_eigenvect = target_eigenvect[:, 0]
# Turn the eigenvector elements into probabilites
stationary_distrib = target_eigenvect / sum(target_eigenvect)
print(stationary_distrib)

contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}

state_posterior = np.zeros((till_session + 1 - from_session, len(GLM_weights)))
state_counter_array = np.zeros((till_session + 1 - from_session, len(GLM_weights)))
observed_states = []
first_appearances = []
session_bounds = [0]
trial_counter = 0
for k, j in enumerate(range(from_session, till_session + 1)):

    data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    side_info = pickle.load(open("../session_data/{}_side_info_{}.p".format(subject, j), "rb"))

    contrasts = np.vectorize(num_to_contrast.get)(data[:, 0])

    predictors = np.zeros(3)
    state_plot = np.zeros(len(GLM_weights))
    count = 0
    curr_state = np.random.choice(6)
    if curr_state not in observed_states:
        first_appearances.append(trial_counter)
        observed_states.append(curr_state)

    observed_states.append(curr_state)
    curr_dur = np.random.negative_binomial(*neg_bin_params[curr_state]) + 1

    prev_choice = 2 * int(np.random.rand() > 0.5)
    data[0, 1] = prev_choice

    state_counter = 0

    for i, c in enumerate(contrasts[1:]):
        predictors[0] = max(c, 0)
        predictors[1] = abs(min(c, 0))
        predictors[2] = 1
        data[i+1, 1] = 2 * (np.random.rand() < 1 / (1 + np.exp(- np.sum(GLM_weights[curr_state] * predictors))))
        state_plot[curr_state] += 1
        curr_dur -= 1
        if curr_dur == 0:
            state_counter += 1
            curr_state = np.random.choice(6, p=transition_mat[curr_state])
            if curr_state not in observed_states:
                first_appearances.append(trial_counter + i)
                observed_states.append(curr_state)
            curr_dur = np.random.negative_binomial(*neg_bin_params[curr_state]) + 1

    state_posterior[k] = state_plot / len(data[:, 0])
    state_counter_array[k] = state_plot
    trial_counter += len(data[:, 0])
    session_bounds.append(trial_counter)
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
    pickle.dump(side_info, open("../session_data/{}_side_info_{}.p".format(new_name, j), "wb"))

plt.vlines(session_bounds, 0, 1.2, color='k')
plt.vlines(first_appearances, 0, 1, color='r')
plt.close()

state_props = state_posterior.sum(0) / state_posterior.sum()
print(state_props)
print(state_props.min(), state_props.max())
plt.figure(figsize=(16, 9))
for s in range(len(GLM_weights)):
    plt.fill_between(range(till_session + 1 - from_session), s, s + state_posterior[:, s])

plt.savefig('states_15')
plt.close()

# 2: 0
truth = {'state_posterior': state_posterior, 'weights': GLM_weights, 'state_map': {1: 1, 5: 5, 4: 3, 0: 4, 2: 0, 3: 2}, 'durs': neg_bin_params}
pickle.dump(truth, open("truth_{}.p".format(new_name), "wb"))