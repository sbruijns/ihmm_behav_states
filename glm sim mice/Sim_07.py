"""
Generate data from a simulated mouse-GLM.

This mouse uses 4 states throughout
same as 06 but more data
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom


subject = 'CSHL059'
new_name = 'GLM_Sim_07'
seed = 8

print(new_name)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
info_dict['bias_start'] = info_dict['bias_start'] * 2
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['bias_start']
from_session = 0

GLM_weights = [np.array([-4.5, 4.3, 0., 1.2, -0.7]),
               np.array([-4.5, 3.2, 1.3, 0.3, -1.5]),
               np.array([-1, 1.2, 2.1, 0.1, 1]),
               np.array([-0.1, 0.2, 0., 3.9, -1.])]
GLM_weights = list(reversed(GLM_weights))
states = [0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 1, 1, 2, 3, 3, 3, 0, 3]

contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}

state_posterior = np.zeros((till_session + 1 - from_session, 4))

for k, j in enumerate(range(from_session, till_session + 1)):
    data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j // 2), "rb"))
    data = np.tile(data, (2, 1))
    side_info = pickle.load(open("../session_data/{}_side_info_{}.p".format(subject, j // 2), "rb"))
    side_info = np.tile(side_info, (2, 1))
    if j % 2 == 1:
        np.random.shuffle(data)
        np.random.shuffle(side_info)

    contrasts = np.vectorize(num_to_contrast.get)(data[:, 0])

    predictors = np.zeros(5)
    state_plot = np.zeros(4)
    count = 0
    curr_state = states[j // 2]

    prev_choice = 2 * int(np.random.rand() > 0.5) - 1
    if (contrasts[0] < 0) == (prev_choice > 0):
        prev_reward = 1.
    elif contrasts[0] > 0:
        prev_reward = 0.
    else:
        prev_reward = int(np.random.rand() > 0.5)
    prev_side = - prev_choice if prev_reward == 0. else prev_choice
    data[0, 1] = prev_choice + 1
    side_info[0, 1] = prev_reward

    for i, c in enumerate(contrasts[1:]):
        predictors[0] = max(c, 0)
        predictors[1] = abs(min(c, 0))
        predictors[2] = prev_choice
        predictors[3] = prev_side
        predictors[4] = 1
        data[i+1, 1] = 2 * (np.random.rand() < 1 / (1 + np.exp(- np.sum(GLM_weights[curr_state] * predictors))))
        state_plot[curr_state] += 1

        prev_choice = data[i+1, 1] - 1
        if (c < 0) == (prev_choice > 0):
            prev_reward = 1.
        elif c > 0:
            prev_reward = 0.
        else:
            prev_reward = int(np.random.rand() > side_info[i+1, 0])
        prev_side = - prev_choice if prev_reward == 0. else prev_choice
        side_info[i+1, 1] = prev_reward
    state_posterior[k] = state_plot / len(data[:, 0])
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
    pickle.dump(side_info, open("../session_data/{}_side_info_{}.p".format(new_name, j), "wb"))

plt.figure(figsize=(16, 9))
for s in range(4):
    plt.fill_between(range(till_session + 1 - from_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)


plt.savefig('states_07')
plt.show()

truth = {'state_posterior': state_posterior, 'weights': list(reversed(GLM_weights)), 'state_map': dict(zip(list(range(len(GLM_weights))), list(range(len(GLM_weights)))))}
pickle.dump(truth, open("truth_{}.p".format(new_name), "wb"))
