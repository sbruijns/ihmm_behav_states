"""
Generate data from a simulated mouse-GLM.

This mouse uses 4 states throughout
They start a certain distance apart, states alternate during the session, duration is negative binomial
Same as 08, but now also noise on the glm weights
this seems to have changed a bit, there are now 18 sessions of this mouse, but we orignially only did 16...
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom


subject = 'CSH_ZAD_022'
new_name = 'GLM_Sim_12'
seed = 13

print(new_name)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['bias_start']
from_session = 0

contrasts_L = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])
contrasts_R = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])[::-1]

def weights_to_pmf(weights, with_bias=1):
    if weights.shape[0] == 3 or weights.shape[0] == 5:
        psi = weights[0] * contrasts_L + weights[1] * contrasts_R + with_bias * weights[-1]
        return 1 / (1 + np.exp(-psi))
    elif weights.shape[0] == 11:
        return weights[:, 0]
    else:
        print('new weight shape')
        quit()

GLM_weights = [np.array([-3.5, 3.3, -0.7]),
               np.array([-3.5, 1.7, -1.5]),
               np.array([-0.3, 1.2, 1]),
               np.array([-0.1, 0.2, -1.])]
neg_bin_params = [(180, 0.2), (75, 0.12), (105, 0.14), (140, 0.15)]
# [5, 15, 30, 50, 75, 105, 140, 180, 225, 275, 330, 390, 455, 525, 600, 680, 765, 855, 950]
GLM_weights = list(reversed(GLM_weights))
states = [(0,), (0,), (0,), (1, 0), (1, 0), (1, 0), (2,), (2, 3), (2, 3, 1), (2, 3), (3,), (1, 3), (1, 2), (3, 2), (3, 2), (3, 2, 1, 0), (3, 2), (3, 2), (3, 2, 1, 0)]

contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}

state_posterior = np.zeros((till_session + 1 - from_session, 4))
for k, j in enumerate(range(from_session, till_session + 1)):
    for i, w in enumerate(GLM_weights):
        if i in states[j]:
            # print(j, i)
            w += np.random.normal(np.zeros(3), 0.03 * np.ones(3))
    # plt.plot(weights_to_pmf(GLM_weights[-3]))

    data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    side_info = pickle.load(open("../session_data/{}_side_info_{}.p".format(subject, j), "rb"))

    if len(states[j]) <= 1:
        print(states[j])
        print(data.shape)
        print(1 - nbinom.cdf(data.shape[0], *neg_bin_params[states[j][0]]))
        print()

    contrasts = np.vectorize(num_to_contrast.get)(data[:, 0])

    predictors = np.zeros(3)
    state_plot = np.zeros(4)
    count = 0
    curr_state = states[j][0]
    curr_dur = np.random.negative_binomial(*neg_bin_params[curr_state]) + 1

    prev_choice = 2 * int(np.random.rand() > 0.5)
    data[0, 1] = prev_choice

    state_counter = 0

    plt.plot(weights_to_pmf(GLM_weights[0], with_bias=1))
    plt.plot(weights_to_pmf(GLM_weights[1], with_bias=1))
    plt.title(k)
    plt.ylim(bottom=0, top=1)

    for i, c in enumerate(contrasts[1:]):
        predictors[0] = max(c, 0)
        predictors[1] = abs(min(c, 0))
        predictors[2] = 1
        data[i+1, 1] = 2 * (np.random.rand() < 1 / (1 + np.exp(- np.sum(GLM_weights[curr_state] * predictors))))
        state_plot[curr_state] += 1
        curr_dur -= 1
        if curr_dur == 0:
            state_counter += 1
            curr_state = states[j][state_counter % len(states[j])]
            curr_dur = np.random.negative_binomial(*neg_bin_params[curr_state]) + 1

    print(k)
    state_posterior[k] = state_plot / len(data[:, 0])
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
    pickle.dump(side_info, open("../session_data/{}_side_info_{}.p".format(new_name, j), "wb"))

plt.show()

plt.figure(figsize=(16, 9))
for s in range(4):
    plt.fill_between(range(till_session + 1 - from_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)

plt.savefig('states_12')
plt.show()

truth = {'state_posterior': state_posterior, 'weights': list(reversed(GLM_weights)), 'state_map': dict(zip(list(range(len(GLM_weights))), list(range(len(GLM_weights))))), 'durs': neg_bin_params}
pickle.dump(truth, open("truth_{}.p".format(new_name), "wb"))
