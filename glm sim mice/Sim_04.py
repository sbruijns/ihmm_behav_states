"""
Generate data from a simulated mouse-GLM.

This mouse uses 4 states throughout
They start a certain distance apart, states alternate during the session, duration is negative binomial
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy.stats import nbinom


subject = 'CSHL059'
new_name = 'GLM_Sim_04'
seed = 5

print('BIAS NOT UPDATED')
quit()
print(new_name)
np.random.seed(seed)

info_dict = pickle.load(open("../session_data/{}_info_dict.p".format(subject), "rb"))
assert info_dict['subject'] == subject
info_dict['subject'] = new_name
pickle.dump(info_dict, open("../session_data/{}_info_dict.p".format(new_name), "wb"))

till_session = info_dict['n_sessions']
from_session = info_dict['bias_start']

GLM_weights = [np.array([-4.5, 4.3, 0, 0., 1.2, -0.7]),
               np.array([-4.5, 3.2, 1.3, 0., 0.3, -1.5]),
               np.array([-1, 1.2, 2.1, 0.5, 0.1, 1]),
               np.array([-0.1, 0.2, 0, 0.1, 3.9, -1.])]

contrast_to_num = {-1.: 0, -0.987: 1, -0.848: 2, -0.555: 3, -0.302: 4, 0.: 5, 0.302: 6, 0.555: 7, 0.848: 8, 0.987: 9, 1.: 10}
num_to_contrast = {v: k for k, v in contrast_to_num.items()}

state_posterior = np.zeros((till_session + 1 - from_session, 4))

for k, j in enumerate(range(from_session, till_session + 1)):
    data = pickle.load(open("../session_data/{}_fit_info_{}.p".format(subject, j), "rb"))
    side_info = pickle.load(open("../session_data/{}_side_info_{}.p".format(subject, j), "rb"))

    contrasts = np.vectorize(num_to_contrast.get)(data[:, 0])

    predictors = np.zeros(5)
    state_plot = np.zeros(4)
    count = 0
    curr_state = np.random.choice(4)
    dur = nbinom.rvs(21 + (curr_state % 2) * 15, 0.3)

    prev_choice = 2 * int(np.random.rand() > 0.5) - 1
    if (contrasts[0] < 0) == (prev_choice > 0):
        prev_reward = 1.
    elif contrasts[0] > 0:
        prev_reward = 0.
    else:
        prev_reward = int(np.random.rand() > 0.5)
    prev_side = - prev_choice if prev_reward == 0. else prev_choice
    data[0, 1] = prev_choice + 1
    side_info[0, 1] = prev_reward

    for i, c in enumerate(contrasts[1:]):
        predictors[0] = max(c, 0)
        predictors[1] = abs(min(c, 0))
        predictors[2] = prev_choice
        predictors[3] = prev_reward
        predictors[4] = prev_side
        data[i+1, 1] = 2 * (np.random.rand() < 1 / (1 + np.exp(- np.sum(GLM_weights[curr_state][:-1] * predictors) - GLM_weights[curr_state][-1])))
        state_plot[curr_state] += 1
        if dur == 0:
            while True:
                next_state = np.random.choice(4)
                if next_state != curr_state:
                    curr_state = next_state
                    break
            dur = nbinom.rvs(21 + (curr_state % 2) * 15, 0.3)
        else:
            dur -= 1
        prev_choice = data[i+1, 1] - 1
        if (c < 0) == (prev_choice > 0):
            prev_reward = 1.
        elif c > 0:
            prev_reward = 0.
        else:
            prev_reward = int(np.random.rand() > side_info[i+1, 0])
        prev_side = - prev_choice if prev_reward == 0. else prev_choice
        side_info[i+1, 1] = prev_reward
    state_posterior[k] = state_plot / len(data[:, 0])
    pickle.dump(data, open("../session_data/{}_fit_info_{}.p".format(new_name, j), "wb"))
    pickle.dump(side_info, open("../session_data/{}_side_info_{}.p".format(new_name, j), "wb"))

plt.figure(figsize=(16, 9))
for s in range(4):
    plt.fill_between(range(till_session + 1 - from_session), s - state_posterior[:, s] / 2, s + state_posterior[:, s] / 2)

plt.show()
