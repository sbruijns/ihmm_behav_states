import multiprocessing as mp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pyhsmm
import pyhsmm.basic.distributions as distributions
import pickle
from itertools import permutations, product
import seaborn as sns
from scipy.stats import nbinom
import sys
from matplotlib.patches import Rectangle
from scipy.special import logsumexp
import pyhsmm.util.profiling as prof
import time
import pyhsmm.util.profiling as prof
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MaxNLocator
import warnings
import matplotlib
# matplotlib.use('Agg')

colors = np.genfromtxt('colors.csv', delimiter=',')

# Todo: fix !!!'s
# use self.models[-1] not self.models[0]

np.set_printoptions(suppress=True)

fs = 16
num_to_cont = dict(zip(range(11), [-1., -0.5, -.25, -.125, -.062, 0., .062, .125, .25, 0.5, 1.]))

all_cont_ticks = (np.arange(11), [-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])
bias_cont_ticks = (np.arange(9), [-1, -.25, -.125, -.062, 0, .062, .125, .25, 1])
conts = np.array([-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])

cmap = cm.get_cmap('coolwarm')


def pmf_acc(x, w):
    """
    Given probabilities of answering right x, and relative contrast frequencies w, compute accuracy.

    A contrast is assumed to be equiprobable right or left (no bias here).
    But e.g. 0 contrast can appear more frequently than rest.
    """
    n = len(x)
    x[:int(n / 2)] = 1 - x[:int(n / 2)]
    x[int(n / 2)] = 0.5
    return np.sum(x * (w / w.sum()))

# remove threshold from class !!!
class MCMC_result:

    def __init__(self, models, infos, data, sessions, fit_variance, gamma_prior, threshold=0.02, seq_start=0, conditioning='nothing'):

        self.models = models
        self.name = infos['subject']
        self.seq_start = seq_start
        self.conditioning = conditioning
        self.type = sessions
        self.fit_variance = str(fit_variance).replace('.', '_')
        self.gamma_prior = gamma_prior
        self.assign_counts = None  # Placeholder
        self.infos = infos
        self.data = data

        self.n_samples = len(self.models)
        self.n_sessions = len(self.models[-1].stateseqs)
        self.n_datapoints = sum([len(s) for s in self.models[-1].stateseqs])
        self.n_all_states = self.models[-1].num_states
        self.threshold = threshold

        self.calc_full_state_posterior()
        total_post = np.zeros(self.n_all_states)
        for i, post in enumerate(self.full_posteriors):
            total_post += post.sum(axis=1)
        self.proto_states = np.where(total_post / self.n_datapoints > self.threshold)[0]
        self.n_pstates = len(self.proto_states)
        print(total_post / self.n_datapoints)
        print(self.proto_states)

        if sessions == 'bias':
            self.n_contrasts = 9
            self.cont_ticks = bias_cont_ticks
        else:
            self.n_contrasts = 11
            self.cont_ticks = all_cont_ticks

        self.state_to_color = {}
        self.state_tendency = {}
        self.pmf_means = {}
        tendencies = []
        for s in self.proto_states:
            pmf = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                if self.fit_variance == '0':
                    pmf[i] = m.obs_distns[s].weights[:, 0]
                else:
                    pmf[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]
            # !!! only do this once: also needed for state_development
            # maybe use a better we to exclude contrasts than uncertainty?
            percentiles = np.percentile(pmf, [2.5, 97.5], axis=0)
            if self.type == 'bias':
                defined_points = np.ones(self.n_contrasts, dtype=bool)
            else:
                uncertainty = np.abs(percentiles[0] - percentiles[1])
                defined_points = uncertainty < 0.5
                defined_points[[0, 1, -2, -1]] = True
            self.pmf_means[s] = pmf[:, defined_points].mean(axis=0)
            temp = 1 - np.sum(pmf[:, defined_points].mean(axis=0)) / (np.sum(defined_points) + np.sum(defined_points) * (self.conditioning != 'nothing'))
            #self.state_to_color[s] = colors[int((temp + 2 * (temp - 0.5)) * 101)]
            self.state_tendency[s] = int(temp * 101 - 1)
            self.state_to_color[s] = colors[int(temp * 101 - 1)]
            tendencies.append(temp)

        ordered_states = [x for _, x in sorted(zip(tendencies, self.proto_states))]
        self.state_map = dict(zip(ordered_states, range(self.n_pstates)))
        print(self.state_map)
        self.count_assigns()
        self.calc_state_posterior()


    def matrix(self, i):
        """Return transition matrix reduced to important states."""
        temp = self.models[i].trans_distn.trans_matrix[self.proto_states]
        temp = temp[:, self.proto_states]
        return temp

    def param_hist(self, param_func, truth=None, title='{}', save=False, xlabel=''):
        """Histogram of whatever parameters for all samples, pick out parameter with lambda func."""
        for s in self.proto_states:
            params = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                params[i] = param_func(m, s)
            plt.hist(params, label='Posterior samples')
            if truth is not None:
                plt.axvline(truth, color='red', label='Truth')
                plt.legend(frameon=False, fontsize=15)
            sns.despine()
            plt.title(title.format(s), size=18)
            plt.ylabel('Occurences', size=16)
            plt.xlabel(xlabel, size=16)
            plt.tight_layout()
            if save:
                plt.savefig("dynamic_figures/" + title.format(s))
            plt.show()

    def param_hist_nonstate(self, param_func, truth=None, title='{}', save=False, xlabel='', show=True):
        """Histogram of whatever parameters for all samples, pick out parameter with lambda func."""
        plt.figure(figsize=(16,9))
        params = np.zeros(self.n_samples)
        for i, m in enumerate(self.models):
            params[i] = param_func(m)
        plt.hist(params, label='Posterior samples')
        if truth is not None:
            plt.axvline(truth, color='red', label='Truth')
            plt.legend(frameon=False, fontsize=15)
        sns.despine()
        plt.title(title, size=18)
        plt.ylabel('Occurences', size=16)
        plt.xlabel(xlabel, size=16)
        plt.tight_layout()
        if save:
            print("dynamic_figures/" + title)
            plt.savefig("dynamic_figures/" + title)
        if show:
            plt.show()
        else:
            plt.close()

    def duration_overview(self):
        """Multiple plots giving information about posterior of duration distributions."""
        for s in self.proto_states:
            r_params = np.zeros(self.n_samples)
            p_params = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                r_params[i] = m.dur_distns[s].r
                p_params[i] = m.dur_distns[s].p

            plt.hist(r_params)
            plt.title(s, color=self.state_to_color[s])
            plt.show()

            plt.scatter(r_params, p_params)
            plt.show()

            plt.plot(r_params, label='r')
            plt.plot(p_params, label='p')
            plt.plot(r_params * p_params / (1 - p_params), label='mean')
            plt.legend()
            plt.show()

    def transition_hists(self):
        """One histogram for each possible (important) transition probability for all samples."""
        for t in permutations(self.proto_states, 2):
            transitions = np.zeros(self.n_samples)
            for i, m in enumerate(self.models):
                transitions[i] = m.trans_distn.trans_matrix[t]
            plt.hist(transitions)
            plt.xlim(left=0, right=1)
            plt.title(t)
            plt.show()

    def count_assigns(self):
        self.assign_counts = np.zeros((self.n_samples, self.n_pstates))
        self.total_counts = np.zeros(self.n_samples)
        for i, m in enumerate(self.models):
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a = np.unique(flat_list, return_counts=1)
            states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]

            self.total_counts[i] = counts.sum()  # this is only model dependent if I threshold...
            for s, c in zip(states, counts):
                if s in self.proto_states:
                    self.assign_counts[i, self.state_map[s]] = c

    def percent_counts(self):
        """
        Seems to calculate the overall percentage of states.

        Could probably be computed faster/better
        """
        counts = np.zeros(self.n_all_states)
        trial_n = self.total_counts[0]

        for m in self.models:
            flat_list = [item for sublist in m.stateseqs for item in sublist]
            a, temp_counts = np.unique(flat_list, return_counts=1)
            counts[a] += temp_counts

        self.percent = counts / self.n_samples / trial_n

    def assign_hist(self):
        """Make simple histogram over how many trials are assigned to proto_states."""
        for i in range(self.n_pstates):
            plt.hist(self.assign_counts[:, i])
        plt.show()

    def assign_evolution(self, show=True):
        """Plot how the number of trials assigned to proto_states changes during sampling (check convergence)."""
        plt.figure(figsize=(11, 8))
        for s in self.proto_states:
            plt.plot(self.assign_counts[:, self.state_map[s]], c=self.state_to_color[s])

        plt.xlabel('Iteration', size=22)
        plt.ylabel('# assigned trials', size=22)
        plt.xticks(size=20-3)
        sns.despine()
        plt.tight_layout()
        plt.savefig("dynamic_figures/convergence/count evolution {} {} {} {}".format(self.name, self.type, self.conditioning, self.fit_variance))
        if show:
            plt.show()
        else:
            plt.close()

    def block_yielder(self, n=1000):
        """Yield block data."""
        add_on = self.infos['bias_start'] if self.type == 'bias' or self.type == 'prebias_plus' else 0
        for seq_num in range(n):
            name = "CSHL062" if self.name == "Sim_04" else self.name
            blocks = pickle.load(open("./session_data/{}_side_info_{}.p".format(name, seq_num + add_on), "rb"))[:, 0]
            yield blocks

    def feedback_yielder(self, n=1000):
        """Yield feedback data."""
        for seq_num in range(n):
            add_on = self.infos['bias_start'] if self.type == 'bias' else 0
            feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, seq_num + add_on), "rb"))[:, 1]
            #print(self.infos['eids'][seq_num + file_add + add_on])
            yield feedback

    def calc_state_posterior(self):
        """Calculate posterior over states by averaging over samples"""
        posteriors = []
        for seq_num in range(self.n_sessions):
            posterior = np.zeros((self.n_pstates, len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in self.proto_states:
                    posterior[self.state_map[s]] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / self.n_samples)
        self.posteriors = posteriors

    def calc_full_state_posterior(self):
        """Calculate posterior over ALL states by averaging over samples"""
        posteriors = []
        for seq_num in range(self.n_sessions):
            posterior = np.zeros((self.n_all_states, len(self.models[0].stateseqs[seq_num])))
            for m in self.models:
                for s in range(self.n_all_states):
                    posterior[s] += m.stateseqs[seq_num] == s

            posteriors.append(posterior / self.n_samples)
        self.full_posteriors = posteriors

    # !!! TODO
    def make_this_func(self):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples

    def contrasts_plot(self, until=None, save=False, show=False, plot_blocks=True, plot_0s=False, plot_perf=False):
        n = self.n_sessions if until is None else min(until, self.n_sessions)
        # priors = pickle.load(open("./session_data/{}_priors_act.p".format('CSHL062'), "rb"))
        by = self.block_yielder(n)
        fy = self.feedback_yielder(n)
        for seq_num in range(n):

            c_n_a = self.data[seq_num]

            plt.figure(figsize=(19, 9))
            for s in self.proto_states:
                label = "Posterior state {}".format(self.state_map[s]) if np.mean(self.posteriors[seq_num][self.state_map[s]]) > 0.1 else None
                plt.plot((self.n_contrasts - 1) * self.posteriors[seq_num][self.state_map[s]], color=self.state_to_color[s], lw=4, label=label)
            # plt.plot(posterior.sum(axis=0) / self.n_samples, 'k')

            # try:
            #     wMode, std = pickle.load(open("./session_data/psytrack/result {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
            #     trialnumbers = pickle.load(open("./session_data/psytrack/numbers {} {}.p".format(subject, seq_num + self.seq_start), 'rb'))
            # wMode[0] -= np.min(wMode[0])
            # wmax = np.max(wMode[0])
            # plt.plot(trialnumbers, wMode[0] / wmax, color='k', alpha=0.5)

            if plot_0s:
                self.plot_0_choices(contrasts_and_answers=c_n_a)

            if plot_perf:
                feedback = next(fy)
                assert len(feedback) == len(self.models[-1].stateseqs[seq_num]), "Block length not aligned"
                self.plot_performance(feedback)

            if (plot_blocks and self.type == 'bias') or (plot_blocks and seq_num >= self.infos['bias_start']):
                blocks = next(by)
                assert len(blocks) == len(self.models[-1].stateseqs[seq_num]), "Block length not aligned"
                self.plot_block_background(blocks)

            ms = 4
            noise = np.zeros(len(c_n_a))# np.random.rand(len(c_n_a)) * 0.4 - 0.2

            kernel_len = 10
            kernel = np.flip(np.exp(-np.arange(kernel_len) * 0.45), axis=0)
            smoothed_action_prob = np.zeros(c_n_a.shape[0])
            for i in range(c_n_a.shape[0]):
                start = max(0, i - kernel_len)
                current_len = i - start
                if i == 0:
                    smoothed_action_prob[i] = 0.5
                else:
                    smoothed_action_prob[i] = np.sum(c_n_a[start:i, 1] / 2 * kernel[-current_len:] / np.sum(kernel[-current_len:]))
            # plt.plot((self.n_contrasts - 1) * smoothed_action_prob, 'k', label="Smoothed choices")

            mask = c_n_a[:, 1] == 0
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] % self.n_contrasts, 'o', c='b', ms=ms, label='Rightward answer')

            mask = c_n_a[:, 1] == 1
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] % self.n_contrasts, 'o', c='k', ms=ms)

            mask = c_n_a[:, 1] == 2
            plt.plot(np.where(mask)[0], noise[mask] + c_n_a[mask, 0] % self.n_contrasts, 'o', c='r', ms=ms, label='Leftward answer')

            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=22)
            plt.yticks(*self.cont_ticks, size=22-2)
            plt.xticks(size=fs-1)
            plt.yticks(size=fs)
            plt.ylabel('P(State) and contrast', size=22)
            plt.xlabel('Trial', size=22)
            sns.despine()
            # plt.xlim(left=250, right=500)
            plt.legend(frameon=False, fontsize=22, bbox_to_anchor=(1.03, 0.78))
            plt.tight_layout()
            if save:
                plt.savefig("dynamic_figures/all posterior and contrasts {} {} {} {}, sess {}.png".format(self.name, self.type, self.conditioning, self.fit_variance, seq_num), dpi=300)#, bbox_inches='tight')
            if show:
                plt.show()
            else:
                plt.close()

    def plot_0_choices(self, contrasts_and_answers, window_length=4):
        """Plot the tendency towards one side on 0 contrasts, given array of contrasts and choices."""
        points = []
        choice_average = []
        internal_count = 0
        window = np.zeros(window_length)
        for i, ca in enumerate(contrasts_and_answers):
            if ca[0] == self.n_contrasts // 2:
                points.append(i)
                window[internal_count % window_length] = ca[1] == 2
                choice_average.append((self.n_contrasts - 1) * np.mean(window))
                internal_count += 1
        plt.plot(points, choice_average, 'k')

    def plot_performance(self, feedback, windowsize=7):
        """Plot local performance."""
        performance = np.zeros(len(feedback) - 2 * windowsize + 1)
        for i, pos in enumerate(np.arange(windowsize, len(feedback) - windowsize + 1)):
            performance[i] = np.mean(feedback[pos - windowsize: pos])
        plt.plot(np.arange(windowsize, len(feedback) - windowsize + 1), (self.n_contrasts - 1) * performance, 'k')

    def sequence_heatmap(self, normalization='adaptive', until=None):
        n = self.n_sessions if until is None else until
        for seq_num in range(n):
            for s1, s2 in product(self.proto_states, repeat=2):
                heatmap = np.zeros((len(self.models[0].stateseqs[seq_num]), len(self.models[0].stateseqs[seq_num])))
                normalize_vector = np.zeros(len(self.models[0].stateseqs[seq_num]), dtype=np.int32)
                for m in self.models:
                    for t in range(len(m.stateseqs[seq_num])):
                        if m.stateseqs[seq_num][t] == s1:
                            normalize_vector[t] += 1
                            heatmap[t] += m.stateseqs[seq_num] == s2

                if normalization == 'adaptive':
                    mask = normalize_vector.nonzero()
                    heatmap[mask] = heatmap[mask] / normalize_vector[mask, None]
                    if s1 == s2:
                        heatmap -= np.diag(normalize_vector >= 1)  # empty diagonal if we look at autocorr
                    heatmap += np.diag(normalize_vector / self.n_samples)
                elif normalization == 'total':
                    heatmap /= self.n_samples

                assert heatmap.max() <= 1.
                assert heatmap.min() >= 0.
                sns.heatmap(heatmap, vmin=0., vmax=1., square=True)
                title = "Sequence {}, states {} and {}".format(seq_num, s1, s2)
                plt.title(title)
                plt.savefig("dynamic_figures/state_heatmaps/" + normalization + '_' + title)
                plt.close()

    def single_state_resample(self, n):
        model = self.models[-1]
        states = [np.zeros((self.n_pstates, x.shape[0])) for x in model.datas]

        for i in range(n):
            model.resample_states()
            for j, seq in enumerate(model.stateseqs):
                for s in self.proto_states:
                    states[j][self.state_map[s]] += seq == s

        for seq_num, blocks in zip(range(n), self.block_yielder(n)):  # how does n work here?

            self.plot_block_background(blocks)

            seq = states[seq_num]
            for s in self.proto_states:
                plt.plot(seq[self.state_map[s]] / n, color=self.state_to_color[s], label=s)

            sns.despine()
            plt.legend()
            plt.show()

    def plot_block_background(self, blocks, block_to_color=None):
        """Plot the blocks in background for other plots."""
        alpha = 0.1
        start = 0
        curr = blocks[0]
        block_to_color = {0.2: 'b', 0.8: 'r', 0.5: 'w'} if block_to_color is None else block_to_color
        try:
            for i, b in enumerate(blocks):
                if b != curr:
                    plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
                    curr = b
                    start = i
            plt.axvspan(start, i, facecolor=block_to_color[curr], alpha=alpha)
        except KeyError as ke:
            print(ke)

    def state_development(self, save=True, show=True):
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples
        fig = plt.figure(figsize=(16, 9))
        spec = gridspec.GridSpec(ncols=100, nrows=3 * self.n_pstates+5, figure=fig)
        spec.update(hspace=0.) # set the spacing between axes.
        ax0 = fig.add_subplot(spec[:5, :69])  # performance line
        ax1 = fig.add_subplot(spec[5:, :69])  # state lines
        ax2 = fig.add_subplot(spec[5:, 76:86])
        ax3 = fig.add_subplot(spec[5:, 90:])

        if not self.name.startswith('Sim_'):
            if self.type != 'bias':
                current, counter = 0, 0
                for c in [2, 3, 4, 5]:
                    if self.infos[c] == current:
                        counter += 1
                    else:
                        counter = 0
                    ax0.axvline(self.infos[c] + 1, color='gray', zorder=0)
                    ax1.axvline(self.infos[c] + 1, color='gray', zorder=0)
                    ax0.plot(self.infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'ko', ms=18)
                    ax0.plot(self.infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'wo', ms=16.8)
                    ax0.plot(self.infos[c] + 1 - 0.25, 0.6 + counter * 0.2, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
                    current = self.infos[c]
        if self.type == 'all' or self.type == 'prebias_plus':
            ax1.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.annotate('Bias', (self.infos['bias_start'] + 1 - 0.5, 0.68), fontsize=22)

        dur_max = 120  # TODO: dont change params just once, also below
        posterior = np.zeros((self.n_pstates, dur_max, self.n_samples))
        for j, s in enumerate(self.proto_states):
            points = np.arange(dur_max)
            for i, m in enumerate(self.models):
                posterior[j, :, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!
        total_max = posterior.max()
        for s in self.proto_states:
            ax1.fill_between(range(1, 1 + self.n_sessions), self.state_map[s] - states_by_session[self.state_map[s]] / 2,
                             self.state_map[s] + states_by_session[self.state_map[s]] / 2, color=self.state_to_color[s])
            print(states_by_session[self.state_map[s]])

            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            dur_max = 120  # TODO: dont change params just once, also above
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i, m in enumerate(self.models):
                if self.fit_variance == '0':
                    pmfs[i] = m.obs_distns[s].weights[:, 0]
                else:
                    pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]
                posterior[:, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!

            alpha_level = 0.3
            ax2.axvline(0.5, c='grey', alpha=alpha_level, zorder=4)
            if self.conditioning == 'nothing':
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                if self.type == 'bias':
                    defined_points = np.ones(self.n_contrasts, dtype=bool)
                else:
                    uncertainty = np.abs(temp[0] - temp[1])
                    defined_points = uncertainty < 0.5
                    defined_points[[0, -1]] = True
                ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='', ms=5, marker='*')
                ax2.fill_between(np.where(defined_points)[0] / (len(defined_points)-1), temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                # if (~defined_points).sum() > 0:
                #     gap = 0.02
                #     plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                ax3.plot(points / dur_max, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                ax3.fill_between(points / dur_max, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                # if defined_points.sum() > 0:
                #     plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                ax2.axhline(self.state_map[s] + 0.5, c='k')
                ax3.axhline(self.state_map[s] + 0.5, c='k')
                ax2.axhline(self.state_map[s], c='grey', alpha=alpha_level, zorder=4)
                ax1.axhline(self.state_map[s] + 0.5, c='grey', alpha=alpha_level, zorder=4)
            else:
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                if self.type == 'bias':
                    defined_points = np.ones(self.n_contrasts, dtype=bool)
                else:
                    uncertainty = np.abs(temp[0] - temp[1])
                    defined_points = uncertainty < 0.5
                    defined_points[[0, 1, -2, -1]] = True
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, self.n_contrasts + np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='-.')

                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if (~defined_points).sum() > 0:
                    gap = 0.02
                    plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                plt.plot(points / dur_max + self.n_sessions + 1, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(points / dur_max + self.n_sessions + 1, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if defined_points.sum() > 0:
                    plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                ax2.axhline(self.state_map[s] + 0.5, c='k')
                ax3.axhline(self.state_map[s] + 0.5, c='k')
                ax2.axhline(self.state_map[s], c='grey', alpha=alpha_level, zorder=4)

        if not self.name.startswith('Sim_'):
            perf = np.zeros(self.n_sessions)
            found_files = 0
            counter = self.infos['bias_start'] - 1 if self.type == 'bias' else -1
            while found_files < self.n_sessions:
                counter += 1
                try:
                    feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
                except FileNotFoundError:
                    continue
                perf[found_files] = np.mean(feedback[:, 1])
                # assert feedback.shape[0] == self.full_posteriors[found_files].shape[1]
                found_files += 1
            ax0.axhline(-0.5, c='k')
            ax0.axhline(0.5, c='k')
            print(perf)
            ax0.fill_between(range(1, 1 + self.n_sessions), perf / 2,
                             - perf / 2, color='k')

        #sns.despine()
        ax0.set_title("{}".format(self.name), size=22, loc='left')
        ax0.set_title("Sessions: {}".format(self.type), size=22)
        ax2.set_title('Psychometric\nfunction', size=12)
        ax3.set_title('Duration\ndistribution', size=12)
        ax1.set_ylabel('Proportion of trials', size=22)
        ax0.set_ylabel('% correct', size=17)
        ax2.set_ylabel('Probability', size=22)
        ax1.set_xlabel('Session', size=22)
        ax2.set_xlabel('Contrast', size=22)
        ax3.set_xlabel('Trials', size=22)
        ax1.set_xlim(left=1, right=self.n_sessions)
        ax0.set_xlim(left=1, right=self.n_sessions)
        ax2.set_xlim(left=0, right=1)
        ax3.set_xlim(left=0, right=1)
        ax1.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax0.set_ylim(bottom=-0.5)
        ax0.spines['top'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax2.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax3.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        y_pos = (np.tile([0, 0.25, 0.5], (self.n_pstates, 1)) + np.arange(self.n_pstates)[:, None]).flatten()
        ax1.set_yticks(y_pos)
        ax1.set_yticklabels(list(np.tile([0, 0.5, 1], self.n_pstates)))
        ax0.set_yticks([0, 0.25, 0.5])
        ax0.set_yticklabels([0, 0.5, 1], size=fs)
        ax0.set_xticks([])
        y_pos = np.linspace(-0.5, self.n_pstates - 0.5, 2 * self.n_pstates + 1)
        ax2.set_yticks(y_pos)
        ax2.set_yticklabels([0] + list(np.tile([0.5, 1], self.n_pstates)), size=fs)
        ax3.set_yticks(y_pos)
        ax3.set_yticklabels([0] + list(np.tile(["{:.2f}".format(total_max / 2), "{:.2f}".format(total_max)], self.n_pstates)), size=fs)

        ax1.tick_params(axis='both', labelsize=fs)
        ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax2.set_xticks([0, 0.5, 1])
        ax2.set_xticklabels([-1, 0, 1], size=fs)
        ax3.set_xticks([0, 0.5, 1])
        ax3.set_xticklabels([0, dur_max // 2, dur_max], size=fs)

        plt.tight_layout()
        if save:
            plt.savefig("dynamic_figures/convergence/states_over_session {} {} {} {} {}".format(self.name, self.type, self.conditioning, self.fit_variance, self.gamma_prior), dpi=300)
        if show:
            plt.show()
        else:
            plt.close()


    def state_development_truth(self, save=True, show=True):
        true_pmf, true_states, true_dur = pickle.load(open("./sim mice/sim_16_truth.p", "rb"))
        truth_map = dict(zip(list(self.proto_states), [0, 2, 3, 1, 4]))
        truth_map2 = dict(zip(list(self.proto_states), [0, 1, 2, 3, 4]))
        print(truth_map)
        states_by_session = np.zeros((self.n_pstates, self.n_sessions))
        for m in self.models:
            for i, seq in enumerate(m.stateseqs):
                for s in self.proto_states:
                    states_by_session[self.state_map[s], i] += np.sum(seq == s) / len(seq)
        states_by_session /= self.n_samples
        fig = plt.figure(figsize=(16, 9))
        spec = gridspec.GridSpec(ncols=100, nrows=self.n_pstates+1, figure=fig)
        spec.update(hspace=0.) # set the spacing between axes.
        ax0 = fig.add_subplot(spec[:1, :69])  # performance line
        ax1 = fig.add_subplot(spec[1:, :69])  # state lines
        ax2 = fig.add_subplot(spec[1:, 76:86])
        ax3 = fig.add_subplot(spec[1:, 90:])

        if self.type != 'bias':
            current, counter = 0, 0
            for c in [2, 3, 4, 5]:
                if self.infos[c] == current:
                    counter += 1
                else:
                    counter = 0
                ax0.axvline(self.infos[c] + 1, color='gray', zorder=0)
                ax1.axvline(self.infos[c] + 1, color='gray', zorder=0)
                ax0.plot(self.infos[c] + 1 - 0.2, counter * 0.15 - 0.4, 'ko', ms=18)
                ax0.plot(self.infos[c] + 1 - 0.2, counter * 0.15 - 0.4, 'wo', ms=16.8)
                ax0.plot(self.infos[c] + 1 - 0.2, counter * 0.15 - 0.4, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
                current = self.infos[c]
        if self.type == 'all' or self.type == 'prebias_plus':
            ax1.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.axvline(self.infos['bias_start'] + 1 - 0.5, color='gray', zorder=0)
            ax0.annotate('Bias', (self.infos['bias_start'] + 1 - 0.5, 0.68), fontsize=22)

        dur_max = 120  # TODO: dont change params just once, also below
        posterior = np.zeros((self.n_pstates, dur_max, self.n_samples))
        for j, s in enumerate(self.proto_states):
            points = np.arange(dur_max)
            for i, m in enumerate(self.models):
                posterior[j, :, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!
        total_max = posterior.max()
        for s in self.proto_states:
            ax1.fill_between(range(1, 1 + self.n_sessions), self.state_map[s] - states_by_session[self.state_map[s]] / 2,
                             self.state_map[s] + states_by_session[self.state_map[s]] / 2, color=self.state_to_color[s])
            ax1.plot(range(1, 1 + self.n_sessions), truth_map[s] - true_states[:, truth_map2[s]] / 2, color='r')
            ax1.plot(range(1, 1 + self.n_sessions), truth_map[s] + true_states[:, truth_map2[s]] / 2, color='r')

            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            dur_max = 120  # TODO: dont change params just once, also above
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i, m in enumerate(self.models):
                if self.fit_variance == '0':
                    pmfs[i] = m.obs_distns[s].weights[:, 0]
                else:
                    pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]
                posterior[:, i] = nbinom.pmf(points, m.dur_distns[s].r, 1 - m.dur_distns[s].p)  # TODO!

            alpha_level = 0.3
            ax2.axvline(0.5, c='grey', alpha=alpha_level, zorder=4)
            if self.conditioning == 'nothing':
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                if self.type == 'bias':
                    defined_points = np.ones(self.n_contrasts, dtype=bool)
                else:
                    uncertainty = np.abs(temp[0] - temp[1])
                    defined_points = uncertainty < 0.5
                    defined_points[[0, -1]] = True
                ax2.plot(np.arange(self.n_contrasts) / (self.n_contrasts-1), true_pmf[truth_map2[s]] - 0.5 + truth_map[s], color='r')
                ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                ax2.plot(np.where(defined_points)[0] / (len(defined_points)-1), pmfs[:, defined_points].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='', ms=5, marker='*')
                ax2.fill_between(np.where(defined_points)[0] / (len(defined_points)-1), temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                # if (~defined_points).sum() > 0:
                #     gap = 0.02
                #     plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                if true_dur[truth_map2[s]] != 'inf':
                    ax3.plot(points / dur_max, nbinom.pmf(points, true_dur[truth_map2[s]][0], true_dur[truth_map2[s]][1]) / total_max - 0.5 + truth_map[s], color='r')
                ax3.plot(points / dur_max, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                ax3.fill_between(points / dur_max, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                # if defined_points.sum() > 0:
                #     plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                ax2.axhline(self.state_map[s] + 0.5, c='k')
                ax3.axhline(self.state_map[s] + 0.5, c='k')
                ax2.axhline(self.state_map[s], c='grey', alpha=alpha_level, zorder=4)
                ax1.axhline(self.state_map[s] + 0.5, c='grey', alpha=alpha_level, zorder=4)
            else:
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                if self.type == 'bias':
                    defined_points = np.ones(self.n_contrasts, dtype=bool)
                else:
                    uncertainty = np.abs(temp[0] - temp[1])
                    defined_points = uncertainty < 0.5
                    defined_points[[0, 1, -2, -1]] = True
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[:, self.n_contrasts + np.where(defined_points)[0]].mean(axis=0) - 0.5 + self.state_map[s], color=self.state_to_color[s], ls='-.')

                plt.fill_between(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, temp[1, defined_points] - 0.5 + self.state_map[s], temp[0, defined_points] - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if (~defined_points).sum() > 0:
                    gap = 0.02
                    plt.gca().add_patch(Rectangle((self.n_sessions + (defined_points.sum()-2) / 18 + gap, self.state_map[s] - 0.5 + gap), 1 - (defined_points.sum()-2) / 9 - 2*gap, 1 - 2*gap, edgecolor='w', facecolor="w", zorder=4))

                temp = np.percentile(posterior, [2.5, 97.5], axis=1)
                plt.plot(points / dur_max + self.n_sessions + 1, posterior.mean(axis=1) / total_max - 0.5 + self.state_map[s], color=self.state_to_color[s])
                plt.fill_between(points / dur_max + self.n_sessions + 1, temp[1] / total_max - 0.5 + self.state_map[s], temp[0] / total_max - 0.5 + self.state_map[s], alpha=0.2, color=self.state_to_color[s])

                if defined_points.sum() > 0:
                    plt.annotate(str(np.round(pmf_acc(pmfs[:, np.where(defined_points)[0]].mean(axis=0), np.ones(defined_points.sum())) * 100, 1)), (self.n_sessions + 0.07, self.state_map[s] + 0.3), zorder=5)
                ax2.axhline(self.state_map[s] + 0.5, c='k')
                ax3.axhline(self.state_map[s] + 0.5, c='k')
                ax2.axhline(self.state_map[s], c='grey', alpha=alpha_level, zorder=4)

        #sns.despine()
        ax0.set_title("{}".format(self.name), size=22, loc='left')
        ax0.set_title("Sessions: {}".format(self.type), size=22)
        ax2.set_title('Psychometric\nfunction', size=12)
        ax3.set_title('Duration\ndistribution', size=12)
        ax1.set_ylabel('Proportion of trials', size=22)
        ax2.set_ylabel('Probability', size=22)
        ax1.set_xlabel('Session', size=22)
        ax2.set_xlabel('Contrast', size=22)
        ax3.set_xlabel('Trials', size=22)
        ax1.set_xlim(left=1, right=self.n_sessions)
        ax0.set_xlim(left=1, right=self.n_sessions)
        ax2.set_xlim(left=0, right=1)
        ax3.set_xlim(left=0, right=1)
        ax1.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax0.set_ylim(bottom=-0.5, top=0.)
        ax0.spines['top'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax2.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        ax3.set_ylim(bottom=-0.5, top=self.n_pstates - 0.5)
        y_pos = (np.tile([0, 0.25, 0.5], (self.n_pstates, 1)) + np.arange(self.n_pstates)[:, None]).flatten()
        ax1.set_yticks(y_pos)
        ax1.set_yticklabels(list(np.tile([0, 0.5, 1], self.n_pstates)))
        ax0.set_xticks([])
        ax0.set_yticks([])
        y_pos = np.linspace(-0.5, self.n_pstates - 0.5, 2 * self.n_pstates + 1)
        ax2.set_yticks(y_pos)
        ax2.set_yticklabels([0] + list(np.tile([0.5, 1], self.n_pstates)), size=fs)
        ax3.set_yticks(y_pos)
        ax3.set_yticklabels([0] + list(np.tile(["{:.2f}".format(total_max / 2), "{:.2f}".format(total_max)], self.n_pstates)), size=fs)

        ax1.tick_params(axis='both', labelsize=fs)
        ax2.set_xticks([0, 0.5, 1])
        ax2.set_xticklabels([-1, 0, 1], size=fs)
        ax3.set_xticks([0, 0.5, 1])
        ax3.set_xticklabels([0, dur_max // 2, dur_max], size=fs)

        plt.tight_layout()
        if save:
            plt.savefig("dynamic_figures/convergence/states_over_session {} {} {} {}".format(self.name, self.type, self.conditioning, self.fit_variance), dpi=300)
        if show:
            plt.show()
        else:
            plt.close()



    def block_alignment(self, show=False, save=False):
        # throws errors when data is missing, e.g. ibl_witten_19
        n = self.n_sessions
        states_by_session = np.zeros((2, self.n_pstates, self.n_sessions))
        for seq_num, blocks in zip(range(n), self.block_yielder(n)): # !!! not working for all fit types

            if self.type != 'bias':
                seq_num += self.infos['bias_start']
            print(seq_num)
            if not (0.2 in blocks and 0.8 in blocks):
                continue
            for m in self.models:
                seq = m.stateseqs[seq_num]
                for s in self.proto_states:
                    b1 = blocks == 0.2
                    states_by_session[0, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b1)) / np.sum(b1)
                    b2 = blocks == 0.8
                    states_by_session[1, self.state_map[s], seq_num] += np.sum(np.logical_and(seq == s, b2)) / np.sum(b2)
        states_by_session /= self.n_samples

        perf = np.zeros(self.n_sessions)
        for i in range(self.n_sessions):
            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                continue
            perf[i] = np.mean(feedback)

        plt.figure(figsize=(14, 8))
        plt.suptitle("{}".format(self.name), size=fs)

        plt.subplot(211)
        plt.title('Right block', size=22)
        #plt.plot(perf)
        for s in self.proto_states:
            plt.plot(states_by_session[0, self.state_map[s]], color=self.state_to_color[s], label=s)

        sns.despine()
        plt.yticks(size=fs-2)
        plt.xticks([], size=fs-3)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.subplot(212)
        plt.title('Left block', size=22)
        for s in self.proto_states:
            plt.plot(states_by_session[1, self.state_map[s]], color=self.state_to_color[s])

        sns.despine()
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('Porportion of trials', size=22)
        plt.xlabel('Session', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        if save:
            plt.savefig("dynamic_figures/convergence/Block alginment {}".format(self.name))
        if show:
            plt.show()
        else:
            plt.close()

    def block_performance(self):
        c_n_a = self.data
        block_performance = np.zeros((2, self.n_sessions))
        for seq_num in range(self.n_sessions):
            try:
                blocks = pickle.load(open("./session_data/{}_df_{}_blocks.p".format(
                                     self.name, seq_num + self.seq_start), "rb"))
            except FileNotFoundError:
                continue
            if len(blocks) < 140:
                continue
            data = c_n_a[seq_num]
            block_performance[0, seq_num] = np.mean(data[np.logical_and(blocks == 1, data[:, 0] == 4)][:, 1] == 0)
            block_performance[1, seq_num] = np.mean(data[np.logical_and(blocks == -1, data[:, 0] == 4)][:, 1] == 0)

        plt.figure(figsize=(11, 8))
        plt.plot(block_performance[0], 'b', label='Right block')
        plt.plot(block_performance[1], 'r', label='Left block')
        plt.plot([0, self.n_sessions], [0.2, 0.2], 'r')
        plt.plot([0, self.n_sessions], [0.8, 0.8], 'b')
        sns.despine()

        plt.title("{}".format(self.name), size=22)
        plt.xticks(size=fs-3)
        plt.yticks(size=fs-2)
        plt.ylabel('P(answer right | Contrast = 0)', size=22)
        plt.xlabel('Session', size=22)

        plt.legend(fontsize=fs)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)

        plt.tight_layout()
        plt.savefig("dynamic_figures/convergence/0 accuracy in blocks {}".format(self.name))
        plt.close()

    def pmf_posterior(self):
        plt.figure(figsize=(11, 9))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]

            if self.conditioning == 'nothing':
                plt.plot(pmfs.mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
            else:
                plt.plot(pmfs[:, :self.n_contrasts].mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                #plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])

                plt.plot(pmfs[:, self.n_contrasts:].mean(axis=0), color=self.state_to_color[s], label=s)
                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                #plt.fill_between(range(self.n_contrasts), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
        plt.title("{}, {} sessions".format(self.name, len(self.models[0].stateseqs)), size=22)
        plt.xticks(*self.cont_ticks, size=22-1)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        #plt.ylim(bottom=0, top=1)
        plt.xlim(left=0)
        sns.despine()

        plt.tight_layout()
        plt.savefig("dynamic_figures/convergence/pmfs with conf {} {} {} {}".format(self.name, self.type, self.conditioning, self.fit_variance))
        plt.show()

    def dynamic_pmf_posterior(self, show=0):
        # pmfs_save = pickle.load(open("./session_data/{}_pmfs.p".format(self.name), "rb"))
        for ii, s in enumerate(self.proto_states):
            plt.figure(figsize=(16, 9))
            pmfs = np.zeros((len(self.models), self.n_sessions, self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                for j in range(self.n_sessions):
                    pmfs[i, j] = m.obs_distns[s].weights[j, :, 0]

            for j in range(self.n_sessions):
                plt.subplot(self.n_sessions // 6 + 1, 6, j+1)
                conts = np.unique(self.data[j][:, 0])
                plt.plot(pmfs[:, j].mean(axis=0)[conts], color=self.state_to_color[s], label="State {}".format(self.state_map[s]))
                # plt.plot(np.array(pmfs_save[j])[conts], label='Truth', c='r')
                temp = np.percentile(pmfs[:, j], [2.5, 97.5], axis=0)[:, conts]
                plt.fill_between(range(len(conts)), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
                plt.ylim(bottom=0, top=1)
                plt.title("Session " + str(j+1))
                if not j % 6 == 0:
                    plt.yticks([], [])
                else:
                    plt.ylabel('P(rightwards)')
                if j >= self.n_sessions - 6:
                    plt.xlabel('Contrasts')
                plt.xticks(range(len(conts)), [])
                if j == 0:
                    plt.legend()
                plt.xlim(left=0, right=len(conts)-1)
                sns.despine()

            plt.tight_layout()
            plt.savefig("dynamic_figures/convergence/pmfs with conf {} {} {} {} {}".format(self.name, self.type, self.conditioning, self.fit_variance, s))
            if show:
                plt.show()
            else:
                plt.close()

    def dynamic_pmf_posterior_one_sess(self, show=1, sess=0):
        pmfs_save = pickle.load(open("./session_data/{}_pmfs.p".format(self.name), "rb"))
        for ii, s in enumerate(self.proto_states):
            plt.figure(figsize=(16, 9))
            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                pmfs[i] = m.obs_distns[s].weights[sess, :, 0]

            conts = np.unique(self.data[sess][:, 0])
            plt.plot(pmfs.mean(axis=0)[conts], color=self.state_to_color[s], label="State {}".format(self.state_map[s]))
            plt.plot(np.array(pmfs_save[sess])[conts], label='Truth', c='r')
            temp = np.percentile(pmfs, [2.5, 97.5], axis=0)[:, conts]
            plt.fill_between(range(len(conts)), temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])
            plt.ylim(bottom=0, top=1)
            plt.title("Session " + str(sess+1), size=22)

            plt.ylabel('P(rightwards)', size=22)
            plt.xlabel('Contrasts', size=22)
            plt.xticks(*bias_cont_ticks, size=22-2)
            plt.yticks(size=22-2)
            plt.legend(frameon=False, fontsize=22)
            plt.xlim(left=0, right=8)
            sns.despine()

            plt.tight_layout()
            plt.savefig("dynamic_figures/convergence/single pmf with conf {} {} {} {} {} {}".format(self.name, self.type, self.conditioning, self.fit_variance, sess, s))
            if show:
                plt.show()
            else:
                plt.close()

    def dynamic_pmf_posterior_single_frame(self, show=0):
        #pmfs_save = pickle.load(open("./session_data/{}_pmfs.p".format(self.name), "rb"))
        for ii, s in enumerate(self.proto_states):
            plt.figure(figsize=(11, 9))
            cmap = cm.get_cmap('magma')
            pmfs = np.zeros((len(self.models), self.n_sessions, self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                for j in range(self.n_sessions):
                    pmfs[i, j] = m.obs_distns[s].weights[j, :, 0]

            for j in range(self.n_sessions):
                plt.plot(pmfs[:, j].mean(axis=0), label=self.state_map[s], c=cmap(j / self.n_sessions))
                plt.ylim(bottom=0, top=1)
                plt.ylabel('P(rightwards)')
                plt.xlabel('Contrasts')

            plt.tight_layout()
            plt.savefig("evolving pmf".format(self.name, self.type, self.conditioning, s, self.fit_variance))
            if show:
                plt.show()
            else:
                plt.close()

    def pmf_diffs(self, show=0):
        plt.figure(figsize=(16, 9))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_sessions, self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                for j in range(self.n_sessions):
                    pmfs[i, j] = m.obs_distns[s].weights[j, :, 0]

            diffs = np.zeros(self.n_sessions - 1)
            for j in range(self.n_sessions - 1):
                diffs[j] = np.sum(np.abs(pmfs[:, j].mean(axis=0) - pmfs[:, j+1].mean(axis=0)))

            plt.plot(diffs, color=self.state_to_color[s], label=self.state_map[s])
        plt.legend()
        plt.tight_layout()
        plt.savefig("dynamic_figures/convergence/pmf drift {} {} {} {} {}".format(self.name, self.type, self.conditioning, s, self.fit_variance))
        if show:
            plt.show()
        else:
            plt.close()

    def single_pmf_posterior(self):
        plt.figure(figsize=(9, 8))
        for s in self.proto_states:
            pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
            for i, m in enumerate(self.models):
                pmfs[i] = np.mean(m.obs_distns[s].weights, axis=0)[:, 0]

            if self.conditioning == 'nothing':
                temp = np.percentile(pmfs, [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                if s == 5:
                    plt.plot(range(defined_points.sum()), pmfs[:, defined_points].mean(axis=0), color=self.state_to_color[s], lw=3)
                else:
                    plt.plot(range(defined_points.sum()), pmfs[:, defined_points].mean(axis=0), color=self.state_to_color[s], lw=3, alpha=0.3)
                # plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])
            else:
                temp = np.percentile(pmfs[:, :self.n_contrasts], [2.5, 97.5], axis=0)
                uncertainty = np.abs(temp[0] - temp[1])
                defined_points = uncertainty < 0.5
                plt.plot(range(defined_points.sum()), pmfs[:, np.where(defined_points)[0]].mean(axis=0), color=self.state_to_color[s], label='previous right')
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])

                temp = np.percentile(pmfs[:, self.n_contrasts:], [2.5, 97.5], axis=0)
                plt.plot(range(defined_points.sum()), pmfs[:, self.n_contrasts + np.where(defined_points)[0]].mean(axis=0), color=self.state_to_color[s], label='previous left', ls='-.')
                plt.fill_between(range(defined_points.sum()), temp[1, defined_points], temp[0, defined_points], alpha=0.2, color=self.state_to_color[s])
        # plt.title("{}, state {}".format(self.name, self.state_map[s]), size=22)
        plt.xticks(*self.cont_ticks, size=22-3)
        plt.yticks(size=22-2)
        plt.xlabel('Contrast', size=22)
        plt.ylabel('P(answer rightward)', size=22)
        plt.ylim(bottom=0, top=1)
        plt.xlim(left=0, right=defined_points.sum() - 1)
        # plt.legend()
        sns.despine()

        plt.tight_layout()
        plt.savefig("dynamic_figures/convergence/single pmf with conf {} {} {} state {}".format(self.name, self.type, self.conditioning, self.state_map[s]))
        plt.show()

    def switchiness(self):
        """Stupid function for seeing how often the posterior switches states."""
        sess_switch = np.zeros(len(self.posteriors))
        sess_acc = np.zeros(len(self.posteriors))
        sess_acc_0 = np.zeros(len(self.posteriors))
        # sess_norm_switch = np.zeros(len(self.posteriors))

        for i, p in enumerate(self.posteriors):
            _, certain_states = np.where((p > 0.6).T)

            switches = 0
            current_state = -1
            counter = 0

            for s in certain_states:
                if s == current_state:
                    counter += 1
                else:
                    switches += counter > 5
                    current_state = s
                    counter = 1

            sess_switch[i] = switches / p.shape[1]

            try:
                feedback = pickle.load(open("./session_data/feedback_{}_{}.p".format(self.name, i), "rb"))
            except FileNotFoundError:
                print("warning {} {}".format(self.name, i))
                continue
            print(feedback.shape)
            print(p.shape[1])
            assert len(feedback) == p.shape[1]
            sess_acc[i] = np.mean(feedback)
            sess_acc_0[i] = np.mean(feedback[self.data[i][:, 0] == 4])
            # sess_norm_switch[i] = switches / np.sum(np.diff(blocks) != 0)

        return sess_switch, sess_acc, sess_acc_0

    def duration_posterior(self):
        for s in self.proto_states:
            plt.figure(figsize=(11, 8))
            params = np.zeros((2, self.n_samples))
            for i, m in enumerate(self.models):
                params[0, i] = m.dur_distns[s].r
                params[1, i] = m.dur_distns[s].p

            dur_max = 150
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, self.n_samples))
            for i in range(self.n_samples):
                posterior[:, i] = nbinom.pmf(points, params[0, i], 1 - params[1, i])
            plt.plot(points+1, posterior.mean(axis=1), color=self.state_to_color[s], label=s)
            temp = np.percentile(posterior, [2.5, 97.5], axis=1)
            plt.fill_between(points+1, temp[1], temp[0], alpha=0.2, color=self.state_to_color[s])

            plt.title("State {}".format(self.state_map[s]), size=22)
            plt.xlabel('Trial duration', size=22)
            plt.ylabel('Probability', size=22)
            plt.xticks(size=20-3)
            plt.yticks(size=20-4)
            plt.xlim(left=1, right=dur_max + 1)
            plt.ylim(bottom=0)
            sns.despine()
            plt.tight_layout()

            plt.savefig("dynamic_figures/convergence/duration dist {} {} {} state {}".format(self.name, self.type, self.conditioning, self.state_map[s]))
            plt.show()

    def waic(self):
        """Compute WAIC for a fitted model."""
        data = self.data
        size = int(self.n_samples / 5)
        arg_list = [(self.models[i*size:(i+1)*size], data, self.n_sessions) for i in range(5)]
        pool = mp.Pool(5)
        log_pred_dens = pool.starmap(waic_helper, arg_list)
        pool.close()
        log_pred_dens = np.concatenate(log_pred_dens, axis=0)
        assert (log_pred_dens == 0).sum() == 0

        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print("Posterior predictive density {}".format(clppd))
        print("Posterior variance {}".format(cpwaic2))
        print("WAIC {}".format(clppd - cpwaic2))
        pickle.dump(log_pred_dens, open("./WAIC/{}_{}_condition_{}.p".format(self.name, self.type, self.conditioning), 'wb'))
        return log_pred_dens

    def goodness_of_fit(self):
        """Compute WAIC for a fitted model."""
        data = self.data
        size = int(self.n_samples / 5)
        arg_list = [(self.models[i*size:(i+1)*size], data, self.n_sessions) for i in range(5)]
        pool = mp.Pool(5)
        log_pred_dens = pool.starmap(goodness_of_fit_helper, arg_list)
        pool.close()
        log_pred_dens = np.concatenate(log_pred_dens, axis=0)
        assert (log_pred_dens == 0).sum() == 0

        gof = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        print("Goodness of fit {}".format(gof))
        pickle.dump(log_pred_dens, open("./WAIC/{}_{}_condition_{}.p".format(self.name, self.type, self.conditioning), 'wb'))
        return log_pred_dens

    def waic_one_core(self):
        # predictive density
        log_pred_dens = np.zeros((self.n_samples, self.n_sessions))
        data = self.data

        for i, m in enumerate(self.models):
            if i % 50 == 0:
                print(i)
            for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, data)):
                s.data = d
                log_pred_dens[i, j] = np.sum(s.aBl[np.arange(s.aBl.shape[0]), ss])

        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)
        pickle.dump(log_pred_dens, open("./WAIC/{}_{}_condition_{}_one_core.p".format(self.name, self.type, self.conditioning), 'wb'))
        return log_pred_dens

    def load_waic(self):
        log_pred_dens = pickle.load(open("./WAIC/{}_{}_condition_{}.p".format(self.name, self.type, self.conditioning), 'rb'))
        assert (log_pred_dens == 0).sum() == 0
        clppd = np.sum(np.log(np.mean(np.exp(log_pred_dens), axis=0)))
        cpwaic2 = np.sum(np.var(log_pred_dens, ddof=1, axis=0))
        print(clppd)
        print(cpwaic2)
        print(clppd - cpwaic2)

        return(clppd - cpwaic2)

    def generate_data(self):
        # need to subtract confounds from contrasts (from models.datas)
        np.random.seed(4)
        fake_data = []
        state_data = []
        for contrasts, sseq in zip(self.data, self.models[-1].stateseqs):
            c_n_a, states = self.models[-1].my_generate(contrasts[:, 0], np.unique(sseq))
            fake_data.append(c_n_a)
            state_data.append(states)

        for i, fd in enumerate(fake_data):
            print(np.unique(fd[:, 0]))
            pickle.dump(fd, open("./session_data/{}_recovery_info_{}.p".format(self.name, i), 'wb'))

        self.truth_diagram(state_data)
        return c_n_a

    def truth_diagram(self, state_data):
        flat_list = [item for sublist in state_data for item in sublist]
        a = np.unique(flat_list, return_counts=1)
        states, counts = a[0][np.argsort(a[1])], a[1][np.argsort(a[1])]
        warnings.warn('This code is not maintained, thresholding doesn\' work anymore')
        proto_states = states[counts > self.threshold]
        n_pstates = len(proto_states)
        tendencies = []
        state_to_color = {}
        model = self.models[-1]
        for s in proto_states:
            temp = 1 - np.sum(np.mean(model.obs_distns[s].weights, axis=0)[:, 0]) / (self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing'))
            state_to_color[s] = colors[int(temp * 101)]
            tendencies.append(temp)

        ordered_states = [x for _, x in sorted(zip(tendencies, proto_states))]
        state_map = dict(zip(ordered_states, range(n_pstates)))

        states_by_session = np.zeros((n_pstates, self.n_sessions))
        for i, seq in enumerate(state_data):
            for s in proto_states:
                states_by_session[state_map[s], i] += np.sum(seq == s) / len(seq)
        plt.figure(figsize=(16, 9))

        current, counter = 0, 0
        for c in [2, 3, 4, 5]:
            if self.infos[c] == current:
                counter += 1
            else:
                counter = 0
            plt.axvline(self.infos[c], color='gray', zorder=0)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'ko', ms=18)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'wo', ms=16.8)
            plt.plot(self.infos[c] - 0.28, n_pstates + 0.68 + counter * 0.28, 'ko', ms=16.8, alpha=abs(num_to_cont[c]))
            current = self.infos[c]

        for s in proto_states:
            plt.fill_between(range(1, 1 + self.n_sessions), state_map[s] - states_by_session[state_map[s]] / 2,
                             state_map[s] + states_by_session[state_map[s]] / 2, color=state_to_color[s])

            pmfs = np.zeros((1, 11 + 11 * (self.conditioning != 'nothing')))
            dur_max = 120
            points = np.arange(dur_max)
            posterior = np.zeros((dur_max, 1))
            pmfs = np.mean(model.obs_distns[s].weights, axis=0)[:, 0]
            posterior = nbinom.pmf(points, model.dur_distns[s].r, 1 - model.dur_distns[s].p)

            alpha_level = 0.3
            plt.plot([self.n_sessions + 1]*2, [-0.5, n_pstates - 0.5], c='k')
            plt.plot([self.n_sessions]*2, [-0.5, n_pstates + 0.5], c='k')
            plt.plot([self.n_sessions + 0.5]*2, [-0.5, n_pstates - 0.5], c='grey', alpha=alpha_level, zorder=4)
            #plt.annotate(str(abs(num_to_cont[c])), ((self.infos[c] - 0.75 + 0.55 * counter) / self.n_sessions, 0.9), color='black', xycoords='figure fraction', size=16)
            if self.conditioning == 'nothing':
                defined_points = np.ones(11, dtype=np.bool)
                plt.plot(np.where(defined_points)[0] / (len(defined_points)-1) + self.n_sessions, pmfs[defined_points] - 0.5 + state_map[s], color=state_to_color[s])

                total_max = max(temp.max(), posterior.mean(axis=1).max())
                plt.plot(points / dur_max + self.n_sessions + 1, posterior / total_max - 0.5 + state_map[s], color=state_to_color[s])

                plt.plot([self.n_sessions, self.n_sessions+2], [state_map[s] + 0.5] * 2, c='k')
                plt.plot([self.n_sessions, self.n_sessions+1], [state_map[s]] * 2, c='grey', alpha=alpha_level, zorder=4)

        perf = np.zeros(self.n_sessions)
        found_files = 0
        counter = -1
        while found_files < self.n_sessions:
            counter += 1
            try:
                feedback = pickle.load(open("./session_data/{}_side_info_{}.p".format(self.name, counter), "rb"))
            except FileNotFoundError:
                continue
            perf[found_files] = np.mean(feedback)
            # no assertions that file is correct
            found_files += 1

        plt.annotate('PMF', (self.n_sessions + 0.5, n_pstates - 0.45), size=12, ha='center')
        plt.annotate('Duration', (self.n_sessions + 1.5, n_pstates - 0.45), size=12, ha='center')
        plt.axhline(n_pstates + 0.5, c='k')
        plt.axhline(n_pstates - 0.5, c='k')
        plt.fill_between(range(1, 1 + self.n_sessions), n_pstates + perf / 2,
                         n_pstates - perf / 2, color='k')

        sns.despine()
        plt.title("{}".format(self.name), size=22, loc='left')
        plt.title("Sessions: {}, Conditioning: {} recovery truth".format(self.type, self.conditioning), size=22)
        plt.yticks(range(n_pstates + 1), list(range(n_pstates)) + ['Acc.'])
        step_size = max(int(self.n_sessions / 7), 1)
        plt.xticks(list(range(0, self.n_sessions, step_size)) + [self.n_sessions+0.5, self.n_sessions+1.5], list(range(0, self.n_sessions, step_size)) + [str(0), str(int(dur_max / 2))])
        plt.xticks(size=fs-1)
        plt.yticks(size=fs-2)
        plt.ylabel('Proportion of trials', size=22)
        plt.xlabel('Session', size=22)
        plt.xlim(left=1, right=self.n_sessions + 2)
        plt.ylim(bottom=-0.5)
        plt.tight_layout()

        plt.savefig("dynamic_figures/convergence/recovery_truth_{} {} {}".format(self.name, self.type, self.conditioning))
        plt.show()

    def pmfs(self, states):
        for s in states:
            plt.plot(np.mean(self.models[-1].obs_distns[s].weights, axis=0)[11:, 0])
            plt.plot(np.mean(self.models[-1].obs_distns[s].weights, axis=0)[:11, 0], '--')
            plt.title(s)
            plt.show()

    def session_entropy(self):
        entropies = np.zeros(self.n_sessions)
        for i, ful_post in enumerate(self.full_posteriors):
            p = ful_post.sum(1) / ful_post.shape[1]
            zero_mask = p != 0.
            entropies[i] = - np.sum(p * np.log(p, where=zero_mask))
        return entropies

    def state_session_pref(self, time_slots=15):
        times = np.zeros((self.n_all_states, time_slots))
        for post in self.full_posteriors:
            trials = post.shape[1]

            slot_sizes = np.ones(time_slots, dtype=np.int16) * int(trials / time_slots)
            slot_sizes[:int(trials - (slot_sizes[1] * time_slots))] += 1  # add enough so as to fill whole session

            so_far = 0
            for i, step in enumerate(slot_sizes):  # could maybe be solved with cumsum, but it sucks
                times[:, i] += post[:, so_far:so_far + step].sum(axis=1)
                so_far += step

        for i in range(self.n_all_states):
            if times[i].sum() < 20:
                continue
            label = "PState {}".format(self.state_map[i]) if i in self.proto_states else i
            line_style = '--' if i in self.proto_states else '-'
            plt.plot(times[i] / times[i].sum(), label=label, ls=line_style)
        plt.legend()
        plt.show()

    def state_correlations(self):
        state_activity = np.zeros((self.n_sessions, self.n_pstates))
        for i, post in enumerate(self.posteriors):
            state_activity[i] = post.sum(axis=1)
        correlations = np.corrcoef(state_activity.T)
        sns.heatmap(correlations, square=True, vmin=-1, vmax=1)
        plt.show()

        return correlations[np.triu_indices(correlations.shape[1], 1)]

    def thresholded_state_correlations(self):
        state_activity = np.zeros((self.n_sessions, self.n_pstates))
        for i, post in enumerate(self.posteriors):
            state_activity[i] = post.mean(axis=1) > 0.05
        state_activity = state_activity[:, state_activity.sum(0) != 0]
        state_activity = state_activity[:, state_activity.sum(0) != self.n_sessions]
        correlations = np.corrcoef(state_activity.T)
        #sns.heatmap(correlations, square=True, vmin=-1, vmax=1)
        #plt.show()
        if correlations.ndim == 0:
            return []
        return list(correlations[np.triu_indices(correlations.shape[1], 1)])

    def states_per_sess(self):
        state_counts = np.zeros(self.n_sessions)
        for i, post in enumerate(self.full_posteriors):
            state_counts[i] = np.sum(np.mean(post, axis=1) > 0.05)
        return list(state_counts)

    def states_per_mouse(self):
        return self.n_pstates

    def pmf_return(self):
        if self.n_pstates == 1:
            return None, None
        prevalences = []
        for s in self.proto_states:
            state_trials = 0
            for post in self.posteriors:
                state_trials += np.sum(post[self.state_map[s]])
            prevalences.append(state_trials)
        print(self.proto_states, prevalences)
        most_prevalent = [x for _, x in sorted(zip(prevalences, self.proto_states))]
        print(most_prevalent)

        if self.state_tendency[most_prevalent[-1]] > self.state_tendency[most_prevalent[-2]]:
            return self.pmf_means[most_prevalent[-1]], self.pmf_means[most_prevalent[-2]]
        else:
            return self.pmf_means[most_prevalent[-2]], self.pmf_means[most_prevalent[-1]]

    def state_appearances(self):
        appearences = []
        for s in self.proto_states:
            for i in range(self.n_sessions):
                activity = np.sum(self.posteriors[i][self.state_map[s]]) / result.posteriors[i].shape[1]
                if activity > 0.1:
                    appearences.append(i)
                    break
        ret = np.array(appearences) / (self.n_sessions - 1)  # is -1 here good?
        return list(ret)

    def sessions_per_state(self):
        sess_list = []
        for s in self.proto_states:
            sess_count = 0
            for i in range(self.n_sessions):
                activity = np.sum(self.posteriors[i][self.state_map[s]]) / result.posteriors[i].shape[1]
                sess_count += activity > 0.1
            sess_list.append(sess_count)
        ret = np.array(sess_list) / self.n_sessions
        return list(ret)

    def parallel_relevant_states(self):
        parallel_states = np.zeros((self.n_all_states, self.n_all_states))
        for i, post in enumerate(self.full_posteriors):
            active = post.mean(axis=1) > 0.05
            parallel_states[active] += active

        print(parallel_states)

        return list(parallel_states)

    def save_states(self):
        all_states = []
        bstart = self.infos['bias_start'] + 1

        for i in range(self.n_sessions):
            if i < bstart:
                states = np.zeros(self.posteriors[i].shape[1])
                states[:] = 2
                all_states.append(states)
                print(len(states))
            else:
                states = np.zeros(self.posteriors[i].shape[1])
                states[self.posteriors[i][1] > self.posteriors[i][2]] = 1
                print(np.mean(states))
                print(len(states))
                all_states.append(states)
            plt.plot(states)
            plt.title(i)
            plt.show()
        pickle.dump(all_states, open("states_{}_{}_condition_{}_{}.p".format(self.name, self.type, self.conditioning, self.fit_variance), 'wb'))

    def pmf_compare(self, show=1, save=0):
        contrasts_and_answers = self.data
        for seq_num in range(self.n_sessions):
            c_n_a = contrasts_and_answers[seq_num]
            true_pmf = np.zeros(self.n_contrasts)
            curr_cont = []
            for c in range(self.n_contrasts):
                mask = c_n_a[:, 0] == c
                if mask.sum() == 0:
                    continue
                curr_cont.append(c)
                true_pmf[c] = (c_n_a[mask, 1] == 0).mean()

            inv_map = {v: k for k, v in self.state_map.items()}
            pmf_mix = np.zeros(self.n_contrasts)
            rel_states, counts = np.unique(np.argmax(self.posteriors[seq_num], axis=0), return_counts=True)
            tot_counts = counts.sum()
            for s, c in zip(rel_states, counts):
                pmfs = np.zeros((len(self.models), self.n_contrasts + self.n_contrasts * (self.conditioning != 'nothing')))
                for i, m in enumerate(self.models):
                    pmfs[i] = m.obs_distns[inv_map[s]].weights[seq_num, :, 0]
                pmf_mix += pmfs.mean(axis=0) * c / tot_counts

            plt.plot(true_pmf[curr_cont], label='True')
            plt.plot(pmf_mix[curr_cont], label='predicted')
            plt.ylim(bottom=0, top=1)
            plt.legend()
            plt.ylabel('% Rightwards', size=20)
            plt.xlabel('Contrasts', size=20)
            sns.despine()
            plt.title("{}, session #{} / {}".format(self.name, 1+seq_num + self.seq_start, self.n_sessions), size=22)
            if save:
                plt.savefig("dynamic_figures/pmf true vs predicted {} {} {} {}, sess {}.png".format(self.name, self.type, self.conditioning, self.fit_variance, seq_num))
            if show:
                plt.show()
            else:
                plt.close()

    def belief_csv(self):
        for seq_num in range(self.n_sessions):
            np.save("./beliefs/{}_{}".format(self.name, self.infos['dates'][seq_num]), self.posteriors[seq_num])

    def belief_hist(self):
        if self.n_pstates == 2:
            plt.hist(np.hstack(result.posteriors)[0], bins=20)
            plt.show()
        else:
            beliefs = np.hstack(result.posteriors)
            from scipy.stats import gaussian_kde
            z = gaussian_kde(beliefs)(beliefs)
            plt.scatter(beliefs[0], beliefs[1], c=z)
            plt.show()


def waic_helper(models, data, n_sessions):
    log_pred_dens = np.zeros((len(models), n_sessions))

    for i, m in enumerate(models):
        for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, data)):
            s.data = d
            log_pred_dens[i, j] = np.sum(s.aBl[np.arange(s.aBl.shape[0]), ss])

    return log_pred_dens


def goodness_of_fit_helper(models, data, n_sessions):
    log_pred_dens = np.zeros((len(models), n_sessions))

    for i, m in enumerate(models):
        for j, (s, ss, d) in enumerate(zip(m.states_list, m.stateseqs, data)):
            s.data = d
            log_pred_dens[i, j] = s.log_likelihood()

    return log_pred_dens



def waic_plots(subjects):
    waic = np.zeros((2, len(subjects)))
    for i, cond in enumerate(['nothing', 'answer']):
        for j, s in enumerate(subjects):
            mcmc_samples = pickle.load(open("./iHMM_fits/{}_{}_withtime_{}_condition_{}.p".format(s, 'bias', False, cond), "rb"))
            info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(s), "rb"))
            result = MCMC_result(mcmc_samples[-1000:], threshold=0.02, infos=info_dict, sessions='bias', conditioning=cond)
            waic[i, j] = result.load_waic()

    norm_waic = waic - waic[0]
    plt.figure(figsize=(12, 8))
    plt.plot(norm_waic)

    plt.xticks([0, 1], ['nothing', 'answer'], size=20)
    plt.xlabel('Conditioning', size=20)
    plt.ylabel('Delta WAIC', size=20)
    sns.despine()
    plt.tight_layout()
    plt.savefig("waic_overview")
    plt.show()
    return waic


subjects = ['CSH_ZAD_025', 'ZM_1897', 'KS014', 'ibl_witten_17', 'KS022', 'SWC_021', 'NYU-06', 'SWC_023', 'CSHL051',
            'CSHL054', 'CSHL059', 'CSHL_007', 'CSHL_014', 'CSHL_015', 'ibl_witten_14', 'CSHL_018', 'KS015',
            'CSH_ZAD_001', 'CSH_ZAD_026', 'KS003', 'ibl_witten_13', 'CSHL_020', 'ZM_3003', 'KS017', 'CSH_ZAD_011',
            'KS016', 'ibl_witten_16', 'KS019', 'CSH_ZAD_022', 'KS021', 'SWC_022', 'CSHL061', 'CSHL062'] # pre bias fits
subjects = ['CSHL045', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL055', 'CSHL059', 'CSHL061', 'CSHL062', 'CSHL065',
            'CSHL066', 'CSH_ZAD_011', 'CSH_ZAD_017', 'CSH_ZAD_019', 'CSH_ZAD_021', 'CSH_ZAD_022', 'CSH_ZAD_024',
            'CSH_ZAD_026', 'KS002', 'KS014', 'KS016', 'KS022', 'KS023', 'SWC_022', 'ZM_1897'] # bias fits
# all : ['CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013'] # 'ZM_1897' last var is missing
# prebias = ['NYU-06', 'KS014', 'CSHL061', 'ibl_witten_13', 'KS022', 'SWC_021', 'CSH_ZAD_025', 'CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013']
# bias = ['CSHL053', 'CSH_ZAD_029', 'CSHL066', 'CSH_ZAD_019', 'CSHL061', 'CSHL055', 'CSHL062', 'CSH_ZAD_022', 'CSHL059', 'ZM_1897', 'DY_013', 'CSHL066', 'CSHL060'] CSHL055 last var missing
subjects = ['Sim_01', 'Sim_02_var_0_2', 'Sim_02_var_0_04', 'Sim_03_5_dist_0_6_0_06', 'Sim_03_dist_0_6', 'Sim_03_dist_0_3']
subjects = ['SWC_054', 'ZFM-01592', 'SWC_060', 'SWC_043', 'CSHL059', 'ZM_2241', 'CSHL049', 'CSHL051',
            'DY_018', 'DY_013', 'DY_009', 'SWC_052', 'CSHL045', 'DY_020', 'CSHL052', 'SWC_058', 'ZFM-01936',
            'ZFM-01576', 'DY_016']  # CSH_ZAD_029 is missing 0.00005
subjects = ['KS014', 'KS015', 'KS022', 'DY_009', 'DY_013', 'DY_016', 'DY_018', 'DY_020',
            'CSHL045', 'CSHL049', 'CSHL051', 'CSHL052', 'CSHL053', 'CSHL054', 'CSHL059', 'CSHL060',
            'CSHL061', 'CSHL062', 'CSHL066', 'SWC_021', 'SWC_023', 'SWC_043', 'SWC_052',
            'SWC_054', 'SWC_058', 'SWC_060', 'ZM_1897', 'ZM_2241', 'CSHL_007', 'CSHL_014', 'CSHL_015',
            'CSHL_018', 'ZFM-01576', 'ZFM-01592', 'ZFM-01936', 'CSH_ZAD_001', 'CSH_ZAD_022', 'CSH_ZAD_025', 'CSH_ZAD_026',
            'ibl_witten_13', 'ibl_witten_14', 'ibl_witten_17'] # pre bias fits
subjects = ['CSH_ZAD_022']
print(len(subjects))
print(np.unique(subjects, return_counts=1))
# subjects = ['CSHL045']  # repeated site hard core mice
fit_type = ['prebias', 'bias', 'all', 'prebias_plus'][0]
with_time = False
conditioned_on = ['nothing', 'reward', 'truth', 'answer'][0]
fit_variance = [0.05, 0.01, 0.002, 0.0005, 'uniform', 0][2]
gamma_prior = [(1, 1)][0]
# hm = waic_plots(subjects)


fit_num = None
if len(sys.argv) == 2:
    fit_num = '_{}'.format(sys.argv[1])

threshold = 0.05
sess_count = 0
state_sesss = []
state_mouse = []
state_appear = []
state_corrs = []
sess_state = []
up_states, down_states = [], []
#for subject, fit_variance in product(subjects, [0.05, 0.01, 0.002, 0.0005, 'uniform']):
for subject, fit_variance in product(subjects, [0.06]):
#for subject in subjects:
#for subject, gamma_prior in product(subjects, [(0.1, 100)]):
    print()
    info_dict = pickle.load(open("./session_data/{}_info_dict.p".format(subject), "rb"))
    if fit_num is None:
        load_file = "./dynamic_GLMiHMM_crossvals/{}_{}_withtime_{}_condition_{}_var_{}.p".format(subject, fit_type, with_time, conditioned_on, fit_variance)
        # load_file = "./dynamic_iHMM_fits/{}_{}_withtime_{}_condition_{}_var_{}_gamma_{}_1.p".format(subject, fit_type, with_time, conditioned_on, fit_variance, str(gamma_prior).replace('.', '_'))
        print(load_file)
        mcmc_samples = pickle.load(open(load_file, "rb"))
        #mcmc_samples = pickle.load(open("./dynamic_iHMM_fits/{}_{}_withtime_{}_condition_{}.p".format(subject, fit_type, with_time, conditioned_on), "rb"))
        print(len(mcmc_samples))
    else:
        # mcmc_samples = pickle.load(open("./dynamic_iHMM_fits/{}_{}_withtime_{}_condition_{}_var_{}_{}.p".format(subject, fit_type, with_time, conditioned_on, fit_variance, fit_num), "rb"))
        mcmc_samples = pickle.load(open("./dynamic_GLMiHMM_crossvals/CSH_ZAD_022_crossval_0_prebias_var_1.0_50_499.p", "rb"))
        print(len(mcmc_samples))
    result = MCMC_result(mcmc_samples[-800:], threshold=threshold, infos=info_dict, data=mcmc_samples[0].datas, sessions=fit_type, conditioning=conditioned_on, fit_variance=fit_variance, gamma_prior=str(gamma_prior).replace('.', '_'))
    sess_count += result.n_sessions
    #result.pmf_compare(show=1, save=1)
    # result.block_alignment(show=1)
    # state_counts += list(result.states_per_sess())
    # continue
    # result.dynamic_pmf_posterior_single_frame(show=1)
    # state_sesss += result.states_per_sess()
    # a, b = result.pmf_return()
    # if a is None:
    #     continue
    # up_states.append(a)
    # down_states.append(b)
    # sess_state += result.sessions_per_state()
    # state_mouse.append(result.states_per_mouse())
    # state_appear += result.state_appearances()
    # state_corrs += result.thresholded_state_correlations()
    # result.param_hist_nonstate((lambda m: m.trans_distn.gamma), title="{}_{}_withtime_{}_condition_{}_var_{}_gamma_{}_3".format(subject, fit_type, with_time, conditioned_on, fit_variance, str(gamma_prior).replace('.', '_')), xlabel='Gamma', save=True)
    result.assign_evolution(show=1)
    result.state_development(save=1, show=1)
    result.contrasts_plot(until=40, show=1, save=0)
    # result.waic()
    # result.state_development_truth(save=1, show=1)
    # result.dynamic_pmf_posterior_one_sess(show=1, sess=19)
    # result.dynamic_pmf_posterior(show=1)
    #result.single_pmf_posterior()
    #result.param_hist((lambda m, s: m.obs_distns[s].sigmasq_states), truth=0.0, title="{} dynamic variance".format(subject), xlabel='Dynamic variance', save=False)

    continue
    result.belief_hist()
    result.waic()


    #result.param_hist((lambda m, s: m.dur_distns[s].p), xlabel='p')
    #result.param_hist((lambda m, s: m.dur_distns[s].r), xlabel='r')
    # result.waic()
    continue
    # result.pmf_posterior()
    #result.belief_csv()
    # result.pmf_diffs(show=1)
prof._prof.print_stats()
print(sess_count)
quit()

plt.figure(figsize=(8, 9))
plt.hist(state_mouse, color='grey', bins=0.5 + np.arange(7))
plt.title('State threshold {}'.format(threshold), fontsize=30)
plt.ylabel('# of mice', fontsize=30)
plt.gca().set_xticks([2, 4, 6])
plt.xlabel('Total # of states', fontsize=30)
plt.ylim(bottom=0, top=13)
plt.gca().tick_params(axis='both', labelsize=20)
sns.despine()
plt.tight_layout()
plt.show()


f, (a0, a1) = plt.subplots(1, 2, figsize=(18,9), gridspec_kw={'width_ratios': [1, 1.4]})

# a2.hist(sess_state, color='grey')
# a2.set_ylabel('# of states', fontsize=30)
# a2.set_xlabel('Proportion of sessions', fontsize=30)
# a2.tick_params(axis='both', labelsize=20)
# sns.despine()

#
# quit()

# plt.figure(figsize=(12, 9))
# for u, d in zip(up_states, down_states):
#     plt.plot(u, 'r')
#     plt.plot(d, 'b')
#
# sns.despine()
# plt.xlabel("Contrast", fontsize=28)
# plt.ylabel("Probability rightward", fontsize=28)
# plt.tight_layout()
# plt.show()
# quit()

a1.hist(state_appear, color='grey')
a1.set_xlim(left=0, right=1)
#plt.title('First appearence of ', fontsize=22)
a1.set_ylabel('# of states', fontsize=30)
a1.set_xlabel('Training proportion at appearance', fontsize=30)
a1.tick_params(axis='both', labelsize=20)
sns.despine()

a0.hist(state_sesss, color='grey', bins=0.5 + np.arange(7))
#plt.title('First appearence of ', fontsize=22)
a0.set_ylabel('# of sessions', fontsize=30)
a0.set_xticks([2, 4, 6])
a0.set_xlabel('# of states in session', fontsize=30)
a0.tick_params(axis='both', labelsize=20)
sns.despine()
plt.tight_layout()
plt.savefig('states in sessions', dpi=300)
plt.show()
