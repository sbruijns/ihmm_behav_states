import os
import re

memory = {}
name_saves = {}
seed_saves = {}

do_it = True

for filename in os.listdir("./dynamic_GLMiHMM_crossvals/"):
    if not filename.endswith('.p'):
        continue
    regexp = re.compile(r'((\w|-)+)_fittype_(\w+)_var_0.03_(\d+)_(\d+)_(\d+)')
    result = regexp.search(filename)
    subject = result.group(1)
    fit_type = result.group(3)
    seed = result.group(4)
    fit_num = result.group(5)
    chain_num = result.group(6)

    if fit_type == 'prebias':

        if subject not in seed_saves:
            seed_saves[subject] = [seed]
        else:
            if seed not in seed_saves[subject]:
                seed_saves[subject].append(seed)

        if (subject, seed) not in name_saves:
            name_saves[(subject, seed)] = []

        if fit_num not in name_saves[(subject, seed)]:
            name_saves[(subject, seed)].append(fit_num)

        if (subject, seed, fit_num) not in memory:
            memory[(subject, seed, fit_num)] = {"chain_num": int(chain_num), "counter": 1}
        else:  # if this is the first file of that chain, save some info
            memory[(subject, seed, fit_num)]["chain_num"] = max(memory[(subject, seed, fit_num)]["chain_num"], int(chain_num))
            memory[(subject, seed, fit_num)]["counter"] += 1

total_move = 0
nyu_11_move = 0
dicts_removed = 0
moved = []
completed = []
incompleted = []
for key in name_saves:
    subject = key[0]
    seed = key[1]
    complete = False
    save_fit_num = -1
    for fit_num in name_saves[key]:
        if memory[(subject, seed, fit_num)]['chain_num'] == 14 and memory[(subject, seed, fit_num)]['counter'] == 15:
            save_fit_num = fit_num
            complete = True
    if len(seed_saves[subject]) == 16:
        if subject not in completed:
            completed.append(subject)
    else:
        if subject not in incompleted:
            incompleted.append(subject)
    if complete and len(name_saves[key]) > 1:
        assert save_fit_num != -1
        for fit_num in name_saves[key]:
            if fit_num != save_fit_num:
                for i in range(15):
                    if do_it:
                        if os.path.exists("./dynamic_GLMiHMM_crossvals/{}_fittype_prebias_var_0.03_{}_{}_{}.p".format(subject, seed, fit_num, i)):
                            os.rename("./dynamic_GLMiHMM_crossvals/{}_fittype_prebias_var_0.03_{}_{}_{}.p".format(subject, seed, fit_num, i),
                                      "./del_test/{}_fittype_prebias_var_0.03_{}_{}_{}.p".format(subject, seed, fit_num, i))
                            if subject not in moved:
                                moved.append(subject)
                            total_move += 1
                    else:
                        if os.path.exists("./dynamic_GLMiHMM_crossvals/{}_fittype_prebias_var_0.03_{}_{}_{}.p".format(subject, seed, fit_num, i)):
                            print("I would move ")
                            print("./dynamic_GLMiHMM_crossvals/{}_fittype_prebias_var_0.03_{}_{}_{}.p".format(subject, seed, fit_num, i))
                            print(" to ")
                            print("./del_test/{}_fittype_prebias_var_0.03_{}_{}_{}.p".format(subject, seed, fit_num, i))
                            total_move += 1
                            nyu_11_move += subject == "NYU-11"

print(moved)
print(completed)
print(incompleted)
print("Would move {} in total, and {} of NYU-11".format(total_move, nyu_11_move))
