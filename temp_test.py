import numpy as np

def find_exceeding_indices(array):
    # Create a boolean mask where elements are greater than 0.5
    mask = array > 0.5

    # Find the column indices where each row first exceeds 0.5
    col_indices = np.argmax(mask, axis=1)

    # Find the row indices where the condition is True
    row_indices = np.flatnonzero(mask.any(axis=1))
    print(mask)
    print(mask.any(axis=1))
    print(np.flatnonzero(mask.any(axis=1)))
    print(np.nonzero(mask.any(axis=1))[0])

    # Sort both lists based on the second (col_indices) list
    sorted_indices = np.argsort(col_indices[row_indices])
    row_indices = row_indices[sorted_indices]
    col_indices = col_indices[row_indices]

    return row_indices.tolist(), col_indices.tolist()

# Test case
if __name__ == "__main__":
    test_array = np.array([[0.1, 0.51, 0.6, 0.7],
                           [0.4, 0.3, 0.8, 0.2],
                           [0.1, 0.5, 0.2, 0.1]])

    row_indices, col_indices = find_exceeding_indices(test_array)
    print("Row Indices that exceed 0.5:", row_indices)  # Output: [0, 2, 1]
    print("Column Indices where rows first exceed 0.5:", col_indices)  # Output: [2, 2, 2]



