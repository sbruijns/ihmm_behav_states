import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import nbinom
import seaborn as sns


ns = [20, 600, 90]
ps = [0.05, 0.6, 0.35]

fs = 70
ts = 30

x = np.arange(600)

for n, p in zip(ns, ps):
    plt.figure(figsize=(11, 9))
    plt.plot(x, nbinom.pmf(x, n, p), 'ko', ms=8)
    plt.xlabel("State Duration", size=fs)
    plt.ylabel("Probability", size=fs)

    plt.ylim(bottom=0, top=0.02)
    plt.xlim(left=0, right=600)
    plt.xticks([0, 150, 300, 450, 600], [0, 150, 300, 450, 600], size=ts)
    plt.yticks([.005, 0.01, 0.015, 0.02], [".005", ".01", ".015", ".02"], size=ts)
    sns.despine()
    plt.tight_layout()
    plt.savefig(str(n) + '.png', dpi=300)
    plt.show()
