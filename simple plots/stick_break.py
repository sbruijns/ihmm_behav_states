import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta
from matplotlib.transforms import Bbox

def next_higher(a, r):
    i = 0
    s = a[0]
    while s < r:
        i += 1
        s += a[i]
    return i

class stick_breaking_DP:

        def __init__(self, dist, alpha):

            self.dist = dist
            self.alpha = alpha

            self.counts = np.array([]) # atom counts
            self.pis = np.array([]) # atom probs
            self.pi_dashs_temp = []
            self.pi_dash_product = 1 # summarized memory of pi dashs for normalizing (no array needed)
            self.atoms = np.array([])

        def draw(self):

            r = np.random.rand()

            if r > 1 - self.pi_dash_product: # this works instead of np.sum(self.pis)
                atom = self.dist()
                pi_dash = np.random.beta(1, self.alpha)
                self.pi_dashs_temp.append(pi_dash)
                pi = pi_dash * self.pi_dash_product

                self.atoms = np.append(self.atoms, atom)
                self.pis = np.append(self.pis, pi)
                self.pi_dash_product *= (1 - pi_dash)
                self.counts = np.append(self.counts, 1)
                return atom

            else:
                num = next_higher(self.pis, r)
                self.counts[num] += 1
                return self.atoms[num]


        def draw_n(self, n):
            for _ in range(n):
                self.draw()

        def plot(self):
            plt.bar(x=self.atoms, height=self.counts, width=0.01, facecolor='k', edgecolor='k')

            plt.ylabel('Count', fontsize=22)
            plt.xlabel('Atom', fontsize=22)
            plt.title(r'$\alpha = {}$'.format(alpha), fontsize=22)
            plt.gca().spines['right'].set_visible(False)
            plt.gca().spines['top'].set_visible(False)

            plt.tight_layout()
            plt.savefig('dp.png')
            plt.show()

        def plot_sticks(self, n, title=''):

            plt.figure(figsize=(8, 2))
            plt.plot([0, 0], [-1.25, 1.25], 'k')
            plt.plot([1, 1], [-1.25, 1.25], 'k')

            so_far = 0
            for i in range(min(n, len(self.pis))):
                plt.plot([so_far, so_far + self.pis[i]], [1, 1], 'k')
                plt.plot([so_far, so_far + self.pis[i]], [-1, -1], 'k')
                plt.plot([so_far + self.pis[i], so_far + self.pis[i]], [-1, 1], 'k')

                if self.pis[i] > 0.05 and (i + 1 < 10):
                    plt.annotate(r'$S_{}$'.format(i+1), (so_far + self.pis[i] / 2 - 0.029, -0.29), fontsize=25)#, rotation='vertical')
                    #plt.annotate('S{0:.0f}'.format(self.atoms[i]), (so_far + self.pis[i] / 2 - 0.029, -0.29), fontsize=25)
                    #plt.annotate('{0:.0f}'.format(self.atoms[i]), (so_far + self.pis[i] / 2 - 0.04, -0.29 + 1.6 * (i % 2 == 0) - 1.6 * (i % 2 != 0)), fontsize=25)
                    #plt.plot([so_far + self.pis[i] / 2, so_far + self.pis[i] / 2], [0, 1.18 * 2 * (i % 2 == 0) - 1.18], 'k')
                so_far += self.pis[i]
            #plt.annotate(r'$\eta_3$', (0.46, 1.2), fontsize=25, clip_on=False)
            #plt.annotate(r'$\eta_{2, a_1}$', (0.44, 1.213), fontsize=25, clip_on=False)
            plt.plot([so_far, 1], [0, 0], 'k')

            plt.xticks([0, 1], [0, 1], fontsize=25)#, rotation='vertical')
            plt.xlim(left=-0.05, right=1)
            plt.ylim(bottom=-1.2, top=1.2)
            plt.gca().tick_params(axis=u'both', which=u'both',length=0)
            plt.yticks([])
            plt.gca().spines['right'].set_visible(False)
            plt.gca().spines['top'].set_visible(False)
            plt.gca().spines['left'].set_visible(False)
            plt.gca().spines['bottom'].set_visible(False)

            plt.tight_layout()
            plt.savefig(title + '_{}.png'.format(n), transparent=True, dpi=300)
            plt.show()

def discrete_dist():
    a = np.random.rand()
    if a < 0.2:
        return 0
    elif a < 0.5:
        return 1
    else:
        return 2

def discrete_dist2():
    a = np.random.rand()
    if a < 0.6:
        return 0
    elif a < 0.8:
        return 1
    else:
        return 2

def discrete_dist3():
    a = np.random.rand()
    if a < 0.2:
        return 0
    elif a < 0.9:
        return 1
    else:
        return 2

if __name__ == "__main__":
    seed = int(10000 * np.random.rand())
    print(seed)
    np.random.seed(2551)
    plt.rcParams.update({'font.size': 20})
    alpha = 3

    dp = stick_breaking_DP(lambda: np.random.normal(0, 1), alpha)

    """x = np.linspace(0, 1) # beta dist and growing sticks
    plt.plot(x, beta.pdf(x, 1, alpha), 'k', label=r'Beta(1, $\alpha$={})'.format(alpha))
    plt.legend(frameon=0, fontsize=22)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    plt.xlim(left=0, right=1)
    plt.ylim(bottom=0, top=3.25)
    plt.yticks([0, 1, 2, 3], [0, 1, 2, 3])
    plt.xticks([0, 0.25, 0.5, 0.75, 1], [0, 0.25, 0.5, 0.75, 1])
    plt.tight_layout()
    plt.savefig('beta_dist.png')
    plt.close()

    dp.draw_n(100)
    print(dp.pis)
    print(dp.pi_dashs_temp)
    dp.plot_sticks(1)
    dp.plot_sticks(2)
    dp.plot_sticks(3)
    dp.plot_sticks(24)

    quit()"""

    # dp = stick_breaking_DP(discrete_dist, alpha)
    # dp.pis = [0.55, 0.35, 0.1]
    # dp.atoms = [1, 2, 3]
    # dp.plot_sticks(31, title='eta_3')
    #
    # quit()

    for i in range(10):
        dp = stick_breaking_DP(lambda: np.random.normal(0, 1), 3)
        dp.draw_n(100)
        dp.plot_sticks(50 + i, title='alpha = 3')

    quit()

    np.random.seed(4)
    dp_t = stick_breaking_DP(discrete_dist, alpha)
    dp_t.draw_n(100)
    dp_t.plot_sticks(30)

    dp = stick_breaking_DP(discrete_dist, alpha)
    dp.pis = [0.2, 0.3, 0.5]
    dp.atoms = [0, 1, 2]
    dp.plot_sticks(31)

    dp = stick_breaking_DP(discrete_dist, alpha)
    dp.pis = [np.sum(dp_t.pis[dp_t.atoms == 0]), np.sum(dp_t.pis[dp_t.atoms == 1]), np.sum(dp_t.pis[dp_t.atoms == 2])]
    dp.atoms = [0, 1, 2]
    dp.plot_sticks(32, extra=1)

    dp_t = stick_breaking_DP(discrete_dist2, alpha)
    dp_t.draw_n(100)
    dp_t.plot_sticks(33)

    dp = stick_breaking_DP(discrete_dist2, alpha)
    dp.pis = [0.2, 0.3, 0.5]
    dp.atoms = [0, 1, 2]
    dp.plot_sticks(34)

    dp = stick_breaking_DP(discrete_dist2, alpha)
    dp.pis = [np.sum(dp_t.pis[dp_t.atoms == 0]), np.sum(dp_t.pis[dp_t.atoms == 1]), np.sum(dp_t.pis[dp_t.atoms == 2])]
    dp.atoms = [0, 1, 2]
    dp.plot_sticks(35, extra=0)

    dp_t = stick_breaking_DP(discrete_dist3, alpha)
    dp_t.draw_n(100)
    dp_t.plot_sticks(36)

    dp = stick_breaking_DP(discrete_dist3, alpha)
    dp.pis = [0.2, 0.3, 0.5]
    dp.atoms = [0, 1, 2]
    dp.plot_sticks(37)

    dp = stick_breaking_DP(discrete_dist3, alpha)
    dp.pis = [np.sum(dp_t.pis[dp_t.atoms == 0]), np.sum(dp_t.pis[dp_t.atoms == 1]), np.sum(dp_t.pis[dp_t.atoms == 2])]
    dp.atoms = [0, 1, 2]
    dp.plot_sticks(38, extra=2)
