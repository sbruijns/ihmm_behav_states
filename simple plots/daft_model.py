"""Plot graphical model of iHMM behavioural state model."""
import daft
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

np.random.seed(3)

rc("font", family="serif", size=12)
rc("text", usetex=True)


# Instantiate the PGM.
pgm = daft.PGM()

local_offset = -0.75
arrow_width = 0.15
arrow_length = 0.2

pgm.add_node('gamma', r"$\gamma$", 0, 0 * local_offset, plot_params={'edgecolor': 'w'}, offset=(5, 0))
pgm.add_node('alpha', r"$\alpha$", 0, 1 * local_offset, plot_params={'edgecolor': 'w'}, offset=(5, 0))
pgm.add_node('eta_vartheta', r"$\eta_{\vartheta}$", 0, 2 * local_offset, plot_params={'edgecolor': 'w'}, offset=(5, 0))
pgm.add_node('eta_theta', r"$\eta_\theta$", 0, 4 * local_offset, plot_params={'edgecolor': 'w'}, offset=(5, 0))

pgm.add_node('beta', r"$\beta$", 1, 0 * local_offset)
pgm.add_node('pi^i', r"$\pi_i$", 1, 1 * local_offset)
pgm.add_node('vartheta', r"$\vartheta_i$", 1, 2 * local_offset)
pgm.add_node('theta', r"$\theta_i$", 1, 4 * local_offset)
# And a plate.
pgm.add_plate([0.6, 4 * local_offset - 1/3, 0.8, -4 * local_offset], label=r"$L$", label_offset=(35, 0))

pgm.add_edge("gamma", "beta", plot_params={'head_width': arrow_width, 'head_length': arrow_length})
pgm.add_edge("alpha", "pi^i", plot_params={'head_width': arrow_width, 'head_length': arrow_length})
pgm.add_edge("eta_vartheta", "vartheta", plot_params={'head_width': arrow_width, 'head_length': arrow_length})
pgm.add_edge("eta_theta", "theta", plot_params={'head_width': arrow_width, 'head_length': arrow_length})
pgm.add_edge("beta", "pi^i", plot_params={'head_width': arrow_width, 'head_length': arrow_length})


internal_dist = 0.65
for i, offset in zip(['1', '2', 'S'], [3, 5, 7.5]):
    pgm.add_node('z_{}'.format(i), r"$z_{}$".format(i), offset, 1.8 * local_offset)
    pgm.add_node('d_{}'.format(i), r"$d_{}$".format(i), offset - internal_dist, 2.3 * local_offset, scale=.75)
    if i == '1':
        pgm.add_node('x_1', r"$x_1$", offset - internal_dist, 3.3 * local_offset)
    elif i == 'S':
        pgm.add_node('x_S', r"$x_S$", offset - internal_dist, 3.3 * local_offset)
    else:
        pgm.add_node('x_{}'.format(i), r"$x_{{t({})}}$".format(i), offset - internal_dist, 3.3 * local_offset)
    pgm.add_node('...{}'.format(i), r"$...$", offset, 3.3 * local_offset, plot_params={'edgecolor': 'w'})
    pgm.add_node('x{}'.format(i), '', offset + internal_dist, 3.3 * local_offset)

    pgm.add_edge('z_{}'.format(i), 'x_{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('z_{}'.format(i), '...{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('z_{}'.format(i), 'x{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('z_{}'.format(i), 'd_{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})

    if i == '1':
        pgm.add_node('y_1', r"$y_1$", offset - internal_dist, 4.8 * local_offset)
    elif i == 'S':
        pgm.add_node('y_S', r"$y_S$", offset - internal_dist, 4.8 * local_offset)
    else:
        pgm.add_node('y_{}'.format(i), r"$y_{{t({})}}$".format(i), offset - internal_dist, 4.8 * local_offset)
    pgm.add_node('y{}'.format(i), '', offset + internal_dist, 4.8 * local_offset)

    pgm.add_edge('x_{}'.format(i), 'y_{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('x{}'.format(i), 'y{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})

    pgm.add_edge('pi^i', 'z_{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('vartheta', 'd_{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('theta', 'y_{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})
    pgm.add_edge('theta', 'y{}'.format(i), plot_params={'head_width': arrow_width, 'head_length': arrow_length})


# Render and save.
pgm.render()
plt.show()
#pgm.savefig("classic.pdf")
pgm.savefig("graphical model.png", dpi=200)
quit()
height = 1.5

# Hierarchical parameters.
gap = 0.8



for i in range(10):
    if i+1 in blank:
        pgm.add_node(str(i+1), "...", i*gap, 0, plot_params={'alpha': 0.})
    else:
        pgm.add_node(str(i+1), r"${}$".format(num_to_lab[i]), i*gap, 0, observed=True)
        c = 'white' if np.random.rand() > 0.5 else 'black'
        pgm.add_node("color square {}".format(i+1), "", i*gap, -0.5, shape='rectangle', plot_params={'facecolor': c, 'edgecolor': 'k'}, aspect=1.5, scale=0.6)
        pgm.add_node("color square 2 {}".format(i+1), "", i*gap, -1, shape='rectangle', plot_params={'facecolor': str(np.random.rand()), 'edgecolor': 'k'}, aspect=1.5, scale=0.6)



# Comment out this to remove contrast dependence
pgm.add_node("contrast", r"$C_n$", -0.5, 1)
for i in range(10):
    if i+1 not in blank:
        pgm.add_edge("contrast", str(i+1))
