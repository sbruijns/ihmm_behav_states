import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.cm as cm
import pickle


bias_cont_ticks = ([0, 2, 5, 8, 10], [-1, -.25, 0, .25, 1])
cmap = cm.get_cmap('magma')

# try
all_pmfs_named = pickle.load(open("../multi_chain_saves/all_pmfs_named.p", 'rb'))

# state_nums = [0, 0, 0, 1, 0, 0, 3, 0, 0, 0, 0]
# subjects = ['SWC_042', 'ZFM-02368', 'ZM_1898', 'ZM_3003', 'NYU-37', 'ibl_witten_17', 'DY_014', 'DY_013', 'DY_008', 'CSH_ZAD_029', 'CSH_ZAD_025']
# for state, subj in zip(state_nums, subjects):
#     plt.figure(figsize=(11, 9))
#     for i, pmf in enumerate(all_pmfs_named[subj][state][1]):
#         plt.plot(pmf, c=cmap(i / max(0.0001, (len(all_pmfs_named[subj][state][1]) - 1))))
#     sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=1, vmax=len(all_pmfs_named[subj][state][1])))  
#     clb = plt.colorbar(sm)
#     clb.ax.set_title('Session', size=43, pad=14)
#     clb.ax.tick_params(labelsize=25.5)

#     plt.xticks(*bias_cont_ticks, size=25.5)
#     plt.xlim(left=0, right=10)
#     plt.ylim(0, 1)
#     plt.yticks(size=25.5)
#     plt.xlabel('Contrast', size=43)
#     plt.ylabel('P(respond rightward)', size=43)

#     sns.despine()
#     plt.tight_layout()
#     plt.savefig("dynamic_pmf_plot", dpi=300)
#     plt.show()

#     if state == 0 and subj ==  'ZFM-02368':
#         break

features = np.ones((3, 11))
features[0] = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])
features[1] = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])[::-1]
all_conts = np.array([-1, -0.5, -.25, -.125, -.062, 0, .062, .125, .25, 0.5, 1])

plt.figure(figsize=(4, 4))
for i, pmf in enumerate([[3.8, -2.2, -0.11], [3.85, -2.8, -0.14], [3.9, -3.4, -0.09], [3.95, -4, -0.12]]):
    plt.plot(1 / (1 + np.exp(np.sum(np.array(pmf)[:, None] * features, axis=0))), c='k', alpha=(i + 2) / 5)

plt.xticks([0, 5, 10], [-1, '', 1], size=18)
plt.xlim(left=0, right=10)
plt.ylim(0, 1)
plt.yticks([0, 1], size=18)
plt.xlabel('Contrast', size=25)
plt.ylabel('P(rightwards)', size=25)

sns.despine()
plt.tight_layout()
plt.savefig("mini_pmf_1_{}".format(i), dpi=300, transparent=True)
plt.show()


plt.figure(figsize=(4, 4))
for i, pmf in enumerate([[0.2, -1.8, 0.1], [0.35, -1.9, 0.1], [0.5, -2, 0.1], [0.65, -2.1, 0.1]]):
    plt.plot(1 / (1 + np.exp(np.sum(np.array(pmf)[:, None] * features, axis=0))), c='k', alpha=(i + 2) / 5)

plt.xticks([0, 5, 10], [-1, '', 1], size=18)
plt.xlim(left=0, right=10)
plt.ylim(0, 1)
plt.yticks([0, 1], size=18)
plt.xlabel('Contrast', size=25)
plt.ylabel('P(rightwards)', size=25)

sns.despine()
plt.tight_layout()
plt.savefig("mini_pmf_2_{}".format(i), dpi=300, transparent=True)
plt.show()


plt.figure(figsize=(4, 4))
for i, pmf in enumerate([[0.1, -0.05, 2.4], [0.05, -0.1, 2.6], [-0.05, 0.05, 2], [0.05, -0.05, 2.2]]):
    plt.plot(1 / (1 + np.exp(np.sum(np.array(pmf)[:, None] * features, axis=0))), c='k', alpha=(i + 2) / 5)

plt.xticks([0, 5, 10], [-1, '', 1], size=18)
plt.xlim(left=0, right=10)
plt.ylim(0, 1)
plt.yticks([0, 1], size=18)
plt.xlabel('Contrast', size=25)
plt.ylabel('P(rightwards)', size=25)

sns.despine()
plt.tight_layout()
plt.savefig("mini_pmf_3_{}".format(i), dpi=300, transparent=True)
plt.show()

quit()

end = np.array([0.04, 0.0425, 0.06, 0.1, 0.21, 0.5, 0.79, 0.9, 0.94, 0.96, 0.98])
start = 1 - np.array([0.21, 0.23, 0.26, 0.47, 0.65, 0.82, 0.88, 0.97, 0.98, 0.99, 0.99])[::-1]

plt.figure(figsize=(11, 9))
for w in range(10):
    plt.plot((1 - (w/10)) * start + (w/10) * end + np.random.normal(0, 0.0008, size=11), c=cmap(w / 10))

sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=1, vmax=10))
clb = plt.colorbar(sm)
clb.ax.set_title('Session', size=38, pad=14)
clb.ax.tick_params(labelsize=22)

plt.xticks(*bias_cont_ticks, size=25.5)
plt.xlim(left=0, right=10)
plt.ylim(0, 1)
plt.yticks(size=25.5)
plt.xlabel('Contrast', size=43)
plt.ylabel('P(respond rightward)', size=43)

sns.despine()
plt.tight_layout()
plt.savefig("dynamic_pmf_plot", dpi=300)
plt.show()
