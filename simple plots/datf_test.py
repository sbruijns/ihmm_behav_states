"""Plot graphical model of iHMM behavioural state model."""
import daft
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

np.random.seed(3)

rc("font", family="serif", size=12)
rc("text", usetex=True)


# Instantiate the PGM.
pgm = daft.PGM()

height = 1.5

# Hierarchical parameters.
gap = 0.8

blank = [3, 6, 9]
num_to_lab = dict(zip(range(10), [1, 2, None, 'i', 'i+1', None, 'i\'', 'i\'+1', None, 'N']))
pgm.add_node('_0', "Trial", -gap, 0., plot_params={'edgecolor': 'w'})
pgm.add_node('_1', "Response", -gap, -0.5, plot_params={'edgecolor': 'w'})
pgm.add_node('_2', "RT", -gap, -1, plot_params={'edgecolor': 'w'})
for i in range(10):
    if i+1 in blank:
        pgm.add_node(str(i+1), "...", i*gap, 0, plot_params={'alpha': 0.})
    else:
        pgm.add_node(str(i+1), r"${}$".format(num_to_lab[i]), i*gap, 0, observed=True)
        c = 'white' if np.random.rand() > 0.5 else 'black'
        pgm.add_node("color square {}".format(i+1), "", i*gap, -0.5, shape='rectangle', plot_params={'facecolor': c, 'edgecolor': 'k'}, aspect=1.5, scale=0.6)
        pgm.add_node("color square 2 {}".format(i+1), "", i*gap, -1, shape='rectangle', plot_params={'facecolor': str(np.random.rand()), 'edgecolor': 'k'}, aspect=1.5, scale=0.6)



pgm.add_node("state1", r"", 1.5*gap, height)
pgm.add_node("state2", r"", 5*gap, height)
pgm.add_node("state3", r"", 8*gap, height)

# Add in the edges.
pgm.add_edge("state1", "1")
pgm.add_edge("state1", "2")
pgm.add_edge("state1", "3")
pgm.add_edge("state1", "4")

pgm.add_edge("state2", "5")
pgm.add_edge("state2", "6")
pgm.add_edge("state2", "7")

pgm.add_edge("state3", "8")
pgm.add_edge("state3", "9")
pgm.add_edge("state3", "10")

pgm.add_edge("state1", "state2")
pgm.add_edge("state2", "state3")


# Comment out this to remove contrast dependence
pgm.add_node("contrast", r"$C_n$", -0.5, 1)
for i in range(10):
    if i+1 not in blank:
        pgm.add_edge("contrast", str(i+1))

# And a plate.
pgm.add_plate([-1, 0.65, 1.35, 1], label=r"$n = 1, \cdots, N$", label_offset=(5, 40))
# Render and save.
pgm.render()
plt.show()
#pgm.savefig("classic.pdf")
pgm.savefig("classic.png", dpi=200)
