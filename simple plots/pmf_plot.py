import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pickle

first_and_last_pmf = np.array(pickle.load(open("../multi_chain_saves/first_and_last_pmf.p", 'rb')))

contrasts_L = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])
contrasts_R = np.array([1., 0.987, 0.848, 0.555, 0.302, 0, 0, 0, 0, 0, 0])[::-1]

bias_cont_ticks = ([0, 2, 5, 8, 10], [-1, -.25, 0, .25, 1])

def weights_to_pmf(weights, prev_resps=0, with_bias=1):
    psi = weights[0] * contrasts_R + weights[1] * contrasts_L + with_bias * weights[-1] + weights[-2] * prev_resps
    return 1 / (1 + np.exp(psi))  # we somehow got the answers twisted, so we drop the minus here to get the opposite response probability for plotting


for i, pmf in enumerate(first_and_last_pmf):
	if i not in [3]:  # , 12]:
		continue
	pmf = pmf[1]
	plt.figure(figsize=(9.1, 8.3))
	plt.plot(range(11), weights_to_pmf(pmf, -1), 'k', lw=6)
	plt.plot(range(11), weights_to_pmf(pmf, 1), 'k', lw=6, ls='--')
	plt.xticks(*bias_cont_ticks, size=25)
	plt.annotate("Previous\nresponses:", (7.6, 0.3), color='k', size=40, horizontalalignment='center')
	arrow_len, head_width, head_length = 1 / 2 - 0.05, 0.025, 0.4 / 3
	gap = 0.8
	plt.arrow(6.25, 0.23, - arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(6.25 + gap, 0.23, - arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(6.25 + gap * 2, 0.23, - arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(6.25 + gap * 3, 0.23, - arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(6.25 + gap * 4, 0.23, - arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.annotate("Previous\nresponses:", (2.8, 0.7), color='k', size=40, horizontalalignment='center')
	# plt.arrow(1, 0.63, 1 / 2, 0, color='k', head_width=0.025, head_length=0.25 / 3)
	plt.arrow(0.85, 0.63, arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(0.85 + gap, 0.63, arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(0.85 + gap * 2, 0.63, arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(0.85 + gap * 3, 0.63, arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.arrow(0.85 + gap * 4, 0.63, arrow_len, 0, color='k', head_width=head_width, head_length=head_length)
	plt.yticks(size=25)
	plt.xlim(left=0, right=10)
	plt.ylim(bottom=0, top=1)
	sns.despine()
	plt.xlabel('Contrast', size=42)
	plt.ylabel('P(respond rightward)', size=42)
	plt.tight_layout()
	plt.savefig('simple_pmf', dpi=300)
	print(i)
	plt.show()
