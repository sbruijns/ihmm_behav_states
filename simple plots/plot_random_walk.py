import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

n = 200
seed = np.random.randint(10000)
print(seed)
np.random.seed(1441)

noise = np.random.normal(size=(n, 2))
walk = np.cumsum(noise, axis=0)
plt.plot(walk[:, 0], walk[:, 1])

plt.ylabel("weight dimension 2", fontsize=24)
plt.xlabel("weight dimension 1", fontsize=24)

plt.xticks([])
plt.yticks([])
sns.despine()
plt.tight_layout()
plt.savefig("rw", dpi=300)
plt.show()
